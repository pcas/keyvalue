// Reader defines a reader for key-value files

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"bufio"
	"context"
	"io"
	"strconv"
	"strings"
	"unicode"
)

// readerError provides additional context about an error whilst parsing "key: value" data.
type readerError struct {
	msg   string // The error message
	cause error  // The cause (if any)
	line  uint64 // The line number
	idx   uint64 // The index of the block
	key   string // The corresponding key (if given)
}

// ReaderError is the interface satisfied by some errors returned by a "key: value" parser.
type ReaderError interface {
	Error() string // Error returns a string representation of the error.
	Line() uint64  // Line returns the line number on which the error occurred.
	Index() uint64 // Index returns the record index for which the error occurred (indexed from 1).
	Key() string   // Key returns the key associated with this error, if known.
}

// reader implements a "key: value" parser as a record.Iterator.
type reader struct {
	r        *bufio.Reader // The underlying reader
	n        uint64        // The current line number
	idx      uint64        // The current index
	hasNext  bool          // Have we populated the record?
	rec      record.Record // The next record
	isEOF    bool          // Have we reached the end of the reader?
	isClosed bool          // Are we closed?
	err      error         // The last error in iteration (if any)
	opts     ReaderOptions // The options
}

// ReaderOptions specified options for a reader.
type ReaderOptions struct {
	Template           record.Record // Specifies the value types.
	AllowDuplicateKeys bool          // If true, duplicate keys are allowed, with only the most recently read value appearing in the read record.
	AllowEmptyKeys     bool          // If true, allow the read record to have empty keys.
	AllowMissingKeys   bool          // If true, allow read records not to have all the keys in Template; that is, there may be keys in Template that are missing from the read record.
	AllowUnknownKeys   bool          // If true, allow read records to have keys not in Template; that is, there may be keys in the read record that are missing from Template. The type of the associated value will be a string. Note that if IgnoreUnknownKeys is true then AllowUnknownKeys is ignored.
	IgnoreUnknownKeys  bool          // If true, ignore any keys not in Template; that is, any keys not in Template will be skipped and will be omitted from the read record. Note that IgnoreUnknownKeys takes priority over AllowUnknownKeys.
}

/////////////////////////////////////////////////////////////////////
// readerError functions
/////////////////////////////////////////////////////////////////////

// Error returns a string representation of the error.
func (e *readerError) Error() string {
	msg := "[Index: " + strconv.FormatUint(e.Index(), 10) +
		", Line: " + strconv.FormatUint(e.Line(), 10)
	if k := e.Key(); len(k) != 0 {
		msg += ", Key: " + k
	}
	msg += "] " + e.msg
	if cause := e.Cause(); cause != nil {
		msg += ": " + cause.Error()
	}
	return msg
}

// Cause returns the cause of this error, if known.
func (e *readerError) Cause() error {
	return e.cause
}

// Line returns the line number on which the error occurred.
func (e *readerError) Line() uint64 {
	return e.line
}

// Index returns the record index for which the error occurred (indexed from 1).
func (e *readerError) Index() uint64 {
	return e.idx
}

// Key returns the key associated with this error, if known.
func (e *readerError) Key() string {
	return e.key
}

/////////////////////////////////////////////////////////////////////
// reader functions
/////////////////////////////////////////////////////////////////////

// NewReader returns a new "key: value" iterator for the given io.Reader.
func NewReader(r io.Reader, opts ReaderOptions) (record.Iterator, error) {
	if opts.Template == nil {
		opts.Template = record.Record{}
	} else if ok, err := opts.Template.IsValid(); !ok {
		return nil, errors.TemplateFailsToValidate.Wrap(err)
	}
	return &reader{
		r:    bufio.NewReader(r),
		rec:  make(record.Record),
		opts: opts,
	}, nil
}

// Close closes the iterator, preventing further iteration. Close is idempotent (so can be called repeatedly and will return the same result every time).
func (r *reader) Close() error {
	r.isClosed = true
	return r.Err()
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (r *reader) Err() error {
	return r.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (r *reader) Next() bool {
	ok, err := r.NextContext(context.Background())
	return err == nil && ok
}

// Line returns the current line number that has been read by the iterator. Note: for huge datasets it is possible for this value to overflow; no attempt is made to detect this.
func (r *reader) Line() uint64 {
	return r.n
}

// Index returns the number of times Next has been successfully called. Note: for huge datasets it is possible for this value to overflow; no attempt is made to detect this.
func (r *reader) Index() uint64 {
	return r.idx
}

// validateKey checks that the given key is valid. Returns nil on success, otherwise returns an error describing the problem.
func (r *reader) validateKey(k string) error {
	// Check for an empty key
	if !r.opts.AllowEmptyKeys && len(k) == 0 {
		return &readerError{
			msg:  "Empty key",
			line: r.Line(),
			idx:  r.Index() + 1,
		}
	}
	// Check for duplicate keys
	if !r.opts.AllowDuplicateKeys {
		if _, ok := r.rec[k]; ok {
			return &readerError{
				msg:  "Duplicate key",
				line: r.Line(),
				idx:  r.Index() + 1,
				key:  k,
			}
		}
	}
	// Check for unknown keys
	if !r.opts.IgnoreUnknownKeys && !r.opts.AllowUnknownKeys {
		if _, ok := r.opts.Template[k]; !ok {
			return &readerError{
				msg:  "Unknown key",
				line: r.Line(),
				idx:  r.Index() + 1,
				key:  k,
			}
		}
	}
	// Looks good
	return nil
}

// ignoreKey returns true if the given key should be ignored.
func (r *reader) ignoreKey(k string) bool {
	if r.opts.IgnoreUnknownKeys {
		_, ok := r.opts.Template[k]
		return !ok
	}
	return false
}

// validateRecord checks that the given record is valid. Returns nil on success, otherwise returns an error describing the problem.
func (r *reader) validateRecord() error {
	// Check for missing keys
	if !r.opts.AllowMissingKeys {
		for k := range r.opts.Template {
			if _, ok := r.rec[k]; !ok {
				return &readerError{
					msg:  "Missing key",
					line: r.Line(),
					idx:  r.Index() + 1,
					key:  k,
				}
			}
		}
	}
	// Try to convert the record using the template
	if err := record.ConvertSkipUnknown(r.rec, r.opts.Template); err != nil {
		return &readerError{
			msg:   "Unable to convert record to match template",
			cause: err,
			line:  r.Line(),
			idx:   r.Index() + 1,
		}
	}
	// Looks good
	return nil
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (r *reader) NextContext(ctx context.Context) (bool, error) {
	// Sanity check
	if r.err != nil || r.isEOF || r.isClosed {
		return false, r.Err()
	}
	// Clear the record ready for new data
	for k := range r.rec {
		delete(r.rec, k)
	}
	// Make a note of the context's done channel
	doneC := ctx.Done()
	// Start working through the reader
	var foundRecord, done bool
	var err error
	for err == nil && !done {
		// Read in the next line
		var l string
		l, err = r.r.ReadString('\n')
		r.n++
		// How we proceed depends on our current state
		if l = strings.TrimSpace(l); len(l) == 0 {
			done = foundRecord
		} else {
			// Mark us as having found a record
			foundRecord = true
			// Split the line into a key and a value and save them on the record
			S := strings.SplitN(l, ":", 2)
			if len(S) != 2 {
				// The line is malformed
				err = &readerError{
					msg:  "Malformed line",
					line: r.Line(),
					idx:  r.Index() + 1,
				}
			} else {
				// Extract the key and value
				k := strings.TrimRightFunc(S[0], unicode.IsSpace)
				v := strings.TrimLeftFunc(S[1], unicode.IsSpace)
				// Validate the key
				if err = r.validateKey(k); err == nil {
					if !r.ignoreKey(k) {
						r.rec[k] = v
					}
				}
			}
		}
		// Should we take the opportunity to check if the context fired?
		if err == nil && !done && r.Line()%100 == 0 {
			select {
			case <-doneC:
				err = ctx.Err()
			default:
			}
		}
	}
	// Handle any errors during read
	if err == io.EOF {
		r.isEOF = true
	} else if err != nil {
		r.err = err
		return false, r.Err()
	}
	// Did we find a record?
	if !foundRecord {
		return false, nil
	}
	// Validate the record
	if err = r.validateRecord(); err != nil {
		r.err = err
		return false, r.Err()
	}
	// Success
	r.hasNext = true
	r.idx++
	return true, nil
}

// Scan copies the current Record into dst. Any previously set keys or values in dst will be deleted or overwritten.
func (r *reader) Scan(dst record.Record) error {
	// Sanity checks
	if r.err != nil {
		return r.Err()
	} else if r.isClosed {
		return errors.IterationFinished.New()
	} else if !r.hasNext {
		return errors.IterationNotStarted.New()
	}
	// Scan into dst
	record.Scan(dst, r.rec)
	return nil
}
