// TableCache defines a cache of driver.Tables.
/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/internal/cache"
	"bitbucket.org/pcastools/log"
	"context"
)

// tableWrapper wraps a driver.Table.
type tableWrapper struct {
	driver.Table
	release func() // The release function
}

// tableWithSelectOne combines a driver.Table and a driver.SelectOneWherer.
type tableWithSelectOne interface {
	driver.Table
	driver.SelectOneWherer
}

// tableWrapperWithSelectOneWhere wraps a driver.Table.
type tableWrapperWithSelectOneWhere struct {
	tableWithSelectOne
	release func() // The release function
}

// tableCache is a cache of driver.Tables.
type tableCache struct {
	cache *cache.Cache[driver.Table] // The underlying cache
}

//////////////////////////////////////////////////////////////////////
// tableWrapper functions
//////////////////////////////////////////////////////////////////////

// Close releases our hold on the table.
func (t *tableWrapper) Close() error {
	t.release()
	return nil
}

//////////////////////////////////////////////////////////////////////
// tableWrapperWithSelectOneWhere functions
//////////////////////////////////////////////////////////////////////

// Close releases our hold on the table.
func (t *tableWrapperWithSelectOneWhere) Close() error {
	t.release()
	return nil
}

//////////////////////////////////////////////////////////////////////
// tableCache functions
//////////////////////////////////////////////////////////////////////

// Close closes the cache.
func (c tableCache) Close() error {
	return c.cache.Close()
}

// Get returns a (cached) table connection to 'name'.
func (c tableCache) Get(ctx context.Context, name string) (driver.Table, error) {
	e, err := c.cache.Get(ctx, name)
	if err != nil {
		return nil, err
	}
	t := e.Value()
	if tt, ok := t.(tableWithSelectOne); ok {
		return &tableWrapperWithSelectOneWhere{
			tableWithSelectOne: tt,
			release:            e.Release,
		}, nil
	}
	return &tableWrapper{
		Table:   t,
		release: e.Release,
	}, nil
}

// MarkAsStale marks any cached table connection to 'name' as stale.
func (c tableCache) MarkAsStale(name string) {
	c.cache.MarkAsStale(name)
}

// newTableCache returns a new table cache for the given database.
func newTableCache(d driver.Database, lg log.Interface) tableCache {
	return tableCache{
		cache: cache.New(
			d.ConnectToTable,
			lg,
		),
	}
}
