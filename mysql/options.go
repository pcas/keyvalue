// Options provides options parsing for the mysql driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mysql

import (
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/stringsbuilder"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
)

// ClientConfig describes the configuration options we allow a user to set on a client connection.
//
// Host: If a host name begins with a slash, it specifies Unix-domain communication rather than TCP/IP communication; the value is the name of the directory in which the socket file is stored.
type ClientConfig struct {
	Host           string // The host
	Username       string // The user to sign in as
	Password       string // The user's password
	PasswordSet    bool   // Is a password set?
	ConnectTimeout int    // Maximum wait for connection, in seconds. Zero means wait indefinitely.
	RequireSSL     bool   // Whether or not to require SSL
}

// The default values for the client configuration, along with controlling mutex. The initial default values are assigned at init.
var (
	defaultsm sync.Mutex
	defaults  *ClientConfig
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets the initial default values. The following environment variables are consulted:
//	PCAS_MYSQL_HOST = "hostname[:port]"
//	PCAS_MYSQL_USERNAME = "myname"
//	PCAS_MYSQL_PASSWORD = "mysecret"
func init() {
	// Create the initial defaults
	defaults = &ClientConfig{
		Host: "localhost:3306",
	}
	// Modify the defaults based on environment variables
	if host, ok := os.LookupEnv("PCAS_MYSQL_HOST"); ok {
		defaults.Host = host
	}
	if username, ok := os.LookupEnv("PCAS_MYSQL_USERNAME"); ok {
		defaults.Username = username
	}
	if password, ok := os.LookupEnv("PCAS_MYSQL_PASSWORD"); ok {
		defaults.Password = password
		defaults.PasswordSet = true
	}
}

// writeOptionValue writes the quoted form of the given string to b.
func writeOptionValue(s string, b *strings.Builder) {
	if len(s) == 0 {
		b.WriteByte('\'')
		b.WriteByte('\'')
		return
	} else if !strings.ContainsAny(s, " '\\") {
		b.WriteString(s)
		return
	}
	b.WriteByte('\'')
	s = strings.ReplaceAll(s, "\\", "\\\\")
	s = strings.ReplaceAll(s, "'", "\\'")
	b.WriteString(s)
	b.WriteByte('\'')
}

// writeOptionKeyValue writes the given key and (quoted form of) value to b in the form "key=value".
func writeOptionKeyValue(key string, value string, b *strings.Builder) {
	b.WriteString(key)
	b.WriteByte('=')
	writeOptionValue(value, b)
}

/////////////////////////////////////////////////////////////////////////
// ClientConfig functions
/////////////////////////////////////////////////////////////////////////

// DefaultConfig returns a new client configuration initialised with the default values.
//
// The initial default value for the host, username, and password will be read from the environment variables
//	PCAS_MYSQL_HOST = "hostname[:port]"
//	PCAS_MYSQL_USERNAME = "myname"
//	PCAS_MYSQL_PASSWORD = "mysecret"
// respectively on package init.
//
// Note that if PCAS_MYSQL_PASSWORD is set but with value equal to the empty string then it still counts as a valid password, since the empty string is a valid password. You must ensure that PCAS_MYSQL_PASSWORD is unset if you do not want to specify an initial default password.
func DefaultConfig() *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	return defaults.Copy()
}

// SetDefaultConfig sets the default client configuration to c and returns the old default configuration. This change will be reflected in future calls to DefaultConfig.
func SetDefaultConfig(c *ClientConfig) *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	oldc := defaults
	defaults = c.Copy()
	return oldc
}

// Validate validates the client configuration, returning an error if there's a problem.
func (c *ClientConfig) Validate() error {
	// Get the nil case out of the way
	if c == nil {
		return errors.New("illegal nil configuration data")
	}
	// Start validating
	if c.ConnectTimeout < 0 {
		return fmt.Errorf("invalid connection timeout: %d", c.ConnectTimeout)
	} else if len(c.Password) != 0 && !c.PasswordSet {
		return errors.New("a password appears to be given, however the value of PasswordSet is 'false'")
	} else if idx := strings.IndexByte(c.Host, ':'); idx != -1 {
		if idx == len(c.Host)-1 {
			return errors.New("invalid port number")
		} else if n, err := strconv.Atoi(c.Host[idx+1:]); err != nil {
			return fmt.Errorf("invalid port number: %w", err)
		} else if n < 1 || n > 65535 {
			return fmt.Errorf("port number (%d) out of range", n)
		}
	}
	// Looks good
	return nil
}

// Copy returns a copy of the configuration.
func (c *ClientConfig) Copy() *ClientConfig {
	cc := *c
	return &cc
}

// Hash returns a hash for the configuration.
func (c *ClientConfig) Hash() uint32 {
	h := hash.String(c.Host)
	h = hash.Combine(h, hash.String(c.Username))
	h = hash.Combine(h, hash.Bool(c.PasswordSet))
	if c.PasswordSet {
		h = hash.Combine(h, hash.String(c.Password))
	}
	h = hash.Combine(h, hash.Int(c.ConnectTimeout))
	h = hash.Combine(h, hash.Bool(c.RequireSSL))
	return h
}

// toPqOptions converts the configuration to a string of connection options, connecting to the database dbName.
func (c *ClientConfig) toMySQLOptions(dbName string) string {
	// Grab a strings builder
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	// Write the username (and password), if any
	if len(c.Username) != 0 {
		b.WriteString(c.Username)
		if c.PasswordSet {
			b.WriteByte(':')
			b.WriteString(c.Password)
		}
		b.WriteByte('@')
	}
	// Write the host
	if len(c.Host) != 0 {
		if strings.HasPrefix(c.Host, "/") {
			b.WriteString("unix(")
		} else {
			b.WriteString("tcp(")
		}
		b.WriteString(c.Host)
		b.WriteByte(')')
	}
	b.WriteByte('/')
	// Write the database name
	b.WriteString(dbName)
	// Write any remaining options
	hasOption := false
	if c.ConnectTimeout > 0 {
		if !hasOption {
			b.WriteByte('?')
			hasOption = true
		} else {
			b.WriteByte('&')
		}
		val := strconv.Itoa(c.ConnectTimeout) + "s"
		writeOptionKeyValue("timeout", val, b)
	}
	if c.RequireSSL {
		if !hasOption {
			b.WriteByte('?')
			hasOption = true
		} else {
			b.WriteByte('&')
		}
		b.WriteString("tls=true")
	}
	// Return the string
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// AreEqual returns true iff the two configurations are equal.
func AreEqual(c *ClientConfig, d *ClientConfig) bool {
	if c.Host != d.Host ||
		c.Username != d.Username ||
		c.PasswordSet != d.PasswordSet ||
		c.ConnectTimeout != d.ConnectTimeout ||
		c.RequireSSL != d.RequireSSL {
		return false
	}
	if c.PasswordSet && (c.Password != d.Password) {
		return false
	}
	return true
}
