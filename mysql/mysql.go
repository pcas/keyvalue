// Package mysql implements the keyvalue interfaces when working with MySQL.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mysql

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/internal/kvcache"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/contextutil"
	"context"
	"database/sql"
	sqldriver "database/sql/driver"
	stderrors "errors"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"strconv"
	"strings"
	"time"
)

// clientCache is a cache of client connections.
var clientCache *kvcache.Cache[*configAndDB, *sql.DB]

// maxInsertSize is the soft-max number of bytes to insert in a single transaction.
const maxInsertSize = 256 * 1024 * 1024 // 256MB

// maxInsertDuration is the soft-max duration allowed for a single transaction.
const maxInsertDuration = 30 * time.Minute

// configAndDB encapsulates a client config and database name.
type configAndDB struct {
	ClientConfig
	dbName string // The database name
}

// connection implements the driver.Connection interface.
type connection struct {
	cfg *ClientConfig // The client config
}

// database implements the driver.Database interface.
type database struct {
	db      *sql.DB // The underlying database connection
	name    string  // The database name
	release func()  // The release function
}

// table implements the driver.Table interface.
type table struct {
	d    *database // The database
	name string    // The table name
}

// iterator implements the record.Iterator interface for a query.
type iterator struct {
	cancel   context.CancelFunc // Cancel the context for these results
	template record.Record      // The template record
	hasNext  bool               // Does the cursor have a next record?
	keys     []string           // The column names
	dest     []interface{}      // A destination buffer for reading a row
	ptrs     []interface{}      // The pointers used for scan
	rows     *sql.Rows          // The underlying rows of results
	isClosed bool               // Are we closed?
	err      error              // The error on iteration (if any)
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init creates the client cache
func init() {
	// Create the cache
	clientCache = kvcache.New(
		func(ctx context.Context, cfg *configAndDB) (*sql.DB, error) {
			db, err := sql.Open("mysql", cfg.toMySQLOptions(cfg.dbName))
			if err != nil {
				return nil, err
			} else if err = db.PingContext(ctx); err != nil {
				db.Close()
				return nil, err
			}
			return db, nil
		},
		func(ctx context.Context, c *sql.DB) bool {
			return c.PingContext(ctx) == nil
		},
		func(c *sql.DB) error {
			return c.Close()
		},
		func(cfg1 *configAndDB, cfg2 *configAndDB) bool {
			return cfg1.dbName == cfg2.dbName && AreEqual(&cfg1.ClientConfig, &cfg2.ClientConfig)
		},
	)
	// Register closing the cache on cleanup
	cleanup.Add(clientCache.Close)
}

// validateTableName returns true if and only if the given table name validates.
func validateTableName(name string) bool {
	return !strings.Contains(name, ".")
}

// isInternalDatabase returns true if it is the name of an internal database.
func isInternalDatabase(name string) bool {
	name = strings.ToLower(name)
	return name == "mysql" ||
		name == "information_schema" ||
		name == "performance_schema" ||
		name == "metrics_schema" ||
		name == "sys"
}

// validateDatabaseName returns true if and only if the given database name validates.
func validateDatabaseName(name string) bool {
	return !strings.Contains(name, ".") && !isInternalDatabase(name)
}

// connect returns a connection to the named database using the given configuration. On success, the second return value is a release function which must be called when you are finished with the database, otherwise resources will leak.
func connect(ctx context.Context, name string, cfg *ClientConfig) (*sql.DB, func(), error) {
	db, release, err := func() (*sql.DB, func(), error) {
		e, err := clientCache.Get(&configAndDB{
			ClientConfig: *cfg,
			dbName:       name,
		})
		if err != nil {
			return nil, nil, err
		}
		db, err := e.Value(ctx)
		if err != nil {
			e.Release()
			return nil, nil, err
		}
		return db, e.Release, nil
	}()
	if err != nil {
		// Check for database not exist error (error no. 1049)
		if e, ok := err.(*mysql.MySQLError); ok && e.Number == 1049 {
			err = errors.DatabaseDoesNotExist.Wrap(err)
		}
		return nil, nil, err
	}
	return db, release, err
}

// areSlicesEqual returns true iff the two slices are equal.
func areSlicesEqual(S1 []string, S2 []string) bool {
	if len(S1) != len(S2) {
		return false
	}
	for i, s := range S1 {
		if S2[i] != s {
			return false
		}
	}
	return true
}

// convertToString attempts to convert the entry with given index to a string.
func convertToString(vals []interface{}, idx int) (string, error) {
	x := vals[idx]
	if s, ok := x.(string); ok {
		return s, nil
	} else if s, ok := x.([]byte); ok {
		return string(s), nil
	}
	return "", fmt.Errorf("unexpected type (%T) in entry %d", x, idx)
}

// indexOfString returns the first index of an entry in S matching match, or -1 if there is not matching entry,
func indexOfString(S []string, match string) int {
	for i, s := range S {
		if s == match {
			return i
		}
	}
	return -1
}

// quoteIdentifier quotes the identifier.
func quoteIdentifier(s string) string {
	return "`" + strings.ReplaceAll(s, "`", "``") + "`"
}

// commitOrRollback commits or rollbacks the given transaction based on the given error.
func commitOrRollback(tx sqldriver.Tx, err error) error {
	if err == nil {
		return tx.Commit()
	}
	return tx.Rollback()
}

// closeStmt closes the given statement.
func closeStmt(s *sql.Stmt) error {
	if s == nil {
		return nil
	}
	return s.Close()
}

// execAndClose executes and closes the given statement.
func execAndClose(ctx context.Context, s *sql.Stmt) (err error) {
	if s != nil {
		_, err = s.ExecContext(ctx)
		if closeErr := s.Close(); err == nil {
			err = closeErr
		}
	}
	return
}

// quotedKeysAndValues returns parallel slices of "key = $N" and values, where the keys are quoted ready for including in an SQL statement. The keys will be sorted in increasing order.
func quotedKeysAndValues(r record.Record) ([]string, []interface{}) {
	idx := 1
	keys := make([]string, 0, len(r))
	vals := make([]interface{}, 0, len(r))
	for _, k := range r.Keys() {
		keys = append(keys, quoteIdentifier(k)+" = ?")
		vals = append(vals, r[k])
		idx++
	}
	return keys, vals
}

// quotedKeyAndType returns a string containing a quoted version of k and the type of v, suitable for including in MySQL CREATE TABLE and ALTER TABLE commands.
func quotedKeyAndType(k string, v interface{}) string {
	var t string
	switch v.(type) {
	case int8:
		t = "TINYINT"
	case int16:
		t = "SMALLINT"
	case int32:
		t = "INT"
	case int, int64:
		t = "BIGINT"
	case uint8:
		t = "TINYINT UNSIGNED"
	case uint16:
		t = "SMALLINT UNSIGNED"
	case uint32:
		t = "INT UNSIGNED"
	case uint, uint64:
		t = "BIGINT UNSIGNED"
	case bool:
		t = "BOOL"
	case float64:
		t = "DOUBLE"
	case []byte:
		t = "LONGBLOB"
	default:
		t = "LONGTEXT"
	}
	return quoteIdentifier(k) + " " + t
}

// recordTypeForMysqlType returns a sample record Value for the named MySQL type.
func recordTypeForMysqlType(t string) interface{} {
	switch t {
	case "boolean", "bool", "tinyint(1)":
		return false
	case "bigint unsigned":
		return uint64(0)
	case "bigint", "bigint(20)":
		return int64(0)
	case "int", "mediumint":
		return int32(0)
	case "int unsigned", "mediumint unsigned":
		return uint32(0)
	case "smallint":
		return int16(0)
	case "smallint unsigned":
		return uint16(0)
	case "tinyint":
		return int8(0)
	case "tinyint unsigned":
		return uint8(0)
	case "float", "double":
		return float64(0)
	case "tinyblob", "blob", "mediumblob", "longblob", "binary", "varbinary":
		return []byte{}
	default:
		return ""
	}
}

// isIndexNotExistsError returns true if and only if the error indicates an index not exists error.
func isIndexNotExistsError(err error) bool {
	if err != nil {
		if e, ok := err.(*mysql.MySQLError); ok && e.Number == 1091 {
			return strings.HasSuffix(e.Message, "check that column/key exists")
		}
	}
	return false
}

// isDuplicateIndexError returns true if and only if the error indicates a duplicate index.
func isDuplicateIndexError(err error) bool {
	if err != nil {
		if e, ok := err.(*mysql.MySQLError); ok && e.Number == 1061 {
			return strings.HasPrefix(e.Message, "Duplicate key name")
		}
	}
	return false
}

// isDuplicateColumnError returns true if and only if the error indicates a duplicate column.
func isDuplicateColumnError(err error) bool {
	if err != nil {
		if e, ok := err.(*mysql.MySQLError); ok && e.Number == 1060 {
			return strings.HasPrefix(e.Message, "Duplicate column name")
		}
	}
	return false
}

// indexName returns the index name for a given table and key.
func indexName(t *table, key string) string {
	return quoteIdentifier(t.name + "_" + key + "_index")
}

//////////////////////////////////////////////////////////////////////
// iterator functions
//////////////////////////////////////////////////////////////////////

// Close prevents future iteration.
func (s *iterator) Close() error {
	if !s.isClosed {
		s.isClosed = true
		defer s.cancel()
		if err := s.rows.Close(); s.err == nil {
			s.err = err
		}
	}
	return s.err
}

// Err returns any errors during iteration.
func (s *iterator) Err() error {
	return s.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *iterator) Next() bool {
	ok, err := s.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *iterator) NextContext(ctx context.Context) (bool, error) {
	// Sanity check
	if s.isClosed || s.err != nil {
		return false, nil
	}
	// Link the context to our cancel function
	defer contextutil.NewLink(ctx, s.cancel).Stop()
	// Is there a next row?
	if !s.rows.Next() {
		s.err = s.rows.Err()
		return false, s.err
	}
	// If necessary, create the backing slices
	if s.keys == nil {
		s.keys = s.template.Keys()
		s.dest = make([]interface{}, 0, len(s.keys))
		s.ptrs = make([]interface{}, 0, len(s.keys))
		for i, k := range s.keys {
			s.dest = append(s.dest, s.template[k])
			s.ptrs = append(s.ptrs, &s.dest[i])
		}
	}
	// Read in the row
	s.err = s.rows.Scan(s.ptrs...)
	if s.err != nil {
		return false, s.err
	}
	s.hasNext = true
	return true, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *iterator) Scan(dest record.Record) error {
	// Sanity check
	if s.isClosed {
		return errors.IterationFinished.New()
	} else if !s.hasNext {
		return errors.IterationNotStarted.New()
	}
	// Delete any existing keys in the destination record
	for key := range dest {
		delete(dest, key)
	}
	// Populate the destination record
	for i, k := range s.keys {
		if s.dest[i] != nil {
			dest[k] = s.dest[i]
		}
	}
	// Convert and return the record
	return record.Convert(dest, s.template)
}

//////////////////////////////////////////////////////////////////////
// table functions
//////////////////////////////////////////////////////////////////////

// quotedName returns the quoted table name, suitable for including in an SQL statement.
func (t *table) quotedName() string {
	return quoteIdentifier(t.name)
}

// createInsertStatement returns an INSERT statement for the given fields.
func (t *table) createInsertStatement(fields []string) string {
	keys := make([]string, 0, len(fields))
	values := make([]string, 0, len(fields))
	for _, k := range fields {
		keys = append(keys, quoteIdentifier(k))
		values = append(values, "?")
	}
	return "INSERT INTO " + t.quotedName() + " (" + strings.Join(keys, ", ") + ") VALUES (" + strings.Join(values, ", ") + ")"
}

// createSelectStatement returns a SELECT statement for the given template and condition. The keys will be sorted in increasing order.
func (t *table) createSelectStatement(template record.Record, cond condition.Condition) (string, []interface{}, error) {
	// Create the slice of keys
	keys := make([]string, 0, len(template))
	for _, k := range template.Keys() {
		keys = append(keys, quoteIdentifier(k))
	}
	// Create the SELECT statement
	stmt := "SELECT " + strings.Join(keys, ", ") + " FROM " + t.quotedName()
	// Create the WHERE component
	where, vals, err := conditionToSQL(cond, nil)
	if err != nil {
		return "", nil, err
	}
	if len(where) != 0 {
		stmt += " WHERE " + where
	}
	return stmt, vals, nil
}

// createOrderBy returns an SQL string describing the given sort order.
func createOrderBy(order sort.OrderBy) string {
	if len(order) == 0 {
		return ""
	}
	S := make([]string, 0, len(order))
	for i := range order {
		s := quoteIdentifier(order[i].Key)
		if order[i].IsAscending() {
			s += " ASC"
		} else {
			s += " DESC"
		}
		S = append(S, s)
	}
	return "ORDER BY " + strings.Join(S, ", ")
}

// Close closes the connection to the table.
func (t *table) Close() error {
	return nil
}

// Describe returns a best-guess template for the data in this table.
func (t *table) Describe(ctx context.Context) (r record.Record, err error) {
	// Query the schema
	var rows *sql.Rows
	stmt := "SHOW COLUMNS FROM " + t.quotedName()
	if rows, err = t.d.db.QueryContext(ctx, stmt); err != nil {
		return
	}
	defer func() {
		if closeErr := rows.Close(); err == nil {
			err = closeErr
			if err == nil {
				err = rows.Err()
			}
		}
	}()
	// Make a note of the column names
	var cols []string
	if cols, err = rows.Columns(); err != nil {
		return
	}
	// Find the index of the "Field" and "Type" columns
	fieldIdx := indexOfString(cols, "Field")
	if fieldIdx == -1 {
		err = stderrors.New("rows do not contain column \"Field\"")
		return
	}
	typeIdx := indexOfString(cols, "Type")
	if typeIdx == -1 {
		err = stderrors.New("rows do not contain column \"Type\"")
		return
	}
	// Prepare values for reading the columns
	vals := make([]interface{}, len(cols))
	ptrs := make([]interface{}, 0, len(vals))
	for i := range vals {
		ptrs = append(ptrs, &vals[i])
	}
	// Extract the column info
	r = make(record.Record)
	for err == nil && rows.Next() {
		// Read in the next row
		if err = rows.Scan(ptrs...); err != nil {
			return
		}
		// Extract the name and type
		var s, T string
		if s, err = convertToString(vals, fieldIdx); err != nil {
			return
		} else if T, err = convertToString(vals, typeIdx); err != nil {
			return
		}
		// Record the type for this column
		r[s] = recordTypeForMysqlType(T)
	}
	return
}

// CountWhere returns the number of records in the table that satisfy condition "cond" (which may be nil if there are no conditions).
//
// Assumes that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool.
func (t *table) CountWhere(ctx context.Context, cond condition.Condition) (n int64, err error) {
	// Create the COUNT statement
	stmt := "SELECT COUNT(*) FROM " + t.quotedName()
	// Create the WHERE component
	where, vals, err := conditionToSQL(cond, nil)
	if err != nil {
		return 0, err
	}
	if len(where) != 0 {
		stmt += " WHERE " + where
	}
	// Perform the count
	err = t.d.db.QueryRowContext(ctx, stmt, vals...).Scan(&n)
	return
}

// Insert inserts the records from the given iterator into the table.
func (t *table) Insert(ctx context.Context, itr record.Iterator) (err error) {
	// We loop inserting records until either the iterator is empty, or an
	// error occurs during the insertion.
	var isEmpty bool
	for err == nil && !isEmpty {
		// Wrap the iterator in a limited-size, limited-duration iterator
		wrappedItr := record.NewDurationIterator(
			record.LimitSizeIterator(itr, maxInsertSize), maxInsertDuration,
		)
		// Perform the insert
		var n int64
		if n, err = t.insertIterator(ctx, wrappedItr); err == nil {
			isEmpty = n == 0
		}
		// Ensure that the duration iterator has released its resources
		wrappedItr.Stop()
	}
	return
}

// insertIterator inserts the records from the given iterator into the table. Returns the number of records inserted.
func (t *table) insertIterator(ctx context.Context, itr record.Iterator) (n int64, err error) {
	// Start a new transaction.
	var tx *sql.Tx
	if tx, err = t.d.db.BeginTx(ctx, nil); err != nil {
		return
	}
	// Defer closing our transaction
	var s *sql.Stmt
	defer func() {
		// Check for iteration errors
		if itrErr := itr.Err(); err == nil {
			err = itrErr
		}
		// Close the statement
		if closeErr := closeStmt(s); err == nil {
			err = closeErr
		}
		// Commit or rollback
		if txErr := commitOrRollback(tx, err); err == nil {
			err = txErr
		}
	}()
	// Start iterating over the records
	var fields []string
	var vals []interface{}
	var ok bool
	ok, err = itr.NextContext(ctx)
	for err == nil && ok {
		// Fetch the next record and validate it
		r := make(record.Record, len(fields))
		if err = itr.Scan(r); err != nil {
			return
		} else if ok, err = r.IsValid(); !ok {
			return
		}
		// Check whether the fields agree with the record's keys
		if keys := r.Keys(); !areSlicesEqual(fields, keys) {
			// The fields need to be changed -- close the current statement
			if err = closeStmt(s); err != nil {
				s = nil
				return
			}
			// Prepare the INSERT statement using the new fields
			fields, vals = keys, make([]interface{}, len(keys))
			if s, err = tx.PrepareContext(ctx, t.createInsertStatement(fields)); err != nil {
				s = nil
				return
			}
		}
		// Insert this record
		for i, k := range fields {
			vals[i] = r[k]
		}
		if _, err = s.ExecContext(ctx, vals...); err != nil {
			return
		}
		n++
		// Move on
		ok, err = itr.NextContext(ctx)
	}
	return
}

// UpdateWhere updates all records in the table that satisfy condition "cond" (which may be nil if there are no conditions) by setting all keys present in "replacement" to the given values. Returns the number of records updated.
//
// Assumes that "replacement" is valid; and that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool.
func (t *table) UpdateWhere(ctx context.Context, replacement record.Record, cond condition.Condition) (n int64, err error) {
	// Create the UPDATE statement
	keys, vals := quotedKeysAndValues(replacement)
	stmt := "UPDATE " + t.quotedName() + " SET " + strings.Join(keys, ", ")
	// Create the WHERE component
	var where string
	where, vals, err = conditionToSQL(cond, vals)
	if err != nil {
		return
	}
	if len(where) != 0 {
		stmt += " WHERE " + where
	}
	// Update the records
	var res sql.Result
	res, err = t.d.db.ExecContext(ctx, stmt, vals...)
	if err != nil {
		return
	}
	n, err = res.RowsAffected()
	return
}

// SelectWhere returns the results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template". Assumes that "cond" and "order" are valid.
func (t *table) SelectWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Iterator, error) {
	return t.SelectWhereLimit(ctx, template, cond, order, -1)
}

// SelectWhereLimit returns at most n results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
//
// Assumes that "template" is valid; that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool; that "order" is valid; and that n is a non-negative integer (internally we use a negative integer to indicate no limit).
func (t *table) SelectWhereLimit(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error) {
	// Create the SELECT statement
	stmt, vals, err := t.createSelectStatement(template, cond)
	if err != nil {
		return nil, err
	}
	if len(order) > 0 {
		stmt += " " + createOrderBy(order)
	}
	if n >= 0 {
		stmt += " LIMIT " + strconv.FormatInt(n, 10)
	}
	// Construct a new context that will last the entire iteration and link it
	// to the user's context for the duration of this call
	newCtx, cancel := context.WithCancel(context.Background())
	defer contextutil.NewLink(ctx, cancel).Stop()
	// Perform the select
	rows, err := t.d.db.QueryContext(newCtx, stmt, vals...)
	if err != nil {
		cancel()
		return nil, err
	}
	return &iterator{
		cancel:   cancel,
		template: template,
		rows:     rows,
	}, nil
}

// DeleteWhere deletes those records in the table that satisfy condition "cond" (which may be nil if there are no conditions). Returns the number of records deleted.
//
// Assumes that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool.
func (t *table) DeleteWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	// Create the DELETE statement
	stmt := "DELETE FROM " + t.quotedName()
	// Create the WHERE component
	where, vals, err := conditionToSQL(cond, nil)
	if err != nil {
		return 0, err
	}
	if len(where) != 0 {
		stmt += " WHERE " + where
	}
	// Delete the records
	res, err := t.d.db.ExecContext(ctx, stmt, vals...)
	if err != nil {
		return 0, err
	}
	n, err := res.RowsAffected()
	return n, err
}

// AddIndex adds an index on the given key. If an index already exists, this index is unmodified and nil is returned.  The driver is free to assume that the key is well-formed.
func (t *table) AddIndex(ctx context.Context, key string) error {
	// We need to know the type of the column for key
	r, err := t.Describe(ctx)
	if err != nil {
		return err
	}
	v, ok := r[key]
	if !ok {
		return fmt.Errorf("unknown key: %s", key)
	}
	// If the type is text, binary, or a blob we need to add a range to the index
	id := quoteIdentifier(key)
	switch v.(type) {
	case string, []byte:
		id += "(5)"
	}
	// Create the index
	stmt := fmt.Sprintf("CREATE INDEX %s ON %s(%s)", indexName(t, key), t.quotedName(), id)
	_, err = t.d.db.ExecContext(ctx, stmt)
	if isDuplicateIndexError(err) {
		err = nil
	}
	return err
}

// DeleteIndex deletes the index on the given key. If no index is present, nil is returned.  The driver is free to assume that the key is well-formed.
func (t *table) DeleteIndex(ctx context.Context, key string) error {
	// Drop the index
	stmt := fmt.Sprintf("DROP INDEX %s ON %s", indexName(t, key), t.quotedName())
	_, err := t.d.db.ExecContext(ctx, stmt)
	// Need to check whether any error was due to the index not existing
	if isIndexNotExistsError(err) {
		err = nil
	}
	return err
}

// ListIndices lists the keys for which indices are present.  The driver is free to assume that the key is well-formed.
func (t *table) ListIndices(ctx context.Context) ([]string, error) {
	stmt := "SHOW INDEXES FROM " + t.quotedName()
	rows, err := t.d.db.QueryContext(ctx, stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	// Make a note of the column names
	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}
	// Find the index of the "Key_name" column
	idx := indexOfString(cols, "Key_name")
	if idx == -1 {
		return nil, stderrors.New("rows do not contain column \"Key_name\"")
	}
	// Prepare values for reading the columns
	vals := make([]interface{}, len(cols))
	ptrs := make([]interface{}, 0, len(vals))
	for i := range vals {
		ptrs = append(ptrs, &vals[i])
	}
	// Look for index names of the form "foo_*_index" where foo is the table name
	prefix := t.name + "_"
	indices := make(map[string]bool)
	for rows.Next() {
		// Read in the next row
		if err = rows.Scan(ptrs...); err != nil {
			return nil, err
		}
		// Extract the name
		s, err := convertToString(vals, idx)
		if err != nil {
			return nil, err
		}
		// Check if it is of the correct form
		if strings.HasPrefix(s, prefix) && strings.HasSuffix(s, "_index") {
			key := strings.TrimSuffix(strings.TrimPrefix(s, prefix), "_index")
			indices[key] = true
		}
	}
	// Handle any error
	if err := rows.Err(); err != nil {
		return nil, err
	}
	// Convert the index names to a slice and return
	result := make([]string, 0, len(indices))
	for k := range indices {
		result = append(result, k)
	}
	return result, nil
}

// AddKeys updates each record r in the table, adding any keys in rec that are not already present along with the corresponding values. Any keys that are already present in r will be left unmodified.
func (t *table) AddKeys(ctx context.Context, rec record.Record) error {
	// Handle the trivial case
	if len(rec) == 0 {
		return nil
	}
	// Add the keys and values one at a time
	for k, v := range rec {
		// Add the column to the schema if necessary
		cmd := fmt.Sprintf("ALTER TABLE %s ADD COLUMN %s", t.quotedName(), quotedKeyAndType(k, v))
		// Ignore any error about the column already existing
		if _, err := t.d.db.ExecContext(ctx, cmd); err != nil && !isDuplicateColumnError(err) {
			return err
		}
		// Build the command string and arguments
		cmd = fmt.Sprintf("UPDATE %s SET %s = ? WHERE %s IS NULL", t.quotedName(), quoteIdentifier(k), quoteIdentifier(k))
		// Do the update
		if _, err := t.d.db.ExecContext(ctx, cmd, v); err != nil {
			return err
		}
	}
	// We succeeded
	return nil
}

// DeleteKeys updates all records in the table, deleting all the specified keys if present.
func (t *table) DeleteKeys(ctx context.Context, keys []string) error {
	// Look up the columns
	schema, err := t.Describe(ctx)
	if err != nil {
		return err
	}
	// Ignore any keys that don't exist, and ignore duplicates
	keysToDelete := make(map[string]bool, len(keys))
	for _, k := range keys {
		if _, ok := schema[k]; ok {
			keysToDelete[k] = true
		}
	}
	// Do we need to do anything?
	if len(keysToDelete) == 0 {
		return nil
	}
	// Build the command string
	S := make([]string, 0, len(keys))
	for k := range keysToDelete {
		S = append(S, "DROP COLUMN "+quoteIdentifier(k))
	}
	cmd := fmt.Sprintf("ALTER TABLE %s %s", t.quotedName(), strings.Join(S, ", "))
	_, err = t.d.db.ExecContext(ctx, cmd)
	return err
}

//////////////////////////////////////////////////////////////////////
// connection functions
//////////////////////////////////////////////////////////////////////

// ListTables returns the names of the tables in the database.
func (d *database) ListTables(ctx context.Context) (S []string, err error) {
	// Create the statement
	stmt := "SHOW TABLES"
	// Query the database
	rows, err := d.db.QueryContext(ctx, stmt)
	if err != nil {
		return nil, err
	}
	defer func() {
		if closeErr := rows.Close(); err == nil {
			err = closeErr
			if err == nil {
				err = rows.Err()
			}
		}
	}()
	// Extract the table names
	S = make([]string, 0)
	for err == nil && rows.Next() {
		var s string
		if err = rows.Scan(&s); err == nil {
			S = append(S, s)
		}
	}
	return
}

// CreateTable creates a table with the given name in the database, using the provided template. Assumes that "template" is valid.
func (d *database) CreateTable(ctx context.Context, name string, template record.Record) error {
	// Sanity check
	if !validateTableName(name) {
		return errors.InvalidTableName.New()
	}
	// Create the statement
	cols := make([]string, 0, len(template))
	for _, k := range template.Keys() {
		cols = append(cols, quotedKeyAndType(k, template[k]))
	}
	stmt := "CREATE TABLE " + quoteIdentifier(name) + " (" + strings.Join(cols, ", ") + ")"
	// Create the table
	_, err := d.db.ExecContext(ctx, stmt)
	return err
}

// DeleteTable deletes the indicated table from the database. Does not return an error if the table does not exist.
func (d *database) DeleteTable(ctx context.Context, name string) error {
	if !validateTableName(name) {
		return errors.InvalidTableName.New()
	}
	stmt := "DROP TABLE IF EXISTS " + quoteIdentifier(name)
	_, err := d.db.ExecContext(ctx, stmt)
	return err
}

// RenameTable changes the name of table src in the database to dst.
func (d *database) RenameTable(ctx context.Context, src string, dst string) error {
	if !validateTableName(src) || !validateTableName(dst) {
		return errors.InvalidTableName.New()
	}
	stmt := fmt.Sprintf("ALTER TABLE %s RENAME TO %s", quoteIdentifier(src), quoteIdentifier(dst))
	_, err := d.db.ExecContext(ctx, stmt)
	return err
}

// ConnectToTable connects to the indicated table in the database.
func (d *database) ConnectToTable(ctx context.Context, name string) (driver.Table, error) {
	// Sanity check
	if !validateTableName(name) {
		return nil, errors.InvalidTableName.New()
	}
	// First we want to check that the table exists
	stmt := "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = ? AND table_name = ?"
	var n int64
	if err := d.db.QueryRowContext(ctx, stmt, d.name, name).Scan(&n); err != nil {
		return nil, err
	} else if n == 0 {
		return nil, errors.TableDoesNotExist.New()
	}
	// Return the table object
	return &table{
		d:    d,
		name: name,
	}, nil
}

// Close closes the connection to the database.
func (d *database) Close() error {
	d.release()
	return nil
}

//////////////////////////////////////////////////////////////////////
// connection functions
//////////////////////////////////////////////////////////////////////

// DriverName returns the name associated with this driver.
func (*connection) DriverName() string {
	return "mysql"
}

// ListDatabases returns the names of the available databases on this connection.
func (c *connection) ListDatabases(ctx context.Context) ([]string, error) {
	// Recover a database connection
	db, release, err := connect(ctx, "", c.cfg)
	if err != nil {
		return nil, err
	}
	defer release()
	// Query the schema
	stmt := "SHOW DATABASES"
	rows, err := db.QueryContext(ctx, stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	// Extract the column info
	names := make([]string, 0)
	for err == nil && rows.Next() {
		var s string
		if err = rows.Scan(&s); err == nil {
			// Remove the internal databases
			if !isInternalDatabase(s) {
				names = append(names, s)
			}
		}
	}
	if err != nil {
		return nil, err
	}
	return names, nil
}

// CreateDatabase creates a database with the given name on this connection.
func (c *connection) CreateDatabase(ctx context.Context, name string) error {
	// Validate the database name
	if !validateDatabaseName(name) {
		return errors.InvalidDatabaseName.New()
	}
	// Recover a database connection
	db, release, err := connect(ctx, "", c.cfg)
	if err != nil {
		return err
	}
	defer release()
	// Create the database
	stmt := "CREATE DATABASE " + quoteIdentifier(name)
	_, err = db.ExecContext(ctx, stmt)
	// If there's an error, we check if the database already exists
	if err != nil {
		stmt := "SELECT COUNT(*) FROM information_schema.schemata WHERE schema_name = ?"
		var n int64
		if myErr := db.QueryRowContext(ctx, stmt, name).Scan(&n); myErr == nil && n != 0 {
			err = errors.DatabaseAlreadyExists.New()
		}
	}
	return err
}

// DeleteDatabase deletes the indicated database on this connection.
// Does not return an error if the database does not exist.
func (c *connection) DeleteDatabase(ctx context.Context, name string) error {
	// Validate the database name
	if !validateDatabaseName(name) {
		return errors.InvalidDatabaseName.New()
	}
	// Recover a database connection
	db, release, err := connect(ctx, "", c.cfg)
	if err != nil {
		return err
	}
	defer release()
	// Delete the database
	stmt := "DROP DATABASE IF EXISTS " + quoteIdentifier(name)
	_, err = db.ExecContext(ctx, stmt)
	return err
}

// ConnectToDatabase connects to the indicated database.
func (c *connection) ConnectToDatabase(ctx context.Context, name string) (driver.Database, error) {
	// Validate the database name
	if !validateDatabaseName(name) {
		return nil, errors.InvalidDatabaseName.New()
	}
	// Fetch (or open) a client connection
	db, release, err := connect(ctx, name, c.cfg)
	if err != nil {
		return nil, err
	}
	// Return the database
	return &database{
		db:      db,
		name:    name,
		release: release,
	}, nil
}

// Close closes the connection.
func (c *connection) Close() error {
	return nil
}

// Open opens a connection to a MySQL database.
func Open(ctx context.Context, cfg *ClientConfig) (keyvalue.Connection, error) {
	// If necessary fetch the default config
	if cfg == nil {
		cfg = DefaultConfig()
	}
	// Validate the config
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Create and wrap the connection
	c := &connection{
		cfg: cfg.Copy(),
	}
	return keyvalue.NewConnection(c), nil
}
