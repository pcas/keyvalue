// mysql_test provides tests for the mysql package.

//go:build integration
// +build integration

/*
This test requires mysqld to be running and listening on localhost:3306.

You must first create a test user account:

CREATE USER 'testuser'@'localhost' IDENTIFIED BY '<test user password>';
GRANT ALL PRIVILEGES ON `test\_%`.* TO 'testuser'@'localhost';

To run, do:

PCAS_MYSQL_HOST=localhost:3306 \
PCAS_MYSQL_USERNAME=testuser \
PCAS_MYSQL_PASSWORD=<test user password> \
go test -tags=integration
*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mysql

import (
	"bitbucket.org/pcas/keyvalue/internal/drivertest"
	"context"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

// contains returns true iff s is contained in S.
func contains(S []string, s string) bool {
	for _, x := range S {
		if x == s {
			return true
		}
	}
	return false
}

func TestDriver(t *testing.T) {
	// Create a context to run the tests
	ctx, cancel := context.WithTimeout(context.Background(), 40*time.Second)
	defer cancel()
	// Connect to MySQL
	c, err := Open(ctx, nil)
	require.NoError(t, err, "error opening connection")
	// Run the tests
	drivertest.Run(ctx, c, t)
	// Close the connection
	err = c.Close()
	require.NoError(t, err, "error closing connection")
}
