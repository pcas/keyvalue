// Mysqlflag provides a standard set of command line flags for the mysql driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mysqlflag

import (
	"bitbucket.org/pcas/keyvalue/mysql"
	"bitbucket.org/pcastools/flag"
	"errors"
	"fmt"
	"os"
)

// The environment variables consulted
const (
	EnvUsername = "PCAS_MYSQL_USERNAME"
	EnvPassword = "PCAS_MYSQL_PASSWORD"
	EnvHost     = "PCAS_MYSQL_HOST"
)

// Descriptions of environment variables.
const (
	unset       = "unset"
	setButEmpty = "set, but empty"
)

// hostFlag defines a flag for setting the MySQL host. It satisfies the flag.Flag interface.
type hostFlag struct {
	host *string // The host
}

// passwordFlag defines a flag for setting the MySQL password. It satisfies the flag.Flag interface.
type passwordFlag struct {
	password *string // The password
	toggle   *bool   // Toggle will be set to true on parse
}

// usernameFlag defines a flag for setting the MySQL username. It satisfies the flag.Flag interface.
type usernameFlag struct {
	username *string // The username
}

// Set represents a set of command-line flags defined by a client config.
type Set struct {
	c     *mysql.ClientConfig // The client config
	flags []flag.Flag         // The flags
}

/////////////////////////////////////////////////////////////////////////
// hostsFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*hostFlag) Name() string {
	return "mysql-host"
}

// Description returns a one-line description of this variable.
func (f *hostFlag) Description() string {
	return fmt.Sprintf("The MySQL host (default: \"%s\")", *f.host)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *hostFlag) Usage() string {
	// read the environment variable
	var status string
	if val, ok := os.LookupEnv(EnvHost); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	} else {
		status = "\"" + val + "\""
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvHost, status)
}

// Parse parses the string.
func (f *hostFlag) Parse(in string) error {
	*f.host = in
	return nil
}

// newHostFlag returns a new hostFlag. It has backing variable host.
func newHostFlag(host *string) flag.Flag {
	return &hostFlag{
		host: host,
	}
}

/////////////////////////////////////////////////////////////////////////
// passwordFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*passwordFlag) Name() string {
	return "mysql-password"
}

// Description returns a one-line description of this variable.
func (f *passwordFlag) Description() string {
	return fmt.Sprintf("The MySQL password (default: \"%s\")", *f.password)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *passwordFlag) Usage() string {
	// read the environment variable
	status := "set"
	if val, ok := os.LookupEnv(EnvPassword); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvPassword, status)
}

// Parse parses the string.
func (f *passwordFlag) Parse(in string) error {
	*f.password = in
	*f.toggle = true
	return nil
}

// newPasswordFlag returns a new passwordFlag. It has backing variable password, and sets toggle to true on parse.
func newPasswordFlag(password *string, toggle *bool) flag.Flag {
	return &passwordFlag{
		password: password,
		toggle:   toggle,
	}
}

/////////////////////////////////////////////////////////////////////////
// usernameFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*usernameFlag) Name() string {
	return "mysql-username"
}

// Description returns a one-line description of this variable.
func (f *usernameFlag) Description() string {
	return fmt.Sprintf("The MySQL username (default: \"%s\")", *f.username)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *usernameFlag) Usage() string {
	// read the environment variable
	var status string
	if val, ok := os.LookupEnv(EnvUsername); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	} else {
		status = "\"" + val + "\""
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvUsername, status)
}

// Parse parses the string.
func (f *usernameFlag) Parse(in string) error {
	*f.username = in
	return nil
}

// newUsernameFlag returns a new usernameFlag. It has backing variable username.
func newUsernameFlag(username *string) flag.Flag {
	return &usernameFlag{
		username: username,
	}
}

/////////////////////////////////////////////////////////////////////////
// Set functions
/////////////////////////////////////////////////////////////////////////

// NewSet returns a set of command-line flags with defaults given by c. If c is nil then the mysql.DefaultConfig() will be used. Note that this does not update the default client config, nor does this update c. To recover the updated client config after parse, call the ClientConfig() method on the returned set.
func NewSet(c *mysql.ClientConfig) *Set {
	// Either move to the default client config, or move to a copy
	if c == nil {
		c = mysql.DefaultConfig()
	} else {
		c = c.Copy()
	}
	// Create the flags
	flags := []flag.Flag{
		newPasswordFlag(&c.Password, &c.PasswordSet),
		newHostFlag(&c.Host),
		flag.Bool(
			"mysql-require-ssl",
			&c.RequireSSL, c.RequireSSL,
			"MySQL requires SSL",
			"",
		),
		flag.Int(
			"mysql-timeout",
			&c.ConnectTimeout, c.ConnectTimeout,
			"Maximum wait for MySQL, in seconds",
			"Setting -mysql-timeout to 0 means no timeout.",
		),
		newUsernameFlag(&c.Username),
	}
	// Return the set
	return &Set{
		c:     c,
		flags: flags,
	}
}

// Flags returns the members of the set.
func (s *Set) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	flags := make([]flag.Flag, len(s.flags))
	copy(flags, s.flags)
	return flags
}

// Name returns the name of this collection of flags.
func (*Set) Name() string {
	return "MySQL options"
}

// UsageFooter returns the footer for the usage message for this flag set.
func (*Set) UsageFooter() string {
	return ""
}

// UsageHeader returns the header for the usage message for this flag set.
func (*Set) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *Set) Validate() error {
	if s == nil {
		return errors.New("flag set is nil")
	}
	return s.c.Validate()
}

// ClientConfig returns the client config described by this set. This should only be called after the set has been successfully validated.
func (s *Set) ClientConfig() *mysql.ClientConfig {
	if s == nil {
		return mysql.DefaultConfig()
	}
	return s.c.Copy()
}

// SetDefault sets the default client config as described by this set. This should only be called after the set has been successfully validated.
func (s *Set) SetDefault() {
	c := s.ClientConfig()
	mysql.SetDefaultConfig(c)
}
