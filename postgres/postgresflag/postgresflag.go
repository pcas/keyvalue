// Postgresflag provides a standard set of command line flags for the postgres driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package postgresflag

import (
	"bitbucket.org/pcas/keyvalue/postgres"
	"bitbucket.org/pcastools/flag"
	"errors"
	"fmt"
	"os"
	"strings"
)

// The environment variables consulted
const (
	EnvUsername = "PCAS_POSTGRES_USERNAME"
	EnvPassword = "PCAS_POSTGRES_PASSWORD"
	EnvHost     = "PCAS_POSTGRES_HOST"
)

// Descriptions of environment variables.
const (
	unset       = "unset"
	setButEmpty = "set, but empty"
)

// hostsFlag defines a flag for setting the PostgreSQL hosts. It satisfies the flag.Flag interface.
type hostsFlag struct {
	hosts *[]string // The hosts
}

// passwordFlag defines a flag for setting the PostgreSQL password. It satisfies the flag.Flag interface.
type passwordFlag struct {
	password *string // The password
	toggle   *bool   // Toggle will be set to true on parse
}

// usernameFlag defines a flag for setting the PostgreSQL username. It satisfies the flag.Flag interface.
type usernameFlag struct {
	username *string // The username
}

// Set represents a set of command-line flags defined by a client config.
type Set struct {
	c     *postgres.ClientConfig // The client config
	flags []flag.Flag            // The flags
}

/////////////////////////////////////////////////////////////////////////
// hostsFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*hostsFlag) Name() string {
	return "postgres-host"
}

// Description returns a one-line description of this variable.
func (f *hostsFlag) Description() string {
	return fmt.Sprintf("The PostgreSQL hosts (default: \"%s\")", strings.Join(*f.hosts, ","))
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *hostsFlag) Usage() string {
	// read the environment variable
	var status string
	if val, ok := os.LookupEnv(EnvHost); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	} else {
		status = "\"" + val + "\""
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvHost, status)
}

// Parse parses the string.
func (f *hostsFlag) Parse(in string) error {
	*f.hosts = strings.Split(in, ",")
	return nil
}

// newHostsFlag returns a new hostsFlag. It has backing variable hosts.
func newHostsFlag(hosts *[]string) flag.Flag {
	return &hostsFlag{
		hosts: hosts,
	}
}

/////////////////////////////////////////////////////////////////////////
// passwordFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*passwordFlag) Name() string {
	return "postgres-password"
}

// Description returns a one-line description of this variable.
func (f *passwordFlag) Description() string {
	return fmt.Sprintf("The PostgreSQL password (default: \"%s\")", *f.password)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *passwordFlag) Usage() string {
	// read the environment variable
	status := "set"
	if val, ok := os.LookupEnv(EnvPassword); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvPassword, status)
}

// Parse parses the string.
func (f *passwordFlag) Parse(in string) error {
	*f.password = in
	*f.toggle = true
	return nil
}

// newPasswordFlag returns a new passwordFlag. It has backing variable password, and sets toggle to true on parse.
func newPasswordFlag(password *string, toggle *bool) flag.Flag {
	return &passwordFlag{
		password: password,
		toggle:   toggle,
	}
}

/////////////////////////////////////////////////////////////////////////
// usernameFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*usernameFlag) Name() string {
	return "postgres-username"
}

// Description returns a one-line description of this variable.
func (f *usernameFlag) Description() string {
	return fmt.Sprintf("The PostgreSQL username (default: \"%s\")", *f.username)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *usernameFlag) Usage() string {
	// read the environment variable
	var status string
	if val, ok := os.LookupEnv(EnvUsername); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	} else {
		status = "\"" + val + "\""
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvUsername, status)
}

// Parse parses the string.
func (f *usernameFlag) Parse(in string) error {
	*f.username = in
	return nil
}

// newUsernameFlag returns a new usernameFlag. It has backing variable username.
func newUsernameFlag(username *string) flag.Flag {
	return &usernameFlag{
		username: username,
	}
}

/////////////////////////////////////////////////////////////////////////
// standardSet functions
/////////////////////////////////////////////////////////////////////////

// NewSet returns a set of command-line flags with defaults given by c. If c is nil then the postgres.DefaultConfig() will be used. Note that this does not update the default client config, nor does this update c. To recover the updated client config after parse, call the ClientConfig() method on the returned set.
func NewSet(c *postgres.ClientConfig) *Set {
	// Either move to the default client config, or move to a copy
	if c == nil {
		c = postgres.DefaultConfig()
	} else {
		c = c.Copy()
	}
	// Create the flags
	flags := []flag.Flag{
		flag.Bool(
			"postgres-fast-copy",
			&c.FastCopy, c.FastCopy,
			"Allow the PostgreSQL COPY command",
			"If -postgres-fast-copy is set to true, PostgreSQL's fast COPY command will be used to perform inserts. Note that the COPY command opens up one connection per shard. If you run M concurrent copies into a destination with N shards, that will result in M*N connections.",
		),
		newHostsFlag(&c.Hosts),
		newPasswordFlag(&c.Password, &c.PasswordSet),
		flag.Bool(
			"postgres-require-ssl",
			&c.RequireSSL, c.RequireSSL,
			"PostgreSQL requires SSL",
			"",
		),
		flag.Int(
			"postgres-timeout",
			&c.ConnectTimeout, c.ConnectTimeout,
			"Maximum wait for PostgreSQL, in seconds",
			"Setting -postgres-timeout to 0 means no timeout.",
		),
		newUsernameFlag(&c.Username),
	}
	// Return the set
	return &Set{
		c:     c,
		flags: flags,
	}
}

// Flags returns the members of the set.
func (s *Set) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	flags := make([]flag.Flag, len(s.flags))
	copy(flags, s.flags)
	return flags
}

// Name returns the name of this collection of flags.
func (*Set) Name() string {
	return "PostgreSQL options"
}

// UsageFooter returns the footer for the usage message for this flag set.
func (*Set) UsageFooter() string {
	return ""
}

// UsageHeader returns the header for the usage message for this flag set.
func (*Set) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *Set) Validate() error {
	if s == nil {
		return errors.New("flag set is nil")
	}
	return s.c.Validate()
}

// ClientConfig returns the client config described by this set. This should only be called after the set has been successfully validated.
func (s *Set) ClientConfig() *postgres.ClientConfig {
	if s == nil {
		return postgres.DefaultConfig()
	}
	return s.c.Copy()
}

// SetDefault sets the default client config as described by this set. The given app name. This should only be called after the set has been successfully validated.
func (s *Set) SetDefault(name string) {
	c := s.ClientConfig()
	c.AppName = name
	postgres.SetDefaultConfig(c)
}
