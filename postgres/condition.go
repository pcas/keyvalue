// Condition provides functions that convert conditions to SQL statements.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package postgres

import (
	"bitbucket.org/pcas/keyvalue/condition"
	"fmt"
	"github.com/lib/pq"
	"strconv"
	"strings"
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// boolToString returns the given boolean as an SQL-formatted string.
func boolToString(b bool) string {
	if b {
		return "TRUE"
	}
	return "FALSE"
}

// formatCondition recursively converts the condition c to an SQL statement suitable for constructing the "where" clause. Also returns the parallel slice of value substitutions.
func formatCondition(c condition.Condition, values []interface{}) (string, []interface{}, error) {
	var s string
	switch cond := c.(type) {
	case condition.Bool: // True or False
		s = boolToString(bool(cond))
	case *condition.LeafOp: // LeafOp
		values = append(values, cond.Rhs())
		s = pq.QuoteIdentifier(cond.Lhs()) + " " +
			cond.OpCode().String() + " $" + strconv.Itoa(len(values))
	case *condition.IsOp: // IS
		s = pq.QuoteIdentifier(cond.Lhs()) + " IS " + boolToString(cond.Value())
	case *condition.InOp: // IN
		newValues := cond.Values()
		n, m := len(newValues), len(values)
		S := make([]string, 0, n)
		for i := 1; i <= n; i++ {
			S = append(S, "$"+strconv.Itoa(m+i))
		}
		s = pq.QuoteIdentifier(cond.Lhs()) + " IN (" +
			strings.Join(S, ", ") + ")"
		values = append(values, newValues...)
	case *condition.NotInOp: // NOT IN
		newValues := cond.Values()
		n, m := len(newValues), len(values)
		S := make([]string, 0, n)
		for i := 1; i <= n; i++ {
			S = append(S, "$"+strconv.Itoa(m+i))
		}
		s = pq.QuoteIdentifier(cond.Lhs()) + " NOT IN (" +
			strings.Join(S, ", ") + ")"
		values = append(values, newValues...)
	case *condition.BetweenOp: // BETWEEN
		m := len(values) + 1
		s = pq.QuoteIdentifier(cond.Lhs()) + " BETWEEN $" +
			strconv.Itoa(m) + " AND $" + strconv.Itoa(m+1)
		values = append(values, cond.Lower())
		values = append(values, cond.Upper())
	case *condition.NotBetweenOp: // NOT BETWEEN
		m := len(values) + 1
		s = pq.QuoteIdentifier(cond.Lhs()) + " NOT BETWEEN $" +
			strconv.Itoa(m) + " AND $" + strconv.Itoa(m+1)
		values = append(values, cond.Lower())
		values = append(values, cond.Upper())
	case *condition.AndOp: // AND
		conds := cond.Conditions()
		S := make([]string, 0, len(conds))
		for _, c := range conds {
			var t string
			var err error
			if t, values, err = formatCondition(c, values); err != nil {
				return "", nil, err
			}
			S = append(S, t)
		}
		s = strings.Join(S, " AND ")
	case *condition.OrOp: // OR
		conds := cond.Conditions()
		S := make([]string, 0, len(conds))
		for _, c := range conds {
			var t string
			var err error
			if t, values, err = formatCondition(c, values); err != nil {
				return "", nil, err
			}
			S = append(S, t)
		}
		s = strings.Join(S, " OR ")
	default:
		return "", nil, fmt.Errorf("unknown condition: %T", c)
	}
	return s, values, nil
}

// conditionToSQL converts the given Condition to the "where" part of an SQL query. Also returns the parallel slice of value substitutions. If the "where" part is empty, then the returns string will have length 0.
func conditionToSQL(cond condition.Condition, vals []interface{}) (string, []interface{}, error) {
	if cond == nil {
		return "", vals, nil
	}
	return formatCondition(cond, vals)
}
