// Options provides options parsing for the postgres driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package postgres

import (
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/stringsbuilder"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
)

// ClientConfig describes the configuration options we allow a user to set on a client connection.
//
// Hosts: If a host name begins with a slash, it specifies Unix-domain communication rather than TCP/IP communication; the value is the name of the directory in which the socket file is stored.
//
// FastCopy: If enabled, the COPY command will be used to perform inserts. Note that, with the citus extension for sharded tables enabled, the COPY command opens up one connection per shard (see http://docs.citusdata.com/en/v8.2/reference/common_errors.html). If you run M concurrent copies into a destination with N shards, that will result in M*N connections. In this circumstance you probably want to disable the COPY command, as it is likely to exhaust the number of available connections.
type ClientConfig struct {
	AppName        string   // The application name to identify ourselves as. This may appear in any logs PostgreSQL produces.
	Hosts          []string // The hosts
	Username       string   // The user to sign in as
	Password       string   // The user's password
	PasswordSet    bool     // Is a password set?
	ConnectTimeout int      // Maximum wait for connection, in seconds. Zero means wait indefinitely.
	RequireSSL     bool     // Whether or not to require SSL
	FastCopy       bool     // Whether to support PostgreSQL's COPY commands
}

// The default values for the client configuration, along with controlling mutex. The initial default values are assigned at init.
var (
	defaultsm sync.Mutex
	defaults  *ClientConfig
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets the initial default values. The following environment variables are consulted:
//	PCAS_POSTGRES_HOST = "hostname1[:port1][,...,hostnamek[:portk]]"
//	PCAS_POSTGRES_USERNAME = "myname"
//	PCAS_POSTGRES_PASSWORD = "mysecret"
func init() {
	// Create the initial defaults
	defaults = &ClientConfig{
		AppName: "keyvalue.postgres",
		Hosts:   []string{"localhost:5432"},
	}
	// Modify the defaults based on environment variables
	if hosts, ok := os.LookupEnv("PCAS_POSTGRES_HOST"); ok {
		defaults.Hosts = strings.Split(hosts, ",")
	}
	if username, ok := os.LookupEnv("PCAS_POSTGRES_USERNAME"); ok {
		defaults.Username = username
	}
	if password, ok := os.LookupEnv("PCAS_POSTGRES_PASSWORD"); ok {
		defaults.Password = password
		defaults.PasswordSet = true
	}
}

// writeOptionValue writes the quoted form of the given string to b.
func writeOptionValue(s string, b *strings.Builder) {
	if len(s) == 0 {
		b.WriteByte('\'')
		b.WriteByte('\'')
		return
	} else if !strings.ContainsAny(s, " '\\") {
		b.WriteString(s)
		return
	}
	b.WriteByte('\'')
	s = strings.ReplaceAll(s, "\\", "\\\\")
	s = strings.ReplaceAll(s, "'", "\\'")
	b.WriteString(s)
	b.WriteByte('\'')
}

// writeOptionKeyValue writes the given key and (quoted form of) value to b in the form "key=value".
func writeOptionKeyValue(key string, value string, b *strings.Builder) {
	b.WriteString(key)
	b.WriteByte('=')
	writeOptionValue(value, b)
}

/////////////////////////////////////////////////////////////////////////
// ClientConfig functions
/////////////////////////////////////////////////////////////////////////

// DefaultConfig returns a new client configuration initialised with the default values.
//
// The initial default value for the hosts, username, and password will be read from the environment variables
//	PCAS_POSTGRES_HOST = "hostname1[:port1][,...,hostnamek[:portk]]"
//	PCAS_POSTGRES_USERNAME = "myname"
//	PCAS_POSTGRES_PASSWORD = "mysecret"
// respectively on package init.
//
// Note that if PCAS_POSTGRES_PASSWORD is set but with value equal to the empty string then it still counts as a valid password, since the empty string is a valid password. You must ensure that PCAS_POSTGRES_PASSWORD is unset if you do not want to specify an initial default password.
func DefaultConfig() *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	return defaults.Copy()
}

// SetDefaultConfig sets the default client configuration to c and returns the old default configuration. This change will be reflected in future calls to DefaultConfig.
func SetDefaultConfig(c *ClientConfig) *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	oldc := defaults
	defaults = c.Copy()
	return oldc
}

// Validate validates the client configuration, returning an error if there's a problem.
func (c *ClientConfig) Validate() error {
	// Get the nil case out of the way
	if c == nil {
		return errors.New("illegal nil configuration data")
	}
	// Start validating
	if c.ConnectTimeout < 0 {
		return fmt.Errorf("invalid connection timeout: %d", c.ConnectTimeout)
	} else if len(c.Password) != 0 && !c.PasswordSet {
		return errors.New("a password appears to be given, however the value of PasswordSet is 'false'")
	}
	for _, host := range c.Hosts {
		if idx := strings.IndexByte(host, ':'); idx != -1 {
			if idx == len(host)-1 {
				return errors.New("invalid port number")
			} else if n, err := strconv.Atoi(host[idx+1:]); err != nil {
				return fmt.Errorf("invalid port number: %w", err)
			} else if n < 1 || n > 65535 {
				return fmt.Errorf("port number (%d) out of range", n)
			}
		}
	}
	// Looks good
	return nil
}

// Copy returns a copy of the configuration.
func (c *ClientConfig) Copy() *ClientConfig {
	cc := *c
	cc.Hosts = make([]string, len(c.Hosts))
	copy(cc.Hosts, c.Hosts)
	return &cc
}

// Hash returns a hash for the configuration.
func (c *ClientConfig) Hash() uint32 {
	h := hash.String(c.AppName)
	h = hash.Combine(h, hash.StringSlice(c.Hosts))
	h = hash.Combine(h, hash.String(c.Username))
	h = hash.Combine(h, hash.Bool(c.PasswordSet))
	if c.PasswordSet {
		h = hash.Combine(h, hash.String(c.Password))
	}
	h = hash.Combine(h, hash.Int(c.ConnectTimeout))
	h = hash.Combine(h, hash.Bool(c.RequireSSL))
	h = hash.Combine(h, hash.Bool(c.FastCopy))
	return h
}

// toPqOptions converts the configuration to a string of pq connection options, connecting to the database dbName.
func (c *ClientConfig) toPqOptions(dbName string) string {
	// Grab a strings builder
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	// Start writing the options
	writeOptionKeyValue("dbname", dbName, b)
	b.WriteByte(' ')
	writeOptionKeyValue("fallback_application_name", c.AppName, b)
	b.WriteByte(' ')
	writeOptionKeyValue("connect_timeout", strconv.Itoa(c.ConnectTimeout), b)
	b.WriteByte(' ')
	if c.RequireSSL {
		writeOptionKeyValue("sslmode", "require", b)
	} else {
		writeOptionKeyValue("sslmode", "disable", b)
	}
	if len(c.Username) != 0 {
		b.WriteByte(' ')
		writeOptionKeyValue("user", c.Username, b)
	}
	if c.PasswordSet {
		b.WriteByte(' ')
		writeOptionKeyValue("password", c.Password, b)
	}
	host := make([]string, 0)
	port := make([]string, 0)
	hasPort := false
	for _, h := range c.Hosts {
		T := strings.SplitN(h, ":", 2)
		host = append(host, T[0])
		if len(T) == 2 {
			port = append(port, T[1])
			hasPort = true
		} else {
			port = append(port, "")
		}
	}
	if len(host) != 0 {
		b.WriteByte(' ')
		writeOptionKeyValue("host", strings.Join(host, ","), b)
		if hasPort {
			b.WriteByte(' ')
			writeOptionKeyValue("port", strings.Join(port, ","), b)
		}
	}
	// Return the string
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// AreEqual returns true iff the two configurations are equal.
func AreEqual(c *ClientConfig, d *ClientConfig) bool {
	if c.AppName != d.AppName ||
		len(c.Hosts) != len(d.Hosts) ||
		c.Username != d.Username ||
		c.PasswordSet != d.PasswordSet ||
		c.ConnectTimeout != d.ConnectTimeout ||
		c.RequireSSL != d.RequireSSL ||
		c.FastCopy != d.FastCopy {
		return false
	}
	if c.PasswordSet && (c.Password != d.Password) {
		return false
	}
	for i, s := range c.Hosts {
		if d.Hosts[i] != s {
			return false
		}
	}
	return true
}
