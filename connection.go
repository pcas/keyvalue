// Connection defines a key-value connection.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"io"
	"sort"
	"sync"
	"time"
)

// timeout is the maximum amount of time allowed attempting to submit a metric point.
const timeout = 100 * time.Millisecond

// Connection provides a connection.
type Connection interface {
	metrics.Metricsable
	log.Logable
	io.Closer
	// DriverName returns the name of the associated driver.
	DriverName() string
	// ListDatabases returns the names of the available databases. The names
	// are sorted in increasing order. This is not supported by every driver,
	// in which case errors.OperationNotSupported will be returned.
	ListDatabases(ctx context.Context) ([]string, error)
	// CreateDatabase creates a database with the given name on the
	// connection.
	CreateDatabase(ctx context.Context, name string) error
	// DeleteDatabase deletes the indicated database on the connection. Does
	// not return an error if the table does not exist.
	DeleteDatabase(ctx context.Context, name string) error
	// ConnectToDatabase connects to the indicated database.
	ConnectToDatabase(ctx context.Context, name string) (Database, error)
}

// connection implements the Connection interface.
type connection struct {
	log.BasicLogable
	metrics.BasicMetricsable
	c        driver.Connection // The underlying connection
	cache    dbCache           // A cache of databases
	s        closeSet          // The set of open databases
	m        sync.RWMutex      // Mutex controlling access to the following
	isClosed bool              // Is the connection closed?
	err      error             // The error on close (if any)
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// submitPoint submits to the metrics endpoint m a point with the specified name and fields. The duration d will be automatically added to the fields.
func submitPoint(m metrics.Interface, name string, fields metrics.Fields, d time.Duration) {
	fields["Time"] = d
	pt, err := metrics.NewPoint("keyvalue."+name, fields, nil)
	if err == nil {
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()
		m.Submit(ctx, pt) // Ignore any error
	}
}

// connectToDatabase attempts to connect to the named database using the connection c.
func connectToDatabase(ctx context.Context, c *connection, name string) (Database, error) {
	// Connect to the database
	db, err := c.cache.Get(ctx, name)
	if err != nil {
		return nil, err
	}
	// Wrap the logger
	lg := log.PrefixWith(c.Log(), "[database=%s]", name)
	// Wrap the database up as a *Database
	d := &database{
		name:  name,
		d:     db,
		cache: newTableCache(db, log.PrefixWith(lg, "[tablecache]")),
		lg:    lg,
		met:   metrics.TagWith(c.Metrics(), metrics.Tags{"Database": name}),
	}
	// Add the wrapped database to the set of open databases
	d.closef = c.s.Include(d)
	return d, nil
}

//////////////////////////////////////////////////////////////////////
// connection functions
//////////////////////////////////////////////////////////////////////

// DriverName returns the name of the associated driver.
func (c *connection) DriverName() string {
	if c == nil {
		return ""
	}
	return c.c.DriverName()
}

// Close closes the connection to the database.
func (c *connection) Close() error {
	// Sanity check
	if c == nil || c.c == nil {
		return nil
	}
	err := func() (err error) {
		// Acquire a write lock
		c.m.Lock()
		defer c.m.Unlock()
		// Is there anything to do?
		if !c.isClosed {
			// Recover from any panics in the driver
			defer func() {
				if e := recover(); e != nil {
					err = fmt.Errorf("panic in Close: %v", e)
				}
			}()
			// Close the open tables
			c.s.Close()
			// Close the cache
			c.cache.Close()
			// Close the underlying connection and mark us as closed
			c.err, c.isClosed = c.c.Close(), true
		}
		// Note and errors and return
		err = c.err
		return
	}()
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.ConnectionCloseError.Wrap(err)
	}
	// Provide log data before returning
	if err == nil {
		c.Log().Printf("Connection to %s closed", c.DriverName())
	} else {
		c.Log().Printf("Connection to %s closed with error: %v", c.DriverName(), err)
	}
	return err
}

// ListDatabases returns the names of the available databases. The names are sorted in increasing order. This is not supported by every driver, in which case errors.OperationNotSupported will be returned.
func (c *connection) ListDatabases(ctx context.Context) ([]string, error) {
	// Sanity check
	if c == nil || c.c == nil {
		return nil, errors.ConnectionClosed.New()
	}
	now := time.Now()
	S, err := func() (S []string, err error) {
		// Acquire a read lock
		c.m.RLock()
		defer c.m.RUnlock()
		// Are we closed?
		if c.isClosed {
			err = errors.ConnectionClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in ListTables: %v", e)
			}
		}()
		// List the databases
		S, err = c.c.ListDatabases(ctx)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToListDatabases.Wrap(err)
	}
	// Provide log and metric data
	submitPoint(c.Metrics(), "ListDatabases", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err != nil {
		c.Log().Printf("ListDatabases failed after %s: %v", dur, err)
		return nil, err
	}
	c.Log().Printf("ListDatabases succeeded in %s", dur)
	// Sort the databases before returning
	sort.Strings(S)
	return S, nil
}

// CreateDatabase creates a database with the given name on the connection.
func (c *connection) CreateDatabase(ctx context.Context, name string) error {
	// Sanity check
	if c == nil || c.c == nil {
		return errors.ConnectionClosed.New()
	} else if len(name) == 0 {
		return errors.InvalidDatabaseName.New()
	}
	now := time.Now()
	err := func() (err error) {
		// Acquire a read lock
		c.m.RLock()
		defer c.m.RUnlock()
		// Are we closed?
		if c.isClosed {
			err = errors.ConnectionClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in CreateDatabase: %v", e)
			}
		}()
		// Create the database
		err = c.c.CreateDatabase(ctx, name)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToCreateDatabase.Wrap(err)
	}
	// Provide log and metric data
	submitPoint(c.Metrics(), "CreateDatabase", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		c.Log().Printf("Create database \"%s\" succeeded in %s", name, dur)
	} else {
		c.Log().Printf("Create database \"%s\" failed after %s: %v", name, dur, err)
	}
	return err
}

// DeleteDatabase deletes the indicated database on the connection. Does not return an error if the table does not exist.
func (c *connection) DeleteDatabase(ctx context.Context, name string) error {
	// Sanity check
	if c == nil || c.c == nil {
		return errors.ConnectionClosed.New()
	} else if len(name) == 0 {
		return errors.InvalidDatabaseName.New()
	}
	now := time.Now()
	err := func() (err error) {
		// Acquire a write lock
		c.m.Lock()
		defer c.m.Unlock()
		// Are we closed?
		if c.isClosed {
			err = errors.ConnectionClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in DeleteDatabase: %v", e)
			}
		}()
		// Delete the database and mark the cache as stale
		err = c.c.DeleteDatabase(ctx, name)
		c.cache.MarkAsStale(name)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToDeleteDatabase.Wrap(err)
	}
	// Provide log and metric data
	submitPoint(c.Metrics(), "DeleteDatabase", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		c.Log().Printf("Delete database \"%s\" succeeded in %s", name, dur)
	} else {
		c.Log().Printf("Delete database \"%s\" failed after %s: %v", name, dur, err)
	}
	return err
}

// ConnectToDatabase connects to the indicated database.
func (c *connection) ConnectToDatabase(ctx context.Context, name string) (Database, error) {
	// Sanity check
	if c == nil || c.c == nil {
		return nil, errors.ConnectionClosed.New()
	} else if len(name) == 0 {
		return nil, errors.InvalidDatabaseName.New()
	}
	now := time.Now()
	d, err := func() (d Database, err error) {
		// Acquire a read lock
		c.m.RLock()
		defer c.m.RUnlock()
		// Are we closed?
		if c.isClosed {
			err = errors.ConnectionClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in ConnectToDatabase: %v", e)
			}
		}()
		// Connect to the database
		d, err = connectToDatabase(ctx, c, name)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToConnectToDatabase.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(c.Metrics(), "ConnectToDatabase", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err != nil {
		c.Log().Printf("Connection to database \"%s\" failed after %s: %v", name, dur, err)
		return nil, err
	}
	c.Log().Printf("Connected to database \"%s\" in %s", name, dur)
	return d, nil
}

// SetMetrics sets a metrics endpoint.
func (c *connection) SetMetrics(m metrics.Interface) {
	c.BasicMetricsable.SetMetrics(metrics.TagWith(m, metrics.Tags{
		"DriverName": c.DriverName(),
	}))
}

// NewConnection returns a new connection wrapping the given driver connection.
func NewConnection(c driver.Connection) Connection {
	cc := &connection{
		c: c,
	}
	cc.cache = newDBCache(c, log.PrefixWith(cc.Log(), "[dbcache]"))
	return cc
}
