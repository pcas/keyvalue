// Kvcache defines a cache of elements with arbitrary keys.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvcache

import (
	"context"
	"errors"
	"sync"
	"time"
)

// purgeInterval is the duration between purges of the cache.
const purgeInterval = time.Minute

// Hasher is an object with a Hash method.
type Hasher interface {
	Hash() uint32
}

// Element is an entry in the cache.
type Element[K Hasher, V any] struct {
	e          *element[K, V] // The wrapped element
	m          sync.Mutex     // Mutex controlling access to the following
	isReleased bool           // Has the element been released?
}

// element is an entry in the cache. This is for internal use only.
type element[K Hasher, V any] struct {
	c          *Cache[K, V] // The parent cache
	k          K            // The key
	m          sync.Mutex   // Mutex controlling the following
	hasValue   bool         // Is the value set?
	v          V            // The value
	refCount   int          // The ref count for this element
	expiryTime time.Time    // Purge this element after this time
}

// Cache is a cache of elements.
type Cache[K Hasher, V any] struct {
	create   CreationFunc[K, V]        // The creation function for values
	isAlive  IsAliveFunc[V]            // Test if a value is alive
	close    CloseFunc[V]              // Closes a value
	equals   EqualsFunc[K]             // The equals function for keys
	m        sync.Mutex                // Mutex controlling the following
	cache    map[uint32]*element[K, V] // The cache of elements
	exitC    chan<- struct{}           // Close to stop the worker
	isClosed bool                      // Are we closed?
	closeErr error                     // The error on close (if any)
}

// CreationFunc creates a new value for the given key.
type CreationFunc[K Hasher, V any] func(context.Context, K) (V, error)

// IsAliveFunc tests whether the given value is alive.
type IsAliveFunc[V any] func(context.Context, V) bool

// CloseFunc closes the given value.
type CloseFunc[V any] func(V) error

// EqualsFunc returns true iff two keys are equal.
type EqualsFunc[K Hasher] func(K, K) bool

//////////////////////////////////////////////////////////////////////
// Element functions
//////////////////////////////////////////////////////////////////////

// Key returns the key for this element. This can be safely called after a call to Release.
func (e *Element[K, _]) Key() K {
	return e.e.Key()
}

// Value returns the value for this element. Calling Value after a call to Release will panic.
func (e *Element[_, V]) Value(ctx context.Context) (V, error) {
	e.m.Lock()
	defer e.m.Unlock()
	if e.isReleased {
		panic("attempting to call Value after Release")
	}
	return e.e.Value(ctx)
}

// Release decrements the ref count for this element. This must be called once you are finished with this element. It is safe to call Release multiple times.
func (e *Element[_, _]) Release() {
	e.m.Lock()
	defer e.m.Unlock()
	if !e.isReleased {
		e.e.Release()
		e.isReleased = true
	}
}

/////////////////////////////////////////////////////////////////////////
// element functions
/////////////////////////////////////////////////////////////////////////

// Key returns the key for this element.
func (e *element[K, _]) Key() K {
	return e.k
}

// Value returns the value for this element.
func (e *element[_, V]) Value(ctx context.Context) (V, error) {
	// Acquire a lock
	e.m.Lock()
	defer e.m.Unlock()
	// Is a value set? Is the value still alive?
	if !e.hasValue || !e.c.isAlive(ctx, e.v) {
		// Create a new value
		v, err := e.c.create(ctx, e.k)
		if err != nil {
			return v, err
		}
		// Note: We simply replace any dead value with the new value
		e.hasValue = true
		e.v = v
	}
	// Return the value
	return e.v, nil
}

// Release decrements the ref count for this element.
func (e *element[_, _]) Release() {
	e.m.Lock()
	defer e.m.Unlock()
	e.refCount--
	if e.refCount == 0 {
		e.expiryTime = time.Now().Add(purgeInterval)
	}
}

// Acquire increments the ref count for this element and returns the wrapped *Element.
func (e *element[K, V]) Acquire() *Element[K, V] {
	e.m.Lock()
	defer e.m.Unlock()
	e.refCount++
	return &Element[K, V]{
		e: e,
	}
}

// Purge purges the underlying value, irrespective of the current ref count.
func (e *element[_, V]) Purge() (err error) {
	e.m.Lock()
	defer e.m.Unlock()
	if e.hasValue {
		e.hasValue = false
		err = e.c.close(e.v)
	}
	return
}

//////////////////////////////////////////////////////////////////////
// Cache functions
//////////////////////////////////////////////////////////////////////

// startPruneWorker starts a background worker calling prune every purgeInterval.
func (c *Cache[_, _]) startPruneWorker() {
	// Create and save the communication channel
	exitC := make(chan struct{})
	c.exitC = exitC
	// Start the worker
	go func(exitC <-chan struct{}) {
		t := time.NewTimer(purgeInterval)
		defer t.Stop()
		for {
			select {
			case <-t.C:
				c.prune()
				t.Reset(purgeInterval)
			case <-exitC:
				return
			}
		}
	}(exitC)
}

// prune removed any expired elements from the cache with zero ref count.
func (c *Cache[K, V]) prune() {
	// Delete the elements with zero ref count from the cache
	es := func() []*element[K, V] {
		// Acquire a lock
		c.m.Lock()
		defer c.m.Unlock()
		// Is there anything to do?
		if c.isClosed || c.cache == nil {
			return nil
		}
		// Make a note of the time
		now := time.Now()
		// Walk the cache looking for elements with zero ref count
		hs, es := make([]uint32, 0), make([]*element[K, V], 0)
		for h, e := range c.cache {
			// Note: We still acquire a lock on e's mutex here because it's
			// possible for refCount to be decremented whilst we perform this
			// check. However it's impossible for refCount to be incremented
			// until we return, since we hold a lock on c.
			e.m.Lock()
			if e.refCount == 0 && e.expiryTime.Before(now) {
				hs = append(hs, h)
				es = append(es, e)
			}
			e.m.Unlock()
		}
		// Remove them from the cache
		for _, h := range hs {
			delete(c.cache, h)
		}
		// Return the slice of elements
		return es
	}()
	// Now we purge the elements
	for _, e := range es {
		e.Purge() // Ignore any errors
	}
}

// Close closes the cache.
func (c *Cache[_, _]) Close() error {
	// Acquire a lock
	c.m.Lock()
	defer c.m.Unlock()
	// Are we closed?
	if !c.isClosed {
		// Mark us as closed
		c.isClosed = true
		// Ask the background worker to exit
		if c.exitC != nil {
			close(c.exitC)
		}
		// Clear the cache
		var err error
		for _, e := range c.cache {
			if closeErr := e.Purge(); err == nil {
				err = closeErr
			}
		}
		c.closeErr = err
		c.cache = nil
	}
	return c.closeErr
}

// Get returns the element with given key.
func (c *Cache[K, V]) Get(k K) (*Element[K, V], error) {
	// Compute the hash
	h := k.Hash()
	// Acquire a lock
	c.m.Lock()
	defer c.m.Unlock()
	// Is there anything to do?
	if c.isClosed {
		return nil, errors.New("cache closed")
	}
	// If necessary, start the background worker
	if c.exitC == nil {
		c.startPruneWorker()
	}
	// If necessary, create the cache
	if c.cache == nil {
		c.cache = make(map[uint32]*element[K, V])
	}
	// Is the key in the cache? Allow for the possibility of a hash collision.
	e, ok := c.cache[h]
	for ok && !c.equals(k, e.k) {
		h++
		e, ok = c.cache[h]
	}
	if !ok {
		// No luck -- add the element to the cache
		e = &element[K, V]{
			c: c,
			k: k,
		}
		c.cache[h] = e
	}
	// Increment the ref count for the element and return the wrapped element
	return e.Acquire(), nil
}

// New returns a new cache.
func New[K Hasher, V any](create CreationFunc[K, V], isAlive IsAliveFunc[V], close CloseFunc[V], equals EqualsFunc[K]) *Cache[K, V] {
	return &Cache[K, V]{
		create:  create,
		isAlive: isAlive,
		close:   close,
		equals:  equals,
	}
}
