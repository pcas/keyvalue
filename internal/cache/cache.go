// Cache defines a cache of elements with string keys.
/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package cache

import (
	"bitbucket.org/pcastools/log"
	"context"
	"io"
	"sync"
	"time"
)

// purgeInterval is the duration between purges of the cache.
const purgeInterval = time.Minute

// Element is an entry in the cache.
type Element[V io.Closer] struct {
	k          string     // The key
	v          V          // The value
	m          sync.Mutex // Mutex controlling access to the following
	release    func()     // The release function
	isReleased bool       // Has the element been released?
}

// element is an entry in the cache. This is for internal use only.
type element[V io.Closer] struct {
	k          string     // The key for this element
	v          V          // The value for this element
	m          sync.Mutex // Mutex controlling the following
	refCount   int        // The ref count for this element
	expiryTime time.Time  // Purge this element after this time
}

// Cache is a cache of elements.
type Cache[V io.Closer] struct {
	create   CreationFunc[V]        // The creation function for values
	lg       log.Interface          // The logger
	m        sync.RWMutex           // Mutex controlling the following
	cache    map[string]*element[V] // The cache of elements
	stale    []*element[V]          // The slice of stale cache elements
	exitC    chan<- struct{}        // Close to stop the worker
	isClosed bool                   // Are we closed?
	closeErr error                  // The error on close (if any)
}

// CreationFunc creates a new value for the given key.
type CreationFunc[V io.Closer] func(context.Context, string) (V, error)

//////////////////////////////////////////////////////////////////////
// Element functions
//////////////////////////////////////////////////////////////////////

// Key returns the key for this element. This can be safely called after a call to Release.
func (e *Element[V]) Key() string {
	return e.k
}

// Value returns the value for this element. Calling Value after a call to Release will panic.
func (e *Element[V]) Value() V {
	e.m.Lock()
	defer e.m.Unlock()
	if e.isReleased {
		panic("attempting to call Value after Release")
	}
	return e.v
}

// Release decrements the ref count for this element. This must be called once you are finished with this element. It is safe to call Release multiple times.
func (e *Element[_]) Release() {
	e.m.Lock()
	defer e.m.Unlock()
	if !e.isReleased {
		e.release()
		e.isReleased, e.release = true, nil
	}
}

// newElement returns a new element wrapping the given key and value.
func newElement[V io.Closer](k string, v V) *Element[V] {
	return &Element[V]{
		k: k,
		v: v,
		release: func() {
			v.Close() // Ignore any errors
		},
	}
}

//////////////////////////////////////////////////////////////////////
// element functions
//////////////////////////////////////////////////////////////////////

// Key returns the key for this element.
func (e *element[_]) Key() string {
	return e.k
}

// Release decrements the ref count for this element.
func (e *element[_]) Release() {
	e.m.Lock()
	defer e.m.Unlock()
	e.refCount--
	if e.refCount == 0 {
		e.expiryTime = time.Now().Add(purgeInterval)
	}
}

// Acquire increments the ref count for this element and returns the wrapped *Element.
func (e *element[V]) Acquire() *Element[V] {
	e.m.Lock()
	defer e.m.Unlock()
	e.refCount++
	return &Element[V]{
		k:       e.Key(),
		v:       e.v,
		release: e.Release,
	}
}

// Purge calls close on the underlying value, irrespective of the current ref count.
func (e *element[_]) Purge() error {
	e.m.Lock()
	defer e.m.Unlock()
	return e.v.Close()
}

//////////////////////////////////////////////////////////////////////
// Cache functions
//////////////////////////////////////////////////////////////////////

// startPruneWorker starts a background worker calling prune every purgeInterval.
func (c *Cache[_]) startPruneWorker() {
	// Create and save the communication channel
	exitC := make(chan struct{})
	c.exitC = exitC
	// Start the worker
	go func(exitC <-chan struct{}) {
		t := time.NewTimer(purgeInterval)
		defer t.Stop()
		for {
			select {
			case <-t.C:
				c.prune()
				t.Reset(purgeInterval)
			case <-exitC:
				return
			}
		}
	}(exitC)
}

// prune removes any expired elements from the cache with zero ref count.
func (c *Cache[V]) prune() {
	// Delete the elements with zero ref count from the cache
	es := func() []*element[V] {
		// Acquire a write lock
		c.m.Lock()
		defer c.m.Unlock()
		// Is there anything to do?
		if c.isClosed || c.cache == nil {
			return nil
		}
		// Make a note of the time
		now := time.Now()
		// Walk the cache looking for elements with zero ref count
		es := make([]*element[V], 0)
		for _, e := range c.cache {
			// Note: We still acquire a lock on e's mutex here because it's
			// possible for refCount to be decremented whilst we perform this
			// check. However it's impossible for refCount to be incremented
			// until we return, since we hold a write lock on c.
			e.m.Lock()
			if e.refCount == 0 && e.expiryTime.Before(now) {
				es = append(es, e)
			}
			e.m.Unlock()
		}
		// Remove them from the cache
		for _, e := range es {
			delete(c.cache, e.Key())
		}
		// Walk the cache of stale elements looking for zero ref count
		if len(c.stale) != 0 {
			stale := make([]*element[V], 0)
			for _, e := range c.stale {
				e.m.Lock()
				if e.refCount == 0 {
					es = append(es, e)
				} else {
					stale = append(stale, e)
				}
				e.m.Unlock()
			}
			// Update the slice of stale elements
			c.stale = stale
		}
		// Return the slice of elements
		return es
	}()
	// Now we purge the elements
	for _, e := range es {
		if err := e.Purge(); err != nil {
			c.lg.Printf("Closing \"%s\" failed: %v", e.Key(), err)
		}
	}
}

// Close closes the cache.
func (c *Cache[_]) Close() error {
	// Acquire a write lock
	c.m.Lock()
	defer c.m.Unlock()
	// Are we closed?
	if !c.isClosed {
		// Mark us as closed
		c.isClosed = true
		// Ask the background worker to exit
		if c.exitC != nil {
			close(c.exitC)
		}
		// Clear the cache
		var err error
		for _, e := range c.cache {
			if closeErr := e.Purge(); closeErr != nil {
				c.lg.Printf("Closing \"%s\" failed: %v", e.Key(), closeErr)
				if err == nil {
					err = closeErr
				}
			}
		}
		// Clear the stale elements
		for _, e := range c.stale {
			if closeErr := e.Purge(); closeErr != nil {
				c.lg.Printf("Closing \"%s\" failed: %v", e.Key(), closeErr)
				if err == nil {
					err = closeErr
				}
			}
		}
		c.closeErr = err
		c.cache = nil
		c.stale = nil
	}
	return c.closeErr
}

// fetch attempts to fetch the element for the given key.
func (c *Cache[V]) fetch(k string) (*Element[V], bool) {
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Attempt to recover the element from the cache
	if c.isClosed || c.cache == nil {
		return nil, false
	}
	e, ok := c.cache[k]
	if !ok {
		return nil, false
	}
	// Increment the ref count for the element and return the wrapped element
	return e.Acquire(), true
}

// set sets the value for the given key and returns the element.
func (c *Cache[V]) set(k string, v V) *Element[V] {
	// Acquire a write lock
	c.m.Lock()
	defer c.m.Unlock()
	// Are we closed?
	if c.isClosed {
		return newElement(k, v)
	}
	// Ensure that the background worker is running
	if c.exitC == nil {
		c.startPruneWorker()
	}
	// Ensure that the map exists
	if c.cache == nil {
		c.cache = make(map[string]*element[V])
	}
	// It's possible that the value was added to the cache whilst we were busy
	if _, ok := c.cache[k]; ok {
		return newElement(k, v)
	}
	// Add the key-value element to the cache
	e := &element[V]{
		k: k,
		v: v,
	}
	c.cache[k] = e
	// Increment the ref count for the element and return the wrapped element
	return e.Acquire()
}

// Get returns the element with given key.
func (c *Cache[V]) Get(ctx context.Context, k string) (*Element[V], error) {
	// Check if this element is already in the cache
	if e, ok := c.fetch(k); ok {
		return e, nil
	}
	// No luck -- create the value for the key
	v, err := c.create(ctx, k)
	if err != nil {
		return nil, err
	}
	// Record the value in the cache and return
	return c.set(k, v), nil
}

// MarkAsStale marks the element with given key as stale. The next call to Get will force the element to be (re)created rather than using any cached copy.
func (c *Cache[V]) MarkAsStale(k string) {
	// Acquire a write lock
	c.m.Lock()
	defer c.m.Unlock()
	// Attempt to recover the element from the cache
	if c.isClosed || c.cache == nil {
		return
	}
	e, ok := c.cache[k]
	if !ok {
		return
	}
	// Delete the element from the cache and place it on the stale slice
	delete(c.cache, k)
	if c.stale == nil {
		c.stale = make([]*element[V], 0)
	}
	c.stale = append(c.stale, e)
}

// New returns a new cache.
func New[V io.Closer](create CreationFunc[V], lg log.Interface) *Cache[V] {
	return &Cache[V]{
		create: create,
		lg:     lg,
	}
}
