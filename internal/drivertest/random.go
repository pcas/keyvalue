// Random describes a record iterator for generating random test data.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package drivertest

import (
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"context"
	"crypto/rand"
)

// RandomIterator is an iterator that generates records containing a large volume of random data. Each record will be of the form:
//
//	"id"   => an int64 counter, incremented on each call to Next
//	"data" => a []byte slice of length 1MB filled with random data
//
// Note that a RandomIterator will generate an infinite number of records. You must wrap it in some form of limiting iterator.
type RandomIterator struct {
	id       int64         // The current id
	cache    []byte        // The cache of data
	r        record.Record // The current record
	isClosed bool          // Are we closed?
}

/////////////////////////////////////////////////////////////////////////
// RandomIterator functions
/////////////////////////////////////////////////////////////////////////

// Close closes the iterator, preventing further iteration.
func (itr *RandomIterator) Close() error {
	if itr != nil {
		itr.isClosed = true
		itr.cache, itr.r = nil, nil
	}
	return nil
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (*RandomIterator) Err() error {
	return nil
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *RandomIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return ok && err == nil
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *RandomIterator) NextContext(ctx context.Context) (bool, error) {
	if itr == nil || itr.isClosed {
		return false, errors.IterationCloseError.New()
	} else if itr.r == nil {
		itr.cache = make([]byte, 1024*1024)
		itr.r = make(record.Record)
	}
	itr.id++
	rand.Read(itr.cache) // Ignore any errors
	itr.r["id"] = itr.id
	itr.r["data"] = itr.cache
	return true, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (itr *RandomIterator) Scan(dest record.Record) error {
	if itr == nil || itr.isClosed {
		return errors.IterationCloseError.New()
	} else if itr.r == nil {
		return errors.IterationNotStarted.New()
	}
	record.Scan(dest, itr.r)
	return nil
}
