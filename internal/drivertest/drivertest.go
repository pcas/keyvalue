// Drivertest provides tests for a keyvalue.Connection.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package drivertest

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"context"
	stderrors "errors"
	"github.com/stretchr/testify/require"
	"math/rand"
	"testing"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialises the rand.Seed
func init() {
	rand.Seed(time.Now().Unix())
}

// randomString returns a random string of length n.
func randomString(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

// wrapConn wraps a test function so that it can be passed to testing.T.Run.
func wrapConn(ctx context.Context, c keyvalue.Connection, f func(context.Context, keyvalue.Connection, *require.Assertions)) func(*testing.T) {
	return func(t *testing.T) {
		f(ctx, c, require.New(t))
	}
}

// wrap wraps a test function so that it can be passed to testing.T.Run.
func wrap(ctx context.Context, db keyvalue.Database, f func(context.Context, keyvalue.Database, string, *require.Assertions)) func(*testing.T) {
	return func(t *testing.T) {
		// Create a random table name for the test
		tableName := "test_" + randomString(5)
		// Run the tests
		f(ctx, db, tableName, require.New(t))
		// Delete the table (if it exists)
		err := db.DeleteTable(ctx, tableName)
		require.NoError(t, err, "error deleting table \"%s\"", tableName)
	}
}

// connectToTestDatabase creates and connects to a test database.
func connectToTestDatabase(ctx context.Context, c keyvalue.Connection, t *testing.T) keyvalue.Database {
	// Create a random database name
	dbName := "test_" + randomString(12)
	// The database shouldn't appear in the list
	S, err := c.ListDatabases(ctx)
	require.NoError(t, err, "unable to list databases")
	require.NotContains(t, S, dbName, "database \"%s\" already exists", dbName)
	// Create the database
	err = c.CreateDatabase(ctx, dbName)
	require.NoError(t, err, "error creating database \"%s\"", dbName)
	// Connect to the database
	db, err := c.ConnectToDatabase(ctx, dbName)
	require.NoError(t, err, "unable to connect to database \"%s\"", dbName)
	return db
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// testDatabase tests basic database operations.
func testDatabase(ctx context.Context, c keyvalue.Connection, require *require.Assertions) {
	// Create a random database name
	dbName := "test_" + randomString(12)
	// The database shouldn't appear in the list
	S, err := c.ListDatabases(ctx)
	require.NoError(err, "unable to list databases")
	require.NotContains(S, dbName, "database \"%s\" already exists", dbName)
	// Deleting a non-existing database should succeed without error
	err = c.DeleteDatabase(ctx, dbName)
	require.NoError(err, "deleting non-existing database \"%s\" should succeed without error", dbName)
	// Opening a non-existing database should fail with an error
	_, err = c.ConnectToDatabase(ctx, dbName)
	require.Error(err, "connecting to non-existing database \"%s\" should fail with an error", dbName)
	require.True(stderrors.Is(err, errors.DatabaseDoesNotExist.New()), "connecting to non-existing database \"%s\" should fail with a DatabaseDoesNotExist error: %v", dbName, err)
	// Create the database
	err = c.CreateDatabase(ctx, dbName)
	require.NoError(err, "error creating database \"%s\"", dbName)
	// The database should now appear in the list
	S, err = c.ListDatabases(ctx)
	require.NoError(err, "unable to list databases")
	require.Contains(S, dbName, "database \"%s\" does not exist in list of databases", dbName)
	// Attempting to create the database again should error
	err = c.CreateDatabase(ctx, dbName)
	require.Error(err, "attempting to create the already existing database \"%s\" should error", dbName)
	require.True(stderrors.Is(err, errors.DatabaseAlreadyExists.New()), "attempting to create the already existing database \"%s\" should fail with a DatabaseAlreadyExists error: %v", dbName, err)
	// Delete the database
	err = c.DeleteDatabase(ctx, dbName)
	require.NoError(err, "error deleting database \"%s\"", dbName)
	// The database shouldn't appear in the list
	S, err = c.ListDatabases(ctx)
	require.NoError(err, "unable to list databases")
	require.NotContains(S, dbName, "deleted database \"%s\" still exists", dbName)
	// Opening a non-existing database should fail with an error
	_, err = c.ConnectToDatabase(ctx, dbName)
	require.Error(err, "connecting to non-existing database \"%s\" should fail with an error", dbName)
	require.True(stderrors.Is(err, errors.DatabaseDoesNotExist.New()), "connecting to non-existing database \"%s\" should fail with a DatabaseDoesNotExist error: %v", dbName, err)
}

// testTable tests basic table operations.
func testTable(ctx context.Context, db keyvalue.Database, tableName string, require *require.Assertions) {
	// The table shouldn't appear in the list
	S, err := db.ListTables(ctx)
	require.NoError(err, "unable to list tables")
	require.NotContains(S, tableName, "table \"%s\" already exists", tableName)
	// Deleting a non-existing table should succeed without error
	err = db.DeleteTable(ctx, tableName)
	require.NoError(err, "deleting non-existing table \"%s\" should succeed without error", tableName)
	// Opening a non-existing table should fail with an error
	_, err = db.ConnectToTable(ctx, tableName)
	require.Error(err, "connecting to non-existing table \"%s\" should fail with an error", tableName)
	require.True(stderrors.Is(err, errors.TableDoesNotExist.New()), "connecting to non-existing table \"%s\" should fail with a TableDoesNotExist error: %v", tableName, err)
	// Create the table
	template := record.Record{
		"a": int64(0),
	}
	err = db.CreateTable(ctx, tableName, template)
	require.NoError(err, "error creating table \"%s\"", tableName)
	// The table should now appear in the list
	S, err = db.ListTables(ctx)
	require.NoError(err, "unable to list tables")
	require.Contains(S, tableName, "table \"%s\" does not exist in list of tables", tableName)
	// Attempting to create the table again should error
	template = record.Record{
		"a": "",
	}
	err = db.CreateTable(ctx, tableName, template)
	require.Error(err, "attempting to create the already existing table \"%s\" should error", tableName)
	require.True(stderrors.Is(err, errors.CreateTableError.New()), "attempting to create the already existing table \"%s\" should fail with a CreateTableError error: %v", tableName, err)
	// Connect to the table
	t, err := db.ConnectToTable(ctx, tableName)
	require.NoError(err, "unable to connect to table \"%s\"", tableName)
	// Close the connection to the table
	err = t.Close()
	require.NoError(err, "error closing connection to table \"%s\"", tableName)
	// Delete the table
	err = db.DeleteTable(ctx, tableName)
	require.NoError(err, "error deleting table \"%s\"", tableName)
	// The table shouldn't appear in the list
	S, err = db.ListTables(ctx)
	require.NoError(err, "unable to list tables")
	require.NotContains(S, tableName, "deleted table \"%s\" still exists", tableName)
	// Opening a non-existing table should fail with an error
	_, err = db.ConnectToTable(ctx, tableName)
	require.Error(err, "connecting to non-existing table \"%s\" should fail with an error", tableName)
	require.True(stderrors.Is(err, errors.TableDoesNotExist.New()), "connecting to non-existing table \"%s\" should fail with a TableDoesNotExist error: %v", tableName, err)
}

// testInsertSelectUpdate bests insert, select, and update.
func testInsertSelectUpdate(ctx context.Context, db keyvalue.Database, tableName string, require *require.Assertions) {
	// Create the table
	template := record.Record{
		"a": int64(0),
		"b": "",
		"c": []byte{},
	}
	err := db.CreateTable(ctx, tableName, template)
	require.NoError(err, "error creating tabgle \"%s\"", tableName)
	// Open the table tableName
	t, err := db.ConnectToTable(ctx, tableName)
	require.NoError(err, "unable to connect to table \"%s\"", tableName)
	defer t.Close()
	// Count the number of elements in the table
	n, err := t.CountWhere(ctx, nil)
	require.NoError(err, "error performing count on table \"%s\"", tableName)
	require.EqualValues(0, n, "unexpected result for count on table \"%s\"", tableName)
	// Insert some elements in the table
	vals := []record.Record{
		{
			"a": int64(5),
			"b": "hello",
		},
		{
			"a": int64(6),
			"b": "world",
		},
	}
	err = t.Insert(ctx, record.SliceIterator(vals))
	require.NoError(err, "errror inserting records in table \"%s\"", tableName)
	// Count the number of elements in the table
	n, err = t.CountWhere(ctx, nil)
	require.NoError(err, "rrror performing count on table \"%s\"", tableName)
	require.EqualValues(2, n, "unexpected result for count on table \"%s\"", tableName)
	// Iterate over the elements in the table
	rs, err := t.SelectWhere(ctx, template, nil, nil)
	require.NoError(err, "error performing select on table \"%s\"", tableName)
	ok, err := rs.NextContext(ctx)
	for err == nil && ok {
		r := record.Record{}
		require.NoError(rs.Scan(r), "error performing scan")
		switch r["a"] {
		case int64(5):
			require.Equal("hello", r["b"], "unexpected value for 'b' when 'a=5'")
		case int64(6):
			require.Equal("world", r["b"], "unexpected value for 'b' when 'a=6'")
		default:
			require.True(false, r["a"], "unexpected value for 'a'")
		}
		ok, err = rs.NextContext(ctx)
		require.NoError(err, "error calling NextContext")
	}
	err = rs.Close()
	require.NoError(err, "error closing iterator")
	require.NoError(rs.Err(), "iteration error")
	// Iterate over exactly one element in the table
	r, err := t.SelectOne(ctx, template, nil, nil)
	require.NoError(err, "error performing SelectOne")
	require.NotNil(r, "expected one record but got zero records")
	require.Equal(int64(5), r["a"], "unexpected value for 'a'")
	require.Equal("hello", r["b"], "unexpected value for 'b'")
	// Update the record with a = 5 to have an extra piece of data
	replacement := record.Record{
		"c": []byte{'t', 'o', 'd', 'a', 'y'},
	}
	selector := condition.FromRecord(record.Record{
		"a": int64(5),
	})
	n, err = t.UpdateWhere(ctx, replacement, selector)
	require.NoError(err, "error performing UpdateWhere")
	require.EqualValues(1, n, "unexpected number of records updated")
	// Recover the record with a = 5 and check c has been set
	r, err = t.SelectOne(ctx, template, record.Record{
		"a": int64(5),
	}, nil)
	require.NoError(err, "error performing SelectOne")
	require.NotNil(r, "expected one record but got zero records")
	require.Equal(replacement["c"], r["c"], "unexpected value for 'c'")
}

// testSort tests CountWhere and SelectWhere with sort orders.
func testSort(ctx context.Context, db keyvalue.Database, tableName string, require *require.Assertions) {
	// Create the table
	template := record.Record{
		"a": int64(0),
	}
	err := db.CreateTable(ctx, tableName, template)
	require.NoError(err, "error creating tabgle \"%s\"", tableName)
	// Open the table tableName
	t, err := db.ConnectToTable(ctx, tableName)
	require.NoError(err, "unable to connect to table \"%s\"", tableName)
	defer t.Close()
	// Populate the data
	data := make([]record.Record, 0, 1000)
	for i := 0; i < 1000; i++ {
		data = append(data, record.Record{
			"a": int64(i),
		})
	}
	err = t.Insert(ctx, record.SliceIterator(data))
	require.NoError(err, "error populating table \"%s\"", tableName)
	// Count that we have the expected number of records
	n, err := t.Count(ctx, nil)
	require.NoError(err, "error counting records in table \"%s\"", tableName)
	require.Equal(int64(len(data)), n, "unexpected number of records in table \"%s\"", tableName)
	// Count with a condition
	cond := condition.Between("a", 51, 70)
	n, err = t.CountWhere(ctx, cond)
	require.NoError(err, "error counting records in table \"%s\" satisfying %s", tableName, cond)
	require.Equal(int64(20), n, "unexpected number of records in table \"%s\" satisfying %s", tableName, cond)
	// Select a range of records in decreasing order
	order := sort.OrderBy{
		{Key: "a", Direction: sort.Descending},
	}
	rs, err := t.SelectWhere(ctx, template, cond, order)
	require.NoError(err, "error performing SelectWhere on table \"%s\" with condition %s", tableName, cond)
	// Iterate over the elements, checking they take the values we expect
	num := 0
	ok, err := rs.NextContext(ctx)
	for err == nil && ok {
		r := record.Record{}
		require.NoError(rs.Scan(r), "error performing scan")
		require.Equal(int64(70-num), r["a"], "unexpected value during iteration")
		num++
		ok, err = rs.NextContext(ctx)
	}
	err = rs.Close()
	require.NoError(err, "error closing iterator")
	require.NoError(rs.Err(), "iteration error")
	require.Equal(20, num, "did not fetch the expected number of records during iteration")
}

// testDelete tests DeleteWhere and then deletes the table tableName.
func testDelete(ctx context.Context, db keyvalue.Database, tableName string, require *require.Assertions) {
	// Create the table
	template := record.Record{
		"a": 0,
	}
	err := db.CreateTable(ctx, tableName, template)
	require.NoError(err, "error creating tabgle \"%s\"", tableName)
	// Open the table tableName
	t, err := db.ConnectToTable(ctx, tableName)
	require.NoError(err, "unable to connect to table \"%s\"", tableName)
	defer t.Close()
	// Populate the data
	data := make([]record.Record, 0, 100)
	for i := 0; i < 100; i++ {
		data = append(data, record.Record{
			"a": i,
		})
	}
	err = t.Insert(ctx, record.SliceIterator(data))
	require.NoError(err, "error populating table \"%s\"", tableName)
	// Delete the element with a = 5
	cond := condition.FromRecord(record.Record{"a": 5})
	n, err := t.DeleteWhere(ctx, cond)
	require.NoError(err, "error deleting record from table \"%s\" with condition %s", tableName, cond)
	require.EqualValues(1, n, "unexpected number of records deleted from table \"%s\" satisfying %s", tableName, cond)
	// Count the number of elements in the table
	n, err = t.Count(ctx, nil)
	require.NoError(err, "error performing count on table \"%s\"", tableName)
	require.EqualValues(len(data)-1, n, "unexpected result for count on table \"%s\"", tableName)
	// Delete the element with a >= 6
	cond = condition.GreaterThanOrEqualTo("a", 6)
	n, err = t.DeleteWhere(ctx, cond)
	require.NoError(err, "error deleting record from table \"%s\" with condition %s", tableName, cond)
	require.EqualValues(94, n, "unexpected number of records deleted from table \"%s\" satisfying %s", tableName, cond)
	// Count the number of elements in the table
	n, err = t.Count(ctx, nil)
	require.NoError(err, "error performing count on table \"%s\"", tableName)
	require.EqualValues(5, n, "unexpected result for count on table \"%s\"", tableName)
}

var idxTestRecords = []record.Record{
	{"a": 1, "b": "fish", "c": "chips"},
	{"a": 2, "b": "fish", "c": "peas"},
	{"a": 3, "b": "salt", "c": "pepper"},
}

// testIndices tests adding, listing, and deleting indices. Creates and deletes the table tableName.
func testIndices(ctx context.Context, db keyvalue.Database, tableName string, require *require.Assertions) {
	// Create the table
	template := record.Record{
		"a": int64(0),
		"b": "",
		"c": "",
	}
	err := db.CreateTable(ctx, tableName, template)
	require.NoError(err, "failed to create table")
	// Connect to the table
	t, err := db.ConnectToTable(ctx, tableName)
	require.NoError(err, "failed to connect to table")
	// Insert the records
	err = t.Insert(ctx, record.SliceIterator(idxTestRecords))
	require.NoError(err, "failed to insert records")
	// There should be no indices present
	L, err := t.ListIndices(ctx)
	require.NoError(err, "failed to list indices")
	require.Empty(L)
	// Add an index
	err = t.AddIndex(ctx, "a")
	require.NoError(err, "failed to add index")
	L, err = t.ListIndices(ctx)
	require.NoError(err, "failed to list indices")
	require.ElementsMatch(L, []string{"a"})
	// Add an already existing index should succeed
	err = t.AddIndex(ctx, "a")
	require.NoError(err, "adding an already existing index should succeed")
	// Adding a index on a key without unique values
	err = t.AddIndex(ctx, "b")
	require.NoError(err, "failed tp add index")
	L, err = t.ListIndices(ctx)
	require.NoError(err, "failed to list indices")
	require.ElementsMatch(L, []string{"a", "b"})
	// Delete an index
	err = t.DeleteIndex(ctx, "a")
	require.NoError(err, "failed to delete index")
	L, err = t.ListIndices(ctx)
	require.NoError(err, "failed to list indices")
	require.ElementsMatch(L, []string{"b"})
	// Deleting a non-existing index should succeed
	err = t.DeleteIndex(ctx, "c")
	require.NoError(err, "deleting a non-existent index should succeed")
	L, err = t.ListIndices(ctx)
	require.NoError(err, "failed to list indices")
	require.ElementsMatch(L, []string{"b"})
	// Delete the remaining index
	err = t.DeleteIndex(ctx, "b")
	require.NoError(err, "failed to delete index")
	L, err = t.ListIndices(ctx)
	require.NoError(err, "failed to list indices")
	require.Empty(L)
	// Delete the table
	err = db.DeleteTable(ctx, tableName)
	require.NoError(err, "failed to delete table")
}

var keyTestRecords = []record.Record{
	{"a": int64(0), "b": "alpha"},
	{"a": int64(1)},
	{"a": int64(2), "b": "gamma"},
}

// testAddDeleteKeys tests adding and deleting keys. Creates and deletes the table tableName.
func testAddDeleteKeys(ctx context.Context, db keyvalue.Database, tableName string, require *require.Assertions) {
	// Create the table
	template := record.Record{
		"a": int64(0),
		"b": "",
	}
	err := db.CreateTable(ctx, tableName, template)
	require.NoError(err, "failed to create table")
	// Connect to the table
	t, err := db.ConnectToTable(ctx, tableName)
	require.NoError(err, "failed to connect to table")
	// Insert the records
	err = t.Insert(ctx, record.SliceIterator(keyTestRecords))
	require.NoError(err, "failed to insert records")
	// Insert completely new keys
	inserter := record.Record{
		"c": int64(7),
		"d": true,
	}
	err = t.AddKeys(ctx, inserter)
	require.NoError(err, "failed to add key")
	template["c"] = int64(0)
	template["d"] = false
	// Check that the new values are correct
	itr, err := t.SelectWhere(ctx, template, nil, nil)
	require.NoError(err, "failed to select records")
	for itr.Next() {
		r := record.Record{}
		require.NoError(itr.Scan(r))
		idx := r["a"].(int64)
		expected := record.Record{}
		for k, v := range keyTestRecords[idx] {
			expected[k] = v
		}
		expected["c"] = int64(7)
		expected["d"] = true
		require.Equal(expected, r)
	}
	itr.Close()
	require.NoError(itr.Err())
	// Delete the new keys
	err = t.DeleteKeys(ctx, []string{"c", "d"})
	require.NoError(err, "failed to delete keys")
	delete(template, "c")
	delete(template, "d")
	desc, err := t.Describe(ctx)
	require.NoError(err, "failed to get table description")
	require.Equal(template, desc)
	// Check the values again
	itr, err = t.SelectWhere(ctx, template, nil, nil)
	require.NoError(err, "failed to select records")
	for itr.Next() {
		r := record.Record{}
		require.NoError(itr.Scan(r))
		idx := r["a"].(int64)
		require.Equal(keyTestRecords[idx], r)
	}
	itr.Close()
	require.NoError(itr.Err(), "error when iterating")
	// Add a partially-existing key
	inserter = record.Record{"b": "beta"}
	err = t.AddKeys(ctx, inserter)
	require.NoError(err, "failed to add key")
	desc, err = t.Describe(ctx)
	require.NoError(err, "failed to get table description")
	require.Equal(desc, template)
	// Check the values
	itr, err = t.SelectWhere(ctx, template, nil, nil)
	require.NoError(err, "failed to select records")
	for itr.Next() {
		r := record.Record{}
		require.NoError(itr.Scan(r))
		idx := r["a"].(int64)
		if idx != 1 {
			require.Equal(keyTestRecords[idx], r)
		} else {
			expected := record.Record{
				"a": int64(1),
				"b": "beta",
			}
			require.Equal(expected, r)
		}
	}
	itr.Close()
	require.NoError(itr.Err(), "error when iterating")
	// Delete the table
	err = db.DeleteTable(ctx, tableName)
	require.NoError(err, "failed to delete table")
}

// testRenameTable tests renaming tables. Modifies and deletes the table tableName.
func testRenameTable(ctx context.Context, db keyvalue.Database, tableName string, require *require.Assertions) {
	// Get the list of table names
	L, err := db.ListTables(ctx)
	require.NoError(err, "failed to list tables")
	require.NotContains(L, tableName)
	// Create the table
	template := record.Record{
		"a": int64(0),
		"b": "",
	}
	err = db.CreateTable(ctx, tableName, template)
	require.NoError(err, "failed to create table")
	// Connect to the table
	t, err := db.ConnectToTable(ctx, tableName)
	require.NoError(err, "failed to connect to table")
	// Populate the table. We reuse keyTestRecords from above
	err = t.Insert(ctx, record.SliceIterator(keyTestRecords))
	require.NoError(err, "failed to insert records")
	// Check the list of tables again
	newL, err := db.ListTables(ctx)
	require.NoError(err, "failed to list tables")
	require.ElementsMatch(newL, append(L, tableName))
	// Rename the table
	newTableName := "test_" + randomString(5)
	require.NoError(err, "failed to get table name")
	err = db.RenameTable(ctx, tableName, newTableName)
	require.NoError(err, "failed to rename table")
	// Check the list of tables again
	newL, err = db.ListTables(ctx)
	require.NoError(err, "failed to list tables")
	require.ElementsMatch(newL, append(L, newTableName))
	// Delete the table
	err = db.DeleteTable(ctx, newTableName)
	require.NoError(err, "failed to delete table")
	// Check the list of tables again
	newL, err = db.ListTables(ctx)
	require.NoError(err, "failed to list tables")
	require.ElementsMatch(newL, L)
}

// testLargeInsert tests inserting a large amount of data.
func testLargeInsert(ctx context.Context, db keyvalue.Database, tableName string, require *require.Assertions) {
	// Create an iterator that will generate 512MB of data
	itr := record.NewCountIterator(
		record.LimitSizeIterator(&RandomIterator{}, 512*1024*1024),
	)
	defer itr.Close()
	// Create the table
	template := record.Record{
		"id":   int64(0),
		"data": []byte{},
	}
	err := db.CreateTable(ctx, tableName, template)
	require.NoError(err, "failed to create table")
	// Connect to the table
	t, err := db.ConnectToTable(ctx, tableName)
	require.NoError(err, "failed to connect to table")
	// Populate the table
	err = t.Insert(ctx, itr)
	require.NoError(err, "failed to insert records")
	// Count that we have the expected number of records
	n, err := t.Count(ctx, nil)
	require.NoError(err, "error counting records in table \"%s\"", tableName)
	require.Equal(itr.Count(), n, "unexpected number of records in table \"%s\"", tableName)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Run runs the standard tests on the given keyvalue.Connection.
func Run(ctx context.Context, c keyvalue.Connection, t *testing.T) {
	// Test that database creation and deletion works as expected
	t.Run("TestDatabase", wrapConn(ctx, c, testDatabase))
	// Connect to the test database
	db := connectToTestDatabase(ctx, c, t)
	// Runt the table tests
	RunTableTests(ctx, db, t)
	// Close the database connection
	err := db.Close()
	require.NoError(t, err, "error closing connection to database \"%s\"", db.Name())
	// Delete the test database
	err = c.DeleteDatabase(ctx, db.Name())
	require.NoError(t, err, "error deleting database \"%s\"", db.Name())
}

// RunTableTests runs the standard table tests on the given keyvalue.Database connection.
func RunTableTests(ctx context.Context, db keyvalue.Database, t *testing.T) {
	// Test basic table operations operations
	t.Run("TestTable", wrap(ctx, db, testTable))
	// Test insert, select, and update operations
	t.Run("TestInsertSelectUpdate", wrap(ctx, db, testInsertSelectUpdate))
	// Test that SelectWhere works with sort orders
	t.Run("TestSort", wrap(ctx, db, testSort))
	// Test DeleteWhere
	t.Run("TestDelete", wrap(ctx, db, testDelete))
	// Test adding, listing, and deleting indices
	t.Run("TestIndices", wrap(ctx, db, testIndices))
	// Test adding and deleting keys
	t.Run("TestAddDeleteKeys", wrap(ctx, db, testAddDeleteKeys))
	// Test renaming tables
	t.Run("TestRenameTable", wrap(ctx, db, testRenameTable))
	// Test large inserts
	t.Run("TestLargeInsert", wrap(ctx, db, testLargeInsert))
}
