// Driver implements the kvdb keyvalue driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdb

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/internal/kvcache"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcastools/cleanup"
	"context"
)

// clientCache is a cache of client connections.
var clientCache *kvcache.Cache[*ClientConfig, *client]

// connection implements the driver.Connection interface.
type connection struct {
	c       *client // The client
	release func()  // The release function
}

// database implements the driver.Database interface.
type database struct {
	c      *client // The client
	dbName string  // The database name
}

// table implements the driver.Table interface.
type table struct {
	c         *client // The client
	dbName    string  // The database name
	tableName string  // The table name
}

// Assert that the table satisfies the optional SelectOneWherer interface
var _ driver.SelectOneWherer = &table{}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init creates the client cache
func init() {
	// Create the cache
	clientCache = kvcache.New(
		newClient,
		func(_ context.Context, _ *client) bool { return true },
		func(c *client) error { return c.Close() },
		AreEqual,
	)
	// Register closing the cache on cleanup
	cleanup.Add(clientCache.Close)
}

// connect returns a connection using the given configuration. On success, the second return value is a release function which must be called when you are finished with the connection, otherwise resources will leak.
func connect(ctx context.Context, cfg *ClientConfig) (*client, func(), error) {
	e, err := clientCache.Get(cfg)
	if err != nil {
		return nil, nil, err
	}
	c, err := e.Value(ctx)
	if err != nil {
		e.Release()
		return nil, nil, err
	}
	return c, e.Release, nil
}

//////////////////////////////////////////////////////////////////////
// table functions
//////////////////////////////////////////////////////////////////////

// Close closes the connection to the table.
func (*table) Close() error {
	return nil
}

// Describe returns a best-guess template for the data in this table.
//
// Note that the accuracy of this template depends on the underlying storage engine. It might not be possible to return an exact description (for example, in a schema-less database), and in this case we return a good guess based on a sample of the data available.
func (t *table) Describe(ctx context.Context) (record.Record, error) {
	return t.c.DescribeTable(ctx, t.dbName, t.tableName)
}

// CountWhere returns the number of records in the table that satisfy the condition "cond" (which may be nil if there are no conditions).
func (t *table) CountWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	return t.c.CountWhere(ctx, t.dbName, t.tableName, cond)
}

// Insert inserts the records from the given iterator into the table.
func (t *table) Insert(ctx context.Context, itr record.Iterator) error {
	return t.c.Insert(ctx, t.dbName, t.tableName, itr)
}

// UpdateWhere updates all records in the table that satisfy the condition "cond" (which may be nil if there are no conditions) by setting the keys in "replacement" to the given values. Returns the number of records updated.
func (t *table) UpdateWhere(ctx context.Context, replacement record.Record, cond condition.Condition) (int64, error) {
	return t.c.UpdateWhere(ctx, t.dbName, t.tableName, replacement, cond)
}

// SelectWhere returns the results that satisfy the condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (t *table) SelectWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Iterator, error) {
	return t.c.SelectWhere(ctx, t.dbName, t.tableName, template, cond, order)
}

// SelectWhereLimit returns at most n results that satisfy the condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required) The returned records will be in the form specified by "template".
func (t *table) SelectWhereLimit(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error) {
	return t.c.SelectWhereLimit(ctx, t.dbName, t.tableName, template, cond, order, n)
}

// SelectOneWhere returns the first record satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned record will be in the form specified by "template". If no records match, then a nil record will be returned.
func (t *table) SelectOneWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Record, error) {
	return t.c.SelectOneWhere(ctx, t.dbName, t.tableName, template, cond, order)
}

// DeleteWhere deletes those records in the table that satisfy the condition "cond" (which may be nil if there are no conditions). Returns the number of records deleted.
func (t *table) DeleteWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	return t.c.DeleteWhere(ctx, t.dbName, t.tableName, cond)
}

// AddIndex adds an index on the given key. If an index already exists, this index is unmodified and nil is returned.  The driver is free to assume that the key is well-formed.
func (t *table) AddIndex(ctx context.Context, key string) error {
	return t.c.AddIndex(ctx, t.dbName, t.tableName, key)
}

// DeleteIndex deletes the index on the given key. If no index is present, nil is returned.  The driver is free to assume that the key is well-formed.
func (t *table) DeleteIndex(ctx context.Context, key string) error {
	return t.c.DeleteIndex(ctx, t.dbName, t.tableName, key)
}

// ListIndices lists the keys for which indices are present.  The driver is free to assume that the key is well-formed.
func (t *table) ListIndices(ctx context.Context) ([]string, error) {
	return t.c.ListIndices(ctx, t.dbName, t.tableName)
}

// AddKeys updates each record r in the table, adding any keys in rec that are not already present along with the corresponding values. Any keys that are already present in r will be left unmodified.
func (t *table) AddKeys(ctx context.Context, rec record.Record) error {
	return t.c.AddKeys(ctx, t.dbName, t.tableName, rec)
}

// DeleteKeys updates all records in the table, deleting all the specified keys if present.
func (t *table) DeleteKeys(ctx context.Context, keys []string) error {
	return t.c.DeleteKeys(ctx, t.dbName, t.tableName, keys)
}

/////////////////////////////////////////////////////////////////////////
// database functions
/////////////////////////////////////////////////////////////////////////

// ListTables returns the names of the tables in the database.
func (d *database) ListTables(ctx context.Context) ([]string, error) {
	return d.c.ListTables(ctx, d.dbName)
}

// CreateTable creates a table with the given name in the database.
//
// The provided template will be used by the underlying storage-engine to create the new table if appropriate; the storage-engine is free to ignore the template if it is not required (for example, in a schema-less database).
func (d *database) CreateTable(ctx context.Context, name string, template record.Record) error {
	return d.c.CreateTable(ctx, d.dbName, name, template)
}

// DeleteTable deletes the indicated table from the database. Does not return an error if the table does not exist.
func (d *database) DeleteTable(ctx context.Context, name string) error {
	return d.c.DeleteTable(ctx, d.dbName, name)
}

// RenameTable changes the name of table src in the database to dst.
func (d *database) RenameTable(ctx context.Context, src string, dst string) error {
	return d.c.RenameTable(ctx, d.dbName, src, dst)
}

// ConnectToTable connects to the indicated table in the database.
func (d *database) ConnectToTable(_ context.Context, name string) (driver.Table, error) {
	return &table{
		c:         d.c,
		dbName:    d.dbName,
		tableName: name,
	}, nil
}

// Close closes the client connection to the database.
func (*database) Close() error {
	return nil
}

/////////////////////////////////////////////////////////////////////////
// connection functions
/////////////////////////////////////////////////////////////////////////

// DriverName returns the name associated with this driver.
func (*connection) DriverName() string {
	return "kvdb"
}

var _ driver.Connection

// CreateDatabase creates a database with the given name on this
// connection.
func (c *connection) CreateDatabase(ctx context.Context, name string) error {
	return c.c.CreateDatabase(ctx, name)
}

// DeleteDatabase deletes the indicated database on this connection.
// Does not return an error if the database does not exist.
func (c *connection) DeleteDatabase(ctx context.Context, name string) error {
	return c.c.DeleteDatabase(ctx, name)
}

// ListDatabases returns the names of the available databases on this connection.
func (c *connection) ListDatabases(ctx context.Context) ([]string, error) {
	return c.c.ListDatabases(ctx)
}

// ConnectToDatabase opens a connection to a mongodb database.
func (c *connection) ConnectToDatabase(ctx context.Context, name string) (driver.Database, error) {
	return &database{
		c:      c.c,
		dbName: name,
	}, nil
}

// Close closes the client connection to the database.
func (c *connection) Close() error {
	c.release()
	return nil
}

// Open opens a connection to kvdb.
func Open(ctx context.Context, cfg *ClientConfig) (keyvalue.Connection, error) {
	// If necessary fetch the default config
	if cfg == nil {
		cfg = DefaultConfig()
	}
	// Validate the config
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Fetch (or open) a client connection
	c, release, err := connect(ctx, cfg)
	if err != nil {
		return nil, err
	}
	// Return the connection
	return keyvalue.NewConnection(&connection{
		c:       c,
		release: release,
	}), nil
}
