// Server implements the kvdb server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdb

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	kverrors "bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/kvdb/internal/kvdbrpc"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/grpcmetrics"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/metadata"
	"google.golang.org/protobuf/types/known/emptypb"
	"io"

	// Make our compressors available
	_ "bitbucket.org/pcastools/grpcutil/grpcs2"
	_ "bitbucket.org/pcastools/grpcutil/grpcsnappy"
	_ "google.golang.org/grpc/encoding/gzip"
)

// The default ports that kvdb listens on.
const (
	DefaultTCPPort = 12356
	DefaultWSPort  = 80
)

// maxInt defines the maximum size of an int.
const maxInt = int64(int(^uint(0) >> 1))

// The metadata keys
const (
	mdDBName    = "db_name"    // The database name (required)
	mdTableName = "table_name" // The table name (required in some contexts)
)

// serverIterator wraps a stream of kvdbrpc.Records as a record.Iterator.
type serverIterator struct {
	stream  kvdbrpc.Kvdb_InsertServer // The underlying stream
	hasNext bool                      // Did the previous call to Next succeed?
	r       record.Record             // The next record
	err     error                     // Any error during iteration
	lg      log.Interface             // The logger
}

// clientMetadata is the metadata associated with a client request.
type clientMetadata struct {
	AppName   string
	DBName    string
	TableName string
}

// kvdbServer implements the tasks on the gRPC server.
type kvdbServer struct {
	kvdbrpc.UnimplementedKvdbServer
	log.BasicLogable
	metrics.BasicMetricsable
	c       keyvalue.Connection // The connection
	insertC chan struct{}       // The insert capacity (may be nil)
}

// Server handles client communication.
type Server struct {
	log.SetLoggerer
	metrics.SetMetricser
	grpcutil.Server
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// readClientMetadata extracts the client metadata from the incoming context.
func readClientMetadata(ctx context.Context) (clientMetadata, error) {
	// Recover the client's metadata from the context
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return clientMetadata{}, fmt.Errorf("failed to recover metadata")
	}
	// Collect the metadata
	d := clientMetadata{}
	// Extract the database name
	if l := md.Get(mdDBName); len(l) != 0 && len(l[0]) != 0 {
		dbName := l[0]
		if err := validateDB(dbName); err != nil {
			return clientMetadata{}, err
		}
		d.DBName = dbName
	}
	// Extract the table name
	if l := md.Get(mdTableName); len(l) != 0 && len(l[0]) != 0 {
		tableName := l[0]
		if err := validateTableName(tableName); err != nil {
			return clientMetadata{}, err
		}
		d.TableName = tableName
	}
	// Return the metadata
	return d, nil
}

// createInsertChannel creates the insert channel of capacity n that keeps track of insert capacity. If n is <= 0 then nil is returned, and the insert capacity should be understood to be unlimited. Otherwise, every read from the channel MUST be balanced with a write to the channel.
func createInsertChannel(n int) chan struct{} {
	// Is there anything to do?
	if n <= 0 {
		return nil
	}
	// Create the communication channel
	insertC := make(chan struct{}, n)
	// Fill the channel
	for i := 0; i < n; i++ {
		insertC <- struct{}{}
	}
	return insertC
}

/////////////////////////////////////////////////////////////////////////
// serverIterator functions
/////////////////////////////////////////////////////////////////////////

// Close closes the iterator.
func (s *serverIterator) Close() error {
	if s.err == nil {
		s.err = io.EOF
	}
	s.lg.Printf("closing")
	return nil
}

// Err returns the last error encountered during iteration, if any.
func (s *serverIterator) Err() error {
	if s.err == io.EOF {
		return nil
	}
	return s.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *serverIterator) Next() bool {
	ok, err := s.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *serverIterator) NextContext(_ context.Context) (bool, error) {
	if s.err != nil {
		return false, s.Err()
	}
	s.lg.Printf("advancing iterator")
	rec, err := s.stream.Recv()
	if err != nil {
		s.err = err
		return false, s.Err()
	}
	if s.r == nil {
		s.r = make(record.Record)
	}
	if err = kvdbrpc.ScanRecord(s.r, rec); err != nil {
		s.err = err
		return false, s.Err()
	}
	s.hasNext = true
	s.lg.Printf("successfully advanced iterator")
	return true, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *serverIterator) Scan(dest record.Record) error {
	if s.err != nil {
		if s.err == io.EOF {
			return kverrors.IterationFinished.New()
		}
		return s.Err()
	} else if !s.hasNext {
		return kverrors.IterationNotStarted.New()
	}
	record.Scan(dest, s.r)
	return nil
}

/////////////////////////////////////////////////////////////////////////
// kvdbServer functions
/////////////////////////////////////////////////////////////////////////

// wrapWithDatabase wraps the user provided function f in a new function that builds up a connection to the database, and then closes the connection afterwards.
func (m *kvdbServer) wrapWithDatabase(f func(context.Context, keyvalue.Database) error) func(context.Context) error {
	return func(ctx context.Context) error {
		// Recover the metadata
		md, err := readClientMetadata(ctx)
		if err != nil {
			return err
		} else if len(md.DBName) == 0 {
			return errors.New("no database specified")
		}
		// Connect to the database
		db, err := m.c.ConnectToDatabase(ctx, md.DBName)
		if err != nil {
			return err
		}
		defer db.Close()
		// Run the user's function
		return f(ctx, db)
	}
}

// wrapWithTable wraps the user provided function f in a new function that builds up a connection to the table, and then closes the connection afterwards.
func (m *kvdbServer) wrapWithTable(f func(context.Context, keyvalue.Table) error) func(context.Context) error {
	return func(ctx context.Context) error {
		// Recover the metadata
		md, err := readClientMetadata(ctx)
		if err != nil {
			return err
		} else if len(md.DBName) == 0 {
			return errors.New("no database specified")
		} else if len(md.TableName) == 0 {
			return errors.New("no table specified")
		}
		// Connect to the database
		db, err := m.c.ConnectToDatabase(ctx, md.DBName)
		if err != nil {
			return err
		}
		defer db.Close()
		// Connect to the table
		t, err := db.ConnectToTable(ctx, md.TableName)
		if err != nil {
			return err
		}
		defer t.Close()
		// Run the user's function
		return f(ctx, t)
	}
}

// CreateDatabase creates a database.
func (m *kvdbServer) CreateDatabase(ctx context.Context, _ *emptypb.Empty) (*emptypb.Empty, error) {
	// Recover the metadata
	md, err := readClientMetadata(ctx)
	if err != nil {
		return &emptypb.Empty{}, err
	} else if len(md.DBName) == 0 {
		return &emptypb.Empty{}, errors.New("no database specified")
	}
	// Create the database
	err = m.c.CreateDatabase(ctx, md.DBName)
	return &emptypb.Empty{}, err
}

// DeleteDatabase deletes a database.
func (m *kvdbServer) DeleteDatabase(ctx context.Context, _ *emptypb.Empty) (*emptypb.Empty, error) {
	// Recover the metadata
	md, err := readClientMetadata(ctx)
	if err != nil {
		return &emptypb.Empty{}, err
	} else if len(md.DBName) == 0 {
		return &emptypb.Empty{}, errors.New("no database specified")
	}
	// Create the database
	err = m.c.DeleteDatabase(ctx, md.DBName)
	return &emptypb.Empty{}, err
}

// ListDatabases returns the available databases.
func (m *kvdbServer) ListDatabases(ctx context.Context, _ *emptypb.Empty) (*kvdbrpc.DatabaseNames, error) {
	// Recover the listing
	S, err := m.c.ListDatabases(ctx)
	if err != nil {
		return &kvdbrpc.DatabaseNames{}, err
	}
	// Return the listing
	return &kvdbrpc.DatabaseNames{
		Name: S,
	}, nil
}

// ListTables returns the tables in the database.
func (m *kvdbServer) ListTables(ctx context.Context, _ *emptypb.Empty) (*kvdbrpc.TableNames, error) {
	// Recover the listing
	var S []string
	err := m.wrapWithDatabase(func(ctx context.Context, db keyvalue.Database) (err error) {
		S, err = db.ListTables(ctx)
		return
	})(ctx)
	if err != nil {
		return &kvdbrpc.TableNames{}, err
	}
	// Return the listing
	return &kvdbrpc.TableNames{
		Name: S,
	}, nil
}

// CountWhere returns the number of records in the table that satisfy the condition "cond".
func (m *kvdbServer) CountWhere(ctx context.Context, cond *kvdbrpc.Condition) (*kvdbrpc.NumberOfRecords, error) {
	n, err := func() (n int64, err error) {
		// Extract the condition
		var c condition.Condition
		c, err = kvdbrpc.ToCondition(cond)
		if err != nil {
			return
		}
		// Perform the count
		err = m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) (err error) {
			n, err = t.CountWhere(ctx, c)
			return
		})(ctx)
		return
	}()
	return kvdbrpc.FromInt64(n), err
}

// Insert inserts the records from the given stream into the table.
func (m *kvdbServer) Insert(stream kvdbrpc.Kvdb_InsertServer) error {
	err := m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) error {
		// Wrap up the stream as an iterator
		itr := &serverIterator{stream: stream, lg: log.PrefixWith(m.Log(), "[serverIterator]")}
		defer itr.Close()
		// Wait for insert capacity
		if m.insertC != nil {
			select {
			case <-m.insertC:
			case <-ctx.Done():
				return ctx.Err()
			}
			defer func() { m.insertC <- struct{}{} }()
		}
		// Perform the insert
		return t.Insert(ctx, itr)
	})(stream.Context())
	closeErr := stream.SendAndClose(&emptypb.Empty{})
	if err != nil {
		return err
	}
	return closeErr
}

// UpdateWhere updates all records in the table that satisfy the given condition by setting all keys present in the replacement to the given values. Returns the number of records updated.
func (m *kvdbServer) UpdateWhere(ctx context.Context, cr *kvdbrpc.ConditionAndReplacement) (*kvdbrpc.NumberOfRecords, error) {
	n, err := func() (n int64, err error) {
		// Extract the condition and replacement
		var cond condition.Condition
		var replacement record.Record
		cond, replacement, err = kvdbrpc.ToConditionAndReplacement(cr)
		if err != nil {
			return
		}
		// Perform the update
		err = m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) (err error) {
			n, err = t.UpdateWhere(ctx, replacement, cond)
			return
		})(ctx)
		return
	}()
	return kvdbrpc.FromInt64(n), err
}

// SelectOneWhere returns zero or one records, encoded in a HasRecord, matching the given condition. The returned record will be in the form specified by the given template.
func (m *kvdbServer) SelectOneWhere(ctx context.Context, cto *kvdbrpc.ConditionTemplateAndOrder) (*kvdbrpc.HasRecord, error) {
	var r record.Record
	err := m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) (err error) {
		// Extract the condition, template, and limit
		var cond condition.Condition
		var template record.Record
		var order sort.OrderBy
		cond, template, order, err = kvdbrpc.ToConditionTemplateAndOrder(cto)
		if err != nil {
			return
		}
		// Perform the select one
		r, err = t.SelectOneWhere(ctx, template, cond, order)
		return
	})(ctx)
	if err != nil {
		return nil, err
	}
	// Return the record
	return kvdbrpc.FromHasRecord(r)
}

// SelectWhereLimit returns the results that satisfy the given condition, in the given order. The returned records will be in the form specified by the given template. At most the given number of records will be returned.
func (m *kvdbServer) SelectWhereLimit(ctol *kvdbrpc.ConditionTemplateOrderAndLimit, stream kvdbrpc.Kvdb_SelectWhereLimitServer) error {
	return m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) (err error) {
		// Extract the condition, template, and limit
		var cond condition.Condition
		var template record.Record
		var order sort.OrderBy
		var limit int64
		cond, template, order, limit, err = kvdbrpc.ToConditionTemplateOrderAndLimit(ctol)
		if err != nil {
			return
		}
		// Perform the select
		var itr record.Iterator
		if limit < 0 {
			itr, err = t.SelectWhere(ctx, template, cond, order)
		} else if limit > maxInt {
			err = kverrors.LimitOutOfRange.New()
		} else {
			itr, err = t.SelectWhereLimit(ctx, template, cond, order, limit)
		}
		if err != nil {
			return
		}
		// Defer closing the iterator
		defer func() {
			if closeErr := itr.Close(); err == nil {
				err = closeErr
			}
			if err == nil {
				err = itr.Err()
			}
		}()
		// Write the records to the stream
		var ok bool
		r := make(record.Record, len(template))
		ok, err = itr.NextContext(ctx)
		for err == nil && ok {
			var rec *kvdbrpc.Record
			if err = itr.Scan(r); err != nil {
				return
			} else if rec, err = kvdbrpc.FromRecord(r); err != nil {
				return
			} else if err = stream.Send(rec); err != nil {
				return
			}
			ok, err = itr.NextContext(ctx)
		}
		return
	})(stream.Context())
}

// DeleteWhere deletes those records in the table that satisfy the given condition. Returns the number of records deleted.
func (m *kvdbServer) DeleteWhere(ctx context.Context, c *kvdbrpc.Condition) (*kvdbrpc.NumberOfRecords, error) {
	n, err := func() (n int64, err error) {
		// Extract the condition
		var cond condition.Condition
		cond, err = kvdbrpc.ToCondition(c)
		if err != nil {
			return
		}
		// Perform the delete
		err = m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) (err error) {
			n, err = t.DeleteWhere(ctx, cond)
			return
		})(ctx)
		return
	}()
	return kvdbrpc.FromInt64(n), err
}

// AddIndex adds an index on the given key. If an index already exists, this index is unmodified and nil is returned.  The driver is free to assume that the key is well-formed.
func (m *kvdbServer) AddIndex(ctx context.Context, idx *kvdbrpc.Index) (*emptypb.Empty, error) {
	err := m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) error {
		return t.AddIndex(ctx, kvdbrpc.ToIndex(idx))
	})(ctx)
	return &emptypb.Empty{}, err
}

// DeleteIndex deletes the index on the given key. If no index is present, nil is returned.  The driver is free to assume that the key is well-formed.
func (m *kvdbServer) DeleteIndex(ctx context.Context, idx *kvdbrpc.Index) (*emptypb.Empty, error) {
	err := m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) error {
		return t.DeleteIndex(ctx, kvdbrpc.ToIndex(idx))
	})(ctx)
	return &emptypb.Empty{}, err
}

// ListIndices lists the keys for which indices are present.  The driver is free to assume that the key is well-formed.
func (m *kvdbServer) ListIndices(ctx context.Context, _ *emptypb.Empty) (*kvdbrpc.IndexList, error) {
	S, err := func() (S []string, err error) {
		// Grab the indices
		err = m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) (err error) {
			S, err = t.ListIndices(ctx)
			return
		})(ctx)
		return
	}()
	return kvdbrpc.FromIndices(S), err
}

// AddKeys updates each record r in the table, adding any keys in rec that are not already present along with the corresponding values. Any keys that are already present in r will be left unmodified.
func (m *kvdbServer) AddKeys(ctx context.Context, rec *kvdbrpc.Record) (*emptypb.Empty, error) {
	// Convert the record
	r, err := kvdbrpc.ToRecord(rec)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Add the keys
	err = m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) error {
		return t.AddKeys(ctx, r)
	})(ctx)
	return &emptypb.Empty{}, err
}

// DeleteKeys updates all records in the table, deleting all the specified keys if present.
func (m *kvdbServer) DeleteKeys(ctx context.Context, kl *kvdbrpc.KeyList) (*emptypb.Empty, error) {
	err := m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) error {
		return t.DeleteKeys(ctx, kvdbrpc.ToKeys(kl))
	})(ctx)
	return &emptypb.Empty{}, err
}

// CreateTable creates the table with the given name and template.
func (m *kvdbServer) CreateTable(ctx context.Context, nt *kvdbrpc.NameAndTemplate) (*emptypb.Empty, error) {
	err := func() (err error) {
		// Extract the name and template
		var name string
		var template record.Record
		name, template, err = kvdbrpc.ToNameAndTemplate(nt)
		if err != nil {
			return
		}
		// Create the table
		err = m.wrapWithDatabase(func(ctx context.Context, db keyvalue.Database) error {
			return db.CreateTable(ctx, name, template)
		})(ctx)
		return
	}()
	return &emptypb.Empty{}, err
}

// DescribeTable returns a best-guess template for the data in the table.
func (m *kvdbServer) DescribeTable(ctx context.Context, _ *emptypb.Empty) (*kvdbrpc.Record, error) {
	template, err := func() (template record.Record, err error) {
		// Grab the template
		err = m.wrapWithTable(func(ctx context.Context, t keyvalue.Table) (err error) {
			template, err = t.Describe(ctx)
			return
		})(ctx)
		return
	}()
	if err != nil {
		return nil, err
	}
	return kvdbrpc.FromRecord(template)
}

// RenameTable changes the name of a table in the database.
func (m *kvdbServer) RenameTable(ctx context.Context, sd *kvdbrpc.SrcAndDstNames) (*emptypb.Empty, error) {
	err := func() (err error) {
		// Rename the table
		err = m.wrapWithDatabase(func(ctx context.Context, db keyvalue.Database) error {
			src, dst := kvdbrpc.ToSrcAndDst(sd)
			return db.RenameTable(ctx, src, dst)
		})(ctx)
		return
	}()
	return &emptypb.Empty{}, err
}

// DeleteTable deletes the table with the given name.
func (m *kvdbServer) DeleteTable(ctx context.Context, name *kvdbrpc.Name) (*emptypb.Empty, error) {
	err := func() (err error) {
		// Delete the table
		err = m.wrapWithDatabase(func(ctx context.Context, db keyvalue.Database) error {
			return db.DeleteTable(ctx, kvdbrpc.ToName(name))
		})(ctx)
		return
	}()
	return &emptypb.Empty{}, err
}

/////////////////////////////////////////////////////////////////////////
// Server functions
/////////////////////////////////////////////////////////////////////////

// New returns a new kvdb server.
func New(options ...Option) (*Server, error) {
	// Parse the options
	opts, err := parseOptions(options...)
	if err != nil {
		return nil, err
	}
	// Create the new server instance
	m := &kvdbServer{
		c:       opts.Connection,
		insertC: createInsertChannel(opts.InsertCapacity),
	}
	// Create the gRPC options
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			unaryServerErrorInterceptor,
			grpclog.UnaryServerInterceptor(m.Log()),
			grpcmetrics.UnaryServerInterceptor(m.Metrics()),
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			streamServerErrorInterceptor,
			grpclog.StreamServerInterceptor(m.Log()),
			grpcmetrics.StreamServerInterceptor(m.Metrics()),
		)),
		grpc.MaxRecvMsgSize(opts.MaxMessageSize),
		grpc.ReadBufferSize(opts.ReadBufferSize),
		grpc.WriteBufferSize(opts.WriteBufferSize),
	}
	// Add the SSL credentials to the gRPC options
	if len(opts.SSLKey) != 0 {
		creds, err := grpcutil.NewServerTLS(opts.SSLCert, opts.SSLKey)
		if err != nil {
			return nil, err
		}
		serverOptions = append(serverOptions, grpc.Creds(creds))
	}
	// Create the gRPC server and register our implementation
	s := grpc.NewServer(serverOptions...)
	kvdbrpc.RegisterKvdbServer(s, m)
	// Bind the health data
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())
	// Wrap and return the server
	return &Server{
		SetLoggerer:  m,
		SetMetricser: m,
		Server:       s,
	}, nil
}
