// Serveroptions provides configuration options for a kvdb server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdb

import (
	"bitbucket.org/pcas/keyvalue"
	"errors"
)

// Option sets options on a kvdb server.
type Option interface {
	apply(*serverOptions) error
}

// funcOption wraps a function that modifies Options into an implementation of the Option interface.
type funcOption struct {
	f func(*serverOptions) error
}

// apply calls the wrapped function f on the given Options.
func (h *funcOption) apply(do *serverOptions) error {
	return h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*serverOptions) error) *funcOption {
	return &funcOption{
		f: f,
	}
}

// serverOptions are the options on a kvdb server.
type serverOptions struct {
	Connection      keyvalue.Connection // The underlying connection
	MaxMessageSize  int                 // The maximum message size (in bytes)
	ReadBufferSize  int                 // The read buffer size (in bytes)
	WriteBufferSize int                 // The write buffer size (in bytes)
	InsertCapacity  int                 // The number of simultaneous inserts
	SSLCert         []byte              // The SSL (public) certificate
	SSLKey          []byte              // The SSL (private) key
}

// The default kvdb server values.
const (
	DefaultMaxMessageSize  = 16 * 1024 * 1024 // 16Mb
	DefaultReadBufferSize  = 512 * 1024       // 512Kb
	DefaultWriteBufferSize = 512 * 1024       // 512Kb
	DefaultInsertCapacity  = 128
)

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseOptions parses the given optional functions.
func parseOptions(options ...Option) (*serverOptions, error) {
	// Create the default options
	opts := &serverOptions{
		MaxMessageSize:  DefaultMaxMessageSize,
		ReadBufferSize:  DefaultReadBufferSize,
		WriteBufferSize: DefaultWriteBufferSize,
		InsertCapacity:  DefaultInsertCapacity,
	}
	// Set the options
	for _, h := range options {
		if err := h.apply(opts); err != nil {
			return nil, err
		}
	}
	// A connection must be specified
	if opts.Connection == nil {
		return nil, errors.New("missing connection")
	}
	// Looks good
	return opts, nil
}

// Connection sets the connection for the server.
func Connection(c keyvalue.Connection) Option {
	return newFuncOption(func(opts *serverOptions) error {
		if c == nil {
			return errors.New("illegal nil-valued connection")
		}
		opts.Connection = c
		return nil
	})
}

// MaxMessageSize sets the maximum message size (in bytes) the server can receive.
func MaxMessageSize(size int) Option {
	return newFuncOption(func(opts *serverOptions) error {
		if size < 0 {
			return errors.New("max message size must be a non-negative integer")
		}
		opts.MaxMessageSize = size
		return nil
	})
}

// ReadBufferSize sets the size of the read buffer (in bytes).
func ReadBufferSize(size int) Option {
	return newFuncOption(func(opts *serverOptions) error {
		if size < 0 {
			return errors.New("read buffer size must be a non-negative integer")
		}
		opts.ReadBufferSize = size
		return nil
	})
}

// WriteBufferSize sets the size of the write buffer (in bytes).
func WriteBufferSize(size int) Option {
	return newFuncOption(func(opts *serverOptions) error {
		if size < 0 {
			return errors.New("write buffer size must be a non-negative integer")
		}
		opts.WriteBufferSize = size
		return nil
	})
}

// InsertCapacity sets the maximum number of simultaneous inserts. Set to 0 to allow an unlimited number of inserts.
func InsertCapacity(n int) Option {
	return newFuncOption(func(opts *serverOptions) error {
		if n < 0 {
			return errors.New("insert capacity must be a non-negative integer")
		}
		opts.InsertCapacity = n
		return nil
	})
}

// SSLCertAndKey adds the given SSL public certificate and private key to the server.
func SSLCertAndKey(crt []byte, key []byte) Option {
	// Copy the certificate and key
	crtCopy := make([]byte, len(crt))
	copy(crtCopy, crt)
	keyCopy := make([]byte, len(key))
	copy(keyCopy, key)
	// Return the option
	return newFuncOption(func(opts *serverOptions) error {
		if len(crtCopy) == 0 {
			return errors.New("missing SSL certificate")
		} else if len(keyCopy) == 0 {
			return errors.New("missing SSL private key")
		}
		opts.SSLCert = crtCopy
		opts.SSLKey = keyCopy
		return nil
	})
}
