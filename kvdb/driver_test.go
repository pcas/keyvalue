// driver_test provides tests for the kvdb driver.

//go:build integration
// +build integration

/*
This test requires mongodb to be running and listening on localhost.

To run, do:
go test -tags=integration
*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdb

import (
	"bitbucket.org/pcas/keyvalue/internal/drivertest"
	"bitbucket.org/pcas/keyvalue/mongodb"
	"bitbucket.org/pcastools/address"
	"context"
	"github.com/stretchr/testify/require"
	"net"
	"testing"
	"time"
)

// contains returns true iff s is contained in S.
func contains(S []string, s string) bool {
	for _, x := range S {
		if x == s {
			return true
		}
	}
	return false
}

// startServer starts the kvdbd server.
func startServer(ctx context.Context, tt *testing.T) <-chan struct{} {
	require := require.New(tt)
	// Create the config for mongo
	cfg := mongodb.DefaultConfig()
	cfg.Hosts = []string{"localhost:27017"}
	cfg.ReadConcern = mongodb.RMajority
	// Connect to mongo
	c, err := mongodb.Open(ctx, cfg)
	require.NoError(err)
	// Establish a TCP connection
	l, err := net.Listen("tcp", "localhost:45555")
	if err != nil {
		c.Close()
		require.NoError(err)
	}
	// Create the server
	s, err := New(Connection(c))
	if err != nil {
		c.Close()
		l.Close()
		require.NoError(err)
	}
	// Run the server in a new go routine
	doneC := make(chan struct{})
	go func(ctx context.Context, doneC chan<- struct{}) {
		defer close(doneC)
		defer c.Close()
		defer l.Close()
		// Stop the server when the context fires
		go func() {
			<-ctx.Done()
			s.GracefulStop()
		}()
		// Start serving
		require.NoError(s.Serve(l))
	}(ctx, doneC)
	return doneC
}

func TestDriver(t *testing.T) {
	// Create the config
	cfg := DefaultConfig()
	addr, err := address.NewTCP("localhost", 45555)
	require.NoError(t, err, "error creating address")
	cfg.Address = addr
	cfg.SSLDisabled = true
	// Create a context to run the tests
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Start the server
	doneC := startServer(ctx, t)
	// Connect to kvdbd
	c, err := Open(ctx, cfg)
	require.NoError(t, err, "error opening connection")
	// Run the tests
	drivertest.Run(ctx, c, t)
	// Close the connection
	err = c.Close()
	require.NoError(t, err, "error closing connection")
	// Wait for the server to exit
	cancel()
	<-doneC
}
