// Condition.go handles conversion of Conditions.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdbrpc

import (
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/errors"
	"fmt"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// FromLeafOp converts a *condition.LeafOp to a *LeafOperator
func FromLeafOp(c *condition.LeafOp) (*LeafOperator, error) {
	if c == nil {
		return nil, fmt.Errorf("nil *LeafOp in FromLeafOp")
	}
	// try to convert the RHS
	v := c.Rhs()
	if v == nil {
		return nil, fmt.Errorf("nil RHS in FromLeafOp")
	}
	rhs, err := convertValue(c.Rhs())
	if err != nil {
		return nil, err
	}
	// convert the Opcode
	var op LeafOperator_LeafType
	switch c.OpCode() {
	case condition.Eq:
		op = LeafOperator_EQ
	case condition.Ne:
		op = LeafOperator_NE
	case condition.Lt:
		op = LeafOperator_LT
	case condition.Gt:
		op = LeafOperator_GT
	case condition.Le:
		op = LeafOperator_LE
	case condition.Ge:
		op = LeafOperator_GE
	}
	return &LeafOperator{
		Lhs:    c.Lhs(),
		Opcode: op,
		Rhs:    rhs,
	}, nil
}

// ToLeafOp converts a *LeafOperator to a *condition.LeafOp
func ToLeafOp(op *LeafOperator) (*condition.LeafOp, error) {
	if op == nil {
		return nil, fmt.Errorf("nil *LeafOperator in ToLeafOp")
	}
	lhs := op.GetLhs()
	v := op.GetRhs()
	if v == nil {
		return nil, fmt.Errorf("nil RHS in ToLeafOp")
	}
	rhs, ok := getValue(v)
	if !ok {
		return nil, fmt.Errorf("unable to convert *Value in ToLeafOp")
	}
	var c condition.Condition
	switch op.Opcode {
	case LeafOperator_EQ:
		c = condition.Equal(lhs, rhs)
	case LeafOperator_NE:
		c = condition.NotEqual(lhs, rhs)
	case LeafOperator_GT:
		c = condition.GreaterThan(lhs, rhs)
	case LeafOperator_LT:
		c = condition.LessThan(lhs, rhs)
	case LeafOperator_GE:
		c = condition.GreaterThanOrEqualTo(lhs, rhs)
	case LeafOperator_LE:
		c = condition.LessThanOrEqualTo(lhs, rhs)
	}
	return c.(*condition.LeafOp), nil
}

// FromLHSBool converts a string and a slice of interface{} to a *LhsBool
func FromLHSBool(lhs string, b bool) *LhsBool {
	return &LhsBool{
		Lhs:       lhs,
		BoolValue: b,
	}
}

// ToLHSBool converts a *LhsBool to a string and a bool.
func ToLHSBool(m *LhsBool) (string, bool, error) {
	if m == nil {
		return "", false, fmt.Errorf("nil *LhsBool in ToLhsBool")
	}
	return m.GetLhs(), m.GetBoolValue(), nil
}

// FromLHSValues converts a string and a slice of interface{} to a *LhsManyValues
func FromLHSValues(lhs string, vs []interface{}) (*LhsManyValues, error) {
	// convert vs to a slice of *Values
	S := make([]*Value, 0, len(vs))
	for _, v := range vs {
		x, err := convertValue(v)
		if err != nil {
			return nil, err
		}
		S = append(S, x)
	}
	return &LhsManyValues{
		Lhs:   lhs,
		Value: S,
	}, nil
}

// ToLHSValues converts a *LhsManyValues to a string and a slice of interface{}s.
func ToLHSValues(m *LhsManyValues) (string, []interface{}, error) {
	if m == nil {
		return "", nil, fmt.Errorf("nil *LhsManyValues in ToLhsValues")
	}
	vs := m.GetValue()
	S := make([]interface{}, 0, len(vs))
	for _, v := range vs {
		if v == nil {
			return "", nil, fmt.Errorf("nil *Value in ToLhsValues")
		}
		x, ok := getValue(v)
		if !ok {
			return "", nil, fmt.Errorf("unable to convert *Value in ToValues")
		}
		S = append(S, x)
	}
	return m.GetLhs(), S, nil
}

// FromLHSLowerUpper converts a string and a pair of interface{}s to a *LhsLowerUpper
func FromLHSLowerUpper(lhs string, lower interface{}, upper interface{}) (*LhsLowerUpper, error) {
	if lower == nil || upper == nil {
		return nil, fmt.Errorf("nil bound in FromLhsLowerUpper")
	}
	l, err := convertValue(lower)
	if err != nil {
		return nil, err
	}
	u, err := convertValue(upper)
	if err != nil {
		return nil, err
	}
	return &LhsLowerUpper{
		Lhs:   lhs,
		Lower: l,
		Upper: u,
	}, nil
}

// ToLHSLowerUpper converts a *LhsLowerUpper to a string and a pair of interface{}s.
func ToLHSLowerUpper(ul *LhsLowerUpper) (lhs string, lower interface{}, upper interface{}, err error) {
	if ul == nil {
		err = fmt.Errorf("nil *LhsLowerUpper in ToLhsLowerUpper")
		return
	}
	var ok bool
	// convert the lower bound
	x := ul.GetLower()
	if x == nil {
		err = fmt.Errorf("nil lower bound in ToLhsLowerUpper")
		return
	}
	lower, ok = getValue(x)
	if !ok {
		err = fmt.Errorf("unable to convert lower *Value in ToLhsLowerUpper")
		return
	}
	// convert the upper bound
	x = ul.GetUpper()
	if x == nil {
		err = fmt.Errorf("nil upper bound in ToLhsLowerUpper")
		lower = nil
		return
	}
	upper, ok = getValue(x)
	if !ok {
		err = fmt.Errorf("unable to convert upper *Value in ToLhsLowerUpper")
		lower = nil
		return
	}
	lhs = ul.GetLhs()
	return
}

// FromConditions converts a slice of condition.Conditions to a *ManyConditions
func FromConditions(cs []condition.Condition) (*ManyConditions, error) {
	// convert cs to a slice of *Conditions
	S := make([]*Condition, 0, len(cs))
	for _, c := range cs {
		x, err := FromCondition(c)
		if err != nil {
			return nil, err
		}
		S = append(S, x)
	}
	// wrap up the slice and return
	return &ManyConditions{
		Cond: S,
	}, nil
}

// ToConditions converts a *ManyConditions to a slice of condition.Conditions
func ToConditions(mc *ManyConditions) ([]condition.Condition, error) {
	cs := mc.GetCond()
	S := make([]condition.Condition, 0, len(cs))
	for _, c := range cs {
		x, err := ToCondition(c)
		if err != nil {
			return nil, err
		}
		S = append(S, x)
	}
	return S, nil
}

// FromCondition converts a condition.Condition to a *Condition
func FromCondition(c condition.Condition) (*Condition, error) {
	// handle the nil case
	if c == nil {
		c = condition.Bool(true)
	}
	// we handle each type of condition separately
	switch cond := c.(type) {
	case condition.Bool: // True or False
		return &Condition{
			Type: Condition_BOOL,
			Value: &Condition_BoolValue{
				BoolValue: bool(cond),
			},
		}, nil
	case *condition.LeafOp: // LeafOp
		leaf, err := FromLeafOp(cond)
		if err != nil {
			return nil, err
		}
		return &Condition{
			Type: Condition_LEAF,
			Value: &Condition_LeafValue{
				LeafValue: leaf,
			},
		}, nil
	case *condition.InOp: // IN
		x, err := FromLHSValues(cond.Lhs(), cond.Values())
		if err != nil {
			return nil, err
		}
		return &Condition{
			Type: Condition_IN,
			Value: &Condition_LhsManyValuesValue{
				LhsManyValuesValue: x,
			},
		}, nil
	case *condition.NotInOp: // NOT IN
		x, err := FromLHSValues(cond.Lhs(), cond.Values())
		if err != nil {
			return nil, err
		}
		return &Condition{
			Type: Condition_NOTIN,
			Value: &Condition_LhsManyValuesValue{
				LhsManyValuesValue: x,
			},
		}, nil
	case *condition.BetweenOp: // BETWEEN
		x, err := FromLHSLowerUpper(cond.Lhs(), cond.Lower(), cond.Upper())
		if err != nil {
			return nil, err
		}
		return &Condition{
			Type: Condition_BETWEEN,
			Value: &Condition_LhsLowerUpperValue{
				LhsLowerUpperValue: x,
			},
		}, nil
	case *condition.NotBetweenOp: // NOT BETWEEN
		x, err := FromLHSLowerUpper(cond.Lhs(), cond.Lower(), cond.Upper())
		if err != nil {
			return nil, err
		}
		return &Condition{
			Type: Condition_NOTBETWEEN,
			Value: &Condition_LhsLowerUpperValue{
				LhsLowerUpperValue: x,
			},
		}, nil
	case *condition.AndOp: // AND
		cs, err := FromConditions(cond.Conditions())
		if err != nil {
			return nil, err
		}
		return &Condition{
			Type: Condition_AND,
			Value: &Condition_ManyConditionsValue{
				ManyConditionsValue: cs,
			},
		}, nil
	case *condition.OrOp: // OR
		cs, err := FromConditions(cond.Conditions())
		if err != nil {
			return nil, err
		}
		return &Condition{
			Type: Condition_OR,
			Value: &Condition_ManyConditionsValue{
				ManyConditionsValue: cs,
			},
		}, nil
	case *condition.IsOp: // IS
		return &Condition{
			Type: Condition_IS,
			Value: &Condition_LhsBoolValue{
				LhsBoolValue: FromLHSBool(cond.Lhs(), cond.Value()),
			},
		}, nil
	default:
		err := fmt.Errorf("unknown condition: %T", c)
		return nil, errors.ConditionFailsToValidate.Wrap(err)
	}
}

// ToCondition converts a *Condition to a condition.Condition
func ToCondition(c *Condition) (condition.Condition, error) {
	// handle the nil case
	if c == nil {
		return condition.Bool(true), nil
	}
	// we handle each condition type separately
	var result condition.Condition
	switch c.GetType() {
	case Condition_AND:
		conds, err := ToConditions(c.GetManyConditionsValue())
		if err != nil {
			return nil, err
		}
		result = condition.And(conds...)
	case Condition_BETWEEN:
		lhs, lower, upper, err := ToLHSLowerUpper(c.GetLhsLowerUpperValue())
		if err != nil {
			return nil, err
		}
		result = condition.Between(lhs, lower, upper)
	case Condition_BOOL:
		result = condition.Bool(c.GetBoolValue())
	case Condition_IN:
		lhs, vals, err := ToLHSValues(c.GetLhsManyValuesValue())
		if err != nil {
			return nil, err
		}
		result = condition.In(lhs, vals...)
	case Condition_LEAF:
		var err error
		result, err = ToLeafOp(c.GetLeafValue())
		if err != nil {
			return nil, err
		}
	case Condition_NOTBETWEEN:
		lhs, lower, upper, err := ToLHSLowerUpper(c.GetLhsLowerUpperValue())
		if err != nil {
			return nil, err
		}
		result = condition.NotBetween(lhs, lower, upper)
	case Condition_NOTIN:
		lhs, vals, err := ToLHSValues(c.GetLhsManyValuesValue())
		if err != nil {
			return nil, err
		}
		result = condition.NotIn(lhs, vals...)
	case Condition_OR:
		conds, err := ToConditions(c.GetManyConditionsValue())
		if err != nil {
			return nil, err
		}
		result = condition.Or(conds...)
	case Condition_IS:
		lhs, b, err := ToLHSBool(c.GetLhsBoolValue())
		if err != nil {
			return nil, err
		}
		result = condition.Is(lhs, b)
	}
	return result, nil
}
