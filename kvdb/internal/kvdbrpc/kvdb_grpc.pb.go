// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.20.1
// source: kvdb.proto

package kvdbrpc

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	Kvdb_ListDatabases_FullMethodName    = "/kvdb.Kvdb/ListDatabases"
	Kvdb_ListTables_FullMethodName       = "/kvdb.Kvdb/ListTables"
	Kvdb_CreateTable_FullMethodName      = "/kvdb.Kvdb/CreateTable"
	Kvdb_DeleteTable_FullMethodName      = "/kvdb.Kvdb/DeleteTable"
	Kvdb_RenameTable_FullMethodName      = "/kvdb.Kvdb/RenameTable"
	Kvdb_DescribeTable_FullMethodName    = "/kvdb.Kvdb/DescribeTable"
	Kvdb_CountWhere_FullMethodName       = "/kvdb.Kvdb/CountWhere"
	Kvdb_Insert_FullMethodName           = "/kvdb.Kvdb/Insert"
	Kvdb_UpdateWhere_FullMethodName      = "/kvdb.Kvdb/UpdateWhere"
	Kvdb_SelectWhereLimit_FullMethodName = "/kvdb.Kvdb/SelectWhereLimit"
	Kvdb_SelectOneWhere_FullMethodName   = "/kvdb.Kvdb/SelectOneWhere"
	Kvdb_DeleteWhere_FullMethodName      = "/kvdb.Kvdb/DeleteWhere"
	Kvdb_AddIndex_FullMethodName         = "/kvdb.Kvdb/AddIndex"
	Kvdb_DeleteIndex_FullMethodName      = "/kvdb.Kvdb/DeleteIndex"
	Kvdb_ListIndices_FullMethodName      = "/kvdb.Kvdb/ListIndices"
	Kvdb_AddKeys_FullMethodName          = "/kvdb.Kvdb/AddKeys"
	Kvdb_DeleteKeys_FullMethodName       = "/kvdb.Kvdb/DeleteKeys"
	Kvdb_DeleteDatabase_FullMethodName   = "/kvdb.Kvdb/DeleteDatabase"
	Kvdb_CreateDatabase_FullMethodName   = "/kvdb.Kvdb/CreateDatabase"
)

// KvdbClient is the client API for Kvdb service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type KvdbClient interface {
	// ListDatabases returns the available databases.
	ListDatabases(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*DatabaseNames, error)
	// ListTables returns the tables in the database.
	// Note that 'db_name' must be set.
	ListTables(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*TableNames, error)
	// CreateTable creates a table with the given name and template.
	// Note that 'db_name' must be set.
	CreateTable(ctx context.Context, in *NameAndTemplate, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// DeleteTable deletes the table with the given name.
	// Note that 'db_name' must be set.
	DeleteTable(ctx context.Context, in *Name, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// RenameTable changes the name of a table in the database.
	// Note that 'db_name' must be set.
	RenameTable(ctx context.Context, in *SrcAndDstNames, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// DescribeTable returns a best-guess template for the data in the table.
	// Note that 'db_name' and 'table_name' must be set.
	DescribeTable(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*Record, error)
	// CountWhere returns the number of records in the table that match the
	// given condition.
	// Note that 'db_name' and 'table_name' must be set.
	CountWhere(ctx context.Context, in *Condition, opts ...grpc.CallOption) (*NumberOfRecords, error)
	// Insert inserts the records from stream into the table.
	// Note that 'db_name' and 'table_name' must be set.
	Insert(ctx context.Context, opts ...grpc.CallOption) (Kvdb_InsertClient, error)
	// UpdateWhere updates all records in the table that match the given
	// condition by setting all keys present in the given record to the
	// corresponding values. Returns the number of records updated.
	// Note that 'db_name' and 'table_name' must be set.
	UpdateWhere(ctx context.Context, in *ConditionAndReplacement, opts ...grpc.CallOption) (*NumberOfRecords, error)
	// SelectWhereLimit returns a stream of records matching the given
	// condition. The returned records will be in the form specified by the
	// given template. If the given limit is non-negative then at most that
	// many records will be returned.
	// Note that 'db_name' and 'table_name' must be set.
	SelectWhereLimit(ctx context.Context, in *ConditionTemplateOrderAndLimit, opts ...grpc.CallOption) (Kvdb_SelectWhereLimitClient, error)
	// SelectOneWhere returns zero or one records, encoded in a HasRecord,
	// matching the given condition. The returned record will be in the form
	// specified by the given template.
	// Note that 'db_name' and 'table_name' must be set.
	SelectOneWhere(ctx context.Context, in *ConditionTemplateAndOrder, opts ...grpc.CallOption) (*HasRecord, error)
	// DeleteWhere deletes those records in the table that match the given
	// condition. Returns the number of records deleted.
	// Note that 'db_name' and 'table_name' must be set.
	DeleteWhere(ctx context.Context, in *Condition, opts ...grpc.CallOption) (*NumberOfRecords, error)
	// AddIndex adds an index on the given key.
	// Note that 'db_name' and 'table_name' must be set.
	AddIndex(ctx context.Context, in *Index, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// DeleteIndex deletes the index on the given key.
	// Note that 'db_name' and 'table_name' must be set.
	DeleteIndex(ctx context.Context, in *Index, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// ListIndices lists the keys for which indices are present.
	// Note that 'db_name' and 'table_name' must be set.
	ListIndices(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*IndexList, error)
	// AddKeys updates each record r in the table, adding any keys in the given
	// record that are not already present along with the corresponding values.
	// Any keys that are already present in r will be left unmodified.
	// Note that 'db_name' and 'table_name' must be set.
	AddKeys(ctx context.Context, in *Record, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// DeleteKeys updates all records in the table, deleting the specified keys
	// if present.
	// Note that 'db_name' and 'table_name' must be set.
	DeleteKeys(ctx context.Context, in *KeyList, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// DeleteDatabase deletes the database 'db_name'
	DeleteDatabase(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// CreateDatabase creates the database 'db_name'
	CreateDatabase(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type kvdbClient struct {
	cc grpc.ClientConnInterface
}

func NewKvdbClient(cc grpc.ClientConnInterface) KvdbClient {
	return &kvdbClient{cc}
}

func (c *kvdbClient) ListDatabases(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*DatabaseNames, error) {
	out := new(DatabaseNames)
	err := c.cc.Invoke(ctx, Kvdb_ListDatabases_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) ListTables(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*TableNames, error) {
	out := new(TableNames)
	err := c.cc.Invoke(ctx, Kvdb_ListTables_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) CreateTable(ctx context.Context, in *NameAndTemplate, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, Kvdb_CreateTable_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) DeleteTable(ctx context.Context, in *Name, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, Kvdb_DeleteTable_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) RenameTable(ctx context.Context, in *SrcAndDstNames, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, Kvdb_RenameTable_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) DescribeTable(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*Record, error) {
	out := new(Record)
	err := c.cc.Invoke(ctx, Kvdb_DescribeTable_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) CountWhere(ctx context.Context, in *Condition, opts ...grpc.CallOption) (*NumberOfRecords, error) {
	out := new(NumberOfRecords)
	err := c.cc.Invoke(ctx, Kvdb_CountWhere_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) Insert(ctx context.Context, opts ...grpc.CallOption) (Kvdb_InsertClient, error) {
	stream, err := c.cc.NewStream(ctx, &Kvdb_ServiceDesc.Streams[0], Kvdb_Insert_FullMethodName, opts...)
	if err != nil {
		return nil, err
	}
	x := &kvdbInsertClient{stream}
	return x, nil
}

type Kvdb_InsertClient interface {
	Send(*Record) error
	CloseAndRecv() (*emptypb.Empty, error)
	grpc.ClientStream
}

type kvdbInsertClient struct {
	grpc.ClientStream
}

func (x *kvdbInsertClient) Send(m *Record) error {
	return x.ClientStream.SendMsg(m)
}

func (x *kvdbInsertClient) CloseAndRecv() (*emptypb.Empty, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(emptypb.Empty)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *kvdbClient) UpdateWhere(ctx context.Context, in *ConditionAndReplacement, opts ...grpc.CallOption) (*NumberOfRecords, error) {
	out := new(NumberOfRecords)
	err := c.cc.Invoke(ctx, Kvdb_UpdateWhere_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) SelectWhereLimit(ctx context.Context, in *ConditionTemplateOrderAndLimit, opts ...grpc.CallOption) (Kvdb_SelectWhereLimitClient, error) {
	stream, err := c.cc.NewStream(ctx, &Kvdb_ServiceDesc.Streams[1], Kvdb_SelectWhereLimit_FullMethodName, opts...)
	if err != nil {
		return nil, err
	}
	x := &kvdbSelectWhereLimitClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type Kvdb_SelectWhereLimitClient interface {
	Recv() (*Record, error)
	grpc.ClientStream
}

type kvdbSelectWhereLimitClient struct {
	grpc.ClientStream
}

func (x *kvdbSelectWhereLimitClient) Recv() (*Record, error) {
	m := new(Record)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *kvdbClient) SelectOneWhere(ctx context.Context, in *ConditionTemplateAndOrder, opts ...grpc.CallOption) (*HasRecord, error) {
	out := new(HasRecord)
	err := c.cc.Invoke(ctx, Kvdb_SelectOneWhere_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) DeleteWhere(ctx context.Context, in *Condition, opts ...grpc.CallOption) (*NumberOfRecords, error) {
	out := new(NumberOfRecords)
	err := c.cc.Invoke(ctx, Kvdb_DeleteWhere_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) AddIndex(ctx context.Context, in *Index, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, Kvdb_AddIndex_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) DeleteIndex(ctx context.Context, in *Index, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, Kvdb_DeleteIndex_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) ListIndices(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*IndexList, error) {
	out := new(IndexList)
	err := c.cc.Invoke(ctx, Kvdb_ListIndices_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) AddKeys(ctx context.Context, in *Record, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, Kvdb_AddKeys_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) DeleteKeys(ctx context.Context, in *KeyList, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, Kvdb_DeleteKeys_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) DeleteDatabase(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, Kvdb_DeleteDatabase_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *kvdbClient) CreateDatabase(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, Kvdb_CreateDatabase_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// KvdbServer is the server API for Kvdb service.
// All implementations must embed UnimplementedKvdbServer
// for forward compatibility
type KvdbServer interface {
	// ListDatabases returns the available databases.
	ListDatabases(context.Context, *emptypb.Empty) (*DatabaseNames, error)
	// ListTables returns the tables in the database.
	// Note that 'db_name' must be set.
	ListTables(context.Context, *emptypb.Empty) (*TableNames, error)
	// CreateTable creates a table with the given name and template.
	// Note that 'db_name' must be set.
	CreateTable(context.Context, *NameAndTemplate) (*emptypb.Empty, error)
	// DeleteTable deletes the table with the given name.
	// Note that 'db_name' must be set.
	DeleteTable(context.Context, *Name) (*emptypb.Empty, error)
	// RenameTable changes the name of a table in the database.
	// Note that 'db_name' must be set.
	RenameTable(context.Context, *SrcAndDstNames) (*emptypb.Empty, error)
	// DescribeTable returns a best-guess template for the data in the table.
	// Note that 'db_name' and 'table_name' must be set.
	DescribeTable(context.Context, *emptypb.Empty) (*Record, error)
	// CountWhere returns the number of records in the table that match the
	// given condition.
	// Note that 'db_name' and 'table_name' must be set.
	CountWhere(context.Context, *Condition) (*NumberOfRecords, error)
	// Insert inserts the records from stream into the table.
	// Note that 'db_name' and 'table_name' must be set.
	Insert(Kvdb_InsertServer) error
	// UpdateWhere updates all records in the table that match the given
	// condition by setting all keys present in the given record to the
	// corresponding values. Returns the number of records updated.
	// Note that 'db_name' and 'table_name' must be set.
	UpdateWhere(context.Context, *ConditionAndReplacement) (*NumberOfRecords, error)
	// SelectWhereLimit returns a stream of records matching the given
	// condition. The returned records will be in the form specified by the
	// given template. If the given limit is non-negative then at most that
	// many records will be returned.
	// Note that 'db_name' and 'table_name' must be set.
	SelectWhereLimit(*ConditionTemplateOrderAndLimit, Kvdb_SelectWhereLimitServer) error
	// SelectOneWhere returns zero or one records, encoded in a HasRecord,
	// matching the given condition. The returned record will be in the form
	// specified by the given template.
	// Note that 'db_name' and 'table_name' must be set.
	SelectOneWhere(context.Context, *ConditionTemplateAndOrder) (*HasRecord, error)
	// DeleteWhere deletes those records in the table that match the given
	// condition. Returns the number of records deleted.
	// Note that 'db_name' and 'table_name' must be set.
	DeleteWhere(context.Context, *Condition) (*NumberOfRecords, error)
	// AddIndex adds an index on the given key.
	// Note that 'db_name' and 'table_name' must be set.
	AddIndex(context.Context, *Index) (*emptypb.Empty, error)
	// DeleteIndex deletes the index on the given key.
	// Note that 'db_name' and 'table_name' must be set.
	DeleteIndex(context.Context, *Index) (*emptypb.Empty, error)
	// ListIndices lists the keys for which indices are present.
	// Note that 'db_name' and 'table_name' must be set.
	ListIndices(context.Context, *emptypb.Empty) (*IndexList, error)
	// AddKeys updates each record r in the table, adding any keys in the given
	// record that are not already present along with the corresponding values.
	// Any keys that are already present in r will be left unmodified.
	// Note that 'db_name' and 'table_name' must be set.
	AddKeys(context.Context, *Record) (*emptypb.Empty, error)
	// DeleteKeys updates all records in the table, deleting the specified keys
	// if present.
	// Note that 'db_name' and 'table_name' must be set.
	DeleteKeys(context.Context, *KeyList) (*emptypb.Empty, error)
	// DeleteDatabase deletes the database 'db_name'
	DeleteDatabase(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
	// CreateDatabase creates the database 'db_name'
	CreateDatabase(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
	mustEmbedUnimplementedKvdbServer()
}

// UnimplementedKvdbServer must be embedded to have forward compatible implementations.
type UnimplementedKvdbServer struct {
}

func (UnimplementedKvdbServer) ListDatabases(context.Context, *emptypb.Empty) (*DatabaseNames, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListDatabases not implemented")
}
func (UnimplementedKvdbServer) ListTables(context.Context, *emptypb.Empty) (*TableNames, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListTables not implemented")
}
func (UnimplementedKvdbServer) CreateTable(context.Context, *NameAndTemplate) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateTable not implemented")
}
func (UnimplementedKvdbServer) DeleteTable(context.Context, *Name) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteTable not implemented")
}
func (UnimplementedKvdbServer) RenameTable(context.Context, *SrcAndDstNames) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RenameTable not implemented")
}
func (UnimplementedKvdbServer) DescribeTable(context.Context, *emptypb.Empty) (*Record, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DescribeTable not implemented")
}
func (UnimplementedKvdbServer) CountWhere(context.Context, *Condition) (*NumberOfRecords, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CountWhere not implemented")
}
func (UnimplementedKvdbServer) Insert(Kvdb_InsertServer) error {
	return status.Errorf(codes.Unimplemented, "method Insert not implemented")
}
func (UnimplementedKvdbServer) UpdateWhere(context.Context, *ConditionAndReplacement) (*NumberOfRecords, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateWhere not implemented")
}
func (UnimplementedKvdbServer) SelectWhereLimit(*ConditionTemplateOrderAndLimit, Kvdb_SelectWhereLimitServer) error {
	return status.Errorf(codes.Unimplemented, "method SelectWhereLimit not implemented")
}
func (UnimplementedKvdbServer) SelectOneWhere(context.Context, *ConditionTemplateAndOrder) (*HasRecord, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SelectOneWhere not implemented")
}
func (UnimplementedKvdbServer) DeleteWhere(context.Context, *Condition) (*NumberOfRecords, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteWhere not implemented")
}
func (UnimplementedKvdbServer) AddIndex(context.Context, *Index) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddIndex not implemented")
}
func (UnimplementedKvdbServer) DeleteIndex(context.Context, *Index) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteIndex not implemented")
}
func (UnimplementedKvdbServer) ListIndices(context.Context, *emptypb.Empty) (*IndexList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListIndices not implemented")
}
func (UnimplementedKvdbServer) AddKeys(context.Context, *Record) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddKeys not implemented")
}
func (UnimplementedKvdbServer) DeleteKeys(context.Context, *KeyList) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteKeys not implemented")
}
func (UnimplementedKvdbServer) DeleteDatabase(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteDatabase not implemented")
}
func (UnimplementedKvdbServer) CreateDatabase(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateDatabase not implemented")
}
func (UnimplementedKvdbServer) mustEmbedUnimplementedKvdbServer() {}

// UnsafeKvdbServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to KvdbServer will
// result in compilation errors.
type UnsafeKvdbServer interface {
	mustEmbedUnimplementedKvdbServer()
}

func RegisterKvdbServer(s grpc.ServiceRegistrar, srv KvdbServer) {
	s.RegisterService(&Kvdb_ServiceDesc, srv)
}

func _Kvdb_ListDatabases_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).ListDatabases(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_ListDatabases_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).ListDatabases(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_ListTables_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).ListTables(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_ListTables_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).ListTables(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_CreateTable_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NameAndTemplate)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).CreateTable(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_CreateTable_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).CreateTable(ctx, req.(*NameAndTemplate))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_DeleteTable_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Name)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).DeleteTable(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_DeleteTable_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).DeleteTable(ctx, req.(*Name))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_RenameTable_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SrcAndDstNames)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).RenameTable(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_RenameTable_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).RenameTable(ctx, req.(*SrcAndDstNames))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_DescribeTable_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).DescribeTable(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_DescribeTable_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).DescribeTable(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_CountWhere_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Condition)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).CountWhere(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_CountWhere_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).CountWhere(ctx, req.(*Condition))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_Insert_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(KvdbServer).Insert(&kvdbInsertServer{stream})
}

type Kvdb_InsertServer interface {
	SendAndClose(*emptypb.Empty) error
	Recv() (*Record, error)
	grpc.ServerStream
}

type kvdbInsertServer struct {
	grpc.ServerStream
}

func (x *kvdbInsertServer) SendAndClose(m *emptypb.Empty) error {
	return x.ServerStream.SendMsg(m)
}

func (x *kvdbInsertServer) Recv() (*Record, error) {
	m := new(Record)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _Kvdb_UpdateWhere_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ConditionAndReplacement)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).UpdateWhere(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_UpdateWhere_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).UpdateWhere(ctx, req.(*ConditionAndReplacement))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_SelectWhereLimit_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(ConditionTemplateOrderAndLimit)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(KvdbServer).SelectWhereLimit(m, &kvdbSelectWhereLimitServer{stream})
}

type Kvdb_SelectWhereLimitServer interface {
	Send(*Record) error
	grpc.ServerStream
}

type kvdbSelectWhereLimitServer struct {
	grpc.ServerStream
}

func (x *kvdbSelectWhereLimitServer) Send(m *Record) error {
	return x.ServerStream.SendMsg(m)
}

func _Kvdb_SelectOneWhere_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ConditionTemplateAndOrder)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).SelectOneWhere(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_SelectOneWhere_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).SelectOneWhere(ctx, req.(*ConditionTemplateAndOrder))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_DeleteWhere_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Condition)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).DeleteWhere(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_DeleteWhere_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).DeleteWhere(ctx, req.(*Condition))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_AddIndex_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Index)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).AddIndex(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_AddIndex_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).AddIndex(ctx, req.(*Index))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_DeleteIndex_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Index)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).DeleteIndex(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_DeleteIndex_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).DeleteIndex(ctx, req.(*Index))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_ListIndices_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).ListIndices(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_ListIndices_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).ListIndices(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_AddKeys_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Record)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).AddKeys(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_AddKeys_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).AddKeys(ctx, req.(*Record))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_DeleteKeys_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(KeyList)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).DeleteKeys(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_DeleteKeys_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).DeleteKeys(ctx, req.(*KeyList))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_DeleteDatabase_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).DeleteDatabase(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_DeleteDatabase_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).DeleteDatabase(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Kvdb_CreateDatabase_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KvdbServer).CreateDatabase(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Kvdb_CreateDatabase_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KvdbServer).CreateDatabase(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

// Kvdb_ServiceDesc is the grpc.ServiceDesc for Kvdb service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Kvdb_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "kvdb.Kvdb",
	HandlerType: (*KvdbServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListDatabases",
			Handler:    _Kvdb_ListDatabases_Handler,
		},
		{
			MethodName: "ListTables",
			Handler:    _Kvdb_ListTables_Handler,
		},
		{
			MethodName: "CreateTable",
			Handler:    _Kvdb_CreateTable_Handler,
		},
		{
			MethodName: "DeleteTable",
			Handler:    _Kvdb_DeleteTable_Handler,
		},
		{
			MethodName: "RenameTable",
			Handler:    _Kvdb_RenameTable_Handler,
		},
		{
			MethodName: "DescribeTable",
			Handler:    _Kvdb_DescribeTable_Handler,
		},
		{
			MethodName: "CountWhere",
			Handler:    _Kvdb_CountWhere_Handler,
		},
		{
			MethodName: "UpdateWhere",
			Handler:    _Kvdb_UpdateWhere_Handler,
		},
		{
			MethodName: "SelectOneWhere",
			Handler:    _Kvdb_SelectOneWhere_Handler,
		},
		{
			MethodName: "DeleteWhere",
			Handler:    _Kvdb_DeleteWhere_Handler,
		},
		{
			MethodName: "AddIndex",
			Handler:    _Kvdb_AddIndex_Handler,
		},
		{
			MethodName: "DeleteIndex",
			Handler:    _Kvdb_DeleteIndex_Handler,
		},
		{
			MethodName: "ListIndices",
			Handler:    _Kvdb_ListIndices_Handler,
		},
		{
			MethodName: "AddKeys",
			Handler:    _Kvdb_AddKeys_Handler,
		},
		{
			MethodName: "DeleteKeys",
			Handler:    _Kvdb_DeleteKeys_Handler,
		},
		{
			MethodName: "DeleteDatabase",
			Handler:    _Kvdb_DeleteDatabase_Handler,
		},
		{
			MethodName: "CreateDatabase",
			Handler:    _Kvdb_CreateDatabase_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Insert",
			Handler:       _Kvdb_Insert_Handler,
			ClientStreams: true,
		},
		{
			StreamName:    "SelectWhereLimit",
			Handler:       _Kvdb_SelectWhereLimit_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "kvdb.proto",
}
