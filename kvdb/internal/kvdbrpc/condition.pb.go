// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.33.0
// 	protoc        v3.20.1
// source: condition.proto

package kvdbrpc

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// LeafType enumerates the possible leaf operators
type LeafOperator_LeafType int32

const (
	LeafOperator_UNDEFINED LeafOperator_LeafType = 0
	LeafOperator_EQ        LeafOperator_LeafType = 1 // equality
	LeafOperator_NE        LeafOperator_LeafType = 2 // not equal to
	LeafOperator_GT        LeafOperator_LeafType = 3 // greater than
	LeafOperator_LT        LeafOperator_LeafType = 4 // less than
	LeafOperator_GE        LeafOperator_LeafType = 5 // greater than or equal to
	LeafOperator_LE        LeafOperator_LeafType = 6 // less than or equal to
)

// Enum value maps for LeafOperator_LeafType.
var (
	LeafOperator_LeafType_name = map[int32]string{
		0: "UNDEFINED",
		1: "EQ",
		2: "NE",
		3: "GT",
		4: "LT",
		5: "GE",
		6: "LE",
	}
	LeafOperator_LeafType_value = map[string]int32{
		"UNDEFINED": 0,
		"EQ":        1,
		"NE":        2,
		"GT":        3,
		"LT":        4,
		"GE":        5,
		"LE":        6,
	}
)

func (x LeafOperator_LeafType) Enum() *LeafOperator_LeafType {
	p := new(LeafOperator_LeafType)
	*p = x
	return p
}

func (x LeafOperator_LeafType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (LeafOperator_LeafType) Descriptor() protoreflect.EnumDescriptor {
	return file_condition_proto_enumTypes[0].Descriptor()
}

func (LeafOperator_LeafType) Type() protoreflect.EnumType {
	return &file_condition_proto_enumTypes[0]
}

func (x LeafOperator_LeafType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use LeafOperator_LeafType.Descriptor instead.
func (LeafOperator_LeafType) EnumDescriptor() ([]byte, []int) {
	return file_condition_proto_rawDescGZIP(), []int{4, 0}
}

// CondType enumerates the possible condition types
type Condition_CondType int32

const (
	Condition_UNDEFINED  Condition_CondType = 0 // Illegal value
	Condition_AND        Condition_CondType = 1 // An AND operator
	Condition_BETWEEN    Condition_CondType = 2 // A BETWEEN operator
	Condition_BOOL       Condition_CondType = 3 // True or False
	Condition_IN         Condition_CondType = 4 // An IN operator
	Condition_LEAF       Condition_CondType = 5 // An operator of the form "lhs op rhs"; see below
	Condition_NOTBETWEEN Condition_CondType = 6 // A NOT BETWEEN operator
	Condition_NOTIN      Condition_CondType = 7 // A NOT IN operator
	Condition_OR         Condition_CondType = 8 // An OR operator
	Condition_IS         Condition_CondType = 9 // An IS operator
)

// Enum value maps for Condition_CondType.
var (
	Condition_CondType_name = map[int32]string{
		0: "UNDEFINED",
		1: "AND",
		2: "BETWEEN",
		3: "BOOL",
		4: "IN",
		5: "LEAF",
		6: "NOTBETWEEN",
		7: "NOTIN",
		8: "OR",
		9: "IS",
	}
	Condition_CondType_value = map[string]int32{
		"UNDEFINED":  0,
		"AND":        1,
		"BETWEEN":    2,
		"BOOL":       3,
		"IN":         4,
		"LEAF":       5,
		"NOTBETWEEN": 6,
		"NOTIN":      7,
		"OR":         8,
		"IS":         9,
	}
)

func (x Condition_CondType) Enum() *Condition_CondType {
	p := new(Condition_CondType)
	*p = x
	return p
}

func (x Condition_CondType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Condition_CondType) Descriptor() protoreflect.EnumDescriptor {
	return file_condition_proto_enumTypes[1].Descriptor()
}

func (Condition_CondType) Type() protoreflect.EnumType {
	return &file_condition_proto_enumTypes[1]
}

func (x Condition_CondType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Condition_CondType.Descriptor instead.
func (Condition_CondType) EnumDescriptor() ([]byte, []int) {
	return file_condition_proto_rawDescGZIP(), []int{5, 0}
}

// ManyConditions represents a sequence of conditions
type ManyConditions struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Cond []*Condition `protobuf:"bytes,1,rep,name=cond,proto3" json:"cond,omitempty"`
}

func (x *ManyConditions) Reset() {
	*x = ManyConditions{}
	if protoimpl.UnsafeEnabled {
		mi := &file_condition_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ManyConditions) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ManyConditions) ProtoMessage() {}

func (x *ManyConditions) ProtoReflect() protoreflect.Message {
	mi := &file_condition_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ManyConditions.ProtoReflect.Descriptor instead.
func (*ManyConditions) Descriptor() ([]byte, []int) {
	return file_condition_proto_rawDescGZIP(), []int{0}
}

func (x *ManyConditions) GetCond() []*Condition {
	if x != nil {
		return x.Cond
	}
	return nil
}

// LhsBool represents an IS condition
type LhsBool struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Lhs       string `protobuf:"bytes,1,opt,name=lhs,proto3" json:"lhs,omitempty"`
	BoolValue bool   `protobuf:"varint,2,opt,name=bool_value,json=boolValue,proto3" json:"bool_value,omitempty"`
}

func (x *LhsBool) Reset() {
	*x = LhsBool{}
	if protoimpl.UnsafeEnabled {
		mi := &file_condition_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LhsBool) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LhsBool) ProtoMessage() {}

func (x *LhsBool) ProtoReflect() protoreflect.Message {
	mi := &file_condition_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LhsBool.ProtoReflect.Descriptor instead.
func (*LhsBool) Descriptor() ([]byte, []int) {
	return file_condition_proto_rawDescGZIP(), []int{1}
}

func (x *LhsBool) GetLhs() string {
	if x != nil {
		return x.Lhs
	}
	return ""
}

func (x *LhsBool) GetBoolValue() bool {
	if x != nil {
		return x.BoolValue
	}
	return false
}

// LhsManyValues represents an IN or NOT IN condition
type LhsManyValues struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Lhs   string   `protobuf:"bytes,1,opt,name=lhs,proto3" json:"lhs,omitempty"`
	Value []*Value `protobuf:"bytes,2,rep,name=value,proto3" json:"value,omitempty"`
}

func (x *LhsManyValues) Reset() {
	*x = LhsManyValues{}
	if protoimpl.UnsafeEnabled {
		mi := &file_condition_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LhsManyValues) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LhsManyValues) ProtoMessage() {}

func (x *LhsManyValues) ProtoReflect() protoreflect.Message {
	mi := &file_condition_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LhsManyValues.ProtoReflect.Descriptor instead.
func (*LhsManyValues) Descriptor() ([]byte, []int) {
	return file_condition_proto_rawDescGZIP(), []int{2}
}

func (x *LhsManyValues) GetLhs() string {
	if x != nil {
		return x.Lhs
	}
	return ""
}

func (x *LhsManyValues) GetValue() []*Value {
	if x != nil {
		return x.Value
	}
	return nil
}

// LhsLowerUpper represents a BETWEEN or NOT BETWEEN condition
type LhsLowerUpper struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Lhs   string `protobuf:"bytes,1,opt,name=lhs,proto3" json:"lhs,omitempty"`
	Lower *Value `protobuf:"bytes,2,opt,name=lower,proto3" json:"lower,omitempty"`
	Upper *Value `protobuf:"bytes,3,opt,name=upper,proto3" json:"upper,omitempty"`
}

func (x *LhsLowerUpper) Reset() {
	*x = LhsLowerUpper{}
	if protoimpl.UnsafeEnabled {
		mi := &file_condition_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LhsLowerUpper) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LhsLowerUpper) ProtoMessage() {}

func (x *LhsLowerUpper) ProtoReflect() protoreflect.Message {
	mi := &file_condition_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LhsLowerUpper.ProtoReflect.Descriptor instead.
func (*LhsLowerUpper) Descriptor() ([]byte, []int) {
	return file_condition_proto_rawDescGZIP(), []int{3}
}

func (x *LhsLowerUpper) GetLhs() string {
	if x != nil {
		return x.Lhs
	}
	return ""
}

func (x *LhsLowerUpper) GetLower() *Value {
	if x != nil {
		return x.Lower
	}
	return nil
}

func (x *LhsLowerUpper) GetUpper() *Value {
	if x != nil {
		return x.Upper
	}
	return nil
}

// LeafOperator represents an operator of the form "lhs op rhs" where lhs is a
// key and rhs is a value.
type LeafOperator struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Lhs    string                `protobuf:"bytes,1,opt,name=lhs,proto3" json:"lhs,omitempty"`
	Opcode LeafOperator_LeafType `protobuf:"varint,2,opt,name=opcode,proto3,enum=kvdb.LeafOperator_LeafType" json:"opcode,omitempty"`
	Rhs    *Value                `protobuf:"bytes,3,opt,name=rhs,proto3" json:"rhs,omitempty"`
}

func (x *LeafOperator) Reset() {
	*x = LeafOperator{}
	if protoimpl.UnsafeEnabled {
		mi := &file_condition_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LeafOperator) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LeafOperator) ProtoMessage() {}

func (x *LeafOperator) ProtoReflect() protoreflect.Message {
	mi := &file_condition_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LeafOperator.ProtoReflect.Descriptor instead.
func (*LeafOperator) Descriptor() ([]byte, []int) {
	return file_condition_proto_rawDescGZIP(), []int{4}
}

func (x *LeafOperator) GetLhs() string {
	if x != nil {
		return x.Lhs
	}
	return ""
}

func (x *LeafOperator) GetOpcode() LeafOperator_LeafType {
	if x != nil {
		return x.Opcode
	}
	return LeafOperator_UNDEFINED
}

func (x *LeafOperator) GetRhs() *Value {
	if x != nil {
		return x.Rhs
	}
	return nil
}

// Condition describes a condition
type Condition struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Type Condition_CondType `protobuf:"varint,1,opt,name=type,proto3,enum=kvdb.Condition_CondType" json:"type,omitempty"` // The condition type
	// Types that are assignable to Value:
	//
	//	*Condition_ManyConditionsValue
	//	*Condition_LhsLowerUpperValue
	//	*Condition_BoolValue
	//	*Condition_LhsManyValuesValue
	//	*Condition_LeafValue
	//	*Condition_LhsBoolValue
	Value isCondition_Value `protobuf_oneof:"value"`
}

func (x *Condition) Reset() {
	*x = Condition{}
	if protoimpl.UnsafeEnabled {
		mi := &file_condition_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Condition) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Condition) ProtoMessage() {}

func (x *Condition) ProtoReflect() protoreflect.Message {
	mi := &file_condition_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Condition.ProtoReflect.Descriptor instead.
func (*Condition) Descriptor() ([]byte, []int) {
	return file_condition_proto_rawDescGZIP(), []int{5}
}

func (x *Condition) GetType() Condition_CondType {
	if x != nil {
		return x.Type
	}
	return Condition_UNDEFINED
}

func (m *Condition) GetValue() isCondition_Value {
	if m != nil {
		return m.Value
	}
	return nil
}

func (x *Condition) GetManyConditionsValue() *ManyConditions {
	if x, ok := x.GetValue().(*Condition_ManyConditionsValue); ok {
		return x.ManyConditionsValue
	}
	return nil
}

func (x *Condition) GetLhsLowerUpperValue() *LhsLowerUpper {
	if x, ok := x.GetValue().(*Condition_LhsLowerUpperValue); ok {
		return x.LhsLowerUpperValue
	}
	return nil
}

func (x *Condition) GetBoolValue() bool {
	if x, ok := x.GetValue().(*Condition_BoolValue); ok {
		return x.BoolValue
	}
	return false
}

func (x *Condition) GetLhsManyValuesValue() *LhsManyValues {
	if x, ok := x.GetValue().(*Condition_LhsManyValuesValue); ok {
		return x.LhsManyValuesValue
	}
	return nil
}

func (x *Condition) GetLeafValue() *LeafOperator {
	if x, ok := x.GetValue().(*Condition_LeafValue); ok {
		return x.LeafValue
	}
	return nil
}

func (x *Condition) GetLhsBoolValue() *LhsBool {
	if x, ok := x.GetValue().(*Condition_LhsBoolValue); ok {
		return x.LhsBoolValue
	}
	return nil
}

type isCondition_Value interface {
	isCondition_Value()
}

type Condition_ManyConditionsValue struct {
	ManyConditionsValue *ManyConditions `protobuf:"bytes,2,opt,name=many_conditions_value,json=manyConditionsValue,proto3,oneof"`
}

type Condition_LhsLowerUpperValue struct {
	LhsLowerUpperValue *LhsLowerUpper `protobuf:"bytes,3,opt,name=lhs_lower_upper_value,json=lhsLowerUpperValue,proto3,oneof"`
}

type Condition_BoolValue struct {
	BoolValue bool `protobuf:"varint,4,opt,name=bool_value,json=boolValue,proto3,oneof"`
}

type Condition_LhsManyValuesValue struct {
	LhsManyValuesValue *LhsManyValues `protobuf:"bytes,5,opt,name=lhs_many_values_value,json=lhsManyValuesValue,proto3,oneof"`
}

type Condition_LeafValue struct {
	LeafValue *LeafOperator `protobuf:"bytes,6,opt,name=leaf_value,json=leafValue,proto3,oneof"`
}

type Condition_LhsBoolValue struct {
	LhsBoolValue *LhsBool `protobuf:"bytes,7,opt,name=lhs_bool_value,json=lhsBoolValue,proto3,oneof"`
}

func (*Condition_ManyConditionsValue) isCondition_Value() {}

func (*Condition_LhsLowerUpperValue) isCondition_Value() {}

func (*Condition_BoolValue) isCondition_Value() {}

func (*Condition_LhsManyValuesValue) isCondition_Value() {}

func (*Condition_LeafValue) isCondition_Value() {}

func (*Condition_LhsBoolValue) isCondition_Value() {}

var File_condition_proto protoreflect.FileDescriptor

var file_condition_proto_rawDesc = []byte{
	0x0a, 0x0f, 0x63, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x12, 0x04, 0x6b, 0x76, 0x64, 0x62, 0x1a, 0x0b, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0x35, 0x0a, 0x0e, 0x4d, 0x61, 0x6e, 0x79, 0x43, 0x6f, 0x6e, 0x64,
	0x69, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x12, 0x23, 0x0a, 0x04, 0x63, 0x6f, 0x6e, 0x64, 0x18, 0x01,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x0f, 0x2e, 0x6b, 0x76, 0x64, 0x62, 0x2e, 0x43, 0x6f, 0x6e, 0x64,
	0x69, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x04, 0x63, 0x6f, 0x6e, 0x64, 0x22, 0x3a, 0x0a, 0x07, 0x4c,
	0x68, 0x73, 0x42, 0x6f, 0x6f, 0x6c, 0x12, 0x10, 0x0a, 0x03, 0x6c, 0x68, 0x73, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x03, 0x6c, 0x68, 0x73, 0x12, 0x1d, 0x0a, 0x0a, 0x62, 0x6f, 0x6f, 0x6c,
	0x5f, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x08, 0x52, 0x09, 0x62, 0x6f,
	0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x44, 0x0a, 0x0d, 0x4c, 0x68, 0x73, 0x4d, 0x61,
	0x6e, 0x79, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x73, 0x12, 0x10, 0x0a, 0x03, 0x6c, 0x68, 0x73, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6c, 0x68, 0x73, 0x12, 0x21, 0x0a, 0x05, 0x76, 0x61,
	0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x0b, 0x2e, 0x6b, 0x76, 0x64, 0x62,
	0x2e, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x67, 0x0a,
	0x0d, 0x4c, 0x68, 0x73, 0x4c, 0x6f, 0x77, 0x65, 0x72, 0x55, 0x70, 0x70, 0x65, 0x72, 0x12, 0x10,
	0x0a, 0x03, 0x6c, 0x68, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6c, 0x68, 0x73,
	0x12, 0x21, 0x0a, 0x05, 0x6c, 0x6f, 0x77, 0x65, 0x72, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x0b, 0x2e, 0x6b, 0x76, 0x64, 0x62, 0x2e, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x05, 0x6c, 0x6f,
	0x77, 0x65, 0x72, 0x12, 0x21, 0x0a, 0x05, 0x75, 0x70, 0x70, 0x65, 0x72, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x0b, 0x2e, 0x6b, 0x76, 0x64, 0x62, 0x2e, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52,
	0x05, 0x75, 0x70, 0x70, 0x65, 0x72, 0x22, 0xbf, 0x01, 0x0a, 0x0c, 0x4c, 0x65, 0x61, 0x66, 0x4f,
	0x70, 0x65, 0x72, 0x61, 0x74, 0x6f, 0x72, 0x12, 0x10, 0x0a, 0x03, 0x6c, 0x68, 0x73, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6c, 0x68, 0x73, 0x12, 0x33, 0x0a, 0x06, 0x6f, 0x70, 0x63,
	0x6f, 0x64, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x1b, 0x2e, 0x6b, 0x76, 0x64, 0x62,
	0x2e, 0x4c, 0x65, 0x61, 0x66, 0x4f, 0x70, 0x65, 0x72, 0x61, 0x74, 0x6f, 0x72, 0x2e, 0x4c, 0x65,
	0x61, 0x66, 0x54, 0x79, 0x70, 0x65, 0x52, 0x06, 0x6f, 0x70, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x1d,
	0x0a, 0x03, 0x72, 0x68, 0x73, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0b, 0x2e, 0x6b, 0x76,
	0x64, 0x62, 0x2e, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x03, 0x72, 0x68, 0x73, 0x22, 0x49, 0x0a,
	0x08, 0x4c, 0x65, 0x61, 0x66, 0x54, 0x79, 0x70, 0x65, 0x12, 0x0d, 0x0a, 0x09, 0x55, 0x4e, 0x44,
	0x45, 0x46, 0x49, 0x4e, 0x45, 0x44, 0x10, 0x00, 0x12, 0x06, 0x0a, 0x02, 0x45, 0x51, 0x10, 0x01,
	0x12, 0x06, 0x0a, 0x02, 0x4e, 0x45, 0x10, 0x02, 0x12, 0x06, 0x0a, 0x02, 0x47, 0x54, 0x10, 0x03,
	0x12, 0x06, 0x0a, 0x02, 0x4c, 0x54, 0x10, 0x04, 0x12, 0x06, 0x0a, 0x02, 0x47, 0x45, 0x10, 0x05,
	0x12, 0x06, 0x0a, 0x02, 0x4c, 0x45, 0x10, 0x06, 0x22, 0xa7, 0x04, 0x0a, 0x09, 0x43, 0x6f, 0x6e,
	0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x2c, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x0e, 0x32, 0x18, 0x2e, 0x6b, 0x76, 0x64, 0x62, 0x2e, 0x43, 0x6f, 0x6e, 0x64,
	0x69, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x43, 0x6f, 0x6e, 0x64, 0x54, 0x79, 0x70, 0x65, 0x52, 0x04,
	0x74, 0x79, 0x70, 0x65, 0x12, 0x4a, 0x0a, 0x15, 0x6d, 0x61, 0x6e, 0x79, 0x5f, 0x63, 0x6f, 0x6e,
	0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x5f, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x14, 0x2e, 0x6b, 0x76, 0x64, 0x62, 0x2e, 0x4d, 0x61, 0x6e, 0x79, 0x43,
	0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x48, 0x00, 0x52, 0x13, 0x6d, 0x61, 0x6e,
	0x79, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x56, 0x61, 0x6c, 0x75, 0x65,
	0x12, 0x48, 0x0a, 0x15, 0x6c, 0x68, 0x73, 0x5f, 0x6c, 0x6f, 0x77, 0x65, 0x72, 0x5f, 0x75, 0x70,
	0x70, 0x65, 0x72, 0x5f, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x13, 0x2e, 0x6b, 0x76, 0x64, 0x62, 0x2e, 0x4c, 0x68, 0x73, 0x4c, 0x6f, 0x77, 0x65, 0x72, 0x55,
	0x70, 0x70, 0x65, 0x72, 0x48, 0x00, 0x52, 0x12, 0x6c, 0x68, 0x73, 0x4c, 0x6f, 0x77, 0x65, 0x72,
	0x55, 0x70, 0x70, 0x65, 0x72, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x12, 0x1f, 0x0a, 0x0a, 0x62, 0x6f,
	0x6f, 0x6c, 0x5f, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x08, 0x48, 0x00,
	0x52, 0x09, 0x62, 0x6f, 0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x12, 0x48, 0x0a, 0x15, 0x6c,
	0x68, 0x73, 0x5f, 0x6d, 0x61, 0x6e, 0x79, 0x5f, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x73, 0x5f, 0x76,
	0x61, 0x6c, 0x75, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x13, 0x2e, 0x6b, 0x76, 0x64,
	0x62, 0x2e, 0x4c, 0x68, 0x73, 0x4d, 0x61, 0x6e, 0x79, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x73, 0x48,
	0x00, 0x52, 0x12, 0x6c, 0x68, 0x73, 0x4d, 0x61, 0x6e, 0x79, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x73,
	0x56, 0x61, 0x6c, 0x75, 0x65, 0x12, 0x33, 0x0a, 0x0a, 0x6c, 0x65, 0x61, 0x66, 0x5f, 0x76, 0x61,
	0x6c, 0x75, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x6b, 0x76, 0x64, 0x62,
	0x2e, 0x4c, 0x65, 0x61, 0x66, 0x4f, 0x70, 0x65, 0x72, 0x61, 0x74, 0x6f, 0x72, 0x48, 0x00, 0x52,
	0x09, 0x6c, 0x65, 0x61, 0x66, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x12, 0x35, 0x0a, 0x0e, 0x6c, 0x68,
	0x73, 0x5f, 0x62, 0x6f, 0x6f, 0x6c, 0x5f, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x07, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x0d, 0x2e, 0x6b, 0x76, 0x64, 0x62, 0x2e, 0x4c, 0x68, 0x73, 0x42, 0x6f, 0x6f,
	0x6c, 0x48, 0x00, 0x52, 0x0c, 0x6c, 0x68, 0x73, 0x42, 0x6f, 0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75,
	0x65, 0x22, 0x76, 0x0a, 0x08, 0x43, 0x6f, 0x6e, 0x64, 0x54, 0x79, 0x70, 0x65, 0x12, 0x0d, 0x0a,
	0x09, 0x55, 0x4e, 0x44, 0x45, 0x46, 0x49, 0x4e, 0x45, 0x44, 0x10, 0x00, 0x12, 0x07, 0x0a, 0x03,
	0x41, 0x4e, 0x44, 0x10, 0x01, 0x12, 0x0b, 0x0a, 0x07, 0x42, 0x45, 0x54, 0x57, 0x45, 0x45, 0x4e,
	0x10, 0x02, 0x12, 0x08, 0x0a, 0x04, 0x42, 0x4f, 0x4f, 0x4c, 0x10, 0x03, 0x12, 0x06, 0x0a, 0x02,
	0x49, 0x4e, 0x10, 0x04, 0x12, 0x08, 0x0a, 0x04, 0x4c, 0x45, 0x41, 0x46, 0x10, 0x05, 0x12, 0x0e,
	0x0a, 0x0a, 0x4e, 0x4f, 0x54, 0x42, 0x45, 0x54, 0x57, 0x45, 0x45, 0x4e, 0x10, 0x06, 0x12, 0x09,
	0x0a, 0x05, 0x4e, 0x4f, 0x54, 0x49, 0x4e, 0x10, 0x07, 0x12, 0x06, 0x0a, 0x02, 0x4f, 0x52, 0x10,
	0x08, 0x12, 0x06, 0x0a, 0x02, 0x49, 0x53, 0x10, 0x09, 0x42, 0x07, 0x0a, 0x05, 0x76, 0x61, 0x6c,
	0x75, 0x65, 0x42, 0x0f, 0x5a, 0x0a, 0x2e, 0x2f, 0x3b, 0x6b, 0x76, 0x64, 0x62, 0x72, 0x70, 0x63,
	0x80, 0x01, 0x01, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_condition_proto_rawDescOnce sync.Once
	file_condition_proto_rawDescData = file_condition_proto_rawDesc
)

func file_condition_proto_rawDescGZIP() []byte {
	file_condition_proto_rawDescOnce.Do(func() {
		file_condition_proto_rawDescData = protoimpl.X.CompressGZIP(file_condition_proto_rawDescData)
	})
	return file_condition_proto_rawDescData
}

var file_condition_proto_enumTypes = make([]protoimpl.EnumInfo, 2)
var file_condition_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_condition_proto_goTypes = []interface{}{
	(LeafOperator_LeafType)(0), // 0: kvdb.LeafOperator.LeafType
	(Condition_CondType)(0),    // 1: kvdb.Condition.CondType
	(*ManyConditions)(nil),     // 2: kvdb.ManyConditions
	(*LhsBool)(nil),            // 3: kvdb.LhsBool
	(*LhsManyValues)(nil),      // 4: kvdb.LhsManyValues
	(*LhsLowerUpper)(nil),      // 5: kvdb.LhsLowerUpper
	(*LeafOperator)(nil),       // 6: kvdb.LeafOperator
	(*Condition)(nil),          // 7: kvdb.Condition
	(*Value)(nil),              // 8: kvdb.Value
}
var file_condition_proto_depIdxs = []int32{
	7,  // 0: kvdb.ManyConditions.cond:type_name -> kvdb.Condition
	8,  // 1: kvdb.LhsManyValues.value:type_name -> kvdb.Value
	8,  // 2: kvdb.LhsLowerUpper.lower:type_name -> kvdb.Value
	8,  // 3: kvdb.LhsLowerUpper.upper:type_name -> kvdb.Value
	0,  // 4: kvdb.LeafOperator.opcode:type_name -> kvdb.LeafOperator.LeafType
	8,  // 5: kvdb.LeafOperator.rhs:type_name -> kvdb.Value
	1,  // 6: kvdb.Condition.type:type_name -> kvdb.Condition.CondType
	2,  // 7: kvdb.Condition.many_conditions_value:type_name -> kvdb.ManyConditions
	5,  // 8: kvdb.Condition.lhs_lower_upper_value:type_name -> kvdb.LhsLowerUpper
	4,  // 9: kvdb.Condition.lhs_many_values_value:type_name -> kvdb.LhsManyValues
	6,  // 10: kvdb.Condition.leaf_value:type_name -> kvdb.LeafOperator
	3,  // 11: kvdb.Condition.lhs_bool_value:type_name -> kvdb.LhsBool
	12, // [12:12] is the sub-list for method output_type
	12, // [12:12] is the sub-list for method input_type
	12, // [12:12] is the sub-list for extension type_name
	12, // [12:12] is the sub-list for extension extendee
	0,  // [0:12] is the sub-list for field type_name
}

func init() { file_condition_proto_init() }
func file_condition_proto_init() {
	if File_condition_proto != nil {
		return
	}
	file_value_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_condition_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ManyConditions); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_condition_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LhsBool); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_condition_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LhsManyValues); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_condition_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LhsLowerUpper); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_condition_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LeafOperator); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_condition_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Condition); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_condition_proto_msgTypes[5].OneofWrappers = []interface{}{
		(*Condition_ManyConditionsValue)(nil),
		(*Condition_LhsLowerUpperValue)(nil),
		(*Condition_BoolValue)(nil),
		(*Condition_LhsManyValuesValue)(nil),
		(*Condition_LeafValue)(nil),
		(*Condition_LhsBoolValue)(nil),
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_condition_proto_rawDesc,
			NumEnums:      2,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_condition_proto_goTypes,
		DependencyIndexes: file_condition_proto_depIdxs,
		EnumInfos:         file_condition_proto_enumTypes,
		MessageInfos:      file_condition_proto_msgTypes,
	}.Build()
	File_condition_proto = out.File
	file_condition_proto_rawDesc = nil
	file_condition_proto_goTypes = nil
	file_condition_proto_depIdxs = nil
}
