//go:generate protoc -I=../proto/ --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --go_out=. --go-grpc_out=. kvdb.proto condition.proto value.proto sort.proto

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdbrpc

import (
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcastools/convert"
	"fmt"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getValue returns the value set on v.
func getValue(v *Value) (val interface{}, ok bool) {
	ok = true
	switch x := v.GetValue().(type) {
	case *Value_Int32Value:
		val = x.Int32Value
	case *Value_Int64Value:
		val = x.Int64Value
	case *Value_Uint32Value:
		val = x.Uint32Value
	case *Value_Uint64Value:
		val = x.Uint64Value
	case *Value_BoolValue:
		val = x.BoolValue
	case *Value_DoubleValue:
		val = x.DoubleValue
	case *Value_StringValue:
		val = x.StringValue
	case *Value_BytesValue:
		val = x.BytesValue
	default:
		ok = false
	}
	return
}

// convertType attempts to convert the object x to the type represented by t.
func convertType(x interface{}, t Value_Type) (val interface{}, err error) {
	// Perform the conversion
	switch t {
	case Value_INT:
		val, err = convert.ToInt(x)
	case Value_INT8:
		val, err = convert.ToInt8(x)
	case Value_INT16:
		val, err = convert.ToInt16(x)
	case Value_INT32:
		val, err = convert.ToInt32(x)
	case Value_INT64:
		val, err = convert.ToInt64(x)
	case Value_UINT:
		val, err = convert.ToUint(x)
	case Value_UINT8:
		val, err = convert.ToUint8(x)
	case Value_UINT16:
		val, err = convert.ToUint16(x)
	case Value_UINT32:
		val, err = convert.ToUint32(x)
	case Value_UINT64:
		val, err = convert.ToUint64(x)
	case Value_BOOL:
		val, err = convert.ToBool(x)
	case Value_FLOAT64:
		val, err = convert.ToFloat64(x)
	case Value_STRING:
		val, err = convert.ToString(x)
	case Value_BYTESLICE:
		if S, ok := x.([]byte); ok {
			T := make([]byte, len(S))
			copy(T, S)
			val = T
		} else if str, e := convert.ToString(x); e != nil {
			err = e
		} else {
			val = []byte(str)
		}
	default:
		err = fmt.Errorf("unknown value type: %s", t)
	}
	return
}

// convertValue attempts to convert v to a *Value
func convertValue(v interface{}) (*Value, error) {
	var val *Value
	switch x := v.(type) {
	case int:
		val = &Value{
			Type:  Value_INT,
			Value: &Value_Int64Value{Int64Value: int64(x)},
		}
	case int8:
		val = &Value{
			Type:  Value_INT8,
			Value: &Value_Int32Value{Int32Value: int32(x)},
		}
	case int16:
		val = &Value{
			Type:  Value_INT16,
			Value: &Value_Int32Value{Int32Value: int32(x)},
		}
	case int32:
		val = &Value{
			Type:  Value_INT32,
			Value: &Value_Int32Value{Int32Value: x},
		}
	case int64:
		val = &Value{
			Type:  Value_INT64,
			Value: &Value_Int64Value{Int64Value: x},
		}
	case uint:
		val = &Value{
			Type:  Value_UINT,
			Value: &Value_Uint64Value{Uint64Value: uint64(x)},
		}
	case uint8:
		val = &Value{
			Type:  Value_UINT8,
			Value: &Value_Uint32Value{Uint32Value: uint32(x)},
		}
	case uint16:
		val = &Value{
			Type:  Value_UINT16,
			Value: &Value_Uint32Value{Uint32Value: uint32(x)},
		}
	case uint32:
		val = &Value{
			Type:  Value_UINT32,
			Value: &Value_Uint32Value{Uint32Value: x},
		}
	case uint64:
		val = &Value{
			Type:  Value_UINT64,
			Value: &Value_Uint64Value{Uint64Value: x},
		}
	case bool:
		val = &Value{
			Type:  Value_BOOL,
			Value: &Value_BoolValue{BoolValue: x},
		}
	case float64:
		val = &Value{
			Type:  Value_FLOAT64,
			Value: &Value_DoubleValue{DoubleValue: x},
		}
	case string:
		val = &Value{
			Type:  Value_STRING,
			Value: &Value_StringValue{StringValue: x},
		}
	case []byte:
		T := make([]byte, len(x))
		copy(T, x)
		val = &Value{
			Type:  Value_BYTESLICE,
			Value: &Value_BytesValue{BytesValue: T},
		}
	default:
		err := fmt.Errorf("unsupported type (%T) in Value conversion", v)
		return nil, errors.RecordUnsupportedType.Wrap(err)
	}
	return val, nil
}

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// ScanRecord scans the contents of "src" into "dst". Any previously set keys or values in "dst" will be deleted or overwritten.
func ScanRecord(dst record.Record, src *Record) error {
	// Ensure that the destination record is empty
	for k := range dst {
		delete(dst, k)
	}
	// Recover the values
	vals := src.GetValues()
	// Start populating the destination record
	for k, v := range vals {
		// Recover the value
		x, ok := getValue(v)
		if !ok {
			err := fmt.Errorf("unsupported value type (%T) for key %s", v, k)
			return errors.RecordUnsupportedType.Wrap(err)
		}
		// Attempt to convert the value to the appropriate type
		val, err := convertType(x, v.GetType())
		if err != nil {
			return errors.RecordUnableToConvertType.Wrap(err)
		}
		dst[k] = val
	}
	return nil
}

// ToRecord converts a *Record to a record.Record
func ToRecord(r *Record) (record.Record, error) {
	result := make(record.Record)
	if err := ScanRecord(result, r); err != nil {
		return nil, err
	}
	return result, nil
}

// FromRecord converts a record.Record to a *Record.
func FromRecord(r record.Record) (*Record, error) {
	// Create a map of values
	vals := make(map[string]*Value, len(r))
	// Add the values to the map
	for k, v := range r {
		val, err := convertValue(v)
		if err != nil {
			return nil, err
		}
		// Set the value
		vals[k] = val
	}
	// Return the map as a Record
	return &Record{Values: vals}, nil
}

// ToInt64 converts a *NumberOfRecords to an int64.
func ToInt64(nr *NumberOfRecords) int64 {
	return nr.N
}

// FromInt64 converts an int64 to a *NumberOfRecords.
func FromInt64(n int64) *NumberOfRecords {
	return &NumberOfRecords{N: n}
}

// ToHasRecord converts a *HasRecord to a record.Record.
func ToHasRecord(hr *HasRecord) (record.Record, error) {
	if !hr.GetHasRecord() {
		return nil, nil
	}
	r, err := ToRecord(hr.GetRecord())
	if err != nil {
		return nil, errors.RecordFailsToValidate.Wrap(err)
	}
	return r, nil
}

// FromHasRecord converts a record.Record to a *HasRecord
func FromHasRecord(r record.Record) (*HasRecord, error) {
	rr, err := FromRecord(r)
	if err != nil {
		return nil, errors.ReplacementFailsToValidate.Wrap(err)
	}
	return &HasRecord{
		HasRecord: r != nil,
		Record:    rr,
	}, nil
}

// ToConditionAndReplacement converts a *ConditionAndReplacement to a
// condition.Condition and a record.Record.
func ToConditionAndReplacement(cr *ConditionAndReplacement) (condition.Condition, record.Record, error) {
	c, err := ToCondition(cr.GetCondition())
	if err != nil {
		return nil, nil, err
	}
	r, err := ToRecord(cr.GetReplacement())
	if err != nil {
		return nil, nil, errors.ReplacementFailsToValidate.Wrap(err)
	}
	return c, r, nil
}

// FromConditionAndReplacement converts a condition.Condition and a
// record.Record to a *ConditionAndReplacement
func FromConditionAndReplacement(cond condition.Condition, replacement record.Record) (*ConditionAndReplacement, error) {
	c, err := FromCondition(cond)
	if err != nil {
		return nil, err
	}
	r, err := FromRecord(replacement)
	if err != nil {
		return nil, errors.ReplacementFailsToValidate.Wrap(err)
	}
	return &ConditionAndReplacement{
		Condition:   c,
		Replacement: r,
	}, nil
}

// ToConditionTemplateAndOrder converts a *ConditionTemplateAndOrder to a
// condition.Condition, a record.Record, and a sort.Order.
func ToConditionTemplateAndOrder(cto *ConditionTemplateAndOrder) (condition.Condition, record.Record, sort.OrderBy, error) {
	c, err := ToCondition(cto.GetCondition())
	if err != nil {
		return nil, nil, nil, err
	}
	t, err := ToRecord(cto.GetTemplate())
	if err != nil {
		return nil, nil, nil, errors.TemplateFailsToValidate.Wrap(err)
	}
	return c, t, ToOrderBy(cto.GetOrder()), nil
}

// FromConditionTemplateAndOrder converts a condition.Condition, a
// record.Record, and a sort.Order to a *ConditionTemplateAndOrder.
func FromConditionTemplateAndOrder(cond condition.Condition, template record.Record, order sort.OrderBy) (*ConditionTemplateAndOrder, error) {
	c, err := FromCondition(cond)
	if err != nil {
		return nil, err
	}
	t, err := FromRecord(template)
	if err != nil {
		return nil, errors.TemplateFailsToValidate.Wrap(err)
	}
	return &ConditionTemplateAndOrder{
		Condition: c,
		Template:  t,
		Order:     FromOrderBy(order),
	}, nil
}

// ToConditionTemplateOrderAndLimit converts a *ConditionTemplateOrderAndLimit to a
// condition.Condition, a record.Record, a sort.Order, and an int64.
func ToConditionTemplateOrderAndLimit(ctol *ConditionTemplateOrderAndLimit) (condition.Condition, record.Record, sort.OrderBy, int64, error) {
	c, err := ToCondition(ctol.GetCondition())
	if err != nil {
		return nil, nil, nil, 0, err
	}
	t, err := ToRecord(ctol.GetTemplate())
	if err != nil {
		return nil, nil, nil, 0, errors.TemplateFailsToValidate.Wrap(err)
	}
	return c, t, ToOrderBy(ctol.GetOrder()), ctol.GetLimit(), nil
}

// FromConditionTemplateOrderAndLimit converts a condition.Condition, a
// record.Record, a sort.Order, and an int64 to a *ConditionTemplateOrderAndLimit.
func FromConditionTemplateOrderAndLimit(cond condition.Condition, template record.Record, order sort.OrderBy, limit int64) (*ConditionTemplateOrderAndLimit, error) {
	c, err := FromCondition(cond)
	if err != nil {
		return nil, err
	}
	t, err := FromRecord(template)
	if err != nil {
		return nil, errors.TemplateFailsToValidate.Wrap(err)
	}
	return &ConditionTemplateOrderAndLimit{
		Condition: c,
		Template:  t,
		Order:     FromOrderBy(order),
		Limit:     limit,
	}, nil
}

// ToDatabaseNames converts a *DatabaseNames to a slice of strings.
func ToDatabaseNames(dn *DatabaseNames) ([]string, error) {
	if dn == nil {
		return nil, fmt.Errorf("nil *DatabaseNames in ToDatabaseNames")
	}
	return dn.GetName(), nil
}

// ToTableNames converts a *TableNames to a slice of strings.
func ToTableNames(tn *TableNames) ([]string, error) {
	if tn == nil {
		return nil, fmt.Errorf("nil *TableNames in ToTableNames")
	}
	return tn.GetName(), nil
}

// ToName converts a *Name to a string.
func ToName(n *Name) string {
	return n.GetName()
}

// FromName converts a string to a *Name.
func FromName(name string) *Name {
	return &Name{
		Name: name,
	}
}

// ToNameAndTemplate converts a *NameAndTemplate to a string and a record.Record.
func ToNameAndTemplate(nt *NameAndTemplate) (string, record.Record, error) {
	t, err := ToRecord(nt.GetTemplate())
	if err != nil {
		return "", nil, err
	}
	return nt.GetName(), t, nil
}

// FromNameAndTemplate converts a string and a record.Record to a *NameAndTemplate.
func FromNameAndTemplate(name string, template record.Record) (*NameAndTemplate, error) {
	t, err := FromRecord(template)
	if err != nil {
		return nil, err
	}
	return &NameAndTemplate{
		Name:     name,
		Template: t,
	}, nil
}

// FromIndex converts a string to a *Index.
func FromIndex(index string) *Index {
	return &Index{
		Index: index,
	}
}

// ToIndex converts a *Index to a string.
func ToIndex(idx *Index) string {
	return idx.GetIndex()
}

// FromIndices converts a slice of strings to a *IndexList.
func FromIndices(indices []string) *IndexList {
	S := make([]*Index, 0, len(indices))
	for _, idx := range indices {
		S = append(S, FromIndex(idx))
	}
	return &IndexList{
		List: S,
	}
}

// ToIndices converts a *IndexList to a slice of strings.
func ToIndices(il *IndexList) []string {
	indices := il.GetList()
	S := make([]string, 0, len(indices))
	for _, idx := range indices {
		S = append(S, ToIndex(idx))
	}
	return S
}

// FromKey converts a string to a *Key.
func FromKey(key string) *Key {
	return &Key{
		Key: key,
	}
}

// ToKey converts a *Key to a string.
func ToKey(key *Key) string {
	return key.GetKey()
}

// FromKeys converts a slice of strings to a *KeyList.
func FromKeys(keys []string) *KeyList {
	S := make([]*Key, 0, len(keys))
	for _, key := range keys {
		S = append(S, FromKey(key))
	}
	return &KeyList{
		List: S,
	}
}

// ToKeys converts a *KeyList to a slice of strings.
func ToKeys(kl *KeyList) []string {
	keys := kl.GetList()
	S := make([]string, 0, len(keys))
	for _, key := range keys {
		S = append(S, ToKey(key))
	}
	return S
}

// FromSrcAndDst converts a pair of strings to a *SrcAndDstNames.
func FromSrcAndDst(src string, dst string) *SrcAndDstNames {
	return &SrcAndDstNames{
		Src: src,
		Dst: dst,
	}
}

// ToSrcAndDst converts a *SrcAndDstNames to a pair of strings.
func ToSrcAndDst(sd *SrcAndDstNames) (string, string) {
	return sd.GetSrc(), sd.GetDst()
}
