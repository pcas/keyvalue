// sort.go handles conversion of sort orders.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdbrpc

import (
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcastools/fatal"
)

var (
	ascending  = &Direction{Direction: false}
	descending = &Direction{Direction: true}
)

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// FromDirection converts a sort.Direction to a *Direction
func FromDirection(d sort.Direction) *Direction {
	switch d {
	case sort.Ascending:
		return ascending
	case sort.Descending:
		return descending
	}
	panic(fatal.ImpossibleToGetHere()) // This should never happen
}

// ToDirection converts a *Direction to a sort.Direction
func ToDirection(d *Direction) sort.Direction {
	switch d.Direction {
	case ascending.Direction:
		return sort.Ascending
	case descending.Direction:
		return sort.Descending
	}
	panic(fatal.ImpossibleToGetHere()) // This should never happen
}

// FromOrder converts a sort.Order to a *Order.
func FromOrder(s sort.Order) *Order {
	return &Order{
		Key:       s.Key,
		Direction: FromDirection(s.Direction),
	}
}

// ToOrder converts a *Order to a sort.Order.
func ToOrder(s *Order) sort.Order {
	if s == nil {
		return sort.Order{} // This should never happen
	}
	return sort.Order{
		Key:       s.Key,
		Direction: ToDirection(s.Direction),
	}
}

// FromOrderBy converts a sort.OrderBy to a *OrderBy.
func FromOrderBy(S sort.OrderBy) *OrderBy {
	SS := make([]*Order, 0, len(S))
	for _, s := range S {
		SS = append(SS, FromOrder(s))
	}
	return &OrderBy{
		Order: SS,
	}
}

// ToOrderBy converts a *OrderBy to a sort.OrderBy.
func ToOrderBy(S *OrderBy) sort.OrderBy {
	result := make([]sort.Order, 0, len(S.Order))
	for _, s := range S.Order {
		result = append(result, ToOrder(s))
	}
	return result
}
