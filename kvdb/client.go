// Client describes the client-view of the kvdbd server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdb

import (
	"bitbucket.org/pcas/keyvalue/condition"
	kverrors "bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/kvdb/internal/kvdbrpc"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpcdialer"
	"bitbucket.org/pcastools/grpcutil/grpcs2"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
	"google.golang.org/protobuf/types/known/emptypb"
	"io"
	"net/url"
	"strconv"
	"sync"
)

// Common errors
var (
	errNilClient = errors.New("uninitialised client")
)

// client is the client-view of the server.
type client struct {
	log.BasicLogable
	io.Closer
	m      sync.RWMutex       // Controls access to the following
	client kvdbrpc.KvdbClient // The gRPC client
}

// clientIterator wraps a stream of kvdbrpc.Records as a record.Iterator.
type clientIterator struct {
	cancel   context.CancelFunc                  // Cancel the context for these results
	template record.Record                       // The template record
	stream   kvdbrpc.Kvdb_SelectWhereLimitClient // The underlying stream
	hasNext  bool                                // Did the previous call to Next succeed?
	r        record.Record                       // The next record
	err      error                               // Any error during iteration
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createDialFunc returns the dial function for the client. Assumes that the URI has scheme "tcp" or "ws", and that a port number is set.
func createDialFunc(uri string, lg log.Interface) grpc.DialOption {
	// Parse the URI
	u, err := url.ParseRequestURI(uri)
	if err != nil {
		panic("invalid URI: " + uri)
	}
	switch u.Scheme {
	case "tcp":
		// Connect via a TCP socket
		port, err := strconv.Atoi(u.Port())
		if err != nil {
			panic("invalid URI: " + uri)
		}
		return grpcdialer.TCPDialer(u.Hostname(), port, lg)
	case "ws":
		// Connect via a websocket
		return grpcdialer.WebSocketDialer(uri, lg)
	default:
		// Unknown scheme
		panic("unsupported URI scheme: " + u.Scheme)
	}
}

// validateDB validates the given database name.
func validateDB(dbName string) error {
	if len(dbName) == 0 {
		return errors.New("no database specified")
	}
	return nil
}

// validateTableName validates the given table name.
func validateTableName(tableName string) error {
	if len(tableName) == 0 {
		return errors.New("no table specified")
	}
	return nil
}

// validateDBAndTableName validates the given database name and table name.
func validateDBAndTableName(dbName string, tableName string) error {
	if err := validateDB(dbName); err != nil {
		return err
	}
	return validateTableName(tableName)
}

// validateCondition validates the given condition.
func validateCondition(cond condition.Condition) (bool, error) {
	if cond == nil {
		return true, nil
	}
	return cond.IsValid()
}

/////////////////////////////////////////////////////////////////////////
// clientIterator functions
/////////////////////////////////////////////////////////////////////////

// Close closes the iterator.
func (s *clientIterator) Close() error {
	if s.err == nil {
		s.err = io.EOF
	}
	s.cancel()
	return nil
}

// Err returns the last error encountered during iteration, if any.
func (s *clientIterator) Err() error {
	if s.err == io.EOF {
		return nil
	}
	return s.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *clientIterator) Next() bool {
	ok, err := s.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *clientIterator) NextContext(ctx context.Context) (bool, error) {
	if s.err != nil {
		return false, s.Err()
	}
	defer contextutil.NewLink(ctx, s.cancel).Stop()
	rec, err := s.stream.Recv()
	if err != nil {
		if err == io.EOF {
			serverErr := metadataToError(s.stream.Trailer())
			if serverErr != nil {
				err = serverErr
			}
		}
		s.err = grpcutil.ConvertError(err)
		return false, s.Err()
	}
	if s.r == nil {
		s.r = make(record.Record)
	}
	if err = kvdbrpc.ScanRecord(s.r, rec); err != nil {
		s.err = err
		return false, s.Err()
	} else if err = record.Convert(s.r, s.template); err != nil {
		s.err = err
		return false, s.Err()
	}
	s.hasNext = true
	return true, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *clientIterator) Scan(dest record.Record) error {
	if s.err != nil {
		if s.err == io.EOF {
			return kverrors.IterationFinished.New()
		}
		return s.Err()
	} else if !s.hasNext {
		return kverrors.IterationNotStarted.New()
	}
	record.Scan(dest, s.r)
	return nil
}

/////////////////////////////////////////////////////////////////////////
// client functions
/////////////////////////////////////////////////////////////////////////

// metadata creates the client metadata for the given data.
func (c *client) metadata(dbName string, tableName string) metadata.MD {
	md := make(map[string]string, 4)
	md[mdDBName] = dbName
	if len(tableName) != 0 {
		md[mdTableName] = tableName
	}
	return metadata.New(md)
}

// ListDatabases returns the names of the available databases.
func (c *client) ListDatabases(ctx context.Context) ([]string, error) {
	// Sanity check
	if c == nil {
		return nil, errNilClient
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return nil, kverrors.ConnectionClosed.New()
	}
	// Request the list of database names
	res, err := c.client.ListDatabases(ctx, &emptypb.Empty{})
	if err != nil {
		return nil, err
	}
	// Extract the results
	return kvdbrpc.ToDatabaseNames(res)
}

// ListTables returns the names of the tables in the database.
func (c *client) ListTables(ctx context.Context, dbName string) ([]string, error) {
	// Sanity check
	if c == nil {
		return nil, errNilClient
	} else if err := validateDB(dbName); err != nil {
		return nil, err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return nil, kverrors.ConnectionClosed.New()
	}
	// Request the list of table names
	res, err := c.client.ListTables(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, "")),
		&emptypb.Empty{},
	)
	if err != nil {
		return nil, err
	}
	// Extract the results
	return kvdbrpc.ToTableNames(res)
}

// DeleteTable deletes the indicated table from the database. Does not return an error if the table does not exist.
func (c *client) DeleteTable(ctx context.Context, dbName string, name string) error {
	// Sanity check
	if c == nil {
		return errNilClient
	} else if err := validateDBAndTableName(dbName, name); err != nil {
		return err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return kverrors.ConnectionClosed.New()
	}
	// Delete the table
	_, err := c.client.DeleteTable(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, "")),
		kvdbrpc.FromName(name),
	)
	return err
}

// RenameTable changes the name of table src in the database to dst.
func (c *client) RenameTable(ctx context.Context, dbName string, oldname string, newname string) error {
	// Sanity check
	if c == nil {
		return errNilClient
	} else if err := validateDBAndTableName(dbName, oldname); err != nil {
		return err
	} else if err := validateTableName(newname); err != nil {
		return err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return kverrors.ConnectionClosed.New()
	}
	// Rename the table
	_, err := c.client.RenameTable(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, "")),
		kvdbrpc.FromSrcAndDst(oldname, newname),
	)
	return err
}

// CreateTable creates a table with the given name in the database.
//
// The provided template will be used by the underlying storage-engine to create the new table if appropriate; the storage-engine is free to ignore the template if it is not required (for example, in a schema-less database).
func (c *client) CreateTable(ctx context.Context, dbName string, name string, template record.Record) error {
	// Sanity check
	if c == nil {
		return errNilClient
	} else if err := validateDBAndTableName(dbName, name); err != nil {
		return err
	}
	// Convert the name and template
	nt, err := kvdbrpc.FromNameAndTemplate(name, template)
	if err != nil {
		return err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return kverrors.ConnectionClosed.New()
	}
	// Create the table
	_, err = c.client.CreateTable(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, "")),
		nt,
	)
	return err
}

// DescribeTable returns a best-guess template for the data in table.
//
// Note that the accuracy of this template depends on the underlying storage engine. It might not be possible to return an exact description (for example, in a schema-less database), and in this case we return a good guess based on a sample of the data available.
func (c *client) DescribeTable(ctx context.Context, dbName string, tableName string) (record.Record, error) {
	// Sanity check
	if c == nil {
		return nil, errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return nil, err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return nil, kverrors.ConnectionClosed.New()
	}
	// Get the template from the client
	t, err := c.client.DescribeTable(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		&emptypb.Empty{},
	)
	if err != nil {
		return nil, err
	}
	// Extract the template and return
	return kvdbrpc.ToRecord(t)
}

// CountWhere returns the number of records in the table that satisfy the condition "cond".
func (c *client) CountWhere(ctx context.Context, dbName string, tableName string, cond condition.Condition) (int64, error) {
	// Sanity check
	if c == nil {
		return 0, errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return 0, err
	} else if ok, err := validateCondition(cond); !ok {
		return 0, kverrors.ConditionFailsToValidate.Wrap(err)
	}
	// Convert the condition
	cc, err := kvdbrpc.FromCondition(cond)
	if err != nil {
		return 0, err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return 0, kverrors.ConnectionClosed.New()
	}
	// Perform the count
	res, err := c.client.CountWhere(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		cc,
	)
	if err != nil {
		return 0, err
	}
	// Extract the results
	return kvdbrpc.ToInt64(res), nil
}

// Insert inserts the records from the given iterator into the table.
func (c *client) Insert(ctx context.Context, dbName string, tableName string, itr record.Iterator) (err error) {
	// Sanity check
	if c == nil {
		err = errNilClient
		return
	} else if err = validateDBAndTableName(dbName, tableName); err != nil {
		return
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		err = kverrors.ConnectionClosed.New()
		return
	}
	// Start the insert
	var stream kvdbrpc.Kvdb_InsertClient
	if stream, err = c.client.Insert(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
	); err != nil {
		return
	}
	// Defer closing the stream and recovering any server errors
	defer func() {
		if _, closeErr := stream.CloseAndRecv(); err == nil {
			err = closeErr
		}
		serverErr := metadataToError(stream.Trailer())
		if serverErr != nil && (err == nil || err == io.EOF) {
			err = serverErr
		}
	}()
	// Iterate over the records
	r := record.Record{}
	var ok bool
	ok, err = itr.NextContext(ctx)
	for err == nil && ok {
		var rec *kvdbrpc.Record
		if err = itr.Scan(r); err != nil {
			return
		} else if ok, err = r.IsValid(); !ok {
			return
		} else if rec, err = kvdbrpc.FromRecord(r); err != nil {
			return
		} else if err = stream.Send(rec); err != nil {
			return
		}
		ok, err = itr.NextContext(ctx)
	}
	if err != nil {
		return
	}
	// Check for iteration errors
	err = itr.Err()
	return
}

// InsertRecords inserts the given records into the table.
func (c *client) InsertRecords(ctx context.Context, dbName string, tableName string, rs ...record.Record) (err error) {
	// Sanity check
	if c == nil {
		err = errNilClient
		return
	} else if err = validateDBAndTableName(dbName, tableName); err != nil {
		return
	}
	// Validate the records
	for _, r := range rs {
		var ok bool
		if ok, err = r.IsValid(); !ok {
			return
		}
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		err = kverrors.ConnectionClosed.New()
		return
	}
	// Start the insert
	var stream kvdbrpc.Kvdb_InsertClient
	if stream, err = c.client.Insert(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
	); err != nil {
		return
	}
	// Defer closing the stream and recovering any server errors
	defer func() {
		if _, closeErr := stream.CloseAndRecv(); err == nil {
			err = closeErr
		}
		serverErr := metadataToError(stream.Trailer())
		if serverErr != nil && (err == nil || err == io.EOF) {
			err = serverErr
		}
	}()
	// Write the records to the stream
	for _, r := range rs {
		var rec *kvdbrpc.Record
		if rec, err = kvdbrpc.FromRecord(r); err != nil {
			return
		} else if err = stream.Send(rec); err != nil {
			return
		}
	}
	return
}

// UpdateWhere updates all records in the table that satisfy the condition "cond" by setting the keys in "replacement" to the given values. Returns the number of records updated.
func (c *client) UpdateWhere(ctx context.Context, dbName string, tableName string, replacement record.Record, cond condition.Condition) (int64, error) {
	// Sanity check
	if c == nil {
		return 0, errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return 0, err
	} else if ok, err := validateCondition(cond); !ok {
		return 0, kverrors.ConditionFailsToValidate.Wrap(err)
	}
	// Convert the condition and replacement
	cr, err := kvdbrpc.FromConditionAndReplacement(cond, replacement)
	if err != nil {
		return 0, err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return 0, kverrors.ConnectionClosed.New()
	}
	// Perform the update
	res, err := c.client.UpdateWhere(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		cr,
	)
	if err != nil {
		return 0, err
	}
	// Extract the results
	return kvdbrpc.ToInt64(res), nil
}

// SelectWhere returns the results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (c *client) SelectWhere(ctx context.Context, dbName string, tableName string, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Iterator, error) {
	return c.selectWhereLimit(ctx, dbName, tableName, template, cond, order, -1)
}

// SelectWhereLimit returns at most n results that satisfy the condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (c *client) SelectWhereLimit(ctx context.Context, dbName string, tableName string, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error) {
	if n < 0 {
		return nil, kverrors.LimitOutOfRange.New()
	}
	return c.selectWhereLimit(ctx, dbName, tableName, template, cond, order, n)
}

// selectWhereLimit returns at most n results that satisfy the condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template". A negative integer is used to indicate no limit on the number of records to return.
func (c *client) selectWhereLimit(ctx context.Context, dbName string, tableName string, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error) {
	// Sanity check
	if c == nil {
		return nil, errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return nil, err
	} else if ok, err := validateCondition(cond); !ok {
		return nil, kverrors.ConditionFailsToValidate.Wrap(err)
	}
	// Convert the condition, template, order, and limit
	ctol, err := kvdbrpc.FromConditionTemplateOrderAndLimit(cond, template, order, n)
	if err != nil {
		return nil, err
	}
	// Construct a new context that will last the entire iteration and link it
	// to the user's context for the duration of this call
	newCtx, cancel := context.WithCancel(context.Background())
	defer contextutil.NewLink(ctx, cancel).Stop()
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return nil, kverrors.ConnectionClosed.New()
	}
	// Perform the select
	stream, err := c.client.SelectWhereLimit(
		metadata.NewOutgoingContext(newCtx, c.metadata(dbName, tableName)),
		ctol,
	)
	if err != nil {
		cancel()
		return nil, grpcutil.ConvertError(err)
	}
	// Wrap the stream up as an iterator
	return &clientIterator{
		cancel:   cancel,
		template: template,
		stream:   stream,
	}, nil
}

// SelectOneWhere returns the first record satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned record will be in the form specified by "template". If no records match, then a nil record will be returned.
func (c *client) SelectOneWhere(ctx context.Context, dbName string, tableName string, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Record, error) {
	// Sanity check
	if c == nil {
		return nil, errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return nil, err
	} else if ok, err := validateCondition(cond); !ok {
		return nil, kverrors.ConditionFailsToValidate.Wrap(err)
	}
	// Convert the condition, template, and order
	cto, err := kvdbrpc.FromConditionTemplateAndOrder(cond, template, order)
	if err != nil {
		return nil, err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return nil, kverrors.ConnectionClosed.New()
	}
	// Perform the select one
	res, err := c.client.SelectOneWhere(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		cto,
	)
	if err != nil {
		return nil, err
	}
	// Extract the results
	return kvdbrpc.ToHasRecord(res)
}

// DeleteWhere deletes those records in the table that satisfy the condition "cond". Returns the number of records deleted.
func (c *client) DeleteWhere(ctx context.Context, dbName string, tableName string, cond condition.Condition) (int64, error) {
	// Sanity check
	if c == nil {
		return 0, errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return 0, err
	} else if ok, err := validateCondition(cond); !ok {
		return 0, kverrors.ConditionFailsToValidate.Wrap(err)
	}
	// Convert the condition
	cc, err := kvdbrpc.FromCondition(cond)
	if err != nil {
		return 0, err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return 0, kverrors.ConnectionClosed.New()
	}
	// Perform the delete
	res, err := c.client.DeleteWhere(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		cc,
	)
	if err != nil {
		return 0, err
	}
	// Extract the results
	return kvdbrpc.ToInt64(res), nil
}

// AddIndex adds an index on the given key. If an index already exists, this index is unmodified and nil is returned.
func (c *client) AddIndex(ctx context.Context, dbName string, tableName string, key string) error {
	// Sanity check
	if c == nil {
		return errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return err
	} else if !record.IsKey(key) {
		return kverrors.InvalidKey.New()
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return kverrors.ConnectionClosed.New()
	}
	// Add the index
	_, err := c.client.AddIndex(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		kvdbrpc.FromIndex(key),
	)
	if err != nil {
		return err
	}
	return nil
}

// DeleteIndex deletes the index on the given key. If no index is present, nil is returned.
func (c *client) DeleteIndex(ctx context.Context, dbName string, tableName string, key string) error {
	// Sanity check
	if c == nil {
		return errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return err
	} else if !record.IsKey(key) {
		return kverrors.InvalidKey.New()
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return kverrors.ConnectionClosed.New()
	}
	// Delete the index
	_, err := c.client.DeleteIndex(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		kvdbrpc.FromIndex(key),
	)
	if err != nil {
		return err
	}
	return nil
}

// ListIndices lists the keys for which indices are present.
func (c *client) ListIndices(ctx context.Context, dbName string, tableName string) ([]string, error) {
	// Sanity check
	if c == nil {
		return nil, errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return nil, err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return nil, kverrors.ConnectionClosed.New()
	}
	// Add the index
	result, err := c.client.ListIndices(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		&emptypb.Empty{},
	)
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return kvdbrpc.ToIndices(result), nil
}

// AddKeys updates each record r in the table, adding any keys in rec that are not already present along with the corresponding values. Any keys that are already present in r will be left unmodified.
func (c *client) AddKeys(ctx context.Context, dbName string, tableName string, rec record.Record) error {
	// Sanity check
	if c == nil {
		return errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return err
	} else if ok, err := rec.IsValid(); !ok {
		return err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return kverrors.ConnectionClosed.New()
	}
	// Convert the keys and values
	r, err := kvdbrpc.FromRecord(rec)
	if err != nil {
		return err
	}
	// Update the records
	_, err = c.client.AddKeys(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		r,
	)
	if err != nil {
		return err
	}
	return nil
}

// DeleteKeys updates all records in the table, deleting all the specified keys if present.
func (c *client) DeleteKeys(ctx context.Context, dbName string, tableName string, keys []string) error {
	// Sanity checks
	if c == nil {
		return errNilClient
	} else if err := validateDBAndTableName(dbName, tableName); err != nil {
		return err
	}
	for _, k := range keys {
		if !record.IsKey(k) {
			return kverrors.InvalidKey.New()
		}
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return kverrors.ConnectionClosed.New()
	}
	// Delete the keys
	_, err := c.client.DeleteKeys(
		metadata.NewOutgoingContext(ctx, c.metadata(dbName, tableName)),
		kvdbrpc.FromKeys(keys),
	)
	if err != nil {
		return err
	}
	return nil
}

// newClient returns a new client view of the server specified by the configuration cfg.
func newClient(ctx context.Context, cfg *ClientConfig) (*client, error) {
	// If necessary fetch the default config
	if cfg == nil {
		cfg = DefaultConfig()
	}
	// Validate the config
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Create a new client
	c := &client{}
	// Set the dial options
	dialOptions := []grpc.DialOption{
		createDialFunc(cfg.URI(), c.Log()),
		grpc.WithDefaultCallOptions(
			grpc.UseCompressor(grpcs2.Name),
			grpc.MaxCallRecvMsgSize(cfg.MaxMessageSize),
			grpc.MaxCallSendMsgSize(cfg.MaxMessageSize),
		),
		grpc.WithChainUnaryInterceptor(
			unaryClientErrorInterceptor,
		),
	}
	// Add the transport credentials
	var creds credentials.TransportCredentials
	if cfg.SSLDisabled {
		creds = insecure.NewCredentials()
	} else {
		creds = grpcutil.NewClientTLS(cfg.SSLCert)
	}
	dialOptions = append(dialOptions, grpc.WithTransportCredentials(creds))
	// Build a connection to the gRPC server
	conn, err := grpc.DialContext(ctx, cfg.Address.Hostname(), dialOptions...)
	if err != nil {
		return nil, err
	}
	// Save the connection on the client and return
	c.Closer = conn
	c.client = kvdbrpc.NewKvdbClient(conn)
	return c, nil
}

// CreateDatabase creates a database with the given name.
func (c *client) CreateDatabase(ctx context.Context, dbName string) error {
	// Sanity check
	if c == nil {
		return errNilClient
	} else if err := validateDB(dbName); err != nil {
		return err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return kverrors.ConnectionClosed.New()
	}
	// Create the database
	_, err := c.client.CreateDatabase(metadata.NewOutgoingContext(ctx, c.metadata(dbName, "")), &emptypb.Empty{})
	return err
}

// DeleteDatabase deletes a database with the given name.
func (c *client) DeleteDatabase(ctx context.Context, dbName string) error {
	// Sanity check
	if c == nil {
		return errNilClient
	} else if err := validateDB(dbName); err != nil {
		return err
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.client == nil {
		return kverrors.ConnectionClosed.New()
	}
	// Create the database
	_, err := c.client.DeleteDatabase(metadata.NewOutgoingContext(ctx, c.metadata(dbName, "")), &emptypb.Empty{})
	return err
}
