// Tests for reader.go

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue/record"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"testing"
)

// TSData stands for Test with String Data
type TSData struct{ k1, v1, k2, v2, k3, v3 string }

// TSIData stands for Test with String and Integer Data
type TSIData struct {
	k1, v1, k2 string
	v2         int
}

func fillRandomTS(s string, w []TSData) string {
	for i := 0; i < len(w); i++ {
		s += fmt.Sprintf("foo1: %d\nfoo2: bar%d\n\n", i, 2*i)
		// add a random number of empty lines
		for j := 0; j < rand.Intn(5); j++ {
			s += "\n"
		}
		w[i].k1 = "foo1"
		w[i].v1 = fmt.Sprintf("%d", i)
		w[i].k2 = "foo2"
		w[i].v2 = fmt.Sprintf("bar%d", 2*i)
	}
	return s
}

func fillRandomTSI(s string, w []TSIData) string {
	for i := 0; i < len(w); i++ {
		s += fmt.Sprintf("foo1: bar%d\nfoo2: %d\n\n", i, 2*i)
		// add a random number of empty lines
		for j := 0; j < rand.Intn(5); j++ {
			s += "\n"
		}
		w[i].k1 = "foo1"
		w[i].v1 = fmt.Sprintf("bar%d", i)
		w[i].k2 = "foo2"
		w[i].v2 = 2 * i
	}
	return s
}

func stringToTempFile(t *testing.T, s string) *os.File {
	file, err := ioutil.TempFile("", "")
	fname := file.Name()
	if err != nil {
		t.Errorf("cannot open temporary file: %s\n", fname)
	}

	err = ioutil.WriteFile(fname, []byte(s), 0644)
	if err != nil {
		t.Errorf("cannot write to file: %s\n", fname)
	}

	file.Close()
	defer os.Remove(fname)

	file, err = os.Open(fname)
	if err != nil {
		t.Errorf("cannot open temporary file: %s\n", fname)
	}

	return file
}

func TestRecord(t *testing.T) {
	input := ""
	var wantTSI [10]TSIData

	input = fillRandomTSI(input, wantTSI[:])

	file := stringToTempFile(t, input)

	r, err := NewReader(file, ReaderOptions{
		Template: record.Record{"foo1": "", "foo2": int(0)},
	})
	if err != nil {
		t.Errorf("error creating reader: %s\n", err)
		return
	}

	output := make(record.Record)
	var j int
	enteredLoop := false

	for r.Next() {
		enteredLoop = true
		if err := r.Scan(output); err != nil {
			t.Errorf("error on scan: %s\n", err)
			return
		}
		if len(output) == 0 {
			t.Errorf("read empty record: %v\n", output)
		}
		for key, val := range output {
			t.Logf("%s: %v", key, val)
			if key == wantTSI[j].k1 && val != wantTSI[j].v1 {
				t.Errorf("read: (%s, %s)\n does not match (%s, %s)\n", key, val, wantTSI[j].k1, wantTSI[j].v1)
			} else if key == wantTSI[j].k2 && val != wantTSI[j].v2 {
				t.Errorf("read: (%s, %d)\n does not match (%s, %d)\n", key, val, wantTSI[j].k2, wantTSI[j].v2)
			} else if key != wantTSI[j].k1 && key != wantTSI[j].k2 {
				t.Errorf("read Key: %s\n does not match Keys: [%s, %s]\n", key, wantTSI[j].k1, wantTSI[j].k2)
			}
		}
		j++
	}
	if err := r.Err(); err != nil {
		t.Errorf("iterator error: %s\n", err)
	}
	if !enteredLoop {
		t.Errorf("did not read file: %s\n", input)
	}

	r.Close()
	file.Close()

	// only one key
	input = ""
	var wantTS [10]TSData

	input = fillRandomTS(input, wantTS[:])

	file = stringToTempFile(t, input)

	r, err = NewReader(file, ReaderOptions{
		Template:          record.Record{"foo2": ""},
		IgnoreUnknownKeys: true,
	})
	if err != nil {
		t.Errorf("error creating reader: %s\n", err)
		return
	}

	j = 0
	enteredLoop = false

	for r.Next() {
		enteredLoop = true
		if err := r.Scan(output); err != nil {
			t.Errorf("error on scan: %s\n", err)
			return
		}
		if len(output) == 0 {
			t.Errorf("read empty record: %v\n", output)
		}
		for key, val := range output {
			t.Logf("%s: %s", key, val)
			if key != wantTS[j].k2 || val != wantTS[j].v2 {
				t.Errorf("key-value pair: (%s, %s)\ndoes not match: (%s, %s)\n", wantTS[j].k2, wantTS[j].v2, key, val)
			}
		}
		j++
	}

	if !enteredLoop {
		t.Errorf("did not read file: %s\nIterator error: %s\n", input, r.Err())
	}

	r.Close()
	file.Close()

	// test with a nonuniform key pattern
	input = ""
	wantTS11 := [11]TSData{}

	input += "foo1: 27017\nfoo2: bar27017\nfoo3: 28017\n\n"
	input = fillRandomTS(input, wantTS11[1:])
	wantTS11[0] = TSData{k1: "foo1", v1: "27017", k2: "foo2", v2: "bar27017", k3: "foo3", v3: "28017"}

	file = stringToTempFile(t, input)

	r, err = NewReader(file, ReaderOptions{
		AllowUnknownKeys: true,
	})
	if err != nil {
		t.Errorf("error creating reader: %s\n", err)
		return
	}

	j = 0
	enteredLoop = false

	for r.Next() {
		enteredLoop = true
		if err := r.Scan(output); err != nil {
			t.Errorf("error on scan: %s\n", err)
			return
		}
		if len(output) == 0 {
			t.Errorf("read empty record: %v\n", output)
		}
		for key, val := range output {
			t.Logf("%s: %s", key, val)
			switch key {
			case wantTS11[j].k1:
				if val != wantTS11[j].v1 {
					t.Errorf("key-value pair: (%s, %s)\ndoes not match: (%s, %s)\n", key, val, wantTS11[j].k1, wantTS11[j].v1)
				}
			case wantTS11[j].k2:
				if val != wantTS11[j].v2 {
					t.Errorf("key-value pair: (%s, %s)\ndoes not match: (%s, %s)\n", key, val, wantTS11[j].k2, wantTS11[j].v2)
				}
			case wantTS11[j].k3:
				if val != wantTS11[j].v3 {
					t.Errorf("key-value pair: (%s, %s)\ndoes not match: (%s, %s)\n", key, val, wantTS11[j].k3, wantTS11[j].v3)
				}
			}
		}
		j++
	}

	if !enteredLoop {
		t.Errorf("did not read file: %s\n", input)
	}

	r.Close()
	file.Close()
}
