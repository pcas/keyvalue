// Pcas-kvdbd implements the key-value database server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/blackhole"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/keyvalue/mongodb"
	"bitbucket.org/pcas/keyvalue/mysql"
	"bitbucket.org/pcas/keyvalue/postgres"
	"bitbucket.org/pcas/keyvalue/readonly"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/runtimemetrics"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/listenutil"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"github.com/c2h5oh/datasize"
	"net"
	"os"
	"strconv"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// openConnection opens the connection to the backend database.
func openConnection(ctx context.Context, opts *Options, lg log.Interface) (keyvalue.Connection, error) {
	// Open the connection
	var c keyvalue.Connection
	var err error
	switch {
	case opts.WithBlackhole:
		lg.Printf("Blackhole is enabled")
		c = blackhole.Open()
	case opts.WithMongo:
		lg.Printf("MongoDB is enabled")
		c, err = mongodb.Open(ctx, nil)
	case opts.WithMysql:
		lg.Printf("MySQL is enabled")
		c, err = mysql.Open(ctx, nil)
	case opts.WithPostgres:
		lg.Printf("PostgreSQL is enabled")
		c, err = postgres.Open(ctx, nil)
	default:
		err = errors.New("no database backend specified")
	}
	// Handle any errors
	if err != nil {
		return nil, err
	}
	// If necessary, force the connection to be read-only
	if opts.ReadOnly {
		lg.Printf("Connection is read-only")
		c = readonly.Connection(c)
	}
	return c, nil
}

// createTCPListener returns a new TCP listener.
func createTCPListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := kvdb.DefaultTCPPort
	if a.HasPort() {
		port = a.Port()
	}
	return listenutil.TCPListener(a.Hostname(), port,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
}

// createWebsocketListener returns a new websocket listener.
func createWebsocketListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := kvdb.DefaultWSPort
	if a.HasPort() {
		port = a.Port()
	}
	uri := "ws://" + a.Hostname() + ":" + strconv.Itoa(port) + a.EscapedPath()
	l, shutdown, err := listenutil.WebsocketListenAndServe(uri,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
	if err != nil {
		return nil, err
	}
	cleanup.Add(shutdown)
	return l, nil
}

// createListener returns a new listener.
func createListener(opts *Options, lg log.Interface) (l net.Listener, err error) {
	switch opts.Address.Scheme() {
	case "tcp":
		l, err = createTCPListener(opts.Address, opts, lg)
	case "ws":
		l, err = createWebsocketListener(opts.Address, opts, lg)
	default:
		err = errors.New("unsupported URI scheme: " + opts.Address.Scheme())
	}
	if err != nil {
		lg.Printf("Error starting listener: %v", err)
	}
	return
}

// run starts up and runs the server. Use the given context to shutdown.
func run(ctx context.Context, opts *Options, lg log.Interface, met metrics.Interface) error {
	// Build the options
	serverOpts := []kvdb.Option{
		kvdb.MaxMessageSize(opts.MaxMessageSize),
		kvdb.ReadBufferSize(opts.ReadBufferSize),
		kvdb.WriteBufferSize(opts.WriteBufferSize),
		kvdb.InsertCapacity(opts.InsertCapacity),
	}
	if len(opts.SSLKey) == 0 {
		lg.Printf("SSL is disabled: connections to this server are not encrypted")
	} else {
		serverOpts = append(serverOpts, kvdb.SSLCertAndKey(opts.SSLKeyCert, opts.SSLKey))
	}
	// Add the endpoint connection
	conn, err := openConnection(ctx, opts, lg)
	if err != nil {
		lg.Printf("Error opening connection to database backend: %v", err)
		return err
	}
	serverOpts = append(serverOpts, kvdb.Connection(conn))
	defer conn.Close()
	// Log the server options
	lg.Printf("Max message size: %s", datasize.ByteSize(opts.MaxMessageSize))
	lg.Printf("Read buffer size: %s", datasize.ByteSize(opts.ReadBufferSize))
	lg.Printf("Write buffer size: %s", datasize.ByteSize(opts.WriteBufferSize))
	lg.Printf("Insert capacity: %d", opts.InsertCapacity)
	// Create the server
	s, err := kvdb.New(serverOpts...)
	if err != nil {
		lg.Printf("Error starting server: %v", err)
		return err
	}
	s.SetLogger(log.PrefixWith(lg, "[kvdbserver]"))
	s.SetMetrics(met)
	// If the context fires, stop the server
	go func() {
		<-ctx.Done()
		s.GracefulStop()
	}()
	// Create the listener and serve any incoming connections
	l, err := createListener(opts, lg)
	if err != nil {
		return err
	}
	defer l.Close()
	return s.Serve(l)
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main function, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Record CPU usage etc
	defer func() {
		if e := runtimemetrics.Start(metrics.Metrics()).Stop(); err == nil {
			err = e
		}
	}()
	// Recover and log any server panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Run the server
	err = run(ctx, opts, lg, metrics.Metrics())
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
