// Config.go handles configuration and logging for the pcas-kvdbd server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/keyvalue/mongodb/mongodbflag"
	"bitbucket.org/pcas/keyvalue/mysql/mysqlflag"
	"bitbucket.org/pcas/keyvalue/postgres/postgresflag"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"context"
	"errors"
	"fmt"
	"math"
	"os"
	"time"
)

// Options describes the options.
type Options struct {
	Address           *address.Address // The address to bind to
	SSLKey            []byte           // The SSL private key
	SSLKeyCert        []byte           // The SSL private key certificate
	MaxNumConnections int              // The maximum number of connection
	MaxMessageSize    int              // The maximum message size (in bytes)
	ReadBufferSize    int              // The read buffer size (in bytes)
	WriteBufferSize   int              // The write buffer size (in bytes)
	InsertCapacity    int              // The insert capacity
	ReadOnly          bool             // Make connections read-only?
	WithBlackhole     bool             // Enable Blackhole
	WithMongo         bool             // Enable MongoDB
	WithMysql         bool             // Enable MySQL
	WithPostgres      bool             // Enable PostgreSQL
}

// Name is the name of the executable.
const Name = "pcas-kvdbd"

// The default values.
const (
	DefaultHostname          = "localhost"
	DefaultPort              = kvdb.DefaultTCPPort
	DefaultMaxNumConnections = 1024
	DefaultMaxMessageSize    = kvdb.DefaultMaxMessageSize
	DefaultReadBufferSize    = kvdb.DefaultReadBufferSize
	DefaultWriteBufferSize   = kvdb.DefaultWriteBufferSize
	DefaultInsertCapacity    = kvdb.DefaultInsertCapacity
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Create the default address
	addr, err := address.NewTCP(DefaultHostname, DefaultPort)
	if err != nil {
		panic(err) // This should never happen
	}
	// Return the default options
	return &Options{
		Address:           addr,
		MaxNumConnections: DefaultMaxNumConnections,
		MaxMessageSize:    DefaultMaxMessageSize,
		ReadBufferSize:    DefaultReadBufferSize,
		WriteBufferSize:   DefaultWriteBufferSize,
		InsertCapacity:    DefaultInsertCapacity,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if opts.MaxNumConnections <= 0 || opts.MaxNumConnections > math.MaxInt32 {
		return fmt.Errorf("invalid maximum number of connections (-max-num-connections=%d)", opts.MaxNumConnections)
	} else if opts.MaxMessageSize <= 0 {
		return fmt.Errorf("invalid maximum message size (-max-message-size=%d)", opts.MaxMessageSize)
	}
	// Check that exactly one backend is selected
	n := 0
	if opts.WithBlackhole {
		n++
	}
	if opts.WithMongo {
		n++
	}
	if opts.WithMysql {
		n++
	}
	if opts.WithPostgres {
		n++
	}
	if n != 1 {
		return errors.New("exactly one of blackhole (-with-blackhole), MongoDB (-with-mongo), MySQL (-with-mysql), or PostgreSQL (-with-postgres) must be selected")
	}
	return nil
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(sslClientSet *sslflag.ClientSet, logdSet *logdflag.LogSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Note the loggers we'll use
	toStderr := logdSet.ToStderr()
	toServer := logdSet.ToServer()
	// Create the config
	c := logdSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, toServer, c, Name)
}

// setMetrics starts the metrics reporting.
func setMetrics(sslClientSet *sslflag.ClientSet, metricsdbSet *metricsdbflag.MetricsSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Create the config
	c := metricsdbSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the metrics
	return metricsdbflag.SetMetrics(ctx, c, Name)
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s is the pcas key-value database server.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		address.NewFlag("address", &opts.Address, opts.Address, "The address to bind to", "The value of the flag -address should either take the form \"hostname[:port]\" or be a web-socket URI \"ws://host/path\"."),
		flag.Int("max-num-connections", &opts.MaxNumConnections, opts.MaxNumConnections, "The maximum number of connections", ""),
		newSizeFlag("max-message-size", &opts.MaxMessageSize, "The maximum message size"),
		newSizeFlag("read-buffer-size", &opts.ReadBufferSize, "The read buffer size"),
		newSizeFlag("write-buffer-size", &opts.WriteBufferSize, "The write buffer size"),
		flag.Int("insert-capacity", &opts.InsertCapacity, opts.InsertCapacity, "The maximum number of concurrent inserts", ""),
		flag.Bool("read-only", &opts.ReadOnly, opts.ReadOnly, "Read-only connections", ""),
		flag.Bool("with-blackhole", &opts.WithBlackhole, opts.WithBlackhole, "Enable Blackhole", ""),
		flag.Bool("with-mongo", &opts.WithMongo, opts.WithMongo, "Enable MongoDB", ""),
		flag.Bool("with-mysql", &opts.WithMysql, opts.WithMysql, "Enable MySQL", ""),
		flag.Bool("with-postgres", &opts.WithPostgres, opts.WithPostgres, "Enable PostgreSQL", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the MongoDB flag set
	mongodbSet := mongodbflag.NewSet(nil)
	flag.AddSet(mongodbSet)
	// Create and add the MySQL flag set
	mysqlSet := mysqlflag.NewSet(nil)
	flag.AddSet(mysqlSet)
	// Create and add the PostgreSQL flag set
	postgresSet := postgresflag.NewSet(nil)
	flag.AddSet(postgresSet)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the metricsdb flag set
	metricsdbSet := metricsdbflag.NewMetricsSet(nil)
	flag.AddSet(metricsdbSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Create and add the standard SSL server set
	sslServerSet := &sslflag.ServerSet{}
	flag.AddSet(sslServerSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL server details
	opts.SSLKey = sslServerSet.Key()
	opts.SSLKeyCert = sslServerSet.Certificate()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Set the default MongoDB, MySQL, and PostgreSQL values
	mongodbSet.SetDefault(Name)
	mysqlSet.SetDefault()
	postgresSet.SetDefault(Name)
	// Set the loggers
	if err := setLoggers(sslClientSet, logdSet); err != nil {
		return err
	}
	// Set the metrics
	if metricsdbSet.WithMetrics() {
		return setMetrics(sslClientSet, metricsdbSet)
	}
	return nil
}
