// sizeflag provides a flag for parsing data sizes.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcastools/flag"
	"errors"
	"fmt"
	"github.com/c2h5oh/datasize"
	"strings"
)

// sizeFlag is a flag for parsing data sizes.
type sizeFlag struct {
	name        string // The flag name
	description string // The flag description
	val         *int   // The value the flag is bound to
}

/////////////////////////////////////////////////////////////////////////
// sizeFlag functions
/////////////////////////////////////////////////////////////////////////

// newSizeFlag returns a new size flag with the given name and description, bound to the given variable.
func newSizeFlag(name string, val *int, description string) flag.Flag {
	return &sizeFlag{
		name:        name,
		description: description,
		val:         val,
	}
}

// Name returns the flag name (without leading hyphen).
func (f *sizeFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *sizeFlag) Description() string {
	val := datasize.ByteSize(*f.val)
	return fmt.Sprintf("%s (default: %s)", f.description, val)
}

// Usage returns long-form usage text.
func (f *sizeFlag) Usage() string {
	return ""
}

// Parse parses the string.
func (f *sizeFlag) Parse(in string) error {
	// Convert the size
	var val datasize.ByteSize
	if err := val.UnmarshalText([]byte(strings.TrimSpace(in))); err != nil {
		return err
	} else if val < 0 {
		return errors.New("size must be non-negative")
	}
	// Check that this fits in an int
	n := int(val)
	if n < 0 || datasize.ByteSize(n) != val {
		return errors.New("size too large")
	}
	// Looks good
	*f.val = n
	return nil
}
