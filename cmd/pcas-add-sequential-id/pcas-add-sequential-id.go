// Pcas-add-sequential-id adds a sequential ID to entries in a given table in a key-value database.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"math"
	"os"
	"runtime"
	"sort"
	"sync"
	"time"
)

// iteratorBufferSize is the size of the iterator buffer.
const iteratorBufferSize = 1024

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// is_contained returns true if and only if s is contained in S. S must be sorted.
func is_contained(s string, S []string) bool {
	idx := sort.SearchStrings(S, s)
	return idx < len(S) && S[idx] == s
}

// ensureIndices creates the key seqID in table, and ensures that we have indexes on both seqID and uniqID. If such indexes do not already exist, they are created as a unique index on uniqID and a regular index on seqID.
func ensureIndices(ctx context.Context, t keyvalue.Table, seqID string, uniqID string) error {
	// Create the new key, and ensure that we have indices on everything we need
	if err := t.AddKey(ctx, seqID, int64(0)); err != nil {
		return err
	}
	// Recover and sort the indices
	idxs, err := t.ListIndices(ctx)
	if err != nil {
		return err
	}
	sort.Strings(idxs)
	// If necessary, add indices
	if !is_contained(uniqID, idxs) {
		if err = t.AddIndex(ctx, uniqID); err != nil {
			return err
		}
	}
	if !is_contained(seqID, idxs) {
		if err = t.AddIndex(ctx, seqID); err != nil {
			return err
		}
	}
	return nil
}

// run adds sequential IDs to the table specified in opts. Use the given context to shutdown.
func run(ctx context.Context, opts *Options, lg log.Interface) error {
	// Open the connection
	c, err := opts.Connection(ctx)
	if err != nil {
		return err
	}
	defer c.Close()
	// Connect to the database
	db, err := c.ConnectToDatabase(ctx, opts.DBName)
	if err != nil {
		return err
	}
	defer db.Close()
	// Connect to the table
	t, err := db.ConnectToTable(ctx, opts.TableName)
	if err != nil {
		return err
	}
	defer t.Close()
	// Grab the keys of a typical entry
	template, err := t.Describe(ctx)
	if err != nil {
		return err
	}
	keys := template.Keys()
	// Sanity checks
	if is_contained(opts.SequentialID, keys) {
		return fmt.Errorf("the key to be created (%s) already exists", opts.SequentialID)
	} else if !is_contained(opts.UniqueID, keys) {
		return fmt.Errorf("the unique identifier key (%s) was not found", opts.UniqueID)
	}
	// Perform a count
	n, err := t.Count(ctx, nil)
	if err != nil {
		return err
	}
	// Make sure we have indices on everything
	if err := ensureIndices(ctx, t, opts.SequentialID, opts.UniqueID); err != nil {
		return err
	}
	// Give some feedback
	lg.Printf("Adding sequential id (key=%s) to table %s with unique id = %s and %d entries\n", opts.SequentialID, t.Name(), opts.UniqueID, n)
	start := time.Now()
	percent := 0.0
	// Create the table iterator and wrap it in a buffer
	itr, err := t.Select(ctx, record.Record{opts.UniqueID: template[opts.UniqueID]}, nil, nil)
	if err != nil {
		return err
	}
	itr = record.NewBufferedIterator(itr, iteratorBufferSize)
	defer itr.Close()
	// Create the records channel and the error channel
	numWorkers := runtime.NumCPU()
	recC := make(chan record.Record, numWorkers)
	errC := make(chan error, numWorkers+1)
	var wg sync.WaitGroup
	wg.Add(numWorkers + 1)
	// Launch the feeder
	go func(ctx context.Context) {
		defer wg.Done()
		defer close(recC)
		// Start iterating
		id := 1
		ok, err := itr.NextContext(ctx)
		for ok && err == nil {
			// Scan the next record
			r := record.Record{}
			err = itr.Scan(r)
			if err == nil {
				// Assign the id and pass the record down the channel
				r[opts.SequentialID] = id
				recC <- r
				// Give some feedback
				f := math.Floor(100 * float64(id) / float64(n))
				if f >= percent+1.0 {
					percent = f
					lg.Printf("updated %d records in %s - %0.0f%% complete\n", id, time.Since(start).Truncate(100*time.Millisecond), percent)
				}
				// Move on
				id++
				ok, err = itr.NextContext(ctx)
			}
		}
		// Handle any errors before returning
		if itrErr := itr.Err(); err == nil {
			err = itrErr
		}
		if err != nil {
			errC <- err
		}
	}(ctx)
	// Launch the insert workers
	for i := 0; i < numWorkers; i++ {
		go func(ctx context.Context) {
			defer wg.Done()
			for r := range recC {
				// Add the sequential id
				numUpdated, err := t.Update(ctx, r, record.Record{opts.UniqueID: r[opts.UniqueID]})
				if err != nil {
					errC <- fmt.Errorf("error whilst updating: %w", err)
					return
				} else if numUpdated != 1 {
					errC <- fmt.Errorf("error whilst updating records with %s=%d: expected to update 1 record but updated %d", opts.UniqueID, r[opts.UniqueID], numUpdated)
					return
				}
			}
		}(ctx)
	}
	// Wait for the workers to shut down
	wg.Wait()
	close(errC)
	return <-errC
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main function, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Run the main function
	err = run(ctx, opts, lg)
	if err != nil {
		lg.Printf("error: %v", err)
	}
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
