// Config.go handles configuration and logging for pcas-add-sequential-id.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/keyvalue/kvdb/kvdbflag"
	"bitbucket.org/pcas/keyvalue/mongodb"
	"bitbucket.org/pcas/keyvalue/mongodb/mongodbflag"
	"bitbucket.org/pcas/keyvalue/mysql"
	"bitbucket.org/pcas/keyvalue/mysql/mysqlflag"
	"bitbucket.org/pcas/keyvalue/postgres"
	"bitbucket.org/pcas/keyvalue/postgres/postgresflag"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"
)

// ConnectionFunc connects to the required keyvalue backend.
type ConnectionFunc func(context.Context) (keyvalue.Connection, error)

// Options describes the options.
type Options struct {
	Connection   ConnectionFunc // The connection function
	DBName       string         // The database name
	TableName    string         // The table name
	SequentialID string         // The key to add
	UniqueID     string         // The key that is a unique ID for entries
}

// Name is the name of the executable.
const Name = "pcas-add-sequential-id"

// The default values for the arguments.
const (
	DefaultSequentialID = "id"
	DefaultUniqueID     = "ulid"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Return the default options structure
	return &Options{
		SequentialID: DefaultSequentialID,
		UniqueID:     DefaultUniqueID,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if opts.Connection == nil {
		return errors.New("no backend specified")
	} else if len(strings.TrimSpace(opts.SequentialID)) == 0 {
		return errors.New("the key for the sequential ID must not be empty")
	} else if len(strings.TrimSpace(opts.UniqueID)) == 0 {
		return errors.New("the key for the unique identifier must not be empty")
	}
	return nil
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(sslClientSet *sslflag.ClientSet, logdSet *logdflag.LogSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Note the loggers we'll use
	toStderr := logdSet.ToStderr()
	toServer := logdSet.ToServer()
	// Create the config
	c := logdSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, toServer, c, Name)
}

// parseRemainingArgs parses the non-flag command-line arguments, returning the connection function, database name, and table name. Assumes that flags have been parsed.
func parseRemainingArgs(kvdbSet *kvdbflag.Set, mongodbSet *mongodbflag.Set, mysqlSet *mysqlflag.Set, postgresSet *postgresflag.Set, sslClientSet *sslflag.ClientSet) (ConnectionFunc, string, string, error) {
	// Check the number of arguments
	if numArgs := flag.NArg(); numArgs < 3 {
		return nil, "", "", errors.New("you must specify a backend, database, and table")
	} else if numArgs > 3 {
		return nil, "", "", errors.New("too many arguments")
	}
	// Note the backend, database name, and table name
	backend, dbName, tableName := flag.Arg(0), flag.Arg(1), flag.Arg(2)
	// Build the connection function
	var err error
	var cf ConnectionFunc
	switch backend {
	case "kvdb":
		cfg := kvdbSet.ClientConfig()
		cfg.SSLDisabled = sslClientSet.Disabled()
		cfg.SSLCert = sslClientSet.Certificate()
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return kvdb.Open(ctx, cfg)
			}
		}
	case "mongodb":
		cfg := mongodbSet.ClientConfig()
		cfg.AppName = Name
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return mongodb.Open(ctx, cfg)
			}
		}
	case "mysql":
		cfg := mysqlSet.ClientConfig()
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return mysql.Open(ctx, cfg)
			}
		}
	case "postgres":
		cfg := postgresSet.ClientConfig()
		cfg.AppName = Name
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return postgres.Open(ctx, cfg)
			}
		}
	default:
		err = fmt.Errorf("unknown backend: %s", backend)
	}
	// Handle any errors and return
	if err != nil {
		return nil, "", "", err
	}
	return cf, dbName, tableName, nil
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf("%s adds sequential IDs to entries in a given table in a key-value database.\n\nUsage: %s [options] backend dbName tableName\n\nThe valid values for \"backend\" are \"kvdb\", \"mongodb\", \"mysql\", and \"postgres\".", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("sequential", &opts.SequentialID, opts.SequentialID, "The key to add", ""),
		flag.String("unique", &opts.UniqueID, opts.UniqueID, "A key that uniquely identifies entries in the table", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the kvdb flag set
	kvdbSet := kvdbflag.NewSet(nil)
	flag.AddSet(kvdbSet)
	// Create and add the mongodb flag set
	mongodbSet := mongodbflag.NewSet(nil)
	flag.AddSet(mongodbSet)
	// Create and add the mysql flag set
	mysqlSet := mysqlflag.NewSet(nil)
	flag.AddSet(mysqlSet)
	// Create and add the postgres flag set
	postgresSet := postgresflag.NewSet(nil)
	flag.AddSet(postgresSet)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Parse the flags
	flag.Parse()
	// Parse the non-flag command-line arguments
	var err error
	if opts.Connection, opts.DBName, opts.TableName, err = parseRemainingArgs(kvdbSet, mongodbSet, mysqlSet, postgresSet, sslClientSet); err != nil {
		return err
	}
	// Set the loggers
	if err := setLoggers(sslClientSet, logdSet); err != nil {
		return err
	}
	// Validate the options
	return validate(opts)
}
