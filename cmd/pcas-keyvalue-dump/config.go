// Config.go handles configuration and logging for pcas-keyvalue-dump.
/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/keyvalue/kvdb/kvdbflag"
	"bitbucket.org/pcas/keyvalue/mongodb"
	"bitbucket.org/pcas/keyvalue/mongodb/mongodbflag"
	"bitbucket.org/pcas/keyvalue/mysql"
	"bitbucket.org/pcas/keyvalue/mysql/mysqlflag"
	"bitbucket.org/pcas/keyvalue/postgres"
	"bitbucket.org/pcas/keyvalue/postgres/postgresflag"
	"bitbucket.org/pcas/keyvalue/readonly"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
	"unicode"
)

// ConnectionFunc connects to the required keyvalue backend.
type ConnectionFunc func(context.Context) (keyvalue.Connection, error)

// Options describes the options.
type Options struct {
	Connection  ConnectionFunc // The connection function
	DBName      string         // The database name
	TableName   string         // The table name
	OutputFile  string         // The path to the output file
	Template    record.Record  // The template for the records
	ProgressBar bool           // Display a progress bar?
	Quote       bool           // Quote all string values?

}

// Name is the name of the executable.
const Name = "pcas-keyvalue-dump"

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// headerPath returns the path to the header file.
func (opts *Options) headerPath() string {
	return opts.OutputFile + ".header"
}

// dataPath returns the path to the output data file.
func (opts *Options) dataPath() string {
	return opts.OutputFile + ".txt.gz"
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Return the default options structure
	return &Options{
		Quote: true,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	// A connection function must be set
	if opts.Connection == nil {
		return errors.New("no backend specified")
	}
	// Validate the template, if present
	for k, v := range opts.Template {
		if !record.IsKey(k) {
			return fmt.Errorf("invalid key: \"%s\"", k)
		}
		if !record.IsValue(v) {
			return fmt.Errorf("invalid value (%v) for key: \"%s\"", v, k)
		}
	}
	// Check if the output files already exist. This can also fail if the path
	// is not writeable or whatever, but we want to fail then too.
	for _, f := range []string{opts.headerPath(), opts.dataPath()} {
		if _, err := os.Stat(f); !os.IsNotExist(err) {
			return fmt.Errorf("error checking output files: the file %s may already exist", f)
		}
	}
	return nil
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(sslClientSet *sslflag.ClientSet, logdSet *logdflag.LogSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Note the loggers we'll use
	toStderr := logdSet.ToStderr()
	toServer := logdSet.ToServer()
	// Create the config
	c := logdSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, toServer, c, Name)
}

// parseRemainingArgs parses the non-flag command-line arguments, returning the connection function, database name, and table name. Assumes that flags have been parsed.
func parseRemainingArgs(kvdbSet *kvdbflag.Set, mongodbSet *mongodbflag.Set, mysqlSet *mysqlflag.Set, postgresSet *postgresflag.Set, sslClientSet *sslflag.ClientSet) (ConnectionFunc, string, string, error) {
	// Check the number of arguments
	if numArgs := flag.NArg(); numArgs < 3 {
		return nil, "", "", errors.New("you must specify a backend, database, and table")
	} else if numArgs > 3 {
		return nil, "", "", errors.New("too many arguments")
	}
	// Note the backend, database name, and table name
	backend, dbName, tableName := flag.Arg(0), flag.Arg(1), flag.Arg(2)
	// Build the connection function
	var err error
	var cf ConnectionFunc
	switch backend {
	case "kvdb":
		cfg := kvdbSet.ClientConfig()
		cfg.SSLDisabled = sslClientSet.Disabled()
		cfg.SSLCert = sslClientSet.Certificate()
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return kvdb.Open(ctx, cfg)
			}
		}
	case "mongodb":
		cfg := mongodbSet.ClientConfig()
		cfg.AppName = Name
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return mongodb.Open(ctx, cfg)
			}
		}
	case "mysql":
		cfg := mysqlSet.ClientConfig()
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return mysql.Open(ctx, cfg)
			}
		}
	case "postgres":
		cfg := postgresSet.ClientConfig()
		cfg.AppName = Name
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return postgres.Open(ctx, cfg)
			}
		}
	default:
		err = fmt.Errorf("unknown backend: %s", backend)
	}
	// Handle any errors
	if err != nil {
		return nil, "", "", err
	}
	// Ensure the connection is read-only and return
	wrappedcf := func(ctx context.Context) (keyvalue.Connection, error) {
		c, err := cf(ctx)
		if err != nil {
			return nil, err
		}
		return readonly.Connection(c), nil
	}
	return wrappedcf, dbName, tableName, nil
}

// sanitizeTableName returns a string derived from s that is safe to use as a filename. This may be empty.
func sanitizeTableName(s string) string {
	first := true
	return strings.Map(func(r rune) rune {
		if first && r == '.' {
			return -1
		}
		if (r >= 'a' && r <= 'z') || (r >= 'A' && r <= 'Z') ||
			(r >= '0' && r <= '9') || r == '_' || r == '.' || r == '-' || r == '+' {
			first = false
			return r
		}
		return -1
	}, s)
}

// undescribe takes a keyvalue record with values equal to strings that describe legal types in a keyvalue record, specifically one of:
//
//	"int"
//	"int8"
//	"int16"
//	"int32"
//	"int64"
//	"uint"
//	"uint8"
//	"uint16"
//	"uint32"
//	"uint64"
//	"float64"
//	"bool"
//	"string"
//	"[]byte"
//
// and returns a keyvalue record with the same keys and values given by the zero value for the corresponding type.
func undescribe(r record.Record) (record.Record, error) {
	result := make(record.Record, len(r))
	var vv interface{}
	for k, v := range r {
		s, ok := v.(string)
		if !ok {
			return nil, fmt.Errorf("expected a string value for %s but found %v", k, v)
		}
		switch s {
		case "int":
			vv = int(0)
		case "int8":
			vv = int8(0)
		case "int16":
			vv = int16(0)
		case "int32":
			vv = int32(0)
		case "int64":
			vv = int64(0)
		case "uint":
			vv = uint(0)
		case "uint8":
			vv = uint8(0)
		case "uint16":
			vv = uint16(0)
		case "uint32":
			vv = uint32(0)
		case "uint64":
			vv = uint64(0)
		case "float64":
			vv = float64(0)
		case "bool":
			vv = false
		case "string":
			vv = ""
		case "[]byte":
			vv = []byte{}
		default:
			return nil, fmt.Errorf("unknown type: %s", s)
		}
		result[k] = vv
	}
	return result, nil
}

// readHeader reads the header file from the specified path and returns a template keyvalue record constructed from the header.
func readHeader(path string) (record.Record, error) {
	// Open the header file
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	// Buffer the file
	r := bufio.NewReader(f)
	// Read the keys and values
	hdr := make(record.Record)
	done := false
	for err == nil && !done {
		// Read in the next line
		var l string
		l, err = r.ReadString('\n')
		// If the line is blank, we end
		if l = strings.TrimSpace(l); len(l) == 0 {
			done = true
		} else {
			// Split the line into a key and a value and store it
			S := strings.SplitN(l, ":", 2)
			if len(S) != 2 {
				return nil, fmt.Errorf("malformed header line: %s", l)
			} else {
				// Extract the key and value
				k := strings.TrimRightFunc(S[0], unicode.IsSpace)
				v := strings.TrimLeftFunc(S[1], unicode.IsSpace)
				if _, ok := hdr[k]; ok {
					return nil, fmt.Errorf("duplicate key in header file: %s", k)
				}
				hdr[k] = v
			}
		}
	}
	// We should end with a blank line
	if err == io.EOF {
		return nil, errors.New("unexpected EOF when reading header file")
	} else if err != nil {
		return nil, err
	}
	// Validate the header file
	for k, v := range hdr {
		if !record.IsKey(k) || !record.IsValue(v) {
			return nil, fmt.Errorf("malformed key-value pair in header file: (%s:%s)", k, v)
		}
	}
	// Convert the header and return
	return undescribe(hdr)
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var headerPath string
	var outputPath string
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf("%s saves the entries in a given table in a key-value database to disk.\n\nUsage: %s [options] backend dbName tableName\n\nThe valid values for \"backend\" are \"kvdb\", \"mongodb\", \"mysql\", and \"postgres\".", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("header-file", &headerPath, headerPath, "The header file", "The header file should be a key-value file with entries like \"key1: int64\" or \"key2: []byte\" that specify the keys and the types of the values in the table. If the header file is empty or omitted, the header will be inferred or estimated from the data in the table."),
		flag.String("output-file", &outputPath, outputPath, "The output file", "If the output file is empty or omitted, the name of the table will be used."),
		flag.Bool("progress", &opts.ProgressBar, opts.ProgressBar, "Display progress bar", ""),
		flag.Bool("quoted", &opts.Quote, opts.Quote, "Quote string values", "All []byte values are base64 encoded before saving."),

		&version.Flag{AppName: Name},
	)
	// Create and add the kvdb flag set
	kvdbSet := kvdbflag.NewSet(nil)
	flag.AddSet(kvdbSet)
	// Create and add the mongodb flag set
	mongodbSet := mongodbflag.NewSet(nil)
	flag.AddSet(mongodbSet)
	// Create and add the mysql flag set
	mysqlSet := mysqlflag.NewSet(nil)
	flag.AddSet(mysqlSet)
	// Create and add the postgres flag set
	postgresSet := postgresflag.NewSet(nil)
	flag.AddSet(postgresSet)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Parse the flags
	flag.Parse()
	// Parse the header
	if headerPath != "" {
		var err error
		opts.Template, err = readHeader(headerPath)
		if err != nil {
			return err
		}
	}
	// Parse the non-flag command-line arguments
	var err error
	if opts.Connection, opts.DBName, opts.TableName, err = parseRemainingArgs(kvdbSet, mongodbSet, mysqlSet, postgresSet, sslClientSet); err != nil {
		return err
	}
	// Set the output file, unless it was set already
	if outputPath == "" {
		safeName := sanitizeTableName(opts.TableName)
		if safeName == "" {
			return errors.New("unable to sanitize table name: please use the -output-file flag")
		}
		opts.OutputFile = safeName
	}
	// We cannot both log to stderr and display a progress bar
	if opts.ProgressBar && logdSet.ToStderr() {
		return errors.New("both -progress and -log-to-stderr cannot be selected")
	}
	// Set the loggers
	if err := setLoggers(sslClientSet, logdSet); err != nil {
		return err
	}
	// Validate the options
	return validate(opts)
}
