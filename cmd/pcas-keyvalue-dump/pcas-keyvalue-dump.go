// Pcas-keyvalue-dump saves records in a keyvalue table to disk. The table can be reconstructed from the resulting output files using pcas-keyvalue-restore.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/compress"
	_ "bitbucket.org/pcastools/compress/gzip"
	"bitbucket.org/pcastools/log"
	"bufio"
	"context"
	"encoding/base64"
	"fmt"
	"github.com/schollz/progressbar/v3"
	"io"
	"math"
	"os"
	"sort"
	"strconv"
	"sync"
	"time"
)

// iteratorBufferSize is the size of the iterator buffer.
const iteratorBufferSize = 1024

// bufGzipWriter represents a gzip writer backed by a buffered file
type bufGzipWriter struct {
	io.WriteCloser
	f  *os.File
	bw *bufio.Writer
}

/////////////////////////////////////////////////////////////////////////
// bufGzipWriter functions
/////////////////////////////////////////////////////////////////////////

// NewGzipWriter returns a new gzip writer backed by a buffered file with the given path.
func NewGzipWriter(path string) (*bufGzipWriter, error) {
	// Open the output file
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0644)
	if err != nil {
		return nil, err
	}
	// Wrap the output file in a buffer
	bw := bufio.NewWriter(f)
	// Create the gzip writer
	zw, err := compress.Gzip.NewWriter(bw)
	if err != nil {
		return nil, err
	}
	return &bufGzipWriter{
		WriteCloser: zw,
		f:           f,
		bw:          bw,
	}, nil
}

func (b *bufGzipWriter) Close() error {
	// Close the gzip writer
	err := b.WriteCloser.Close()
	// Flush the buffer
	if err2 := b.bw.Flush(); err == nil {
		err = err2
	}
	// Close the underlying file
	if err2 := b.f.Close(); err == nil {
		err = err2
	}
	return err
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// describe takes a keyvalue record r with keys k, v and returns a keyvalue record with keys k and values that are strings specifying the type of v.
// These are obtained by doing fmt.Sprintf("%T", v); the possible values are:
//
//	"int"
//	"int8"
//	"int16"
//	"int32"
//	"int64"
//	"uint"
//	"uint8"
//	"uint16"
//	"uint32"
//	"uint64"
//	"float64"
//	"bool"
//	"string"
//	"[]byte"
func describe(r record.Record) record.Record {
	result := make(record.Record, len(r))
	for k, v := range r {
		result[k] = fmt.Sprintf("%T", v)
	}
	return result
}

// kvWrite writes r, followed by a blank line, to w. []byte values are base64 encoded before writing. If quote is true then string values are quoted before writing.
func kvWrite(r record.Record, w io.Writer, quote bool) error {
	// Handle the trivial case
	if len(r) == 0 {
		return nil
	}
	// Sort the keys
	ks := make([]string, 0, len(r))
	for k := range r {
		ks = append(ks, k)
	}
	sort.Strings(ks)
	// Write out the keys and values
	for _, k := range ks {
		v := r[k]
		if quote {
			if s, ok := v.(string); ok {
				v = strconv.Quote(s)
			} else if b, ok := v.([]byte); ok {
				v = base64.StdEncoding.EncodeToString(b)
			}
		}
		_, err := fmt.Fprintf(w, "%s: %v\n", k, v)
		if err != nil {
			return err
		}
	}
	_, err := fmt.Fprintf(w, "\n")
	return err
}

// writeHeader writes out the header file as specified by template to the specified path.
func writeHeader(template record.Record, path string, lg log.Interface) (err error) {
	// Open the header file
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0644)
	if err != nil {
		return err
	}
	defer func() {
		err2 := f.Close()
		if err == nil {
			err = err2
		}
	}()
	// Buffer the file
	w := bufio.NewWriter(f)
	defer func() {
		err2 := w.Flush()
		if err == nil {
			err = err2
		}
	}()
	return kvWrite(describe(template), w, false)
}

// progressWorker reports progress of itr to lg. n is the total number of inserts to do. Exits when the context fires. Intended to be run in its own goroutine.
func progressWorker(ctx context.Context, wg *sync.WaitGroup, itr *record.CountIterator, n int64, lg log.Interface) {
	defer wg.Done()
	tick := time.NewTicker(10 * time.Second)
	defer tick.Stop()
	t := time.Now()
	for {
		select {
		case <-tick.C:
			count := itr.Count()
			since := time.Since(t).Truncate(100 * time.Millisecond)
			if count != 0 {
				percent := math.Floor(100 * float64(count) / float64(n))
				rate := float64(count) / (float64(time.Since(t)) / float64(time.Second))
				eta := t.Add(time.Duration(float64(n) * (float64(time.Since(t)) / float64(count))))
				lg.Printf("Wrote %d records in %s (%0.0f%% complete, %0.0f records/sec, ETA: %s)", count, since, percent, rate, eta.Format("15:04:05"))
			} else {
				lg.Printf("Wrote 0 records in %s (0%% complete, 0 records/sec)", since)
			}
		case <-ctx.Done():
			return
		}
	}
}

// progressBar updates a progress bar indicating the progress of itr. n is the total number of inserts to do. Exits when the context fires. Intended to be run in its own goroutine.
func progressBar(ctx context.Context, wg *sync.WaitGroup, itr *record.CountIterator, n int64) {
	defer wg.Done()
	bar := progressbar.NewOptions64(n,
		progressbar.OptionClearOnFinish(),
		progressbar.OptionShowCount(),
		progressbar.OptionSetPredictTime(true),
		progressbar.OptionSetTheme(progressbar.Theme{
			Saucer:        "=",
			SaucerHead:    ">",
			SaucerPadding: " ",
			BarStart:      "[",
			BarEnd:        "]",
		}),
	)
	defer bar.Close()
	tick := time.NewTicker(500 * time.Millisecond)
	defer tick.Stop()
	for {
		select {
		case <-tick.C:
			bar.Set64(itr.Count())
		case <-ctx.Done():
			bar.Set64(itr.Count())
			return
		}
	}
}

// writeRecords writes the records in itr to w. n is the total number of records.
func writeRecords(ctx context.Context, itr *record.CountIterator, w io.Writer, n int64, opts *Options, lg log.Interface) (err error) {
	// Create a context to cancel the progress workers when finished
	ctx, cancel := context.WithCancel(ctx)
	// Start the progress workers
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go progressWorker(ctx, wg, itr, n, lg)
	if opts.ProgressBar {
		wg.Add(1)
		go progressBar(ctx, wg, itr, n)
	}
	// Start iterating in a new goroutine
	go func() {
		defer cancel()
		var ok bool
		ok, err = itr.NextContext(ctx)
		for ok && err == nil {
			r := record.Record{}
			err = itr.Scan(r)
			if err == nil {
				err = kvWrite(r, w, opts.Quote)
			}
			// Move on
			if err == nil {
				ok, err = itr.NextContext(ctx)
			}
		}
	}()
	// Wait for the progress workers to exit before returning
	wg.Wait()
	return
}

// writeData writes the table as specified by opts and template to disk.
func writeData(ctx context.Context, t keyvalue.Table, template record.Record, opts *Options, lg log.Interface) (err error) {
	// Perform a count
	var n int64
	n, err = t.Count(ctx, nil)
	if err != nil {
		return
	}
	// Give some feedback
	lg.Printf("Dumping table '%s' with %d entries to file: %s", t.Name(), n, opts.dataPath())
	// Create the table iterator and wrap it in a buffer
	var itr record.Iterator
	itr, err = t.Select(ctx, template, nil, nil)
	if err != nil {
		return
	}
	itr = record.NewBufferedIterator(itr, iteratorBufferSize)
	citr := record.NewCountIterator(itr)
	defer func() {
		closeErr := citr.Close()
		if closeErr == nil {
			closeErr = citr.Err()
		}
		if err == nil {
			err = closeErr
		}
	}()
	// Create the gzip writer
	var zw io.WriteCloser
	zw, err = NewGzipWriter(opts.dataPath())
	if err != nil {
		return
	}
	defer func() {
		closeErr := zw.Close()
		if err == nil {
			err = closeErr
		}
	}()
	// Write out the records
	start := time.Now()
	err = writeRecords(ctx, citr, zw, n, opts, lg)
	if err == nil {
		lg.Printf("Wrote %d records in %s", citr.Count(), time.Since(start).Truncate(100*time.Millisecond))
	}
	return
}

// run dumps the contents of the key-value table specified in opts to disk. Use the given context to shutdown.
func run(ctx context.Context, opts *Options, lg log.Interface) error {
	// Open the connection
	c, err := opts.Connection(ctx)
	if err != nil {
		return err
	}
	defer c.Close()
	// Connect to the database
	db, err := c.ConnectToDatabase(ctx, opts.DBName)
	if err != nil {
		return err
	}
	defer db.Close()
	// Connect to the table
	t, err := db.ConnectToTable(ctx, opts.TableName)
	if err != nil {
		return err
	}
	defer t.Close()
	// Grab the format of a typical entry
	typical, err := t.Describe(ctx)
	if err != nil {
		return err
	}
	// Create the template
	var template record.Record
	if opts.Template != nil {
		template = opts.Template
	} else {
		template = typical
	}
	// Write out the header file
	if err := writeHeader(template, opts.headerPath(), lg); err != nil {
		return err
	}
	// Write out the data file
	return writeData(ctx, t, template, opts, lg)
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main function, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Run the main function
	err = run(ctx, opts, lg)
	if err != nil {
		lg.Printf("error: %v", err)
	}
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
