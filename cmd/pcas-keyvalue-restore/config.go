// Config.go handles configuration and logging for pcas-keyvalue-restore.
/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/keyvalue/kvdb/kvdbflag"
	"bitbucket.org/pcas/keyvalue/mongodb"
	"bitbucket.org/pcas/keyvalue/mongodb/mongodbflag"
	"bitbucket.org/pcas/keyvalue/mysql"
	"bitbucket.org/pcas/keyvalue/mysql/mysqlflag"
	"bitbucket.org/pcas/keyvalue/postgres"
	"bitbucket.org/pcas/keyvalue/postgres/postgresflag"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"
)

// ConnectionFunc connects to the required keyvalue backend.
type ConnectionFunc func(context.Context) (keyvalue.Connection, error)

// Options describes the options.
type Options struct {
	Connection  ConnectionFunc // The connection function
	DBName      string         // The database name
	TableName   string         // The table name
	InputFile   string         // The path to the input file, with any trailing ".header" or ".txt.gz" removed
	ProgressBar bool           // Display a progress bar?
	Quote       bool           // If true, require all string values in the input file to be quoted, and unquote them.
}

// Name is the name of the executable.
const Name = "pcas-keyvalue-restore"

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// headerPath returns the path to the header file.
func (opts *Options) headerPath() string {
	return opts.InputFile + ".header"
}

// dataPath returns the path to the output data file.
func (opts *Options) dataPath() string {
	return opts.InputFile + ".txt.gz"
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Return the default options structure
	return &Options{
		Quote: true,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	// A connection function must be set
	if opts.Connection == nil {
		return errors.New("no backend specified")
	}
	// Check if the input files exist
	for _, f := range []string{opts.headerPath(), opts.dataPath()} {
		if _, err := os.Stat(f); err != nil {
			return err
		}
	}
	return nil
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(sslClientSet *sslflag.ClientSet, logdSet *logdflag.LogSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Note the loggers we'll use
	toStderr := logdSet.ToStderr()
	toServer := logdSet.ToServer()
	// Create the config
	c := logdSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, toServer, c, Name)
}

// parseRemainingArgs parses the non-flag command-line arguments, returning the connection function, database name, table name, and file name. Assumes that flags have been parsed.
func parseRemainingArgs(kvdbSet *kvdbflag.Set, mongodbSet *mongodbflag.Set, mysqlSet *mysqlflag.Set, postgresSet *postgresflag.Set, sslClientSet *sslflag.ClientSet) (ConnectionFunc, string, string, string, error) {
	// Check the number of arguments
	if numArgs := flag.NArg(); numArgs < 4 {
		return nil, "", "", "", errors.New("you must specify a backend, database, table, and input file")
	} else if numArgs > 4 {
		return nil, "", "", "", errors.New("too many arguments")
	}
	// Note the backend, database name, table name, and file name
	backend, dbName, tableName, fileName := flag.Arg(0), flag.Arg(1), flag.Arg(2), flag.Arg(3)
	// Build the connection function
	var err error
	var cf ConnectionFunc
	switch backend {
	case "kvdb":
		cfg := kvdbSet.ClientConfig()
		cfg.SSLDisabled = sslClientSet.Disabled()
		cfg.SSLCert = sslClientSet.Certificate()
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return kvdb.Open(ctx, cfg)
			}
		}
	case "mongodb":
		cfg := mongodbSet.ClientConfig()
		cfg.AppName = Name
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return mongodb.Open(ctx, cfg)
			}
		}
	case "mysql":
		cfg := mysqlSet.ClientConfig()
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return mysql.Open(ctx, cfg)
			}
		}
	case "postgres":
		cfg := postgresSet.ClientConfig()
		cfg.AppName = Name
		if err = cfg.Validate(); err == nil {
			cf = func(ctx context.Context) (keyvalue.Connection, error) {
				return postgres.Open(ctx, cfg)
			}
		}
	default:
		err = fmt.Errorf("unknown backend: %s", backend)
	}
	// Handle any errors
	if err != nil {
		return nil, "", "", "", err
	}
	return cf, dbName, tableName, fileName, nil
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf("%s reconstructs a table in a keyvalue database from files produced by pcas-keyvalue-dump.\n\nUsage: %s [options] backend dbName tableName fileName\n\nThe valid values for \"backend\" are \"kvdb\", \"mongodb\", \"mysql\", and \"postgres\".", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Bool("progress", &opts.ProgressBar, opts.ProgressBar, "Display progress bar", ""),
		flag.Bool("quoted", &opts.Quote, opts.Quote, "Expect string values to be quoted", "If the flag -quoted is supplied, string values in the input should be quoted, and are unquoted when the table is restored.\n\nAll []byte values in the input should be base64 encoded."),
		&version.Flag{AppName: Name},
	)
	// Create and add the kvdb flag set
	kvdbSet := kvdbflag.NewSet(nil)
	flag.AddSet(kvdbSet)
	// Create and add the mongodb flag set
	mongodbSet := mongodbflag.NewSet(nil)
	flag.AddSet(mongodbSet)
	// Create and add the mysql flag set
	mysqlSet := mysqlflag.NewSet(nil)
	flag.AddSet(mysqlSet)
	// Create and add the postgres flag set
	postgresSet := postgresflag.NewSet(nil)
	flag.AddSet(postgresSet)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Parse the flags
	flag.Parse()
	// Parse the non-flag command-line arguments
	var err error
	var fileName string
	if opts.Connection, opts.DBName, opts.TableName, fileName, err = parseRemainingArgs(kvdbSet, mongodbSet, mysqlSet, postgresSet, sslClientSet); err != nil {
		return err
	}
	// Normalise the input file name
	if strings.HasSuffix(fileName, ".header") {
		fileName = strings.TrimSuffix(fileName, ".header")
	} else if strings.HasSuffix(fileName, ".txt.gz") {
		fileName = strings.TrimSuffix(fileName, ".txt.gz")
	}
	opts.InputFile = fileName
	// We cannot both log to stderr and display a progress bar
	if opts.ProgressBar && logdSet.ToStderr() {
		return errors.New("both -progress and -log-to-stderr cannot be selected")
	}
	// Set the loggers
	if err := setLoggers(sslClientSet, logdSet); err != nil {
		return err
	}
	// Validate the options
	return validate(opts)
}
