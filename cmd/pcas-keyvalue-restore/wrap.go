// Wrap wraps a key-value iterator, base64-decoding []byte values in the output and optionally unquoting string values in the output.

package main

import (
	"encoding/base64"
	"fmt"

	"bitbucket.org/pcas/keyvalue/record"
)

// witr wraps a record.Iterator and trims string values
type witr struct {
	record.Iterator
	mustTrim   map[string]bool // the keys that have string values
	mustDecode map[string]bool // the keys that have []byte values
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (w *witr) Scan(dest record.Record) error {
	x := make(record.Record)
	// Zero the destination
	for k := range dest {
		delete(dest, k)
	}
	// Read the current value
	err := w.Iterator.Scan(x)
	if err != nil {
		return err
	}
	// Decode or trim if necessary
	for k, v := range x {
		if w.mustTrim[k] {
			s := v.(string)
			if len(s) < 2 || s[0] != '"' || s[len(s)-1] != '"' {
				return fmt.Errorf("expected a quoted string value for '%s' but found '%s'", k, s)
			}
			dest[k] = s[1 : len(s)-1]
		} else if w.mustDecode[k] {
			s, ok := v.(string)
			if !ok {
				return fmt.Errorf("expected base64-encoded string for '%s' but found '%v'", k, v)
			}
			b, err := base64.StdEncoding.DecodeString(s)
			if err != nil {
				return fmt.Errorf("expected base64-encoded string for '%s' but got error in decoding: %v", k, err)
			}
			dest[k] = b
		} else {
			dest[k] = v
		}
	}
	return nil
}

// wrap wraps itr, base64 decoding []byte values. If unquote is true then in addition string values are unquoted. Here template is a template for records in itr.
func wrap(itr record.Iterator, template record.Record, unquote bool) record.Iterator {
	// Record the keys to process
	mustDecode := make(map[string]bool, len(template))
	var mustTrim map[string]bool
	if unquote {
		mustTrim = make(map[string]bool, len(template))
	}
	for k, v := range template {
		if _, ok := v.(string); unquote && ok {
			mustTrim[k] = true
		} else if _, ok := v.([]byte); ok {
			mustDecode[k] = true
		}
	}

	return &witr{
		Iterator:   itr,
		mustTrim:   mustTrim,
		mustDecode: mustDecode,
	}
}
