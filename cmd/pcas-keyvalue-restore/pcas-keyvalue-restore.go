// Pcas-keyvalue-restore reconstructs a table in a keyvalue database from files produced by pcas-keyvalue-dump.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	kverrors "bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/compress"
	_ "bitbucket.org/pcastools/compress/gzip"
	"bitbucket.org/pcastools/log"
	"bufio"
	"context"
	"errors"
	"fmt"
	"github.com/schollz/progressbar/v3"
	"io"
	"os"
	"strings"
	"sync"
	"time"
	"unicode"
)

// iteratorBufferSize is the size of the iterator buffer.
const iteratorBufferSize = 1024

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isContained returns true if and only if s is contained in S.
func isContained(s string, S []string) bool {
	for _, t := range S {
		if s == t {
			return true
		}
	}
	return false
}

// undescribe takes a keyvalue record with values equal to strings that describe legal types in a keyvalue record, specifically one of:
//
//	"int"
//	"int8"
//	"int16"
//	"int32"
//	"int64"
//	"uint"
//	"uint8"
//	"uint16"
//	"uint32"
//	"uint64"
//	"float64"
//	"bool"
//	"string"
//	"[]byte"
//
// and returns a keyvalue record with the same keys and values given by the zero value for the corresponding type.
func undescribe(r record.Record) (record.Record, error) {
	result := make(record.Record, len(r))
	var vv interface{}
	for k, v := range r {
		s, ok := v.(string)
		if !ok {
			return nil, fmt.Errorf("expected a string value for %s but found %v", k, v)
		}
		switch s {
		case "int":
			vv = int(0)
		case "int8":
			vv = int8(0)
		case "int16":
			vv = int16(0)
		case "int32":
			vv = int32(0)
		case "int64":
			vv = int64(0)
		case "uint":
			vv = uint(0)
		case "uint8":
			vv = uint8(0)
		case "uint16":
			vv = uint16(0)
		case "uint32":
			vv = uint32(0)
		case "uint64":
			vv = uint64(0)
		case "float64":
			vv = float64(0)
		case "bool":
			vv = false
		case "string":
			vv = ""
		case "[]byte":
			vv = []byte{}
		default:
			return nil, fmt.Errorf("unknown type: %s", s)
		}
		result[k] = vv
	}
	return result, nil
}

// readHeader reads the header file from the specified path and returns a template keyvalue record constructed from the header.
func readHeader(path string) (record.Record, error) {
	// Open the header file
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	// Buffer the file
	r := bufio.NewReader(f)
	// Read the keys and values
	hdr := make(record.Record)
	done := false
	for err == nil && !done {
		// Read in the next line
		var l string
		l, err = r.ReadString('\n')
		// If the line is blank, we end
		if l = strings.TrimSpace(l); len(l) == 0 {
			done = true
		} else {
			// Split the line into a key and a value and store it
			S := strings.SplitN(l, ":", 2)
			if len(S) != 2 {
				return nil, fmt.Errorf("malformed header line: %s", l)
			} else {
				// Extract the key and value
				k := strings.TrimRightFunc(S[0], unicode.IsSpace)
				v := strings.TrimLeftFunc(S[1], unicode.IsSpace)
				if _, ok := hdr[k]; ok {
					return nil, fmt.Errorf("duplicate key in header file: %s", k)
				}
				hdr[k] = v
			}
		}
	}
	// We should end with a blank line
	if err == io.EOF {
		return nil, errors.New("unexpected EOF when reading header file")
	} else if err != nil {
		return nil, err
	}
	// Validate the header file
	for k, v := range hdr {
		if !record.IsKey(k) || !record.IsValue(v) {
			return nil, fmt.Errorf("malformed key-value pair in header file: (%s:%s)", k, v)
		}
	}
	// Convert the header and return
	return undescribe(hdr)
}

// progressWorker reports progress of itr to lg. Exits when the context fires. Intended to be run in its own goroutine.
func progressWorker(ctx context.Context, wg *sync.WaitGroup, itr *record.CountIterator, lg log.Interface) {
	defer wg.Done()
	tick := time.NewTicker(10 * time.Second)
	defer tick.Stop()
	t := time.Now()
	for {
		select {
		case <-tick.C:
			count := itr.Count()
			since := time.Since(t).Truncate(100 * time.Millisecond)
			if count != 0 {
				rate := float64(count) / (float64(time.Since(t)) / float64(time.Second))
				lg.Printf("Inserted %d records in %s (%0.0f records/sec)", count, since, rate)
			} else {
				lg.Printf("Inserted 0 records in %s (0 records/sec)", since)
			}
		case <-ctx.Done():
			return
		}
	}
}

// progressBar updates a progress bar indicating the progress of itr. Exits when the context fires. Intended to be run in its own goroutine.
func progressBar(ctx context.Context, wg *sync.WaitGroup, itr *record.CountIterator) {
	defer wg.Done()
	bar := progressbar.NewOptions64(-1,
		progressbar.OptionShowCount(),
		progressbar.OptionClearOnFinish(),
		progressbar.OptionSetItsString("rec"),
		progressbar.OptionShowIts(),
	)
	defer bar.Close()
	tick := time.NewTicker(500 * time.Millisecond)
	defer tick.Stop()
	for {
		select {
		case <-tick.C:
			bar.Set64(itr.Count())
		case <-ctx.Done():
			bar.Set64(itr.Count())
			return
		}
	}
}

// insertData inserts the records in itr into t.
func insertData(ctx context.Context, itr *record.CountIterator, t keyvalue.Table, opts *Options, lg log.Interface) (err error) {
	// Create a context to cancel the progress workers when finished
	ctx, cancel := context.WithCancel(ctx)
	// Start the progress workers
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go progressWorker(ctx, wg, itr, lg)
	if opts.ProgressBar {
		wg.Add(1)
		go progressBar(ctx, wg, itr)
	}
	// Perform the insert in a new goroutine
	go func() {
		defer cancel()
		err = t.Insert(ctx, itr)
	}()
	// Wait for the progress workers to exit before returning
	wg.Wait()
	return
}

// connectOrCreate connects to the database on c with the given name, creating it if necessary.
func connectOrCreate(ctx context.Context, c keyvalue.Connection, name string) (keyvalue.Database, error) {
	// Try to create the database
	if err := c.CreateDatabase(ctx, name); err != nil {
		if errors.Is(err, kverrors.DatabaseAlreadyExists.New()) {
			return nil, err
		}
	}
	return c.ConnectToDatabase(ctx, name)
}

// run dumps the contents of the key-value table specified in opts to disk. Use the given context to shutdown.
func run(ctx context.Context, opts *Options, lg log.Interface) (err error) {
	// closeWithErr returns a function that calls close on 'c', storing any
	// error in 'err'
	closeWithErr := func(c io.Closer) func() {
		return func() {
			closeErr := c.Close()
			if err == nil {
				err = closeErr
			}
		}
	}
	// Open the connection
	var c keyvalue.Connection
	if c, err = opts.Connection(ctx); err != nil {
		return
	}
	defer closeWithErr(c)()
	// Connect to the database
	var db keyvalue.Database
	if db, err = connectOrCreate(ctx, c, opts.DBName); err != nil {
		return
	}
	defer closeWithErr(db)
	// Check that the table doesn't exist already
	var S []string
	if S, err = db.ListTables(ctx); err != nil {
		return
	} else if isContained(opts.TableName, S) {
		err = errors.New("table exists")
		return
	}
	// Read the header file
	var template record.Record
	if template, err = readHeader(opts.headerPath()); err != nil {
		return
	}
	// Create the table
	if err = db.CreateTable(ctx, opts.TableName, template); err != nil {
		return
	}
	var t keyvalue.Table
	if t, err = db.ConnectToTable(ctx, opts.TableName); err != nil {
		return
	}
	defer closeWithErr(t)()
	// Open the file for reading and wrap it in a buffered gzip reader
	var f io.ReadCloser
	if f, err = os.Open(opts.dataPath()); err != nil {
		return
	}
	defer closeWithErr(f)()
	var zr io.ReadCloser
	if zr, err = compress.Gzip.NewReader(bufio.NewReader(f)); err != nil {
		return
	}
	defer closeWithErr(zr)()
	// Wrap the gzip reader in a keyvalue reader
	var kr record.Iterator
	if kr, err = keyvalue.NewReader(zr, keyvalue.ReaderOptions{
		Template:           template,
		AllowDuplicateKeys: false,
		AllowEmptyKeys:     false,
		AllowMissingKeys:   true,
		AllowUnknownKeys:   false,
	}); err != nil {
		return
	}
	defer closeWithErr(kr)()
	// Decode []byte values and, if appropriate, unquote strings
	kr = wrap(kr, template, opts.Quote)
	// Buffer the keyvalue reader and wrap it in a count iterator
	itr := record.NewBufferedIterator(kr, iteratorBufferSize)
	citr := record.NewCountIterator(itr)
	defer closeWithErr(citr)()
	// Finally, insert the data into the table
	start := time.Now()
	err = insertData(ctx, citr, t, opts, lg)
	if err == nil {
		lg.Printf("Inserted %d records in %s", citr.Count(), time.Since(start).Truncate(100*time.Millisecond))
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main function, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Run the main function
	err = run(ctx, opts, lg)
	if err != nil {
		lg.Printf("error: %v", err)
	}
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
