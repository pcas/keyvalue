// Parser_test provides tests for the parse package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package parse

import (
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/sort"
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

type parseTest struct {
	statement string
	condition condition.Condition
	order     sort.OrderBy
	limit     int64
}

// conditionTests contains tests that just involve conditions
var conditionTests = []parseTest{
	{
		"",
		condition.True,
		nil, -1,
	},
	{
		"where is_smooth = True or is_canonical != False",
		condition.Or(condition.Equal("is_smooth", true), condition.NotEqual("is_canonical", false)),
		nil, -1,
	},
	{
		"where vertices=\"[(0,1)\\t(1,0)\\t(-1,-1)]\"",
		condition.Equal("vertices", "[(0,1)\t(1,0)\t(-1,-1)]"),
		nil, -1,
	},
	{
		"number_of_points=3 and number_of_points = 4",
		condition.And(condition.Equal("number_of_points", int64(3)), condition.Equal("number_of_points", int64(4))),
		nil, -1,
	},
	{
		"number_of_points=3 or number_of_points = 4 or number_of_points = 5",
		condition.Or(condition.Equal("number_of_points", int64(3)), condition.Equal("number_of_points", int64(4)), condition.Equal("number_of_points", int64(5))),
		nil, -1,
	},
	{
		"number_of_points in     (-3,4)",
		condition.In("number_of_points", int64(-3), int64(4)),
		nil, -1,
	},
	{
		"number_of_points    not  in     (3,4)",
		condition.NotIn("number_of_points", int64(3), int64(4)),
		nil, -1,
	},
	{
		"number_of_points between    3   and  5",
		condition.Between("number_of_points", int64(3), int64(5)),
		nil, -1,
	},
	{
		"number_of_points not  between    3   and  5",
		condition.NotBetween("number_of_points", int64(3), int64(5)),
		nil, -1,
	},
	{
		"where string_var = 'string with many escape sequences \\a \\b \\f \\n \\r \\t \\v'",
		condition.Equal("string_var", "string with many escape sequences \a \b \f \n \r \t \v"),
		nil, -1,
	},
	{
		"where very_large = 9223372036854775808", // this is MaxInt+1
		condition.Equal("very_large", uint64(9223372036854775808)),
		nil, -1,
	},
	{
		"where comp > 7",
		condition.GreaterThan("comp", int64(7)),
		nil, -1,
	},
	{
		"where comp >= 7",
		condition.GreaterThanOrEqualTo("comp", int64(7)),
		nil, -1,
	},
	{
		"where comp < 7",
		condition.LessThan("comp", int64(7)),
		nil, -1,
	},
	{
		"where comp <= 7",
		condition.LessThanOrEqualTo("comp", int64(7)),
		nil, -1,
	},
	{
		"where comp != 7",
		condition.NotEqual("comp", int64(7)),
		nil, -1,
	},
	{
		"where comp = 7",
		condition.Equal("comp", int64(7)),
		nil, -1,
	},
	{
		"true",
		condition.True,
		nil, -1,
	},
	{
		"false",
		condition.False,
		nil, -1,
	},
	{
		"not true",
		condition.False,
		nil, -1,
	},
	{
		"not false",
		condition.True,
		nil, -1,
	},
	{
		"(true)",
		condition.True,
		nil, -1,
	},
	{
		"not (false)",
		condition.True,
		nil, -1,
	},
	{
		"where x in ()",
		condition.False,
		nil, -1,
	},
	{
		"where x is true",
		condition.IsTrue("x"),
		nil, -1,
	},
	{
		"where x is false",
		condition.IsFalse("x"),
		nil, -1,
	},
	{
		"where x is not true",
		condition.IsFalse("x"),
		nil, -1,
	},
	{
		"where x is not false",
		condition.IsTrue("x"),
		nil, -1,
	},
}

// complexTests contains tests that involve conditions, limits, and orders, where at least one of the limit and the order are non-trivial
var complexTests = []parseTest{
	{
		"where number_of_points <    6 and   (number_of_points >= 3.123) limit 0",
		condition.And(condition.LessThan("number_of_points", int64(6)), condition.GreaterThanOrEqualTo("number_of_points", float64(3.123))),
		nil, 0,
	},
	{
		"(a = true and b = true or c = true) or (a = false or b = false and c = false) limit 5000",
		condition.Or(
			condition.And(condition.Equal("a", true), condition.Equal("b", true)), condition.Equal("c", true),
			condition.Equal("a", false), condition.And(condition.Equal("b", false), condition.Equal("c", false))),
		nil, 5000,
	},
	{
		"(a is true and b is true or c = 4) and (a is false or b is not false and c = 7) limit 1000",
		condition.And(
			condition.Or(condition.And(condition.IsTrue("a"), condition.IsTrue("b")), condition.Equal("c", int64(4))),
			condition.Or(condition.IsFalse("a"), condition.And(condition.IsTrue("b"), condition.Equal("c", int64(7)))),
		),
		nil, 1000,
	},
	{
		"order by x",
		condition.True,
		sort.By(sort.Order{Key: "x", Direction: sort.Ascending}), -1,
	},
	{
		"order by x limit 12",
		condition.True,
		sort.By(sort.Order{Key: "x", Direction: sort.Ascending}), 12,
	},
	{
		"order by x limit 9223372036854775807", // This is MaxInt64
		condition.True,
		sort.By(sort.Order{Key: "x", Direction: sort.Ascending}), 9223372036854775807,
	},
	{
		"order by foo asc, bar, cat desc limit 200",
		condition.True,
		sort.OrderBy{{Key: "foo", Direction: sort.Ascending}, {Key: "bar", Direction: sort.Ascending}, {Key: "cat", Direction: sort.Descending}},
		200,
	},
}

// invalidConditions contains conditions that should not parse.
var invalidConditions = []string{
	"where string_var = \"oops, I forgot to close the string",
	"where string_var = 'oops, I forgot to close the string",
	"where (string_var = 'I forgot to close the bracket'",
	"where (string_var = 'unexpected closing bracket'))",
	"where (string_var = 'invalid character in string \xE5'))",
	"where (string_var = \xE5'invalid character outside string'))",
	"where (string_var = 'invalid character in string\xE5'))",
	"where (string_var\xE5 = 'invalid character outside string'))",
	"where (string_var = 'bogus escape sequence \\h'))",
	"where int_var = --7",
	"where float_var = .7", // there needs to be a digit before the decimal point
	"where float_var = 0.7.55",
	"where float_var = 0.755oops",
	"where overflows = 18446744073709551616", // this is MaxUint64 + 1
	"where overflows = -9223372036854775809", // this is MinInt - 1
	"where not_an_operator !, 7",
	"where",
	"cheese",
	"where cheese",
	"where not cheese",
	"and",
	"where x in (and)",
	"where x in ('cheese' and)",
	"where x between 3 and and",
	"where x between or and 3",
	"where x between 3 'and' 4",
	"where x between 3 4",
	"where x is not (true)",
	"where x is not (false)",
	"where x is cheese",
	"where x < and",
	"where x not in (and)",
	"where x not in ('cheese' and)",
	"where x not between 3 and and",
	"where x not between or and 3",
	"where x not between 3 'and' 4",
	"where x not between 3 4",
	"where x notin ('fish')",
	"where x not and",
}

// invalidOrders contains orders that should not parse.
var invalidOrders = []string{
	"order by x,",
	"order by x, and",
	"order by x and",
	"order by x desc and",
	"order x desc",
	"order by x asc order by y desc",
}

// invalidLimits contains limits that should not parse.
var invalidLimits = []string{
	"limit",
	"limit -3",
	"limit 9223372036854775808", // this is MaxInt64 + 1
	"limit ,",
	"limit or",
	"limit 8 limit 8",
	"limit 8 limit 9",
}

func TestParser(tt *testing.T) {
	require := require.New(tt)
	// Specify the input and expected output
	tests := append(conditionTests, complexTests...)
	// Run the tests
	for _, s := range tests {
		q, err := SQL(s.statement)
		require.NoError(err, "test: %s", s.statement)
		require.True(q.Cond.IsEqualTo(s.condition), "test: %s, expected: %s, got: %s", s.statement, s.condition, q.Cond)
		require.Equal(s.order, q.Order, "test: %s, expected: %s, got: %s", s.statement, s.order, q.Order)
		if s.limit == -1 {
			require.False(q.HasLimit, "test: %s, expected: no limit", s.statement)
		} else {
			require.Equal(s.limit, q.Limit, "test: %s", s.statement)
		}
	}
}

func TestParserInvalidStatements(tt *testing.T) {
	require := require.New(tt)
	// Specify the input
	statements := append(invalidConditions, invalidOrders...)
	statements = append(statements, invalidLimits...)
	// Run the tests
	for _, s := range statements {
		_, err := SQL(s)
		require.Error(err, "test: %s", s)
	}
}

func TestParserError(tt *testing.T) {
	require := require.New(tt)
	// a nil error
	var p *ParseError
	require.Equal("<nil>", p.Error())
	require.Equal(0, p.StartPosition())
	require.Equal(0, p.FinishPosition())
	// an actual error
	_, err := SQL("where x < and")
	require.Equal("expected value [near AND, offset 11-13]", err.Error())
}

func TestParseTokensWhere(tt *testing.T) {
	require := require.New(tt)
	// Run the tests
	for _, s := range conditionTests {
		// strip off the leading where, if present
		st := strings.TrimPrefix(s.statement, "where ")
		cond, err := SQLWhere(st)
		require.NoError(err, "test: %s", st)
		require.True(cond.IsEqualTo(s.condition), "test: %s, expected: %s, got: %s", st, s.condition, cond)
	}
	// These statements should give errors
	invalidStatements := append(invalidConditions, "where x = 'perfectly_valid' and")
	for _, s := range invalidStatements {
		// strip off the leading where, if present
		s = strings.TrimPrefix(s, "where ")
		_, err := SQLWhere(s)
		require.Error(err, "test: %s", s)
	}
}

func TestParseTokensOrderBy(tt *testing.T) {
	require := require.New(tt)
	// Specify some tests
	tests := []parseTest{
		{
			"x",
			condition.True,
			sort.By(sort.Order{Key: "x", Direction: sort.Ascending}), -1,
		},
		{
			"foo asc, bar, cat desc",
			condition.True,
			sort.OrderBy{{Key: "foo", Direction: sort.Ascending}, {Key: "bar", Direction: sort.Ascending}, {Key: "cat", Direction: sort.Descending}},
			-1,
		},
		{
			"",
			condition.True,
			nil, -1,
		},
	}
	// Run the tests
	for _, s := range tests {
		order, err := SQLOrderBy(s.statement)
		require.NoError(err, "test: %s", s.statement)
		require.Equal(s.order, order, "test: %s, expected: %s, got: %s", s.statement, s.order, order)
	}
	// These statements should give errors
	invalidStatements := append(invalidOrders,
		"order by x desc,",
		"order by x desc and",
		"order by x asc limit 12",
	)
	for _, s := range invalidStatements {
		// strip off the leading order by, if present
		s = strings.TrimPrefix(s, "order by ")
		_, err := SQLOrderBy(s)
		require.Error(err, "test: %s", s)
	}
}
