// Lexer_test provides tests for the parse package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package parse

import (
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

type stringer interface {
	String() string
}

func TestLexer(tt *testing.T) {
	require := require.New(tt)
	// The input and expected output
	statements := []string{
		"where is_smooth = True or is_canonical != False",
		"vertices=\"[(0,1)\t(1,0)\\t(-1,-1)]\"",
		"number_of_points=3 and number_of_points = 4",
		"number_of_points=3 or number_of_points = 4 or number_of_points = 5",
		"number_of_points in     (-3,4)",
		"number_of_points    not  in     (3,4)",
		"number_of_points between    3   and  5",
		"number_of_points not  between    3   and  5",
		"number_of_points <    6 and   (number_of_points >= 3.123)",
		"(a = true and b = true or c = true) or (a = false or b = false and c = false)",
		"(a is true and b is true or c = 4) and (a is false or b is not false and c = 7)",
		"order by foo asc, bar, cat desc limit 6",
	}
	types := [][]Type{
		{Where, String, Eq, True, Or, String, Ne, False},
		{String, Eq, String},
		{String, Eq, Int64, And, String, Eq, Int64},
		{String, Eq, Int64, Or, String, Eq, Int64, Or, String, Eq, Int64},
		{String, In, OpenBracket, Int64, Comma, Int64, CloseBracket},
		{String, Not, In, OpenBracket, Int64, Comma, Int64, CloseBracket},
		{String, Between, Int64, And, Int64},
		{String, Not, Between, Int64, And, Int64},
		{String, Lt, Int64, And, OpenBracket, String, Ge, Float64, CloseBracket},
		{OpenBracket, String, Eq, True, And, String, Eq, True, Or, String, Eq, True, CloseBracket, Or, OpenBracket, String, Eq, False, Or, String, Eq, False, And, String, Eq, False, CloseBracket},
		{OpenBracket, String, Is, True, And, String, Is, True, Or, String, Eq, Int64, CloseBracket, And, OpenBracket, String, Is, False, Or, String, Is, Not, False, And, String, Eq, Int64, CloseBracket},
		{Order, By, String, Asc, Comma, String, Comma, String, Desc, Limit, Int64},
	}
	require.Equal(len(statements), len(types), "mismatched test data")
	// Run the tests
	for i, s := range statements {
		t := types[i]
		T := NewTokeniser(NewScanner(strings.NewReader(s)))
		tok, err := T.Pop()
		idx := 0
		for err == nil && tok.Type() != EOF {
			require.Equal(t[idx], tok.Type(), "test: %s, offset: %d", s, idx+1)
			idx++
			tok, err = T.Pop()
		}
		require.NoError(err, "test: %s", s)
	}
}

func TestStringAndValue(tt *testing.T) {
	require := require.New(tt)
	statements := []string{
		"where is_smooth = True or is_canonical != False",
		"12 -12 9223372036854775807 9223372036854775808 1.234 -1.234 < <= > >= = != , ( ) AND NOT BETWEEN IS ASC DESC LIMIT IN ORDER BY",
		"'fish' fish \"fish\"",
		"fish in 'wrapping paper'",
	}
	types := [][]Type{
		{Where, String, Eq, True, Or, String, Ne, False},
		{Int64, Int64, Int64, Uint64, Float64, Float64, Lt, Le, Gt, Ge, Eq, Ne, Comma, OpenBracket, CloseBracket, And, Not, Between, Is, Asc, Desc, Limit, In, Order, By},
		{String, String, String},
		{String, In, String},
	}
	typeStrings := [][]string{
		{"WHERE", "STRING", "=", "TRUE", "OR", "STRING", "!=", "FALSE"},
		{"INT", "INT", "INT", "UINT", "FLOAT", "FLOAT", "<", "<=", ">", ">=", "=", "!=", ",", "(", ")", "AND", "NOT", "BETWEEN", "IS", "ASC", "DESC", "LIMIT", "IN", "ORDER", "BY"},
		{"STRING", "STRING", "STRING"},
		{"STRING", "IN", "STRING"},
	}
	S := [][]string{
		{"WHERE", "\"is_smooth\"", "=", "TRUE", "OR", "\"is_canonical\"", "!=", "FALSE"},
		{"12", "-12", "9223372036854775807", "9223372036854775808", "1.234", "-1.234", "<", "<=", ">", ">=", "=", "!=", ",", "(", ")", "AND", "NOT", "BETWEEN", "IS", "ASC", "DESC", "LIMIT", "IN", "ORDER", "BY"},
		{"\"fish\"", "\"fish\"", "\"fish\""},
		{"\"fish\"", "IN", "\"wrapping paper\""},
	}
	isValues := [][]bool{
		{false, true, false, true, false, true, false, true},
		{true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
		{true, true, true},
		{true, false, true},
	}
	values := [][]interface{}{
		{nil, "is_smooth", nil, true, nil, "is_canonical", nil, false},
		{int64(12), int64(-12), int64(9223372036854775807), uint64(9223372036854775808), float64(1.234), float64(-1.234), nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil},
		{"fish", "fish", "fish"},
		{"fish", nil, "wrapping paper"},
	}
	require.Equal(len(statements), len(types), "mismatched test data")
	require.Equal(len(statements), len(typeStrings), "mismatched test data")
	require.Equal(len(statements), len(S), "mismatched test data")
	require.Equal(len(statements), len(isValues), "mismatched test data")
	require.Equal(len(statements), len(values), "mismatched test data")
	// Run the tests
	for i, s := range statements {
		ts := types[i]
		ss := S[i]
		tss := typeStrings[i]
		isVs := isValues[i]
		vs := values[i]
		T := NewTokeniser(NewScanner(strings.NewReader(s)))
		tok, err := T.Pop()
		idx := 0
		for err == nil && tok.Type() != EOF {
			require.Equal(ts[idx], tok.Type(), "test: %s, offset: %d", s, idx+1)
			require.Equal(tss[idx], tok.Type().String(), "test: %s, offset: %d", s, idx+1)
			require.Equal(ss[idx], tok.(stringer).String(), "test: %s, offset: %d", s, idx+1)
			require.Equal(isVs[idx], tok.IsValue(), "test: %s, offset: %d", s, idx+1)
			require.Equal(vs[idx], tok.Value(), "test: %s, offset: %d", s, idx+1)
			idx++
			tok, err = T.Pop()
		}
		require.NoError(err, "test: %s", s)
		require.Equal(EOF, tok.Type(), "test: %s, at EOF", s)
		require.Equal("<eof>", tok.Type().String(), "test: %s, at EOF", s)
		require.Equal("EOF", tok.(stringer).String(), "test: %s, at EOF", s)
		require.Equal(false, tok.IsValue(), "test: %s, at EOF", s)
		require.Equal(nil, tok.Value(), "test: %s, at EOF", s)
	}
}
