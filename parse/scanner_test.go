// Scanner_test provides tests for the parse package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package parse

import (
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

func TestScannerError(tt *testing.T) {
	require := require.New(tt)
	// a nil error
	var e *ScanError
	require.Equal("<nil>", e.Error())
	require.Equal(0, e.Position())
	// an actual error
	t := NewTokeniser(NewScanner(strings.NewReader("\xE5 a string with an invalid leading character")))
	_, err := t.Pop()
	require.Equal("invalid character [near offset 0]", err.Error())
	require.Equal(0, err.(*ScanError).Position())
}

func TestPosition(tt *testing.T) {
	require := require.New(tt)
	// a nil scanner should have position 0
	var e *Scanner
	require.Equal(0, e.Position())
}
