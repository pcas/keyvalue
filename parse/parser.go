// Package parse defines a parser for parsing SQL-formatted queries.
/*
The SQL should be formatted as follows:

	[[WHERE] <where condition>] [ORDER BY <sort order>] [LIMIT <limit>]

Note that prefixing the WHERE condition with "WHERE" is currently optional, although this might change in the future.

* <where condition>

The following types are supported:
	string -	surrounded by matching double- (") or single-quotes (')\
	integer -	must fit in an int64 or a uint64
	float -		must fit in a float64
	boolean -	TRUE or FALSE
The following standard SQL operators are supported:
	=, !=
	<, >, <=, >=
	IS, IS NOT
	IN, NOT IN
	BETWEEN, NOT BETWEEN
	AND
	OR

* <sort order>

This should be formatted
	key1 [ASC | DESC], key2 [ASC | DESC], ..., keyn [ASC | DESC]
where ASC and DESC denote increasing and decreasing order, respectively. Precisely what this means is determined by the underlying storage engine and data type. If ASC or DESC is omitted, then ASC is assumed by default.

* <limit>

A non-negative integer (that must fit in an int64) must be provided.

*/
package parse

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

import (
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/sort"
	"fmt"
	"math"
	"strings"
)

// ParseError represents an error whilst parsing input.
type ParseError struct {
	Msg string // The error message
	T   Token  // The current token
}

// Query describes a query.
type Query struct {
	Cond     condition.Condition // The condition
	Order    sort.OrderBy        // The sort order
	HasLimit bool                // Was a limit specified?
	Limit    int64               // The limit
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// whereLoop parses a stream of tokens from t into a WHERE condition.
func whereLoop(t *Tokeniser) (condition.Condition, error) {
	// The conditions that need OR-ing together
	var conds []condition.Condition
	// Parse the condition
	cond, err := parseCondition(t)
	if err != nil {
		return nil, err
	}
	conds = append(conds, cond)
	// Start looping over the conditions
	for {
		// Read the next token
		tok, err := t.Pop()
		if err != nil {
			return nil, err
		}
		// Switch on the token type
		switch tok.Type() {
		case CloseBracket, Order, Limit, EOF:
			t.Push(tok)
			return condition.Or(conds...), nil
		case Or, And:
			// Parse the condition
			cond, err := parseCondition(t)
			if err != nil {
				return nil, err
			}
			// Update the conditions
			switch tok.Type() {
			case Or:
				conds = append(conds, cond)
			case And:
				n := len(conds)
				conds[n-1] = condition.And(conds[n-1], cond)
			}
		default:
			return nil, syntaxError(tok)
		}
	}
}

// parseCondition parses the next condition.
func parseCondition(t *Tokeniser) (condition.Condition, error) {
	// Read the next token
	tok, err := t.Pop()
	if err != nil {
		return nil, err
	}
	// Switch on the token type
	switch tok.Type() {
	case EOF:
		t.Push(tok)
		return nil, parseError(tok, "unexpected EOF")
	case OpenBracket:
		// Parse the contents of the brackets
		cond, err := whereLoop(t)
		if err != nil {
			return nil, err
		}
		// The next token had better be a close bracket
		if tok, err = t.Pop(); err != nil {
			return nil, err
		} else if tok.Type() != CloseBracket {
			return nil, parseError(tok, "expected close bracket ')'")
		}
		// Return the bracketed condition
		return cond, nil
	case True:
		return condition.True, nil
	case False:
		return condition.False, nil
	case String:
		t.Push(tok)
		return parseLeaf(t)
	case Not:
		// Parse what comes after the NOT
		cond, err := parseCondition(t)
		if err != nil {
			return nil, err
		}
		// Return the negated condition
		return cond.Negate(), nil
	default:
		return nil, syntaxError(tok)
	}
}

// commaSeparatedTokens reads tokens of the form:
//	'(' 'tok1' ',' 'tok2' ',' ... ',' 'tokN' ')'
// and returns the slice 'tok1', 'tok2', ..., 'tokN'. If 'toki' is not a value then an error will be returned.
func commaSeparatedTokens(t *Tokeniser) ([]Token, error) {
	// Read the first token
	tok, err := t.Pop()
	if err != nil {
		return nil, err
	} else if tok.Type() != OpenBracket {
		return nil, parseError(tok, "expected open bracket '('")
	}
	// Create the slice in which to store the tokens
	T := make([]Token, 0)
	// Read the next token
	tok, err = t.Pop()
	if err != nil {
		return nil, err
	}
	// Are we done?
	if tok.Type() == CloseBracket {
		return T, nil
	}
	// Start reading in the tokens
	for {
		// Is this a value?
		if !tok.IsValue() {
			return nil, parseError(tok, "expected value")
		}
		// Append the token to the slice
		T = append(T, tok)
		// The next token must be a comma or close bracket
		tok, err = t.Pop()
		if err != nil {
			return nil, err
		} else if tok.Type() == CloseBracket {
			return T, nil
		} else if tok.Type() != Comma {
			return nil, parseError(tok, "expected comma ','")
		}
		// Read in the next value
		tok, err = t.Pop()
		if err != nil {
			return nil, err
		}
	}
}

// valueAndValue parses "val1 AND val2".
func valueAndValue(t *Tokeniser) (interface{}, interface{}, error) {
	// Read the first value
	tok, err := t.Pop()
	if err != nil {
		return nil, nil, err
	} else if !tok.IsValue() {
		return nil, nil, parseError(tok, "expected value")
	}
	val1 := tok.Value()
	// Read the next token -- this should be an "and"
	tok, err = t.Pop()
	if err != nil {
		return nil, nil, err
	} else if tok.Type() != And {
		return nil, nil, parseError(tok, "expected AND")
	}
	// Read the second value
	tok, err = t.Pop()
	if err != nil {
		return nil, nil, err
	} else if !tok.IsValue() {
		return nil, nil, parseError(tok, "expected value")
	}
	return val1, tok.Value(), nil
}

// boolNotBool parses "[NOT] TRUE|FALSE" and returns the (simplified) bool.
func boolNotBool(t *Tokeniser) (bool, error) {
	// Read the first token
	tok, err := t.Pop()
	if err != nil {
		return false, err
	}
	// Switch on the type
	switch tok.Type() {
	case True:
		return true, nil
	case False:
		return false, nil
	case Not:
		// Continue
	default:
		return false, parseError(tok, "expected boolean or not")
	}
	// If we're here the then the token was a NOT -- read the next token
	tok, err = t.Pop()
	if err != nil {
		return false, err
	}
	// Switch on the type
	switch tok.Type() {
	case True:
		return false, nil // Negate the boolean
	case False:
		return true, nil // Negate the boolean
	default:
		return false, parseError(tok, "expected boolean")
	}
}

// tokensToValues converts a slice of tokens to a slice of values. Does not check that the tokens have values.
func tokensToValues(T []Token) []interface{} {
	S := make([]interface{}, 0, len(T))
	for _, t := range T {
		S = append(S, t.Value())
	}
	return S
}

// parseLeaf parses a leaf condition. Assumes that the next token is a string.
func parseLeaf(t *Tokeniser) (condition.Condition, error) {
	// Read the next token
	tok, err := t.Pop()
	if err != nil {
		return nil, err
	} else if tok.Type() != String {
		return nil, parseError(tok, "expected string")
	}
	lhs := tok.Value().(string)
	// Read in the operator
	op, err := t.Pop()
	if err != nil {
		return nil, err
	}
	switch op.Type() {
	case Eq, Lt, Gt, Le, Ge, Ne:
		// Read in the rhs
		rhs, err := t.Pop()
		if err != nil {
			return nil, err
		} else if !rhs.IsValue() {
			return nil, parseError(rhs, "expected value")
		}
		// Return the condition
		switch op.Type() {
		case Eq:
			return condition.Equal(lhs, rhs.Value()), nil
		case Lt:
			return condition.LessThan(lhs, rhs.Value()), nil
		case Gt:
			return condition.GreaterThan(lhs, rhs.Value()), nil
		case Le:
			return condition.LessThanOrEqualTo(lhs, rhs.Value()), nil
		case Ge:
			return condition.GreaterThanOrEqualTo(lhs, rhs.Value()), nil
		case Ne:
			return condition.NotEqual(lhs, rhs.Value()), nil
		default:
			return nil, parseError(op, "expected operator")
		}
	case Is:
		// Read in the rhs
		val, err := boolNotBool(t)
		if err != nil {
			return nil, err
		}
		// Return the IS
		return condition.Is(lhs, val), nil
	case In:
		// Read in the values
		T, err := commaSeparatedTokens(t)
		if err != nil {
			return nil, err
		}
		// Return the IN
		return condition.In(lhs, tokensToValues(T)...), nil
	case Between:
		// Read in the values
		val1, val2, err := valueAndValue(t)
		if err != nil {
			return nil, err
		}
		// Return the BETWEEN
		return condition.Between(lhs, val1, val2), nil
	case Not:
		// Read the next token
		op, err = t.Pop()
		if err != nil {
			return nil, err
		}
		switch op.Type() {
		case In:
			// Read in the values
			T, err := commaSeparatedTokens(t)
			if err != nil {
				return nil, err
			}
			// Return the NOT IN
			return condition.NotIn(lhs, tokensToValues(T)...), nil
		case Between:
			// Read in the values
			val1, val2, err := valueAndValue(t)
			if err != nil {
				return nil, err
			}
			// Return the NOT BETWEEN
			return condition.NotBetween(lhs, val1, val2), nil
		default:
			return nil, syntaxError(op)
		}
	default:
		return nil, syntaxError(op)
	}
}

// sortLoop parses a stream of tokens into an ORDER BY sort order.
func sortLoop(t *Tokeniser) (sort.OrderBy, error) {
	// The slice of key-direction pairs
	var order sort.OrderBy
	// Start parsing the tokens
	for {
		// Read the next token
		tok, err := t.Pop()
		if err != nil {
			return nil, err
		}
		// Switch on the token type
		switch tok.Type() {
		case Limit, EOF:
			if len(order) != 0 {
				return nil, syntaxError(tok)
			}
			t.Push(tok)
			return order, nil
		case String:
			// Carry on
		default:
			return nil, syntaxError(tok)
		}
		// Make a note of the key
		key := tok.Value().(string)
		// Read in the next token - this should be a comma or the sort direction
		tok, err = t.Pop()
		if err != nil {
			return nil, err
		}
		// Switch on the token type
		switch tok.Type() {
		case Asc, Desc:
			// Add the sort order
			var dir sort.Direction
			if tok.Type() == Asc {
				dir = sort.Ascending
			} else {
				dir = sort.Descending
			}
			order = append(order, sort.Order{
				Key:       key,
				Direction: dir,
			})
			// We need to consider the next token
			tok, err = t.Pop()
			if err != nil {
				return nil, err
			}
			switch tok.Type() {
			case Comma:
				// Carry on
			case Limit, EOF:
				t.Push(tok)
				return order, nil
			default:
				return nil, syntaxError(tok)
			}
		case Comma:
			// Add the sort order
			order = append(order, sort.Order{
				Key:       key,
				Direction: sort.Ascending,
			})
		case Limit, EOF:
			// Add the sort order
			order = append(order, sort.Order{
				Key:       key,
				Direction: sort.Ascending,
			})
			t.Push(tok)
			return order, nil
		default:
			return nil, syntaxError(tok)
		}
	}
}

// limitLoop parses a stream of tokens into a LIMIT value.
func limitLoop(t *Tokeniser) (int64, bool, error) {
	// Read the next token
	tok, err := t.Pop()
	if err != nil {
		return 0, false, err
	}
	// Switch on the token type
	switch tok.Type() {
	case EOF:
		t.Push(tok)
		return 0, false, nil
	case Int64:
		n := tok.Value().(int64)
		if n < 0 {
			return 0, false, parseError(tok, "limit out of range")
		}
		return n, true, nil
	case Uint64:
		n := tok.Value().(uint64)
		if n > math.MaxInt64 {
			return 0, false, parseError(tok, "limit out of range")
		}
		return int64(n), true, nil
	default:
		return 0, false, syntaxError(tok)
	}
}

/////////////////////////////////////////////////////////////////////////
// ParseError functions
/////////////////////////////////////////////////////////////////////////

// parseError returns an error for the given token. The returned error is of type *ParseError.
func parseError(t Token, msg string) error {
	return &ParseError{
		Msg: msg,
		T:   t,
	}
}

// syntaxError returns a syntax error for the given token. The returned error is of type *ParseError.
func syntaxError(t Token) error {
	return parseError(t, "syntax error")
}

// Error returns the error message.
func (e *ParseError) Error() string {
	if e == nil {
		return "<nil>"
	}
	return fmt.Sprintf("%s [near %s, offset %d-%d]", e.Msg, e.T, e.StartPosition(), e.FinishPosition())
}

// StartPosition returns the start position (inclusive) of the error in the input.
func (e *ParseError) StartPosition() int {
	if e == nil {
		return 0
	}
	return e.T.StartPosition()
}

// FinishPosition returns the finish position (inclusive) of the error in the input.
func (e *ParseError) FinishPosition() int {
	if e == nil {
		return 0
	}
	return e.T.FinishPosition()
}

/////////////////////////////////////////////////////////////////////////
// Query functions
/////////////////////////////////////////////////////////////////////////

// newQuery returns a new, empty query.
func newQuery() *Query {
	return &Query{
		Cond: condition.True,
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ParseTokensWhere parses a stream of tokens into a condition.
func ParseTokensWhere(t *Tokeniser) (condition.Condition, error) {
	// Check for EOF
	tok, err := t.Pop()
	if err != nil {
		return nil, err
	} else if tok.Type() == EOF {
		return condition.True, nil
	}
	// We allow for WHERE clauses to begin without a WHERE
	if tok.Type() != Where {
		t.Push(tok)
	}
	// Parse the WHERE condition
	cond, err := whereLoop(t)
	if err != nil {
		return nil, err
	}
	// This has better be the end of the stream
	tok, err = t.Pop()
	if err != nil {
		return nil, err
	} else if tok.Type() != EOF {
		return nil, syntaxError(tok)
	}
	// Return the condition
	return cond, nil
}

// ParseTokensOrderBy parses a stream of tokens into a sort order.
func ParseTokensOrderBy(t *Tokeniser) (sort.OrderBy, error) {
	// Check for EOF
	tok, err := t.Pop()
	if err != nil {
		return nil, err
	} else if tok.Type() == EOF {
		return nil, nil
	}
	// We allow for WHERE clauses to begin without an ORDER BY
	if tok.Type() == Order {
		tok, err = t.Pop()
		if err != nil {
			return nil, err
		} else if tok.Type() != By {
			return nil, syntaxError(tok)
		}
	} else {
		t.Push(tok)
	}
	// Parse the ORDER BY condition
	order, err := sortLoop(t)
	if err != nil {
		return nil, err
	}
	// This has better be the end of the stream
	tok, err = t.Pop()
	if err != nil {
		return nil, err
	} else if tok.Type() != EOF {
		return nil, syntaxError(tok)
	}
	// Return the sort order
	return order, nil
}

// ParseTokens parses a stream of tokens into a query.
func ParseTokens(t *Tokeniser) (*Query, error) {
	var hasWhere, hasOrder, hasLimit bool
	// Start parsing the tokens
	q := newQuery()
	for {
		// Read the next token
		tok, err := t.Pop()
		if err != nil {
			return nil, err
		}
		// How we proceed depends on the type
		switch tok.Type() {
		case EOF:
			return q, nil
		case Order:
			// Check and update our state
			if hasOrder {
				return nil, syntaxError(tok)
			}
			hasWhere = true
			hasOrder = true
			// The next token must be BY
			tok, err = t.Pop()
			if err != nil {
				return nil, err
			} else if tok.Type() != By {
				return nil, syntaxError(tok)
			}
			// Parse the SORT BY sort order
			order, err := sortLoop(t)
			if err != nil {
				return nil, err
			} else if len(order) == 0 {
				return nil, syntaxError(tok)
			}
			q.Order = order
		case Limit:
			// Check and update our state
			if hasLimit {
				return nil, syntaxError(tok)
			}
			hasWhere = true
			hasOrder = true
			hasLimit = true
			// Parse the LIMIT
			n, hasLimit, err := limitLoop(t)
			if err != nil {
				return nil, err
			} else if !hasLimit {
				return nil, syntaxError(tok)
			}
			q.HasLimit = true
			q.Limit = n
		default:
			// Check and update our state
			if hasWhere {
				return nil, syntaxError(tok)
			}
			hasWhere = true
			// We allow for WHERE clauses to begin without a WHERE
			if tok.Type() != Where {
				t.Push(tok)
			}
			// Parse the WHERE condition
			cond, err := whereLoop(t)
			if err != nil {
				return nil, err
			}
			q.Cond = cond
		}
	}
}

// SQLWhere parses an SQL-formatted string into a condition.
func SQLWhere(s string) (condition.Condition, error) {
	return ParseTokensWhere(NewTokeniser(NewScanner(strings.NewReader(s))))
}

// SQLOrderBy parses an SQL-formatted string into a sort order.
func SQLOrderBy(s string) (sort.OrderBy, error) {
	return ParseTokensOrderBy(NewTokeniser(NewScanner(strings.NewReader(s))))
}

// SQL parses an SQL-formatted string into a query.
func SQL(s string) (*Query, error) {
	return ParseTokens(NewTokeniser(NewScanner(strings.NewReader(s))))
}
