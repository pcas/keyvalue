// Scanner defines a scanner for parsing SQL-formatted queries.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package parse

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
)

// ScanError represents an error whilst parsing input with a Scanner.
type ScanError struct {
	Msg string // The error message
	Pos int    // The position in the input
}

// Scanner allows for rune-by-rune reading of input.
type Scanner struct {
	r      *bufio.Reader // The underlying reader
	bufIdx int           // The index of the first empty entry in the buffer
	buf    []rune        // A buffer of pushed runes
	pos    int           // The position in the input
}

/////////////////////////////////////////////////////////////////////////
// ScanError functions
/////////////////////////////////////////////////////////////////////////

// scanErrorf returns an error for the given scanner. The returned error is of type *ScanError.
func scanErrorf(s *Scanner, format string, a ...interface{}) error {
	return &ScanError{
		Msg: fmt.Sprintf(format, a...),
		Pos: s.Position(),
	}
}

// Error returns the error message.
func (e *ScanError) Error() string {
	if e == nil {
		return "<nil>"
	}
	return e.Msg + " [near offset " + strconv.Itoa(e.Pos) + "]"
}

// Position returns the position of the error in the input.
func (e *ScanError) Position() int {
	if e == nil {
		return 0
	}
	return e.Pos
}

/////////////////////////////////////////////////////////////////////////
// Scanner functions
/////////////////////////////////////////////////////////////////////////

// NewScanner returns a new scanner wrapping the reader r.
func NewScanner(r io.Reader) *Scanner {
	return &Scanner{
		r: bufio.NewReader(r),
	}
}

// Pop pops the next rune off the input.
func (s *Scanner) Pop() (rune, error) {
	if s.bufIdx != 0 {
		r := s.buf[s.bufIdx-1]
		s.bufIdx--
		s.pos++
		return r, nil
	}
	r, _, err := s.r.ReadRune()
	if err != nil {
		return 0, err
	}
	s.pos++
	return r, nil
}

// Push pushes the given rune back onto the input.
func (s *Scanner) Push(r rune) {
	if s.bufIdx < len(s.buf) {
		s.buf[s.bufIdx] = r
	} else {
		s.buf = append(s.buf, r)
	}
	s.bufIdx++
	s.pos--
}

// Position returns the current position of the scanner in the input.
func (s *Scanner) Position() int {
	if s == nil {
		return 0
	}
	return s.pos
}
