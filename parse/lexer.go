// Lexer defines a lexer for parsing SQL-formatted queries.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package parse

import (
	"bitbucket.org/pcastools/stringsbuilder"
	"io"
	"strconv"
	"strings"
	"unicode"
)

// Type is the type of a token.
type Type int

// The valid token types.
const (
	String = Type(iota)
	Int64
	Uint64
	Float64
	True
	False
	OpenBracket
	CloseBracket
	Comma
	Where
	Is
	Eq
	Lt
	Gt
	Le
	Ge
	Ne
	Not
	And
	Or
	In
	Between
	Order
	By
	Asc
	Desc
	Limit
	EOF
)

// Token is the interface describing a token.
type Token interface {
	// Type returns the token type.
	Type() Type
	// StartPosition returns the initial position (inclusive) of this token in
	// the stream.
	StartPosition() int
	// FinishPosition returns the final position (inclusive) of this token in
	// the stream.
	FinishPosition() int
	// IsValue returns true if the token could represent a value.
	IsValue() bool
	// Value returns the value associated with this token (if any).
	Value() interface{}
}

// basicToken is the common implementation of a Token.
type basicToken struct {
	startPos  int // The start position for this token
	finishPos int // The finish position for this token
}

// StringToken represents a string.
type StringToken struct {
	basicToken
	val string
}

// BoolToken represents a boolean.
type BoolToken struct {
	basicToken
	val bool
}

// Int64Token represents an int64.
type Int64Token struct {
	basicToken
	val int64
}

// Uint64Token represents a uint64.
type Uint64Token struct {
	basicToken
	val uint64
}

// Float64Token represents a float64.
type Float64Token struct {
	basicToken
	val float64
}

// CommaToken represents a comma.
type CommaToken struct {
	basicToken
}

// WordToken represents a reserved word.
type WordToken struct {
	basicToken
	t Type
}

// EOFToken represents the end of the stream.
type EOFToken struct {
	basicToken
}

// Tokeniser tokenises a scanner.
type Tokeniser struct {
	s      *Scanner // The underlying scanner
	bufIdx int      // The index of the first empty entry in the buffer
	buf    []Token  // A buffer of pushed tokens
}

/////////////////////////////////////////////////////////////////////////
// basicToken functions
/////////////////////////////////////////////////////////////////////////

// newBasicToken returns a new basicToken with given start and finish positions.
func newBasicToken(startPos int, finishPos int) basicToken {
	return basicToken{
		startPos:  startPos,
		finishPos: finishPos,
	}
}

// StartPosition returns the initial position (inclusive) of this token in the stream.
func (t *basicToken) StartPosition() int {
	if t == nil {
		return 0
	}
	return t.startPos
}

// FinishPosition returns the final position (inclusive) of this token in the stream.
func (t *basicToken) FinishPosition() int {
	if t == nil {
		return 0
	}
	return t.finishPos
}

/////////////////////////////////////////////////////////////////////////
// Type functions
/////////////////////////////////////////////////////////////////////////

// String returns a string description of the type.
func (t Type) String() string {
	switch t {
	case String:
		return "STRING"
	case Int64:
		return "INT"
	case Uint64:
		return "UINT"
	case Float64:
		return "FLOAT"
	case True:
		return "TRUE"
	case False:
		return "FALSE"
	case OpenBracket:
		return "("
	case CloseBracket:
		return ")"
	case Comma:
		return ","
	case Where:
		return "WHERE"
	case Is:
		return "IS"
	case Eq:
		return "="
	case Lt:
		return "<"
	case Gt:
		return ">"
	case Le:
		return "<="
	case Ge:
		return ">="
	case Ne:
		return "!="
	case Not:
		return "NOT"
	case And:
		return "AND"
	case Or:
		return "OR"
	case In:
		return "IN"
	case Between:
		return "BETWEEN"
	case Order:
		return "ORDER"
	case By:
		return "BY"
	case Asc:
		return "ASC"
	case Desc:
		return "DESC"
	case Limit:
		return "LIMIT"
	case EOF:
		return "<eof>"
	default:
		return "<unknown type>"
	}
}

/////////////////////////////////////////////////////////////////////////
// Tokeniser functions
/////////////////////////////////////////////////////////////////////////

// NewTokeniser returns a new tokeniser for the given scanner.
func NewTokeniser(s *Scanner) *Tokeniser {
	return &Tokeniser{
		s: s,
	}
}

// Pop pops the next token off the input.
func (t *Tokeniser) Pop() (Token, error) {
	// Is there a token already in the buffer?
	if t.bufIdx != 0 {
		tok := t.buf[t.bufIdx-1]
		t.bufIdx--
		return tok, nil
	}
	// Fetch the next non-space rune
	var r = rune(' ')
	for unicode.IsSpace(r) {
		var err error
		if r, err = t.s.Pop(); err != nil {
			if err == io.EOF {
				// Convert io.EOF to an EOFToken
				pos := t.s.Position()
				return &EOFToken{
					basicToken: newBasicToken(pos, pos),
				}, nil
			}
			return nil, err
		}
	}
	// Switch on the rune
	switch r {
	case '(':
		pos := t.s.Position()
		return &WordToken{
			basicToken: newBasicToken(pos, pos),
			t:          OpenBracket,
		}, nil
	case ')':
		pos := t.s.Position()
		return &WordToken{
			basicToken: newBasicToken(pos, pos),
			t:          CloseBracket,
		}, nil
	case ',':
		pos := t.s.Position()
		return &CommaToken{
			basicToken: newBasicToken(pos, pos),
		}, nil
	case '"', '\'':
		t.s.Push(r)
		return t.quotedString()
	case '=', '>', '<', '!':
		t.s.Push(r)
		return t.operator()
	default:
		t.s.Push(r)
		if r == '-' || unicode.IsDigit(r) {
			return t.number()
		} else if (r >= 'a' && r <= 'z') || (r >= 'A' && r <= 'Z') {
			return t.word()
		}
		return nil, scanErrorf(t.s, "invalid character")
	}
}

// Push pushes the given token back onto the input.
func (t *Tokeniser) Push(tok Token) {
	if t.bufIdx < len(t.buf) {
		t.buf[t.bufIdx] = tok
	} else {
		t.buf = append(t.buf, tok)
	}
	t.bufIdx++
}

// quotedString returns the token corresponding to a quoted string. Assumes that the next rune is the opening quote.
func (t *Tokeniser) quotedString() (Token, error) {
	// Read the next rune -- this is the opening quote
	delim, err := t.s.Pop()
	if err != nil {
		return nil, err
	}
	startPos := t.s.Position()
	// Start building the string
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	var isEscaped bool
	for {
		// Read the next rune
		r, err := t.s.Pop()
		if err != nil {
			if err == io.EOF {
				return nil, scanErrorf(t.s, "illegal EOF in string")
			}
			return nil, err
		}
		// Are we done?
		if !isEscaped && r == delim {
			return &StringToken{
				basicToken: newBasicToken(startPos, t.s.Position()),
				val:        b.String(),
			}, nil
		}
		// Handle escaping
		if isEscaped {
			switch r {
			case '"', '\'', '\\':
				// Nothing to do
			case 'a':
				r = '\a'
			case 'b':
				r = '\b'
			case 'f':
				r = '\f'
			case 'n':
				r = '\n'
			case 'r':
				r = '\r'
			case 't':
				r = '\t'
			case 'v':
				r = '\v'
			default:
				return nil, scanErrorf(t.s, "unknown escape sequence: \\"+string(r))
			}
			isEscaped = false
		} else if r == '\\' {
			isEscaped = true
		}
		// Write out the rune
		if !isEscaped {
			b.WriteRune(r) // Ignore any errors
		}
	}
}

// convertFloat converts the given string to a float64.
func (t *Tokeniser) convertFloat(s string, startPos int, finishPos int) (Token, error) {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return nil, scanErrorf(t.s, "unable to parse floating point number: %s", err)
	}
	return &Float64Token{
		basicToken: newBasicToken(startPos, finishPos),
		val:        f,
	}, nil
}

// convertInteger converts the given string to either an int64 or a uint64. 'hasMinus' is used to determine whether it is worth attempting to convert to a uint64.
func (t *Tokeniser) convertInteger(s string, hasMinus bool, startPos int, finishPos int) (Token, error) {
	n, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		if !hasMinus {
			var m uint64
			m, err = strconv.ParseUint(s, 10, 64)
			if err == nil {
				return &Uint64Token{
					basicToken: newBasicToken(startPos, finishPos),
					val:        m,
				}, nil
			}
		}
		return nil, scanErrorf(t.s, "unable to parse integer: %s", err)
	}
	return &Int64Token{
		basicToken: newBasicToken(startPos, finishPos),
		val:        n,
	}, nil
}

// number returns the token corresponding to a number. Assumes that the next rune to be read is either a digit or '-'.
func (t *Tokeniser) number() (Token, error) {
	// Read the first rune for the number
	r, err := t.s.Pop()
	if err != nil {
		return nil, err
	}
	startPos := t.s.Position()
	hasMinus := r == '-'
	hasDigit := !hasMinus
	// Parse the remainder of the number
	var hasPeriod bool
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
digitloop:
	for {
		// Write the rune to our string
		b.WriteRune(r) // Ignore any errors
		// Read the next rune
		r, err = t.s.Pop()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		// Parse the rune
		switch r {
		case '-':
			return nil, scanErrorf(t.s, "malformed number")
		case '.':
			if hasPeriod || !hasDigit {
				return nil, scanErrorf(t.s, "malformed number")
			}
			hasPeriod = true
		default:
			if unicode.IsDigit(r) {
				hasDigit = true
			} else {
				// Replace the rune and break
				t.s.Push(r)
				break digitloop
			}
		}
	}
	// We've reached the end of the number
	s := b.String()
	finishPos := t.s.Position()
	// Attempt to convert the string into a number
	if hasPeriod {
		return t.convertFloat(s, startPos, finishPos)
	}
	return t.convertInteger(s, hasMinus, startPos, finishPos)
}

// isReservedWord checks whether s is a reserved word. If so, returns true followed by the corresponding token.
func isReservedWord(s string, startPos int, finishPos int) (bool, Token) {
	switch strings.ToUpper(s) {
	case "TRUE":
		return true, &BoolToken{
			basicToken: newBasicToken(startPos, finishPos),
			val:        true,
		}
	case "FALSE":
		return true, &BoolToken{
			basicToken: newBasicToken(startPos, finishPos),
			val:        false,
		}
	case "WHERE":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Where,
		}
	case "IS":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Is,
		}
	case "NOT":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Not,
		}
	case "AND":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          And,
		}
	case "OR":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Or,
		}
	case "IN":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          In,
		}
	case "BETWEEN":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Between,
		}
	case "ORDER":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Order,
		}
	case "BY":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          By,
		}
	case "ASC":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Asc,
		}
	case "DESC":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Desc,
		}
	case "LIMIT":
		return true, &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Limit,
		}
	default:
		return false, nil
	}
}

// word returns the token corresponding to the next word. Assumes that the first rune satisfies a-zA-Z. All subsequent runes must satisfy a-zA-Z0-9_.
func (t *Tokeniser) word() (Token, error) {
	// Read the first rune
	r, err := t.s.Pop()
	if err != nil {
		return nil, err
	}
	startPos := t.s.Position()
	// Parse the remainder of the word
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	for {
		// Write the rune to our string
		b.WriteRune(r) // Ignore any errors
		// Read the next rune
		r, err = t.s.Pop()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		// Are we done?
		if !((r >= 'a' && r <= 'z') || (r >= 'A' && r <= 'Z') || (r >= '0' && r <= '9') || r == '_') {
			t.s.Push(r)
			break
		}
	}
	// Is this a reserved word? Otherwise this is a string.
	s := b.String()
	finishPos := t.s.Position()
	if ok, tok := isReservedWord(s, startPos, finishPos); ok {
		return tok, nil
	}
	return &StringToken{
		basicToken: newBasicToken(startPos, finishPos),
		val:        s,
	}, nil
}

// operator returns the token corresponding to the next operator. Assumes that the first rune is one of '=', '>', '<', or '!'.
func (t *Tokeniser) operator() (Token, error) {
	// Read the first rune
	r1, err := t.s.Pop()
	if err != nil {
		return nil, err
	}
	startPos := t.s.Position()
	// Are we done?
	if r1 == '=' {
		return &WordToken{
			basicToken: newBasicToken(startPos, startPos),
			t:          Eq,
		}, nil
	}
	// Read the second rune
	r2, err := t.s.Pop()
	if err != nil {
		return nil, err
	}
	finishPos := t.s.Position()
	// Return the operator
	if r2 != '=' {
		t.s.Push(r2)
		switch r1 {
		case '>':
			return &WordToken{
				basicToken: newBasicToken(startPos, finishPos),
				t:          Gt,
			}, nil
		case '<':
			return &WordToken{
				basicToken: newBasicToken(startPos, finishPos),
				t:          Lt,
			}, nil
		case '!':
			return nil, scanErrorf(t.s, "unknown operator")
		}
	}
	switch r1 {
	case '>':
		return &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Ge,
		}, nil
	case '<':
		return &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Le,
		}, nil
	case '!':
		return &WordToken{
			basicToken: newBasicToken(startPos, finishPos),
			t:          Ne,
		}, nil
	default:
		return nil, scanErrorf(t.s, "unknown operator")
	}
}

/////////////////////////////////////////////////////////////////////////
// Token types
/////////////////////////////////////////////////////////////////////////

// Type returns the token type.
func (*StringToken) Type() Type {
	return String
}

// String returns the token as a string.
func (t *StringToken) String() string {
	return strconv.Quote(t.val)
}

// IsValue returns true if the token could represent a value.
func (*StringToken) IsValue() bool {
	return true
}

// Value returns the value associated with this token.
func (t *StringToken) Value() interface{} {
	return t.val
}

// Type returns the token type.
func (t *BoolToken) Type() Type {
	if t.val {
		return True
	}
	return False
}

// String returns the token as a string.
func (t *BoolToken) String() string {
	return t.Type().String()
}

// IsValue returns true if the token could represent a value.
func (*BoolToken) IsValue() bool {
	return true
}

// Value returns the value associated with this token.
func (t *BoolToken) Value() interface{} {
	return t.val
}

// Type returns the token type.
func (*Int64Token) Type() Type {
	return Int64
}

// String returns the token as a string.
func (t *Int64Token) String() string {
	return strconv.FormatInt(t.val, 10)
}

// IsValue returns true if the token could represent a value.
func (*Int64Token) IsValue() bool {
	return true
}

// Value returns the value associated with this token.
func (t *Int64Token) Value() interface{} {
	return t.val
}

// Type returns the token type.
func (*Uint64Token) Type() Type {
	return Uint64
}

// String returns the token as a string.
func (t *Uint64Token) String() string {
	return strconv.FormatUint(t.val, 10)
}

// IsValue returns true if the token could represent a value.
func (*Uint64Token) IsValue() bool {
	return true
}

// Value returns the value associated with this token.
func (t *Uint64Token) Value() interface{} {
	return t.val
}

// Type returns the token type.
func (*Float64Token) Type() Type {
	return Float64
}

// String returns the token as a string.
func (t *Float64Token) String() string {
	return strconv.FormatFloat(t.val, 'f', -1, 64)
}

// IsValue returns true if the token could represent a value.
func (*Float64Token) IsValue() bool {
	return true
}

// Value returns the value associated with this token.
func (t *Float64Token) Value() interface{} {
	return t.val
}

// Type returns the token type.
func (*CommaToken) Type() Type {
	return Comma
}

// String returns the token as a string.
func (*CommaToken) String() string {
	return ","
}

// IsValue returns true if the token could represent a value.
func (*CommaToken) IsValue() bool {
	return false
}

// Value returns nil.
func (t *CommaToken) Value() interface{} {
	return nil
}

// Type returns the token type.
func (t *WordToken) Type() Type {
	return t.t
}

// String returns the token as a string.
func (t *WordToken) String() string {
	return t.Type().String()
}

// IsValue returns true if the token could represent a value.
func (*WordToken) IsValue() bool {
	return false
}

// Value returns nil.
func (t *WordToken) Value() interface{} {
	return nil
}

// Type returns the token type.
func (*EOFToken) Type() Type {
	return EOF
}

// String returns the token as a string.
func (*EOFToken) String() string {
	return "EOF"
}

// IsValue returns true if the token could represent a value.
func (*EOFToken) IsValue() bool {
	return false
}

// Value returns nil.
func (*EOFToken) Value() interface{} {
	return nil
}
