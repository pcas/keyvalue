// Sqlite_test provides tests for the SQLite package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package sqlite

import (
	"bitbucket.org/pcas/keyvalue/internal/drivertest"
	"context"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

// contains returns true iff s is contained in S.
func contains(S []string, s string) bool {
	for _, x := range S {
		if x == s {
			return true
		}
	}
	return false
}

func TestDriver(t *testing.T) {
	// Create a context to run the tests
	ctx, cancel := context.WithTimeout(context.Background(), 40*time.Second)
	defer cancel()
	// Connect to sqlite
	c, err := Open(ctx, ":memory:")
	require.NoError(t, err, "error opening connection")
	// Connect to the test database
	db, err := c.ConnectToDatabase(ctx, "main")
	require.NoError(t, err, "error opening test database")
	// Run the tests
	drivertest.RunTableTests(ctx, db, t)
	// Close the database
	err = db.Close()
	require.NoError(t, err, "error closing test database")
	// Close the connection
	err = c.Close()
	require.NoError(t, err, "error closing connection")
}
