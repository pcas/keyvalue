// Package sqlite implements the keyvalue interfaces when working with SQLite.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package sqlite

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcastools/contextutil"
	"context"
	"database/sql"
	sqldriver "database/sql/driver"
	stderrors "errors"
	"fmt"
	"math"
	"modernc.org/sqlite"
	"strconv"
	"strings"
)

// connection implements the driver.Connection interface.
type connection struct {
	db *sql.DB // The underlying database connection
}

// database implements the driver.Database interface.
type database struct {
	c       *connection // The connection
	release func()      // The release function
}

// table implements the driver.Table interface.
type table struct {
	c    *connection // The connection
	name string      // The table name
}

// iterator implements the record.Iterator interface for a query.
type iterator struct {
	cancel   context.CancelFunc // Cancel the context for these results
	template record.Record      // The template record
	hasNext  bool               // Does the cursor have a next record?
	keys     []string           // The column names
	dest     []interface{}      // A destination buffer for reading a row
	ptrs     []interface{}      // The pointers used for scan
	rows     *sql.Rows          // The underlying rows of results
	isClosed bool               // Are we closed?
	err      error              // The error on iteration (if any)
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// validateTableName returns true if and only if the given table name validates.
func validateTableName(name string) bool {
	return !strings.HasPrefix(name, "sqlite_")
}

// areSlicesEqual returns true iff the two slices are equal.
func areSlicesEqual(S1 []string, S2 []string) bool {
	if len(S1) != len(S2) {
		return false
	}
	for i, s := range S1 {
		if S2[i] != s {
			return false
		}
	}
	return true
}

// indexOfString returns the first index of an entry in S matching match, or -1 if there is not matching entry,
func indexOfString(S []string, match string) int {
	for i, s := range S {
		if s == match {
			return i
		}
	}
	return -1
}

// quoteIdentifier quotes the identifier.
func quoteIdentifier(s string) string {
	// https://www.sqlite.org/lang_keywords.html
	return "\"" + strings.ReplaceAll(s, "\"", "\"\"") + "\""
}

// commitOrRollback commits or rollbacks the given transaction based on the given error.
func commitOrRollback(tx sqldriver.Tx, err error) error {
	if err == nil {
		return tx.Commit()
	}
	return tx.Rollback()
}

// closeStmt closes the given statement.
func closeStmt(s *sql.Stmt) error {
	if s == nil {
		return nil
	}
	return s.Close()
}

// execAndClose executes and closes the given statement.
func execAndClose(ctx context.Context, s *sql.Stmt) (err error) {
	if s != nil {
		_, err = s.ExecContext(ctx)
		if closeErr := s.Close(); err == nil {
			err = closeErr
		}
	}
	return
}

// quotedKeysAndValues returns parallel slices of "key = ?" and values, where the keys are quoted ready for including in an SQL statement. The keys will be sorted in increasing order.
func quotedKeysAndValues(r record.Record) ([]string, []interface{}) {
	keys := make([]string, 0, len(r))
	vals := make([]interface{}, 0, len(r))
	for _, k := range r.Keys() {
		keys = append(keys, quoteIdentifier(k)+" = ?")
		vals = append(vals, r[k])
	}
	return keys, vals
}

// quotedKeyAndType returns a string containing a quoted version of k and the type of v, suitable for including in SQLite CREATE TABLE and ALTER TABLE commands.
func quotedKeyAndType(k string, v interface{}) string {
	var t string
	switch v.(type) {
	case int8, int16, int32, int, int64, uint8, uint16, uint32, uint64:
		t = "INTEGER"
	case bool:
		t = "INTEGER"
	case float64:
		t = "REAL"
	case []byte:
		t = "BLOB"
	default:
		t = "TEXT"
	}
	return quoteIdentifier(k) + " " + t
}

// recordTypeForSqliteType returns a sample record Value for the named SQLite type.
func recordTypeForSqliteType(t string) interface{} {
	switch strings.ToUpper(t) {
	case "INTEGER":
		return int64(0)
	case "REAL":
		return float64(0)
	case "BLOB":
		return []byte{}
	default:
		return ""
	}
}

// validateRecord validates the given record, returning true on success, false otherwise. If validation fails, an error will also be returned explaining the failure.
func validateRecord(r record.Record) (bool, error) {
	// First we check the record validates
	ok, err := r.IsValid()
	if !ok {
		return false, err
	}
	// Now we look for uint64 values in the record that will overflow. The
	// problem is that SQLite's INTEGER doesn't support a large enough range.
	for k, v := range r {
		if n, ok := v.(uint64); ok && n > math.MaxInt64 {
			return false, fmt.Errorf("value for key \"%s\" too large", k)
		}
	}
	return true, nil
}

// isDuplicateColumnError returns true if and only if the error indicates a duplicate column.
func isDuplicateColumnError(err error) bool {
	if err != nil {
		if e, ok := err.(*sqlite.Error); ok {
			return strings.HasPrefix(e.Error(), "SQL logic error: duplicate column name: ")
		}
	}
	return false
}

// indexName returns the index name for a given table and key.
func indexName(t *table, key string) string {
	return quoteIdentifier(t.name + "_" + key + "_index")
}

//////////////////////////////////////////////////////////////////////
// iterator functions
//////////////////////////////////////////////////////////////////////

// Close prevents future iteration.
func (s *iterator) Close() error {
	if !s.isClosed {
		s.isClosed = true
		defer s.cancel()
		if err := s.rows.Close(); s.err == nil {
			s.err = err
		}
	}
	return s.err
}

// Err returns any errors during iteration.
func (s *iterator) Err() error {
	return s.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *iterator) Next() bool {
	ok, err := s.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *iterator) NextContext(ctx context.Context) (bool, error) {
	// Sanity check
	if s.isClosed || s.err != nil {
		return false, nil
	}
	// Link the context to our cancel function
	defer contextutil.NewLink(ctx, s.cancel).Stop()
	// Is there a next row?
	if !s.rows.Next() {
		s.err = s.rows.Err()
		return false, s.err
	}
	// If necessary, create the backing slices
	if s.keys == nil {
		s.keys = s.template.Keys()
		s.dest = make([]interface{}, 0, len(s.keys))
		s.ptrs = make([]interface{}, 0, len(s.keys))
		for i, k := range s.keys {
			s.dest = append(s.dest, s.template[k])
			s.ptrs = append(s.ptrs, &s.dest[i])
		}
	}
	// Read in the row
	s.err = s.rows.Scan(s.ptrs...)
	if s.err != nil {
		return false, s.err
	}
	s.hasNext = true
	return true, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *iterator) Scan(dest record.Record) error {
	// Sanity check
	if s.isClosed {
		return errors.IterationFinished.New()
	} else if !s.hasNext {
		return errors.IterationNotStarted.New()
	}
	// Delete any existing keys in the destination record
	for key := range dest {
		delete(dest, key)
	}
	// Populate the destination record
	for i, k := range s.keys {
		if s.dest[i] != nil {
			dest[k] = s.dest[i]
		}
	}
	// Convert and return the record
	return record.Convert(dest, s.template)
}

//////////////////////////////////////////////////////////////////////
// table functions
//////////////////////////////////////////////////////////////////////

// quotedName returns the quoted table name, suitable for including in an SQL statement.
func (t *table) quotedName() string {
	return quoteIdentifier(t.name)
}

// createInsertStatement returns an INSERT statement for the given fields.
func (t *table) createInsertStatement(fields []string) string {
	keys := make([]string, 0, len(fields))
	values := make([]string, 0, len(fields))
	for _, k := range fields {
		keys = append(keys, quoteIdentifier(k))
		values = append(values, "?")
	}
	return "INSERT INTO " + t.quotedName() + " (" + strings.Join(keys, ", ") + ") VALUES (" + strings.Join(values, ", ") + ")"
}

// createSelectStatement returns a SELECT statement for the given template and condition. The keys will be sorted in increasing order.
func (t *table) createSelectStatement(template record.Record, cond condition.Condition) (string, []interface{}, error) {
	// Create the slice of keys
	keys := make([]string, 0, len(template))
	for _, k := range template.Keys() {
		keys = append(keys, quoteIdentifier(k))
	}
	// Create the SELECT statement
	stmt := "SELECT " + strings.Join(keys, ", ") + " FROM " + t.quotedName()
	// Create the WHERE component
	where, vals, err := conditionToSQL(cond, nil)
	if err != nil {
		return "", nil, err
	}
	if len(where) != 0 {
		stmt += " WHERE " + where
	}
	return stmt, vals, nil
}

// createOrderBy returns an SQL string describing the given sort order.
func createOrderBy(order sort.OrderBy) string {
	if len(order) == 0 {
		return ""
	}
	S := make([]string, 0, len(order))
	for i := range order {
		s := quoteIdentifier(order[i].Key)
		if order[i].IsAscending() {
			s += " ASC"
		} else {
			s += " DESC"
		}
		S = append(S, s)
	}
	return "ORDER BY " + strings.Join(S, ", ")
}

// Close closes the connection to the table.
func (t *table) Close() error {
	return nil
}

// Describe returns a best-guess template for the data in this table.
func (t *table) Describe(ctx context.Context) (record.Record, error) {
	// https://stackoverflow.com/questions/3330435/is-there-an-sqlite-equivalent-to-mysqls-describe-table
	stmt := "PRAGMA table_info(" + t.quotedName() + ")"
	rows, err := t.c.db.QueryContext(ctx, stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	// Make a note of the column names
	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}
	// Find the index of the "name" and "type" columns
	nameIdx := indexOfString(cols, "name")
	if nameIdx == -1 {
		return nil, stderrors.New("rows do not contain column \"name\"")
	}
	typeIdx := indexOfString(cols, "type")
	if typeIdx == -1 {
		return nil, stderrors.New("rows do not contain column \"type\"")
	}
	// Prepare values for reading the columns
	vals := make([]interface{}, len(cols))
	ptrs := make([]interface{}, 0, len(vals))
	for i := range vals {
		ptrs = append(ptrs, &vals[i])
	}
	// Extract the description
	r := make(record.Record)
	for rows.Next() {
		// Read the row
		if err := rows.Scan(ptrs...); err != nil {
			return nil, err
		}
		// Extract the name and type
		var name, t string
		var ok bool
		if name, ok = vals[nameIdx].(string); !ok {
			return nil, fmt.Errorf("unexpect type (%T) in entry %d", vals[nameIdx], nameIdx)
		} else if t, ok = vals[typeIdx].(string); !ok {
			return nil, fmt.Errorf("unexpect type (%T) in entry %d", vals[typeIdx], typeIdx)
		}
		// Convert the type
		r[name] = recordTypeForSqliteType(t)
	}
	// Handle any errors
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return r, nil
}

// CountWhere returns the number of records in the table that satisfy condition "cond" (which may be nil if there are no conditions).
//
// Assumes that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool.
func (t *table) CountWhere(ctx context.Context, cond condition.Condition) (n int64, err error) {
	// Create the COUNT statement
	stmt := "SELECT COUNT(*) FROM " + t.quotedName()
	// Create the WHERE component
	where, vals, err := conditionToSQL(cond, nil)
	if err != nil {
		return 0, err
	}
	if len(where) != 0 {
		stmt += " WHERE " + where
	}
	// Perform the count
	err = t.c.db.QueryRowContext(ctx, stmt, vals...).Scan(&n)
	return
}

// Insert inserts the records from the given iterator into the table.
func (t *table) Insert(ctx context.Context, itr record.Iterator) (err error) {
	// Start a new transaction.
	var tx *sql.Tx
	if tx, err = t.c.db.BeginTx(ctx, nil); err != nil {
		return
	}
	// Defer closing our transaction
	var s *sql.Stmt
	defer func() {
		// Check for iteration errors
		if itrErr := itr.Err(); err == nil {
			err = itrErr
		}
		// Close the statement
		if closeErr := closeStmt(s); err == nil {
			err = closeErr
		}
		// Commit or rollback
		if txErr := commitOrRollback(tx, err); err == nil {
			err = txErr
		}
	}()
	// Start iterating over the records
	var fields []string
	var vals []interface{}
	var ok bool
	ok, err = itr.NextContext(ctx)
	for err == nil && ok {
		// Fetch the next record and validate it
		r := make(record.Record, len(fields))
		if err = itr.Scan(r); err != nil {
			return
		} else if ok, err = validateRecord(r); !ok {
			return
		}
		// Check whether the fields agree with the record's keys
		if keys := r.Keys(); !areSlicesEqual(fields, keys) {
			// The fields need to be changed -- close the current statement
			if err = closeStmt(s); err != nil {
				s = nil
				return
			}
			// Prepare the INSERT statement using the new fields
			fields, vals = keys, make([]interface{}, len(keys))
			if s, err = tx.PrepareContext(ctx, t.createInsertStatement(fields)); err != nil {
				s = nil
				return
			}
		}
		// Insert this record
		for i, k := range fields {
			vals[i] = r[k]
		}
		if _, err = s.ExecContext(ctx, vals...); err != nil {
			return
		}
		// Move on
		ok, err = itr.NextContext(ctx)
	}
	return
}

// UpdateWhere updates all records in the table that satisfy condition "cond" (which may be nil if there are no conditions) by setting all keys present in "replacement" to the given values. Returns the number of records updated.
//
// Assumes that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool.
func (t *table) UpdateWhere(ctx context.Context, replacement record.Record, cond condition.Condition) (n int64, err error) {
	// Validate the replacement
	var ok bool
	if ok, err = validateRecord(replacement); !ok {
		return
	}
	// Create the UPDATE statement
	keys, vals := quotedKeysAndValues(replacement)
	stmt := "UPDATE " + t.quotedName() + " SET " + strings.Join(keys, ", ")
	// Create the WHERE component
	var where string
	where, vals, err = conditionToSQL(cond, vals)
	if err != nil {
		return
	}
	if len(where) != 0 {
		stmt += " WHERE " + where
	}
	// Update the records
	var res sql.Result
	res, err = t.c.db.ExecContext(ctx, stmt, vals...)
	if err != nil {
		return
	}
	n, err = res.RowsAffected()
	return
}

// SelectWhere returns the results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template". Assumes that "cond" and "order" are valid.
func (t *table) SelectWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Iterator, error) {
	return t.SelectWhereLimit(ctx, template, cond, order, -1)
}

// SelectWhereLimit returns at most n results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
//
// Assumes that "template" is valid; that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool; that "order" is valid; and that n is a non-negative integer (internally we use a negative integer to indicate no limit).
func (t *table) SelectWhereLimit(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error) {
	// Create the SELECT statement
	stmt, vals, err := t.createSelectStatement(template, cond)
	if err != nil {
		return nil, err
	}
	if len(order) > 0 {
		stmt += " " + createOrderBy(order)
	}
	if n >= 0 {
		stmt += " LIMIT " + strconv.FormatInt(n, 10)
	}
	// Construct a new context that will last the entire iteration and link it
	// to the user's context for the duration of this call
	newCtx, cancel := context.WithCancel(context.Background())
	defer contextutil.NewLink(ctx, cancel).Stop()
	// Perform the select
	rows, err := t.c.db.QueryContext(newCtx, stmt, vals...)
	if err != nil {
		cancel()
		return nil, err
	}
	return &iterator{
		cancel:   cancel,
		template: template,
		rows:     rows,
	}, nil
}

// DeleteWhere deletes those records in the table that satisfy condition "cond" (which may be nil if there are no conditions). Returns the number of records deleted.
//
// Assumes that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool.
func (t *table) DeleteWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	// Create the DELETE statement
	stmt := "DELETE FROM " + t.quotedName()
	// Create the WHERE component
	where, vals, err := conditionToSQL(cond, nil)
	if err != nil {
		return 0, err
	}
	if len(where) != 0 {
		stmt += " WHERE " + where
	}
	// Delete the records
	res, err := t.c.db.ExecContext(ctx, stmt, vals...)
	if err != nil {
		return 0, err
	}
	n, err := res.RowsAffected()
	return n, err
}

// AddIndex adds an index on the given key. If an index already exists, this index is unmodified and nil is returned.  The driver is free to assume that the key is well-formed.
func (t *table) AddIndex(ctx context.Context, key string) error {
	// Create the index
	stmt := fmt.Sprintf("CREATE INDEX IF NOT EXISTS %s ON %s(%s)", indexName(t, key), t.quotedName(), quoteIdentifier(key))
	_, err := t.c.db.ExecContext(ctx, stmt)
	return err
}

// DeleteIndex deletes the index on the given key. If no index is present, nil is returned.  The driver is free to assume that the key is well-formed.
func (t *table) DeleteIndex(ctx context.Context, key string) error {
	// Drop the index, if it exists
	stmt := "DROP INDEX IF EXISTS " + indexName(t, key)
	_, err := t.c.db.ExecContext(ctx, stmt)
	return err
}

// ListIndices lists the keys for which indices are present.  The driver is free to assume that the key is well-formed.
func (t *table) ListIndices(ctx context.Context) ([]string, error) {
	stmt := "SELECT name FROM sqlite_master WHERE type = 'index' AND tbl_name = ?"
	rows, err := t.c.db.QueryContext(ctx, stmt, t.name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	// Look for index names of the form "foo_*_index" where foo is the table name
	prefix := t.name + "_"
	indices := make(map[string]bool)
	for rows.Next() {
		// s will hold the index name
		var s string
		if err := rows.Scan(&s); err != nil {
			return nil, err
		}
		// Check if it is of the correct form
		if strings.HasPrefix(s, prefix) && strings.HasSuffix(s, "_index") {
			key := strings.TrimSuffix(strings.TrimPrefix(s, prefix), "_index")
			indices[key] = true
		}
	}
	// Handle any error
	if err := rows.Err(); err != nil {
		return nil, err
	}
	// Convert the index names to a slice and return
	result := make([]string, 0, len(indices))
	for k := range indices {
		result = append(result, k)
	}
	return result, nil
}

// AddKeys updates each record r in the table, adding any keys in rec that are not already present along with the corresponding values. Any keys that are already present in r will be left unmodified.
func (t *table) AddKeys(ctx context.Context, rec record.Record) error {
	// Handle the trivial case
	if len(rec) == 0 {
		return nil
	}
	// Add the keys and values one at a time
	for k, v := range rec {
		// Add the column to the schema if necessary
		cmd := fmt.Sprintf("ALTER TABLE %s ADD COLUMN %s", t.quotedName(), quotedKeyAndType(k, v))
		// Ignore any error about the column already existing
		if _, err := t.c.db.ExecContext(ctx, cmd); err != nil && !isDuplicateColumnError(err) {
			return err
		}
		// Build the command string and arguments
		cmd = fmt.Sprintf("UPDATE %s SET %s = ? WHERE %s IS NULL", t.quotedName(), quoteIdentifier(k), quoteIdentifier(k))
		// Do the update
		if _, err := t.c.db.ExecContext(ctx, cmd, v); err != nil {
			return err
		}
	}
	// We succeeded
	return nil
}

// DeleteKeys updates all records in the table, deleting all the specified keys if present.
func (t *table) DeleteKeys(ctx context.Context, keys []string) error {
	// Look up the columns
	schema, err := t.Describe(ctx)
	if err != nil {
		return err
	}
	// Ignore any keys that don't exist, and ignore duplicates
	keysToDelete := make(map[string]bool, len(keys))
	for _, k := range keys {
		if _, ok := schema[k]; ok {
			keysToDelete[k] = true
		}
	}
	// Do we need to do anything?
	if len(keysToDelete) == 0 {
		return nil
	}
	// Drop the columns
	stmt := "ALTER TABLE " + t.quotedName() + " DROP COLUMN "
	for k := range keysToDelete {
		_, err = t.c.db.ExecContext(ctx, stmt+quoteIdentifier(k))
		if err != nil {
			return err
		}
	}
	return nil
}

//////////////////////////////////////////////////////////////////////
// connection functions
//////////////////////////////////////////////////////////////////////

// ListTables returns the names of the tables in the database.
func (d *database) ListTables(ctx context.Context) (S []string, err error) {
	// https://database.guide/2-ways-to-list-tables-in-sqlite-database/
	rows, err := d.c.db.QueryContext(ctx, "SELECT name FROM sqlite_schema WHERE type = 'table' AND name NOT LIKE 'sqlite_%' ORDER BY name")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	// Extract the table names
	names := make([]string, 0)
	for rows.Next() {
		// Read the row
		var name string
		if err := rows.Scan(&name); err != nil {
			return nil, err
		}
		// Add the name
		names = append(names, name)
	}
	// Handle any error
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return names, nil
}

// CreateTable creates a table with the given name in the database, using the provided template. Assumes that "template" is valid.
func (d *database) CreateTable(ctx context.Context, name string, template record.Record) error {
	// Sanity check
	if !validateTableName(name) {
		return errors.InvalidTableName.New()
	}
	// Create the statement
	cols := make([]string, 0, len(template))
	for _, k := range template.Keys() {
		cols = append(cols, quotedKeyAndType(k, template[k]))
	}
	stmt := "CREATE TABLE " + quoteIdentifier(name) + " (" + strings.Join(cols, ", ") + ")"
	// Create the table
	_, err := d.c.db.ExecContext(ctx, stmt)
	return err
}

// DeleteTable deletes the indicated table from the database. Does not return an error if the table does not exist.
func (d *database) DeleteTable(ctx context.Context, name string) error {
	if !validateTableName(name) {
		return errors.InvalidTableName.New()
	}
	stmt := "DROP TABLE IF EXISTS " + quoteIdentifier(name)
	_, err := d.c.db.ExecContext(ctx, stmt)
	return err
}

// RenameTable changes the name of table src in the database to dst.
func (d *database) RenameTable(ctx context.Context, src string, dst string) error {
	if !validateTableName(src) || !validateTableName(dst) {
		return errors.InvalidTableName.New()
	}
	stmt := fmt.Sprintf("ALTER TABLE %s RENAME TO %s", quoteIdentifier(src), quoteIdentifier(dst))
	_, err := d.c.db.ExecContext(ctx, stmt)
	return err
}

// ConnectToTable connects to the indicated table in the database.
func (d *database) ConnectToTable(ctx context.Context, name string) (driver.Table, error) {
	// Sanity check
	if !validateTableName(name) {
		return nil, errors.InvalidTableName.New()
	}
	// First we want to check that the table exists
	stmt := "SELECT COUNT(*) FROM sqlite_schema WHERE type = 'table' AND name = ?"
	var n int64
	if err := d.c.db.QueryRowContext(ctx, stmt, name).Scan(&n); err != nil {
		return nil, err
	} else if n == 0 {
		return nil, errors.TableDoesNotExist.New()
	}
	// Return the table object
	return &table{
		c:    d.c,
		name: name,
	}, nil
}

// Close closes the connection to the database.
func (d *database) Close() error {
	return nil
}

//////////////////////////////////////////////////////////////////////
// connection functions
//////////////////////////////////////////////////////////////////////

// DriverName returns the name associated with this driver.
func (*connection) DriverName() string {
	return "sqlite"
}

// ListDatabases returns the names of the available databases on this connection.
func (c *connection) ListDatabases(ctx context.Context) ([]string, error) {
	return []string{"main"}, nil
}

// CreateDatabase returns an error, because the only valid database is "main" and this already exists.
func (c *connection) CreateDatabase(ctx context.Context, name string) error {
	return errors.OperationNotPermitted.New()
}

// DeleteDatabase returns an error, unless the name is not "main" in which case this is a nop.
func (c *connection) DeleteDatabase(ctx context.Context, name string) error {
	if name != "main" {
		return nil
	}
	return errors.OperationNotPermitted.New()
}

// ConnectToDatabase connects to the indicated database.
func (c *connection) ConnectToDatabase(ctx context.Context, name string) (driver.Database, error) {
	// The only valid database is "main"
	if name != "main" {
		return nil, fmt.Errorf("unknown database: %s", name)
	}
	// Return the database
	return &database{
		c: c,
	}, nil
}

// Close closes the connection.
func (c *connection) Close() error {
	return c.db.Close()
}

// Open opens a connection to an SQLite database using the given path.
func Open(ctx context.Context, p string) (keyvalue.Connection, error) {
	// Open the database connection
	db, err := sql.Open("sqlite", p)
	if err != nil {
		return nil, err
	}
	// Wrap the connection
	c := &connection{
		db: db,
	}
	return keyvalue.NewConnection(c), nil
}
