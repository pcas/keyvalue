// Readonly provides read-only wrappers around keyvalue connections.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package readonly

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"context"
)

// connection provides a read-only wrapper around a keyvalue.Connection.
type connection struct {
	keyvalue.Connection
}

// database provides a read-only wrapper around a keyvalue.Database.
type database struct {
	keyvalue.Database
}

// table provides a read-only wrapper around a keyvalue.Table.
type table struct {
	keyvalue.Table
}

//////////////////////////////////////////////////////////////////////
// connection functions
//////////////////////////////////////////////////////////////////////

// CreateDatabase always returns an OperationNotPermitted error.
func (connection) CreateDatabase(_ context.Context, _ string) error {
	return errors.OperationNotPermitted.New()
}

// DeleteDatabase always returns an OperationNotPermitted error.
func (connection) DeleteDatabase(_ context.Context, _ string) error {
	return errors.OperationNotPermitted.New()
}

// ConnectToDatabase connects to the indicated database. The returned database will be read-only.
func (c connection) ConnectToDatabase(ctx context.Context, name string) (keyvalue.Database, error) {
	d, err := c.Connection.ConnectToDatabase(ctx, name)
	if err != nil {
		return nil, err
	}
	return Database(d), nil
}

// Connection wraps the given connection, only allowing read operations.
func Connection(c keyvalue.Connection) keyvalue.Connection {
	if _, ok := c.(connection); ok {
		return c
	}
	return connection{c}
}

//////////////////////////////////////////////////////////////////////
// database functions
//////////////////////////////////////////////////////////////////////

// CreateTable always returns an OperationNotPermitted error.
func (database) CreateTable(_ context.Context, _ string, _ record.Record) error {
	return errors.OperationNotPermitted.New()
}

// DeleteTable always returns an OperationNotPermitted error.
func (database) DeleteTable(_ context.Context, _ string) error {
	return errors.OperationNotPermitted.New()
}

// RenameTable always returns an OperationNotPermitted error.
func (database) RenameTable(_ context.Context, _ string, _ string) error {
	return errors.OperationNotPermitted.New()
}

// ConnectToTable connects to the indicated table in the database. The returned table will be read-only.
func (d database) ConnectToTable(ctx context.Context, name string) (keyvalue.Table, error) {
	t, err := d.Database.ConnectToTable(ctx, name)
	if err != nil {
		return nil, err
	}
	return Table(t), nil
}

// Database wraps the given database, only allowing read operations.
func Database(d keyvalue.Database) keyvalue.Database {
	if _, ok := d.(database); ok {
		return d
	}
	return database{d}
}

//////////////////////////////////////////////////////////////////////
// table functions
//////////////////////////////////////////////////////////////////////

// InsertRecords always returns an OperationNotPermitted error.
func (table) InsertRecords(_ context.Context, _ ...record.Record) error {
	return errors.OperationNotPermitted.New()
}

// Insert always returns an OperationNotPermitted error.
func (table) Insert(_ context.Context, _tr record.Iterator) error {
	return errors.OperationNotPermitted.New()
}

// Update always returns an OperationNotPermitted error.
func (table) Update(_ context.Context, _ record.Record, _ record.Record) (int64, error) {
	return 0, errors.OperationNotPermitted.New()
}

// UpdateWhere always returns an OperationNotPermitted error.
func (table) UpdateWhere(_ context.Context, _ record.Record, _ condition.Condition) (int64, error) {
	return 0, errors.OperationNotPermitted.New()
}

// Delete always returns an OperationNotPermitted error.
func (table) Delete(_ context.Context, _ record.Record) (int64, error) {
	return 0, errors.OperationNotPermitted.New()
}

// DeleteWhere always returns an OperationNotPermitted error.
func (table) DeleteWhere(_ context.Context, _ condition.Condition) (int64, error) {
	return 0, errors.OperationNotPermitted.New()
}

// AddIndex always returns an OperationNotPermitted error.
func (table) AddIndex(_ context.Context, _ string) error {
	return errors.OperationNotPermitted.New()
}

// DeleteIndex always returns an OperationNotPermitted error.
func (table) DeleteIndex(_ context.Context, _ string) error {
	return errors.OperationNotPermitted.New()
}

// AddKeys always returns an OperationNotPermitted error.
func (table) AddKeys(ctx context.Context, rec record.Record) error {
	return errors.OperationNotPermitted.New()
}

// AddKey always returns an OperationNotPermitted error.
func (table) AddKey(ctx context.Context, key string, value interface{}) error {
	return errors.OperationNotPermitted.New()
}

// DeleteKeys always returns an OperationNotPermitted error.
func (table) DeleteKeys(ctx context.Context, keys []string) error {
	return errors.OperationNotPermitted.New()
}

// DeleteKey always returns an OperationNotPermitted error.
func (table) DeleteKey(ctx context.Context, key string) error {
	return errors.OperationNotPermitted.New()
}

// Table wraps the given table, only allowing read operations.
func Table(t keyvalue.Table) keyvalue.Table {
	if _, ok := t.(table); ok {
		return t
	}
	return table{t}
}
