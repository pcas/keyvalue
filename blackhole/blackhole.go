// Package blackhole implements a key-value driver that discards all data written to it, and always reads the empty results set.
//
// The dataSource should be in the format:
//
//	blackhole://[ignored]

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package blackhole

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"context"
)

// connection describes a connection.
type connection struct{}

// database describes a connection to a database.
type database struct{}

// table describes a connection to a table.
type table struct{}

/////////////////////////////////////////////////////////////////////////
// table functions
/////////////////////////////////////////////////////////////////////////

// Close closes the connection to the table.
func (table) Close() error {
	return nil
}

// Describe returns a best-guess template for the data in this table.
func (table) Describe(_ context.Context) (record.Record, error) {
	return record.Record{}, nil
}

// CountWhere returns the number of records in the table that satisfy condition "cond" (which may be nil if there are no conditions). Assumes that "cond" is valid.
func (table) CountWhere(_ context.Context, _ condition.Condition) (int64, error) {
	return 0, nil
}

// Insert inserts the records from the given iterator into the table.
func (table) Insert(ctx context.Context, itr record.Iterator) error {
	ok, err := itr.NextContext(ctx)
	for err == nil && ok {
		ok, err = itr.NextContext(ctx)
	}
	if err != nil {
		return err
	}
	return itr.Err()
}

// UpdateWhere updates all records in the table that satisfy condition "cond" (which may be nil if there are no conditions) by setting all keys present in "replacement" to the given values. Returns the number of records updated. Assumes that "cond" is valid.
func (table) UpdateWhere(_ context.Context, _ record.Record, _ condition.Condition) (int64, error) {
	return 0, nil
}

// SelectWhere returns the results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template". Assumes that "cond" is valid.
func (table) SelectWhere(_ context.Context, _ record.Record, _ condition.Condition, _ sort.OrderBy) (record.Iterator, error) {
	return record.EmptyIterator(), nil
}

// SelectWhereLimit returns at most n results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template". Assumes that "cond" is valid.
func (table) SelectWhereLimit(_ context.Context, _ record.Record, _ condition.Condition, _ sort.OrderBy, _ int64) (record.Iterator, error) {
	return record.EmptyIterator(), nil
}

// DeleteWhere deletes those records in the table that satisfy condition "cond" (which may be nil if there are no conditions). Returns the number of records deleted. Assumes that "cond" is valid.
func (table) DeleteWhere(_ context.Context, _ condition.Condition) (int64, error) {
	return 0, nil
}

// AddIndex adds an index on the given key. If an index already exists, this index is unmodified and nil is returned.  The driver is free to assume that the key is well-formed.
func (table) AddIndex(_ context.Context, _ string) error {
	return nil
}

// DeleteIndex deletes the index on the given key. If no index is present, nil is returned.  The driver is free to assume that the key is well-formed.
func (table) DeleteIndex(_ context.Context, _ string) error {
	return nil
}

// ListIndices lists the keys for which indices are present.  The driver is free to assume that the key is well-formed.
func (table) ListIndices(_ context.Context) ([]string, error) {
	return nil, nil
}

// AddKeys updates each record r in the table, adding any keys in rec that are not already present along with the corresponding values. Any keys that are already present in r will be left unmodified.
func (table) AddKeys(_ context.Context, _ record.Record) error {
	return nil
}

// DeleteKeys updates all records in the table, deleting all the specified keys if present.
func (table) DeleteKeys(_ context.Context, _ []string) error {
	return nil
}

/////////////////////////////////////////////////////////////////////////
// database functions
/////////////////////////////////////////////////////////////////////////

// ListTables returns the names of the tables in the database.
func (database) ListTables(_ context.Context) ([]string, error) {
	return nil, errors.OperationNotSupported.New()
}

// CreateTable creates a table with the given name in the database.
func (database) CreateTable(_ context.Context, _ string, _ record.Record) error {
	return errors.OperationNotSupported.New()
}

// RenameTable changes the name of table src in the database to dst.
func (database) RenameTable(_ context.Context, _ string, _ string) error {
	return errors.OperationNotSupported.New()
}

// DeleteTable deletes the indicated table from the database. Does not return an error if the table does not exist.
func (database) DeleteTable(_ context.Context, _ string) error {
	return errors.OperationNotSupported.New()
}

// ConnectToTable connects to the indicated table in the database.
func (database) ConnectToTable(_ context.Context, _ string) (driver.Table, error) {
	return table{}, nil
}

// Close closes the connection to the database.
func (database) Close() error {
	return nil
}

/////////////////////////////////////////////////////////////////////////
// connection functions
/////////////////////////////////////////////////////////////////////////

// DriverName returns the name associated with this driver.
func (connection) DriverName() string {
	return "blackhole"
}

// ListDatabases returns the names of the available databases on this connection.
func (connection) ListDatabases(_ context.Context) ([]string, error) {
	return nil, errors.OperationNotSupported.New()
}

// CreateDatabase creates a database with the given name on this connection.
func (connection) CreateDatabase(_ context.Context, _ string) error {
	return errors.OperationNotSupported.New()
}

// DeleteDatabase deletes the indicated database on this connection. Does not return an error if the database does not exist.
func (connection) DeleteDatabase(_ context.Context, _ string) error {
	return nil
}

// ConnectToTable connects to the indicated database.
func (connection) ConnectToDatabase(_ context.Context, _ string) (driver.Database, error) {
	return database{}, nil
}

// Close closes the connection.
func (connection) Close() error {
	return nil
}

// Open opens a connection.
func Open() keyvalue.Connection {
	return keyvalue.NewConnection(connection{})
}
