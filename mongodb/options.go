// Options provides options parsing for the mongodb driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mongodb

import (
	"bitbucket.org/pcastools/hash"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

// ClientConfig describes the configuration options we allow a user to set on a client connection.
//
// PoolSize: The maximum size of the connection pool.
//
// ReadConcern: Valid values are given by the constants RAvailable, RLinearizable, RLocal, and RMajority.
//
// WriteConcern: Valid values are given by the constants WMajority.
//
// WriteN: Here the value N must be a positive integer. This requests acknowledgement that write operations propagate to N mongod instances.
//
// WriteJournal: A value of true requests acknowledgement from MongoDB that write operations are written to the journal.
//
// SessionRefreshInterval: The duration after which the mongoDB client session is automatically refreshed. A duration of 0 will disable automatic session refresh.
type ClientConfig struct {
	AppName                string        // The application name to identify ourselves as
	Hosts                  []string      // The hosts
	Username               string        // The username
	Password               string        // The password
	PasswordSet            bool          // Is a password set?
	PoolSize               int           // The connection pool size
	ReadConcern            string        // The read concern to use
	WriteConcern           string        // The write concern to use
	WriteN                 int           // The number of writes to confirm
	WriteJournal           bool          // Journal confirmation?
	SessionRefreshInterval time.Duration // The session refresh interval
}

// The read concern strings.
//
// RAvailable specifies that the query should return data from the instance with no guarantee that the data has been written to a majority of the replica set members (i.e. may be rolled back).
//
// RLinearizable specifies that the query should return data that reflects all successful writes issued with a write concern of "majority" and acknowledged prior to the start of the read operation.
//
// RLocal specifies that the query should return the instance’s most recent data.
//
// RMajority specifies that the query should return the instance’s most recent data acknowledged as having been written to a majority of members in the replica set.
const (
	RAvailable    = "available"
	RLinearizable = "linearizable"
	RLocal        = "local"
	RMajority     = "majority"
)

// The write concern strings.
//
// WMajority requests acknowledgement that write operations propagate to the majority of mongod instances.
const (
	WMajority = "majority"
)

// The default values for the client configuration, along with controlling mutex. The initial default values are assigned at init.
var (
	defaultsm sync.Mutex
	defaults  *ClientConfig
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets the initial default values. The following environment variables are consulted:
//
//	PCAS_MONGO_HOST = "hostname1[:port1][,...,hostnamek[:portk]]"
//	PCAS_MONGO_USERNAME = "myname"
//	PCAS_MONGO_PASSWORD = "mysecret"
func init() {
	// Create the initial defaults
	defaults = &ClientConfig{
		AppName:                "keyvalue.mongodb",
		Hosts:                  []string{"localhost:27017"},
		PoolSize:               100,
		ReadConcern:            RMajority,
		WriteConcern:           WMajority,
		SessionRefreshInterval: 5 * time.Minute,
	}
	// Modify the defaults based on environment variables
	if hosts, ok := os.LookupEnv("PCAS_MONGO_HOST"); ok {
		defaults.Hosts = strings.Split(hosts, ",")
	}
	if username, ok := os.LookupEnv("PCAS_MONGO_USERNAME"); ok {
		defaults.Username = username
	}
	if password, ok := os.LookupEnv("PCAS_MONGO_PASSWORD"); ok {
		defaults.Password = password
		defaults.PasswordSet = true
	}
}

/////////////////////////////////////////////////////////////////////////
// ClientConfig functions
/////////////////////////////////////////////////////////////////////////

// DefaultConfig returns a new client configuration initialised with the default values.
//
// The initial default value for the hosts, username, and password will be read from the environment variables
//
//	PCAS_MONGO_HOST = "hostname1[:port1][,...,hostnamek[:portk]]"
//	PCAS_MONGO_USERNAME = "myname"
//	PCAS_MONGO_PASSWORD = "mysecret"
//
// respectively on package init.
//
// Note that if PCAS_MONGO_PASSWORD is set but with value equal to the empty string then it still counts as a valid password, since the empty string is a valid password. You must ensure that PCAS_MONGO_PASSWORD is unset if you do not want to specify an initial default password.
func DefaultConfig() *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	return defaults.Copy()
}

// SetDefaultConfig sets the default client configuration to c and returns the old default configuration. This change will be reflected in future calls to DefaultConfig.
func SetDefaultConfig(c *ClientConfig) *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	oldc := defaults
	defaults = c.Copy()
	return oldc
}

// Validate validates the client configuration, returning an error if there's a problem.
func (c *ClientConfig) Validate() error {
	// Get the nil case out of the way
	if c == nil {
		return errors.New("illegal nil configuration data")
	}
	// Start validating
	for _, host := range c.Hosts {
		if idx := strings.IndexByte(host, ':'); idx != -1 {
			if idx == len(host)-1 {
				return errors.New("invalid port number")
			} else if n, err := strconv.Atoi(host[idx+1:]); err != nil {
				return errors.New("invalid port number")
			} else if n < 1 || n > 65535 {
				return fmt.Errorf("port number (%d) out of range", n)
			}
		}
	}
	if c.PoolSize < 0 {
		return fmt.Errorf("invalid pool size: %d", c.PoolSize)
	}
	switch c.ReadConcern {
	case RAvailable, RLinearizable, RLocal, RMajority:
	default:
		return fmt.Errorf("invalid read concern: %s", c.ReadConcern)
	}
	if len(c.WriteConcern) == 0 && c.WriteN == 0 {
		return errors.New("either a write concern should be specified, or a non-zero number of writes to confirm should be given")
	} else if len(c.WriteConcern) != 0 && c.WriteN != 0 {
		return errors.New("either a write concern should be specified, or a non-zero number of writes to confirm should be given, but not both")
	} else if len(c.WriteConcern) != 0 {
		switch c.WriteConcern {
		case WMajority:
		default:
			return fmt.Errorf("invalid write concern: %s", c.WriteConcern)
		}
	} else if c.WriteN <= 0 {
		return fmt.Errorf("invalid number of writes to confirm: %d", c.WriteN)
	}
	if len(c.Password) != 0 && !c.PasswordSet {
		return errors.New("a password appears to be given, however the value of PasswordSet is 'false'")
	}
	if c.SessionRefreshInterval < 0 {
		return fmt.Errorf("invalid session refresh interval: %s", c.SessionRefreshInterval)
	}
	// Looks good
	return nil
}

// Copy returns a copy of the configuration.
func (c *ClientConfig) Copy() *ClientConfig {
	cc := *c
	cc.Hosts = make([]string, len(c.Hosts))
	copy(cc.Hosts, c.Hosts)
	return &cc
}

// Hash returns a hash for the configuration.
func (c *ClientConfig) Hash() uint32 {
	h := hash.String(c.AppName)
	h = hash.Combine(h, hash.StringSlice(c.Hosts))
	h = hash.Combine(h, hash.String(c.Username))
	h = hash.Combine(h, hash.Bool(c.PasswordSet))
	if c.PasswordSet {
		h = hash.Combine(h, hash.String(c.Password))
	}
	h = hash.Combine(h, hash.Int(c.PoolSize))
	h = hash.Combine(h, hash.String(c.ReadConcern))
	h = hash.Combine(h, hash.String(c.WriteConcern))
	h = hash.Combine(h, hash.Int(c.WriteN))
	h = hash.Combine(h, hash.Bool(c.WriteJournal))
	h = hash.Combine(h, hash.Int64(int64(c.SessionRefreshInterval)))
	return h
}

// toClientOptions converts the configuration to an *options.ClientOptions.
func (c *ClientConfig) toClientOptions() *options.ClientOptions {
	cl := options.Client()
	if len(c.AppName) != 0 {
		cl.SetAppName(c.AppName)
	}
	if len(c.Hosts) != 0 {
		cl.SetHosts(c.Hosts)
	}
	if len(c.Username) != 0 || c.PasswordSet {
		cl.SetAuth(options.Credential{
			Username:    c.Username,
			Password:    c.Password,
			PasswordSet: c.PasswordSet,
		})
	}
	cl.SetCompressors([]string{"snappy", "zlib", "zstd"})
	cl.SetMaxPoolSize(uint64(c.PoolSize))
	return cl
}

// toReadConcern converts the configuration to a *readconcern.ReadConcern.
func (c *ClientConfig) toReadConcern() *readconcern.ReadConcern {
	switch c.ReadConcern {
	case RAvailable:
		return readconcern.Available()
	case RLinearizable:
		return readconcern.Linearizable()
	case RLocal:
		return readconcern.Local()
	case RMajority:
		return readconcern.Majority()
	}
	return readconcern.New()
}

// toWriteConcern converts the configuration to a *writeconcern.WriteConcern.
func (c *ClientConfig) toWriteConcern() *writeconcern.WriteConcern {
	opts := make([]writeconcern.Option, 0, 2)
	switch c.WriteConcern {
	case WMajority:
		opts = append(opts, writeconcern.WMajority())
	case "":
		opts = append(opts, writeconcern.W(c.WriteN))
	}
	opts = append(opts, writeconcern.J(c.WriteJournal))
	return writeconcern.New(opts...)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// AreEqual returns true iff the two configurations are equal.
func AreEqual(c *ClientConfig, d *ClientConfig) bool {
	if c.AppName != d.AppName ||
		len(c.Hosts) != len(d.Hosts) ||
		c.Username != d.Username ||
		c.PasswordSet != d.PasswordSet ||
		c.ReadConcern != d.ReadConcern ||
		c.PoolSize != d.PoolSize ||
		c.WriteConcern != d.WriteConcern ||
		c.WriteN != d.WriteN ||
		c.WriteJournal != d.WriteJournal ||
		c.SessionRefreshInterval != d.SessionRefreshInterval {
		return false
	}
	if c.PasswordSet && (c.Password != d.Password) {
		return false
	}
	for i, s := range c.Hosts {
		if d.Hosts[i] != s {
			return false
		}
	}
	return true
}
