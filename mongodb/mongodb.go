// Package mongodb implements the keyvalue interfaces when working with mongodb.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mongodb

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/internal/kvcache"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/contextutil"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"sync"
	"time"
)

// clientCache is a cache of client connections.
var clientCache *kvcache.Cache[*ClientConfig, *mongo.Client]

// connection implements the driver.Connection interface.
type connection struct {
	cfg     *ClientConfig // The configuration options
	client  *mongo.Client // The wrapped mongodb client
	release func()        // The release function
}

// database implements the driver.Database interface.
type database struct {
	c        *connection     // The parent connection
	db       *mongo.Database // The underlying db connection
	session  mongo.Session   // The mongodb client session
	m        sync.Mutex      // Mutex protecting the data below
	doneC    chan<- struct{} // Close this channel to shut down the worker that auto-refreshes the session
	isClosed bool            // True if and only if the connection is closed
}

// table implements the driver.Table interface.
type table struct {
	c       *mongo.Collection // The underlying collection
	session mongo.Session     // The MongoDB client session for this table. This session should not be ended when the table closes -- it lives on the database that created this table.
}

// iterator implements the record.Iterator interface.
type iterator struct {
	cur      *mongo.Cursor // The underlying cursor
	template record.Record // The template record
	hasNext  bool          // Does the cursor have a next record?
	isClosed bool          // Are we closed?
	session  mongo.Session // The MongoDB client session for this iterator. This session should not be ended when the iterator closes -- it lives on the connection that created the table that created this iterator.
	err      error         // The error on iteration (if any)
}

// closeTimeout is the maximum time a Close is allowed to take before being abandoned and allowing resources to leak.
const closeTimeout = 100 * time.Millisecond

// describeLimit is the maximum number of documents to sample when building the Describe template.
const describeLimit = 100

// dummyTableName is the name of the dummy table that we create to ensure database creation succeeds.
const dummyTableName = "_pcas_dummy_table"

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init creates the client cache
func init() {
	// Create the cache
	clientCache = kvcache.New(
		func(ctx context.Context, cfg *ClientConfig) (*mongo.Client, error) {
			c, err := mongo.NewClient(cfg.toClientOptions())
			if err != nil {
				return nil, err
			} else if err = c.Connect(ctx); err != nil {
				return nil, err
			}
			return c, nil
		},
		func(ctx context.Context, c *mongo.Client) bool {
			err := c.Ping(ctx, nil)
			return err == nil
		},
		func(c *mongo.Client) error {
			ctx, cancel := context.WithTimeout(context.Background(), closeTimeout)
			defer cancel()
			return c.Disconnect(ctx)
		},
		AreEqual,
	)
	// Register closing the cache on cleanup
	cleanup.Add(clientCache.Close)
}

// connect returns a connection to mongo using the given configuration. On success, the second return value is a release function which must be called when you are finished with the connection, otherwise resources will leak.
func connect(ctx context.Context, cfg *ClientConfig) (*mongo.Client, func(), error) {
	e, err := clientCache.Get(cfg)
	if err != nil {
		return nil, nil, err
	}
	c, err := e.Value(ctx)
	if err != nil {
		e.Release()
		return nil, nil, err
	}
	return c, e.Release, nil
}

// refreshSession silently attempts to refresh the session with given ID on the given database.
func refreshSession(ctx context.Context, db *mongo.Database, sessID bson.RawValue) {
	ctx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()
	db.RunCommand(ctx, bson.D{{
		Key:   "refreshSessions",
		Value: []bson.M{{"id": sessID}},
	}})
}

// startSessionRefreshWorker starts a background worker which will refresh the session with given ID on the given database every d duration. If d is <= 0 then the worker will do nothing (and immediately exit). To request the worker exits, close the returned channel.
func startSessionRefreshWorker(db *mongo.Database, sessID bson.RawValue, d time.Duration) chan<- struct{} {
	// Create the communication channel
	doneC := make(chan struct{})
	// Start the background worker (if necessary)
	if d > 0 {
		go func(doneC <-chan struct{}) {
			// Create a new context and bind it to the done channel
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			defer contextutil.NewChannelLink(doneC, cancel).Stop()
			// Start a timer
			t := time.NewTimer(d)
			defer t.Stop()
			// Loop until done
			for {
				select {
				case <-t.C:
					// Refresh the session
					refreshSession(ctx, db, sessID)
					t.Reset(d)
				case <-doneC:
					// Exit
					return
				}
			}
		}(doneC)
	}
	// Return the channel
	return doneC
}

// recordToBson converts the given record to BSON.
func recordToBson(r record.Record) bson.M {
	g := make(bson.M, len(r))
	for k, v := range r {
		switch x := v.(type) {
		case []byte:
			g[k] = string(x)
		default:
			g[k] = x
		}
	}
	return g
}

// orderToBson converts the given sort order to bson.
func orderToBson(order sort.OrderBy) bson.D {
	s := make(bson.D, 0, len(order))
	for i := range order {
		d := 1
		if order[i].IsDescending() {
			d = -1
		}
		s = append(s, bson.E{
			Key:   order[i].Key,
			Value: d,
		})
	}
	return s
}

// insertBlock inserts docs into t.
func insertBlock(ctx context.Context, t *table, docs []interface{}) error {
	_, err := t.c.InsertMany(ctx, docs, options.InsertMany().SetOrdered(false))
	if err != nil {
		return err
	}
	return nil
}

// toRecordValue returns a (zero-valued) valid Record value for the given type.
func toRecordValue(v interface{}) interface{} {
	switch v.(type) {
	case int:
		return int(0)
	case int8:
		return int8(0)
	case int16:
		return int16(0)
	case int32:
		return int32(0)
	case int64:
		return int64(0)
	case uint:
		return uint(0)
	case uint8:
		return uint8(0)
	case uint16:
		return uint16(0)
	case uint32:
		return uint32(0)
	case uint64:
		return uint64(0)
	case float32, float64:
		return float64(0)
	case bool:
		return false
	case []byte:
		return []byte{}
	default:
		return ""
	}
}

// indexNamesForKey returns a slice of all index names that are indexing the given key, using the given IndexView.
//
// Note: This is intended to be called from DeleteIndex, and so may return an errors.DeleteIndexError error. If you call this in some other context, you may wish to intercept this error.
func indexNamesForKey(ctx context.Context, iv mongo.IndexView, key string) ([]string, error) {
	// Get a cursor for the indices
	cursor, err := iv.List(ctx)
	if err != nil {
		return nil, err
	}
	// Put all the indices into a slice
	var S []bson.M
	if err = cursor.All(ctx, &S); err != nil {
		return nil, err
	}
	// Extract the matching names
	names := make([]string, 0)
	for _, s := range S {
		if k, ok := s["key"]; !ok {
			return nil, errors.DeleteIndexError.New()
		} else if T, ok := k.(primitive.M); !ok || len(T) != 1 {
			return nil, errors.DeleteIndexError.New()
		} else if _, ok = T[key]; ok {
			if name, ok := s["name"]; !ok {
				return nil, errors.DeleteIndexError.New()
			} else if n, ok := name.(string); !ok {
				return nil, errors.DeleteIndexError.New()
			} else {
				names = append(names, n)
			}
		}
	}
	return names, nil
}

// isInternalDb returns true if and only if name is the name of a MongoDB internal database.
func isInternalDb(name string) bool {
	return name == "admin" || name == "config"
}

// isInternal returns true if and only if name is the name of a PCAS internal table.
func isInternalTable(name string) bool {
	return name == dummyTableName
}

//////////////////////////////////////////////////////////////////////
// noCloseSession functions
//////////////////////////////////////////////////////////////////////

// noCloseSession wraps a mongo.Session and panics if EndSession is called.
type noCloseSession struct {
	mongo.Session
}

func (noCloseSession) EndSession(_ context.Context) {
	panic("illegal call to EndSession")
}

//////////////////////////////////////////////////////////////////////
// iterator functions
//////////////////////////////////////////////////////////////////////

// Close prevents future iteration.
func (s *iterator) Close() error {
	if !s.isClosed {
		s.isClosed = true
		ctx, cancel := context.WithTimeout(context.Background(), closeTimeout)
		defer cancel()
		if err := s.cur.Close(ctx); s.err == nil {
			s.err = err
		}
	}
	return s.err
}

// Err returns any errors during iteration.
func (s *iterator) Err() error {
	return s.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *iterator) Next() bool {
	ok, err := s.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *iterator) NextContext(ctx context.Context) (bool, error) {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, s.session)
	// Sanity check
	if s.isClosed || s.err != nil {
		return false, nil
	}
	// Does the cursor have a next record?
	if !s.cur.Next(ctx) {
		s.err = s.cur.Err()
		return false, s.err
	}
	s.hasNext = true
	return true, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *iterator) Scan(dest record.Record) error {
	// Sanity check
	if s.isClosed {
		return errors.IterationFinished.New()
	} else if !s.hasNext {
		return errors.IterationNotStarted.New()
	}
	// Delete any existing keys in dest
	for key := range dest {
		delete(dest, key)
	}
	// Decode and convert the record
	if err := s.cur.Decode(&dest); err != nil {
		return err
	}
	return record.Convert(dest, s.template)
}

//////////////////////////////////////////////////////////////////////
// table functions
//////////////////////////////////////////////////////////////////////

// Close closes the connection to the table.
func (t *table) Close() error {
	return nil
}

// Describe returns a best-guess template for the data in this table.
//
// Note: because mongo is a schema-less database, we return a guess based on a sample of the data available in the table.
func (t *table) Describe(ctx context.Context) (r record.Record, err error) {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Select the sample documents
	var cur *mongo.Cursor
	cur, err = t.c.Find(ctx, bson.D{}, options.Find().SetLimit(describeLimit))
	if err != nil {
		return
	}
	// Defer tidying up
	defer func() {
		// Close the iterator
		if closeErr := cur.Close(ctx); err == nil {
			if closeErr == nil {
				closeErr = cur.Err()
			}
			err = closeErr
		}
		// If the error is non-nil, clear the returned template
		if err != nil {
			r = nil
		}
	}()
	// Start building the template
	r = make(record.Record)
	dest := make(record.Record)
	for cur.Next(ctx) {
		// Delete any existing keys in dest
		for key := range dest {
			delete(dest, key)
		}
		// Decode into dest
		if err = cur.Decode(&dest); err != nil {
			return
		}
		// Copy over the keys and values
		for k, v := range dest {
			if k != "_id" {
				x := toRecordValue(v)
				if y, ok := r[k]; ok {
					x, err = record.CommonValue(y, x)
					if err != nil {
						return
					}
				}
				r[k] = x
			}
		}
	}
	return
}

// CountWhere returns the number of records in the table that satisfy condition "cond" (which may be nil if there are no conditions).
//
// Assumes that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool.
func (t *table) CountWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Prepare the selector
	sel, err := conditionToBson(cond)
	if err != nil {
		return 0, err
	}
	// Close the session on exit
	return t.c.CountDocuments(ctx, sel)
}

// Insert inserts the records from the given iterator into the table.
func (t *table) Insert(ctx context.Context, itr record.Iterator) error {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// mongo has a maximum BSON document size of 16MB. It's entirely possible
	// for a large Insert to exceed this size, in which case mongo
	// unceremoniously drops the connection, resulting in an io.EOF. So we
	// split the Insert into blocks of size less than 16MB. This limit can be
	// avoided by using GridFS.
	const bsonLimit = 16 * 1024 * 1024
	docs := make([]interface{}, 0)
	docssize := 0
	r := record.Record{}
	ok, err := itr.NextContext(ctx)
	for err == nil && ok {
		// Fetch the next record, validate it, and encode it as BSON
		if err = itr.Scan(r); err != nil {
			return err
		} else if ok, err = r.IsValid(); !ok {
			return err
		}
		g := recordToBson(r)
		// Marshal the BSON
		var b []byte
		b, err = bson.Marshal(g)
		if err != nil {
			return errors.RecordUnableToEncodeValue.Wrap(err)
		}
		// Calculate the BSON size
		size := len(b)
		if size >= bsonLimit {
			// This single document is too large
			return errors.RecordUnableToEncodeValue.Wrap(fmt.Errorf("record too large (%d bytes) - BSON encoding exceeds mongo's document size limit (%d bytes)", size, bsonLimit-1))
		}
		// Should we submit what we have?
		if docssize+size >= bsonLimit {
			if err = insertBlock(ctx, t, docs); err != nil {
				return err
			}
			docs = make([]interface{}, 0)
			docssize = 0
		}
		// Move on
		docs = append(docs, g)
		docssize += size
		ok, err = itr.NextContext(ctx)
	}
	if err != nil {
		return err
	}
	// Insert the last block and return
	if len(docs) != 0 {
		if err = insertBlock(ctx, t, docs); err != nil {
			return err
		}
	}
	return itr.Err()
}

// UpdateWhere updates all records in the table that satisfy condition "cond" (which may be nil if there are no conditions) by setting all keys present in "replacement" to the given values. Returns the number of records updated.
//
// Assumes that "replacement" is valid; and that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool.
func (t *table) UpdateWhere(ctx context.Context, replacement record.Record, cond condition.Condition) (int64, error) {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Prepare the selector
	sel, err := conditionToBson(cond)
	if err != nil {
		return 0, err
	}
	// Do the update
	info, err := t.c.UpdateMany(ctx, sel, bson.D{
		{
			Key:   "$set",
			Value: recordToBson(replacement),
		},
	})
	if err != nil {
		return 0, err
	}
	return info.ModifiedCount, nil
}

// SelectWhere returns the results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
//
// Assumes that "template" is valid; that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool; and that "order" is valid.
func (t *table) SelectWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Iterator, error) {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Prepare the selector and sort order
	sel, err := conditionToBson(cond)
	if err != nil {
		return nil, err
	}
	ord := orderToBson(order)
	// Create the cursor
	cur, err := t.c.Find(ctx, sel, options.Find().SetSort(ord).SetNoCursorTimeout(true))
	if err != nil {
		return nil, err
	}
	return &iterator{
		template: template,
		cur:      cur,
		session:  t.session,
	}, nil
}

// SelectWhereLimit returns at most n results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
//
// Assumes that "template" is valid; that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool; that "order" is valid; and that n is a non-negative integer.
func (t *table) SelectWhereLimit(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error) {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Prepare the selector and sort order
	sel, err := conditionToBson(cond)
	if err != nil {
		return nil, err
	}
	ord := orderToBson(order)
	// Create the cursor
	cur, err := t.c.Find(ctx, sel, options.Find().SetSort(ord).SetNoCursorTimeout(true).SetLimit(n))
	if err != nil {
		return nil, err
	}
	return &iterator{
		template: template,
		cur:      cur,
		session:  t.session,
	}, nil
}

// DeleteWhere deletes those records in the table that satisfy condition "cond" (which may be nil if there are no conditions). Returns the number of records deleted.
//
// Assumes that "cond" is either nil, or "cond" is non-nil, valid, and is not of type condition.Bool.
func (t *table) DeleteWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Prepare the selector
	sel, err := conditionToBson(cond)
	if err != nil {
		return 0, err
	}
	// Do the delete
	info, err := t.c.DeleteMany(ctx, sel)
	if err != nil {
		return 0, err
	}
	return info.DeletedCount, nil
}

// AddIndex adds an index on the given key. If an index already exists, this index is unmodified and nil is returned. The driver is free to assume that the key is well-formed.
func (t *table) AddIndex(ctx context.Context, key string) error {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Sanity check
	if key == "_id" {
		return errors.InvalidKey.New()
	}
	// Grab an IndexView
	iv := t.c.Indexes()
	// Specify the index
	model := mongo.IndexModel{
		Keys: bson.D{{Key: key, Value: 1}},
	}
	// Create the index
	_, err := iv.CreateOne(ctx, model)
	return err
}

// DeleteIndex deletes the index on the given key. If no index is present, nil is returned. The driver is free to assume that the key is well-formed.
func (t *table) DeleteIndex(ctx context.Context, key string) error {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Sanity check
	if key == "_id" {
		return errors.InvalidKey.New()
	}
	// Grab an IndexView
	iv := t.c.Indexes()
	// Extract the index names for the key
	names, err := indexNamesForKey(ctx, iv, key)
	if err != nil {
		return err
	}
	// Drop the indices
	for _, name := range names {
		// Drop the index with given name
		_, dropErr := iv.DropOne(ctx, name)
		// Intercept an "IndexNotFound" error. This has code 27.
		if e, ok := dropErr.(mongo.CommandError); ok && e.Code == 27 {
			dropErr = nil
		}
		// Record any error
		if err == nil {
			err = dropErr
		}
	}
	return err
}

// ListIndices lists the keys for which indices are present. The driver is free to assume that the key is well-formed.
func (t *table) ListIndices(ctx context.Context) ([]string, error) {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Grab an IndexView
	iv := t.c.Indexes()
	// Get a cursor for the indices
	cursor, err := iv.List(ctx)
	if err != nil {
		return nil, err
	}
	// Put all the indices into a slice
	var S []bson.M
	if err = cursor.All(ctx, &S); err != nil {
		return nil, err
	}
	// Extract the keys
	keys := make([]string, 0, len(S))
	for _, s := range S {
		k, ok := s["key"]
		if !ok {
			return nil, errors.ListIndexError.New()
		}
		T, ok := k.(primitive.M)
		if !ok || len(T) != 1 {
			return nil, errors.ListIndexError.New()
		}
		for t := range T {
			if t != "_id" {
				keys = append(keys, t)
			}
		}
	}
	return keys, nil
}

// AddKeys updates each record r in the table, adding any keys in rec that are not already present along with the corresponding values. Any keys that are already present in r will be left unmodified.
func (t *table) AddKeys(ctx context.Context, rec record.Record) error {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Handle the trivial case
	if len(rec) == 0 {
		return nil
	}
	// Loop over the keys
	for k, v := range rec {
		// Create a filter matching documents where k is unset
		filter := bson.M{
			k: bson.M{"$exists": false},
		}
		// Create the update for this key
		update := bson.D{
			{
				Key:   "$set",
				Value: recordToBson(record.Record{k: v}),
			},
		}
		// Do the update
		_, err := t.c.UpdateMany(ctx, filter, update)
		if err != nil {
			return err
		}
	}
	return nil
}

// DeleteKeys updates all records in the table, deleting all the specified keys if present.
func (t *table) DeleteKeys(ctx context.Context, keys []string) error {
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, t.session)
	// Handle the trivial case
	if len(keys) == 0 {
		return nil
	}
	// Create a filter which matches everything
	filter := bson.D{}
	// Create the update
	d := bson.D{}
	for _, k := range keys {
		d = append(d, bson.E{Key: k, Value: 1})
	}
	update := bson.D{{Key: "$unset", Value: d}}
	// Do the update
	_, err := t.c.UpdateMany(ctx, filter, update)
	return err
}

//////////////////////////////////////////////////////////////////////
// database functions
//////////////////////////////////////////////////////////////////////

// ListTables returns the names of the tables in the database.
func (d *database) ListTables(ctx context.Context) ([]string, error) {
	ctx = mongo.NewSessionContext(ctx, d.session)
	return d.db.ListCollectionNames(ctx, bson.D{{
		Key: "name",
		Value: bson.D{{
			Key:   "$ne",
			Value: dummyTableName}},
	}})
}

// CreateTable creates a table with the given name in the database. Since mongo is a schema-less database, the template Record will be ignored.
func (d *database) CreateTable(ctx context.Context, name string, _ record.Record) error {
	// Sanity check
	if isInternalTable(name) {
		return errors.OperationNotPermitted.New()
	}
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, d.session)
	// Check whether a collection with this name already exists
	S, err := d.db.ListCollectionNames(ctx, bson.D{{
		Key:   "name",
		Value: name,
	}})
	if err != nil {
		return err
	} else if len(S) != 0 {
		return fmt.Errorf("table \"%s\" already exists", name)
	}
	// Create the collection. There is a race, but it is impossible to avoid.
	return d.db.CreateCollection(ctx, name)
}

// DeleteTable deletes the indicated table from the database. Does not return an error if the table does not exist.
func (d *database) DeleteTable(ctx context.Context, name string) error {
	// Sanity check
	if isInternalTable(name) {
		return errors.OperationNotPermitted.New()
	}
	ctx = mongo.NewSessionContext(ctx, d.session)
	return d.db.Collection(name).Drop(ctx)
}

// RenameTable changes the name of table src in the database to dst.
func (d *database) RenameTable(ctx context.Context, src string, dst string) error {
	// Sanity check
	if isInternalTable(src) || isInternalTable(dst) {
		return errors.OperationNotPermitted.New()
	}
	// Add the session data to the context
	ctx = mongo.NewSessionContext(ctx, d.session)
	// Connect to the "admin" database
	admin := d.c.client.Database(
		"admin",
		options.Database().
			SetReadConcern(d.c.cfg.toReadConcern()).
			SetWriteConcern(d.c.cfg.toWriteConcern()),
	)
	// We need the collections in the form db.tableName
	db := d.db.Name()
	fullSrc := db + "." + src
	fullDst := db + "." + dst
	// Build the command
	command := bson.D{
		{Key: "renameCollection", Value: fullSrc},
		{Key: "to", Value: fullDst},
	}
	// We run the command in the session
	return admin.RunCommand(ctx, command, nil).Err()
}

// ConnectToTable connects to the indicated table in the database.
func (d *database) ConnectToTable(ctx context.Context, name string) (driver.Table, error) {
	// Sanity check
	if isInternalTable(name) {
		return nil, errors.OperationNotPermitted.New()
	}
	// Check if the table exists
	if S, err := d.db.ListCollectionNames(ctx, bson.D{{Key: "name", Value: name}}); err != nil {
		return nil, err
	} else if len(S) == 0 {
		return nil, errors.TableDoesNotExist.New()
	}
	return &table{
		c:       d.db.Collection(name),
		session: noCloseSession{d.session},
	}, nil
}

// Close closes the connection to the database.
func (d *database) Close() error {
	d.m.Lock()
	defer d.m.Unlock()
	// Check if we are already closed
	if !d.isClosed {
		d.isClosed = true
		// Shut down the auto-refresh worker
		close(d.doneC)
		// End the session
		d.session.EndSession(context.Background())
	}
	return nil
}

//////////////////////////////////////////////////////////////////////
// connection functions
//////////////////////////////////////////////////////////////////////

// DriverName returns the name associated with this driver.
func (*connection) DriverName() string {
	return "mongodb"
}

// CreateDatabase creates a database with the given name on this connection.
func (c *connection) CreateDatabase(ctx context.Context, name string) error {
	// We don't allow the creation of MongoDB internal databases
	if isInternalDb(name) {
		return errors.OperationNotPermitted.New()
	}
	// Does the database exist already?
	L, err := c.client.ListDatabaseNames(ctx, bson.D{{
		Key:   "name",
		Value: name,
	}})
	if err != nil {
		return err
	} else if len(L) != 0 {
		return errors.DatabaseAlreadyExists.New()
	}
	// Create the database. There is a race here, but this is unavoidable.
	db := c.client.Database(
		name,
		options.Database().
			SetReadConcern(c.cfg.toReadConcern()).
			SetWriteConcern(c.cfg.toWriteConcern()),
	)
	return db.CreateCollection(ctx, dummyTableName)
}

// DeleteDatabase deletes the indicated database on this connection.
// Does not return an error if the database does not exist.
func (c *connection) DeleteDatabase(ctx context.Context, name string) error {
	// We don't allow deleting internal databases
	if isInternalDb(name) {
		return errors.OperationNotPermitted.New()
	}
	// Connect to the database
	db := c.client.Database(
		name,
		options.Database().
			SetReadConcern(c.cfg.toReadConcern()).
			SetWriteConcern(c.cfg.toWriteConcern()),
	)
	return db.Drop(ctx)
}

// ListDatabases returns the names of the available databases on this connection.
func (c *connection) ListDatabases(ctx context.Context) ([]string, error) {
	return c.client.ListDatabaseNames(ctx, bson.D{{
		Key: "name",
		Value: bson.D{{
			Key:   "$nin",
			Value: []string{"admin", "config"},
		}},
	}})
}

// ConnectToDatabase opens a connection to a mongodb database.
func (c *connection) ConnectToDatabase(ctx context.Context, name string) (driver.Database, error) {
	// We don't allow connections to "admin" or "config"
	if isInternalDb(name) {
		return nil, errors.OperationNotPermitted.New()
	}
	// Check if the database exists
	if S, err := c.client.ListDatabaseNames(ctx, bson.D{{Key: "name", Value: name}}); err != nil {
		return nil, err
	} else if len(S) == 0 {
		return nil, errors.DatabaseDoesNotExist.New()
	}
	// Connect to the database
	db := c.client.Database(
		name,
		options.Database().
			SetReadConcern(c.cfg.toReadConcern()).
			SetWriteConcern(c.cfg.toWriteConcern()),
	)
	// Create a session
	sess, err := c.client.StartSession()
	if err != nil {
		return nil, err
	}
	// Start the auto-refresh worker
	sessID := sess.ID().Lookup("id")
	doneC := startSessionRefreshWorker(db, sessID, c.cfg.SessionRefreshInterval)
	// Wrap the database connection up and return
	return &database{
		c:       c,
		db:      db,
		session: sess,
		doneC:   doneC,
	}, nil
}

// Close closes the connection to mongodb.
func (c *connection) Close() error {
	c.release()
	return nil
}

// Open opens a connection to mongodb.
func Open(ctx context.Context, cfg *ClientConfig) (keyvalue.Connection, error) {
	// If necessary fetch the default config
	if cfg == nil {
		cfg = DefaultConfig()
	}
	// Validate the config
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Fetch (or open) a client connection
	client, release, err := connect(ctx, cfg)
	if err != nil {
		return nil, err
	}
	// Create and wrap the connection
	c := &connection{
		cfg:     cfg,
		client:  client,
		release: release,
	}
	return keyvalue.NewConnection(c), nil
}
