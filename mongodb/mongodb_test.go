// Mongodb_test provides tests for the mongodb driver.

//go:build integration
// +build integration

/*
This test requires mongodb to be running and listening on localhost.

To run, do:
go test -tags=integration
*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mongodb

import (
	"bitbucket.org/pcas/keyvalue/internal/drivertest"
	"context"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

// contains returns true iff s is contained in S.
func contains(S []string, s string) bool {
	for _, x := range S {
		if x == s {
			return true
		}
	}
	return false
}

func TestDriver(t *testing.T) {
	// Create the config
	cfg := DefaultConfig()
	cfg.AppName = "keyvalue.mongodb_test"
	cfg.Hosts = []string{"localhost:27017"}
	cfg.ReadConcern = RMajority
	cfg.WriteJournal = true
	// Create a context to run the tests
	ctx, cancel := context.WithTimeout(context.Background(), 40*time.Second)
	defer cancel()
	// Connect to mongo
	c, err := Open(ctx, cfg)
	require.NoError(t, err, "error opening connection")
	// Run the tests
	drivertest.Run(ctx, c, t)
	// Close the connection
	err = c.Close()
	require.NoError(t, err, "error closing connection")
}
