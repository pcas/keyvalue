// Condition.go implements conditions for mongodb.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mongodb

import (
	"bitbucket.org/pcas/keyvalue/condition"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// convertValue sanitises a record value for bson.
func convertValue(v interface{}) interface{} {
	switch x := v.(type) {
	case []byte:
		return string(x)
	default:
		return x
	}
}

// formatCondition recursively converts the condition c to a bson document.
func formatCondition(c condition.Condition) (interface{}, error) {
	switch cond := c.(type) {
	case condition.Bool: // True or False
		return bool(cond), nil
	case *condition.LeafOp: // LeafOp
		var op string
		switch cond.OpCode() {
		case condition.Eq:
			op = "$eq"
		case condition.Ne:
			op = "$ne"
		case condition.Lt:
			op = "$lt"
		case condition.Gt:
			op = "$gt"
		case condition.Le:
			op = "$lte"
		case condition.Ge:
			op = "$gte"
		}
		return bson.M{
			cond.Lhs(): bson.M{op: convertValue(cond.Rhs())},
		}, nil
	case *condition.IsOp: // IS
		return bson.M{
			cond.Lhs(): bson.M{"$eq": cond.Value()},
		}, nil
	case *condition.InOp: // IN
		xs := cond.Values()
		vals := make([]interface{}, 0, len(xs))
		for _, x := range xs {
			vals = append(vals, convertValue(x))
		}
		return bson.M{
			cond.Lhs(): bson.M{"$in": vals},
		}, nil
	case *condition.NotInOp: // NOT IN
		xs := cond.Values()
		vals := make([]interface{}, 0, len(xs))
		for _, x := range xs {
			vals = append(vals, convertValue(x))
		}
		return bson.M{
			cond.Lhs(): bson.M{"$nin": vals},
		}, nil
	case *condition.BetweenOp: // BETWEEN
		return bson.M{
			cond.Lhs(): bson.M{
				"$gte": convertValue(cond.Lower()),
				"$lte": convertValue(cond.Upper()),
			},
		}, nil
	case *condition.NotBetweenOp: // NOT BETWEEN
		return bson.M{
			cond.Lhs(): bson.M{
				"$not": bson.M{
					"$gte": convertValue(cond.Lower()),
					"$lte": convertValue(cond.Upper()),
				},
			},
		}, nil
	case *condition.AndOp: // AND
		xs := cond.Conditions()
		vals := make([]interface{}, 0, len(xs))
		for _, x := range xs {
			j, err := formatCondition(x)
			if err != nil {
				return nil, err
			}
			vals = append(vals, j)
		}
		return bson.M{
			"$and": vals,
		}, nil
	case *condition.OrOp: // OR
		xs := cond.Conditions()
		vals := make([]interface{}, 0, len(xs))
		for _, x := range xs {
			j, err := formatCondition(x)
			if err != nil {
				return nil, err
			}
			vals = append(vals, j)
		}
		return bson.M{
			"$or": vals,
		}, nil
	default:
		return nil, fmt.Errorf("unknown condition: %T", c)
	}
}

// conditionToBson converts the given Condition to a format suitable for mongo.
func conditionToBson(c condition.Condition) (bson.M, error) {
	if c == nil {
		return bson.M{}, nil
	}
	b, err := formatCondition(c)
	if err != nil {
		return nil, err
	}
	x, ok := b.(bson.M)
	if !ok {
		return nil, fmt.Errorf("condition conversion failed with unexpected type: %T", b)
	}
	return x, nil
}
