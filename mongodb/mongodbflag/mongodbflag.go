// Mongodbflag provides a standard set of command line flags for the mongodb driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mongodbflag

import (
	"bitbucket.org/pcas/keyvalue/mongodb"
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/flag"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// The environment variables consulted
const (
	EnvUsername = "PCAS_MONGO_USERNAME"
	EnvPassword = "PCAS_MONGO_PASSWORD"
	EnvHost     = "PCAS_MONGO_HOST"
)

// Descriptions of environment variables.
const (
	unset       = "unset"
	setButEmpty = "set, but empty"
)

// writeConcernFlag defines a flag for setting the MongoDB write concern. It satisfies the flag.Flag interface.
type writeConcernFlag struct {
	writeConcern *string // The write concern for this flag, if it is "majority"
	writeN       *int    // The write concern for this flag, if it is an integer
}

// hostsFlag defines a flag for setting the MongoDB hosts. It satisfies the flag.Flag interface.
type hostsFlag struct {
	hosts *[]string // The hosts
}

// passwordFlag defines a flag for setting the MongoDB password. It satisfies the flag.Flag interface.
type passwordFlag struct {
	password *string // The password
	toggle   *bool   // Toggle will be set to true on parse
}

// usernameFlag defines a flag for setting the MongoDB username. It satisfies the flag.Flag interface.
type usernameFlag struct {
	username *string // The username
}

// Set represents a set of command-line flags defined by a client config.
type Set struct {
	c     *mongodb.ClientConfig // The client config
	flags []flag.Flag           // The flags
}

/////////////////////////////////////////////////////////////////////////
// writeConcernFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*writeConcernFlag) Name() string {
	return "mongo-write-concern"
}

// Description returns a one-line description of this variable.
func (f *writeConcernFlag) Description() string {
	def := *f.writeConcern
	if len(def) == 0 {
		def = strconv.Itoa(*f.writeN)
	}
	return fmt.Sprintf("The MongoDB write concern (default: \"%s\")", def)
}

// Usage returns long-form usage text.
func (f *writeConcernFlag) Usage() string {
	return fmt.Sprintf("The possible values for the flag -%s are \"%s\" and N, where N is a positive integer. \"%s\" requests acknowledgement that write operations propagate to the majority of mongod instances. N requests acknowledgement that write operations propagate to N mongod instances.", f.Name(), mongodb.WMajority, mongodb.WMajority)
}

// Parse parses the string.
func (f *writeConcernFlag) Parse(in string) error {
	var err error
	var n int
	s := strings.ToLower(in)
	switch s {
	case mongodb.WMajority:
		*(f.writeConcern) = in
		*(f.writeN) = 0
	default:
		n, err = convert.ToInt(s)
		*(f.writeConcern) = ""
		*(f.writeN) = n
	}
	return err
}

// newWriteConcernFlag returns a writeConcernFlag. It has backing variables writeConcern and writeN.
func newWriteConcernFlag(writeConcern *string, writeN *int) flag.Flag {
	return &writeConcernFlag{
		writeConcern: writeConcern,
		writeN:       writeN,
	}
}

/////////////////////////////////////////////////////////////////////////
// hostsFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*hostsFlag) Name() string {
	return "mongo-host"
}

// Description returns a one-line description of this variable.
func (f *hostsFlag) Description() string {
	return fmt.Sprintf("The MongoDB hosts (default: \"%s\")", strings.Join(*f.hosts, ","))
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *hostsFlag) Usage() string {
	// read the environment variable
	var status string
	if val, ok := os.LookupEnv(EnvHost); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	} else {
		status = "\"" + val + "\""
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvHost, status)
}

// Parse parses the string.
func (f *hostsFlag) Parse(in string) error {
	*(f.hosts) = strings.Split(in, ",")
	return nil
}

// newHostsFlag returns a new hostsFlag. It has backing variable hosts.
func newHostsFlag(hosts *[]string) flag.Flag {
	return &hostsFlag{
		hosts: hosts,
	}
}

/////////////////////////////////////////////////////////////////////////
// passwordFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*passwordFlag) Name() string {
	return "mongo-password"
}

// Description returns a one-line description of this variable.
func (f *passwordFlag) Description() string {
	return fmt.Sprintf("The MongoDB password (default: \"%s\")", *f.password)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *passwordFlag) Usage() string {
	// read the environment variable
	status := "set"
	if val, ok := os.LookupEnv(EnvPassword); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvPassword, status)
}

// Parse parses the string.
func (f *passwordFlag) Parse(in string) error {
	*f.password = in
	*f.toggle = true
	return nil
}

// newPasswordFlag returns a new passwordFlag. It has backing variable password, and sets toggle to true on parse.
func newPasswordFlag(password *string, toggle *bool) flag.Flag {
	return &passwordFlag{
		password: password,
		toggle:   toggle,
	}
}

/////////////////////////////////////////////////////////////////////////
// usernameFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*usernameFlag) Name() string {
	return "mongo-username"
}

// Description returns a one-line description of this variable.
func (f *usernameFlag) Description() string {
	return fmt.Sprintf("The MongoDB username (default: \"%s\")", *f.username)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *usernameFlag) Usage() string {
	// read the environment variable
	var status string
	if val, ok := os.LookupEnv(EnvUsername); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	} else {
		status = "\"" + val + "\""
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvUsername, status)
}

// Parse parses the string.
func (f *usernameFlag) Parse(in string) error {
	*f.username = in
	return nil
}

// newUsernameFlag returns a new usernameFlag. It has backing variable username.
func newUsernameFlag(username *string) flag.Flag {
	return &usernameFlag{
		username: username,
	}
}

/////////////////////////////////////////////////////////////////////////
// Set functions
/////////////////////////////////////////////////////////////////////////

// NewSet returns a set of command-line flags with defaults given by c. If c is nil then the mongodb.DefaultConfig() will be used. Note that this does not update the default client config, nor does this update c. To recover the updated client config after parse, call the ClientConfig() method on the returned set.
func NewSet(c *mongodb.ClientConfig) *Set {
	// Either move to the default client config, or move to a copy
	if c == nil {
		c = mongodb.DefaultConfig()
	} else {
		c = c.Copy()
	}
	// Create the usage messages for -mongo-read-concern
	readConcernUsage := fmt.Sprintf("The possible values for the flag -mongo-read-concern are \"%s\", \"%s\", \"%s\", and \"%s\". Available specifies that the query should return data from the instance with no guarantee that the data has been written to a majority of the replica set members (i.e. may be rolled back). Linearizable specifies that the query should return data that reflects all successful writes issued with a write concern of 'majority' and acknowledged prior to the start of the read operation. Local specifies that the query should return the instance’s most recent data. Majority specifies that the query should return the instance’s most recent data acknowledged as having been written to a majority of members in the replica set.", mongodb.RAvailable, mongodb.RLinearizable, mongodb.RLocal, mongodb.RMajority)
	// Create the flags
	flags := []flag.Flag{
		newHostsFlag(&c.Hosts),
		flag.Bool(
			"mongo-journal",
			&c.WriteJournal, c.WriteJournal,
			"Require acknowledgement of journal writes",
			"",
		),
		newPasswordFlag(&c.Password, &c.PasswordSet),
		flag.Int(
			"mongo-pool-size",
			&c.PoolSize, c.PoolSize,
			"The MongoDB connection pool size",
			"",
		),
		flag.String(
			"mongo-read-concern",
			&c.ReadConcern, c.ReadConcern,
			"The MongoDB read concern",
			readConcernUsage,
		),
		flag.Duration(
			"mongo-refresh-interval",
			&c.SessionRefreshInterval, c.SessionRefreshInterval,
			"The MongoDB session refresh interval",
			"",
		),
		newUsernameFlag(&c.Username),
		newWriteConcernFlag(&c.WriteConcern, &c.WriteN),
	}
	// Return the set
	return &Set{
		c:     c,
		flags: flags,
	}
}

// Flags returns the members of the set.
func (s *Set) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	flags := make([]flag.Flag, len(s.flags))
	copy(flags, s.flags)
	return flags
}

// Name returns the name of this collection of flags.
func (*Set) Name() string {
	return "MongoDB options"
}

// UsageFooter returns the footer for the usage message for this flag set.
func (*Set) UsageFooter() string {
	return ""
}

// UsageHeader returns the header for the usage message for this flag set.
func (*Set) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *Set) Validate() error {
	if s == nil {
		return errors.New("flag set is nil")
	}
	return s.c.Validate()
}

// ClientConfig returns the client config described by this set. This should only be called after the set has been successfully validated.
func (s *Set) ClientConfig() *mongodb.ClientConfig {
	if s == nil {
		return mongodb.DefaultConfig()
	}
	return s.c.Copy()
}

// SetDefault sets the default client config as described by this set. The given app name. This should only be called after the set has been successfully validated.
func (s *Set) SetDefault(name string) {
	c := s.ClientConfig()
	c.AppName = name
	mongodb.SetDefaultConfig(c)
}
