// Database defines a connection to a database.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"io"
	"sort"
	"sync"
	"time"
)

// Database provides a connection to a database.
type Database interface {
	metrics.Metricser
	log.Logger
	io.Closer
	// Name return the database name.
	Name() string
	// ListTables returns the names of the tables in the database. The names
	// are sorted in increasing order.
	ListTables(ctx context.Context) ([]string, error)
	// CreateTable creates a table with the given name in the database.
	//
	// The provided template will be used by the underlying storage-engine
	// to create the new table if appropriate; the storage-engine is free to
	// ignore the template if it is not required (for example, in a
	// schema-less database).
	CreateTable(ctx context.Context, name string, template record.Record) error
	// DeleteTable deletes the indicated table from the database. Does not
	// return an error if the table does not exist.
	DeleteTable(ctx context.Context, name string) error
	// RenameTable changes the name of a table in the database.
	RenameTable(ctx context.Context, oldname string, newname string) error
	// ConnectToTable connects to the indicated table in the database.
	ConnectToTable(ctx context.Context, name string) (Table, error)
}

// database implements the Database interface.
type database struct {
	name     string            // The database name
	d        driver.Database   // The underlying connection to the database
	cache    tableCache        // A cache of tables
	s        closeSet          // The set of open tables
	closef   func()            // The close function
	lg       log.Interface     // The logger
	met      metrics.Interface // The metrics endpoint
	m        sync.RWMutex      // Mutex controlling access to the following
	isClosed bool              // Is the database closed?
	err      error             // The error on close (if any)
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// connectToTable attempts to connect to the named table using the database d.
func connectToTable(ctx context.Context, d *database, name string) (Table, error) {
	// Connect to the table
	tt, err := d.cache.Get(ctx, name)
	if err != nil {
		return nil, err
	}
	// Wrap the connection up as a *Table
	t := &table{
		name: name,
		t:    tt,
		lg:   log.PrefixWith(d.Log(), "[table=%s]", name),
		met:  metrics.TagWith(d.Metrics(), metrics.Tags{"Table": name}),
	}
	// Add the wrapped table to the set of open tables
	t.closef = d.s.Include(t)
	return t, nil
}

//////////////////////////////////////////////////////////////////////
// database functions
//////////////////////////////////////////////////////////////////////

// Name return the database name.
func (d *database) Name() string {
	if d == nil {
		return ""
	}
	return d.name
}

// Close closes the connection to the database.
func (d *database) Close() error {
	// Sanity check
	if d == nil || d.d == nil {
		return nil
	}
	err := func() (err error) {
		// Acquire a write lock
		d.m.Lock()
		defer d.m.Unlock()
		// Is there anything to do?
		if !d.isClosed {
			// Recover from any panics in the driver
			defer func() {
				if e := recover(); e != nil {
					err = fmt.Errorf("panic in Close: %v", e)
				}
			}()
			// Close the open tables
			d.s.Close()
			// Close the cache
			d.cache.Close()
			// Close the underlying database and mark us as closed
			d.err, d.isClosed = d.d.Close(), true
			// Finally call the close function
			if d.closef != nil {
				d.closef()
			}
		}
		// Note and errors and return
		err = d.err
		return
	}()
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.DatabaseCloseError.Wrap(err)
	}
	// Provide log data before returning
	if err == nil {
		d.Log().Printf("Connection to database closed")
	} else {
		d.Log().Printf("Connection to database closed with error: %v", err)
	}
	return err
}

// ListTables returns the names of the tables in the database. The names are sorted in increasing order.
func (d *database) ListTables(ctx context.Context) ([]string, error) {
	// Sanity check
	if d == nil || d.d == nil {
		return nil, errors.DatabaseClosed.New()
	}
	now := time.Now()
	S, err := func() (S []string, err error) {
		// Acquire a read lock
		d.m.RLock()
		defer d.m.RUnlock()
		// Are we closed?
		if d.isClosed {
			err = errors.DatabaseClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in ListTables: %v", e)
			}
		}()
		// List the tables
		S, err = d.d.ListTables(ctx)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToListTables.Wrap(err)
	}
	// Provide log and metric data
	submitPoint(d.Metrics(), "ListTables", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err != nil {
		d.Log().Printf("ListTables failed after %s: %v", dur, err)
		return nil, err
	}
	d.Log().Printf("ListTables succeeded in %s", dur)
	// Sort the tables before returning
	sort.Strings(S)
	return S, nil
}

// CreateTable creates a table with the given name in the database.
//
// The provided template will be used by the underlying storage-engine to create the new table if appropriate; the storage-engine is free to ignore the template if it is not required (for example, in a schema-less database).
func (d *database) CreateTable(ctx context.Context, name string, template record.Record) error {
	// Sanity check
	if d == nil || d.d == nil {
		return errors.DatabaseClosed.New()
	} else if ok, err := template.IsValid(); !ok {
		return errors.TemplateFailsToValidate.Wrap(err)
	} else if len(name) == 0 {
		return errors.InvalidTableName.New()
	}
	now := time.Now()
	err := func() (err error) {
		// Acquire a read lock
		d.m.RLock()
		defer d.m.RUnlock()
		// Are we closed?
		if d.isClosed {
			err = errors.DatabaseClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in CreateTable: %v", e)
			}
		}()
		// Create the table
		err = d.d.CreateTable(ctx, name, template)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.CreateTableError.Wrap(err)
	}
	// Provide log and metric data
	submitPoint(d.Metrics(), "CreateTable", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		d.Log().Printf("Create table \"%s\" succeeded in %s", name, dur)
	} else {
		d.Log().Printf("Create table \"%s\" failed after %s: %v", name, dur, err)
	}
	return err
}

// DeleteTable deletes the indicated table from the database. Does not return an error if the table does not exist.
func (d *database) DeleteTable(ctx context.Context, name string) error {
	// Sanity check
	if d == nil || d.d == nil {
		return errors.DatabaseClosed.New()
	} else if len(name) == 0 {
		return errors.InvalidTableName.New()
	}
	now := time.Now()
	err := func() (err error) {
		// Acquire a write lock
		d.m.Lock()
		defer d.m.Unlock()
		// Are we closed?
		if d.isClosed {
			err = errors.DatabaseClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in DeleteTable: %v", e)
			}
		}()
		// Delete the table and mark the cache as stale
		err = d.d.DeleteTable(ctx, name)
		d.cache.MarkAsStale(name)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.DeleteTableError.Wrap(err)
	}
	// Provide log and metric data
	submitPoint(d.Metrics(), "DeleteTable", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		d.Log().Printf("Delete table \"%s\" succeeded in %s", name, dur)
	} else {
		d.Log().Printf("Delete table \"%s\" failed after %s: %v", name, dur, err)
	}
	return err
}

// RenameTable changes the name of a table in the database.
func (d *database) RenameTable(ctx context.Context, oldname string, newname string) error {
	// Sanity check
	if d == nil || d.d == nil {
		return errors.DatabaseClosed.New()
	} else if len(oldname) == 0 || len(newname) == 0 {
		return errors.InvalidTableName.New()
	}
	now := time.Now()
	err := func() (err error) {
		// Acquire a write lock
		d.m.Lock()
		defer d.m.Unlock()
		// Are we closed?
		if d.isClosed {
			err = errors.DatabaseClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in RenameTable: %v", e)
			}
		}()
		// Rename the table and mark the cache as stale
		err = d.d.RenameTable(ctx, oldname, newname)
		d.cache.MarkAsStale(oldname)
		d.cache.MarkAsStale(newname)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.RenameTableError.Wrap(err)
	}
	// Provide log and metric data
	submitPoint(d.Metrics(), "RenameTable", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		d.Log().Printf("Rename table (\"%s\"->\"%s\") succeeded in %s", oldname, newname, dur)
	} else {
		d.Log().Printf("Rename table (\"%s\"->\"%s\") failed after %s: %v", oldname, newname, dur, err)
	}
	return err
}

// ConnectToTable connects to the indicated table in the database.
func (d *database) ConnectToTable(ctx context.Context, name string) (Table, error) {
	// Sanity check
	if d == nil || d.d == nil {
		return nil, errors.ConnectionClosed.New()
	} else if len(name) == 0 {
		return nil, errors.InvalidTableName.New()
	}
	now := time.Now()
	t, err := func() (t Table, err error) {
		// Acquire a read lock
		d.m.RLock()
		defer d.m.RUnlock()
		// Are we closed?
		if d.isClosed {
			err = errors.DatabaseClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in ConnectToTable: %v", e)
			}
		}()
		// Connect to the table
		t, err = connectToTable(ctx, d, name)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToConnectToTable.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(d.Metrics(), "ConnectToTable", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err != nil {
		d.Log().Printf("Connection to table \"%s\" failed after %s: %v", name, dur, err)
		return nil, err
	}
	d.Log().Printf("Connected to table \"%s\" in %s", name, dur)
	return t, nil
}

// Log returns the log for this database.
func (d *database) Log() log.Interface {
	if d == nil || d.lg == nil {
		return log.Discard
	}
	return d.lg
}

// Metrics returns the metrics endpoint for this database.
func (d *database) Metrics() metrics.Interface {
	if d == nil || d.met == nil {
		return metrics.Discard
	}
	return d.met
}
