// Records provides an iterator for iterating over results.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcastools/log"
	"context"
	"sort"
	"time"
)

// recordIterator describes the records returned by a query to a table. This implements the record.Iterator interface.
type recordIterator struct {
	record.Iterator
	keys     []string      // The keys
	start    time.Time     // The time the iterator was created
	n        int64         // The number of records iterated over
	isClosed bool          // Have we closed?
	closef   func()        // The close function
	err      error         // The error on close (if any)
	lg       log.Interface // The log
}

//////////////////////////////////////////////////////////////////////
// recordIterator functions
//////////////////////////////////////////////////////////////////////

// Close closes the recordIterator, preventing further iteration. Close is idempotent (so can be called repeatedly and will return the same result every time).
func (rs *recordIterator) Close() error {
	// Is there anything to do?
	if !rs.isClosed {
		// Mark us as closed and close the underlying iterator
		rs.isClosed = true
		rs.err = rs.Iterator.Close()
		// Call the close function
		if rs.closef != nil {
			rs.closef()
		}
		// Provide some logging feedback
		dur := time.Since(rs.start)
		if rs.err == nil {
			rs.Log().Printf("Iterator closed after %s and %d iterations", dur, rs.Count())
		} else {
			rs.Log().Printf("Iterator closed after %s and %d iterations with error: %v", dur, rs.Count(), rs.err)
		}
	}
	// Return any close error
	return rs.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (rs *recordIterator) Next() bool {
	ok := rs.Iterator.Next()
	if ok {
		// Give some feedback
		if rs.n == 0 {
			rs.Log().Printf("Initial record fetched")
		} else {
			rs.Log().Printf("Fetched record %d. %s elapsed since iteration started.", rs.n+1, time.Since(rs.start))
		}
		// Increment the count (ignore the possibility of overflow)
		rs.n++
	}
	return ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (rs *recordIterator) NextContext(ctx context.Context) (bool, error) {
	ok, err := rs.Iterator.NextContext(ctx)
	if err == nil && ok {
		// Give some feedback
		if rs.n == 0 {
			rs.Log().Printf("Initial record fetched")
		} else {
			rs.Log().Printf("Fetched record %d. %s elapsed since iteration started.", rs.n+1, time.Since(rs.start))
		}
		// Increment the count (ignore the possibility of overflow)
		rs.n++
	}
	return ok, err
}

// Count returns the number of record iterated over.
func (rs *recordIterator) Count() int64 {
	return rs.n
}

// Keys returns the keys as specified by the original template record. The keys are returned sorted using sort.Strings.
func (rs *recordIterator) Keys() []string {
	// Make a copy of the keys
	keys := make([]string, len(rs.keys))
	copy(keys, rs.keys)
	// Sort the keys and return
	sort.Strings(keys)
	return keys
}

// Log returns the log for these records.
func (rs *recordIterator) Log() log.Interface {
	if rs.lg == nil {
		return log.Discard
	}
	return rs.lg
}
