// DBCache defines a cache of driver.Databases.
/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/internal/cache"
	"bitbucket.org/pcastools/log"
	"context"
)

// dbWrapper wraps a driver.Database.
type dbWrapper struct {
	driver.Database
	release func() // The release function
}

// dbCache is a cache of driver.Databases.
type dbCache struct {
	cache *cache.Cache[driver.Database] // The underlying cache
}

//////////////////////////////////////////////////////////////////////
// dbWrapper functions
//////////////////////////////////////////////////////////////////////

// Close releases our hold on the database.
func (d *dbWrapper) Close() error {
	d.release()
	return nil
}

//////////////////////////////////////////////////////////////////////
// dbCache functions
//////////////////////////////////////////////////////////////////////

// Close closes the cache.
func (c dbCache) Close() error {
	return c.cache.Close()
}

// Get returns a (cached) database connection to 'name'.
func (c dbCache) Get(ctx context.Context, name string) (driver.Database, error) {
	e, err := c.cache.Get(ctx, name)
	if err != nil {
		return nil, err
	}
	return &dbWrapper{
		Database: e.Value(),
		release:  e.Release,
	}, nil
}

// MarkAsStale marks any cached database connection to 'name' as stale.
func (c dbCache) MarkAsStale(name string) {
	c.cache.MarkAsStale(name)
}

// newDBCache returns a new database cache for the given connection.
func newDBCache(c driver.Connection, lg log.Interface) dbCache {
	return dbCache{
		cache: cache.New(
			c.ConnectToDatabase,
			lg,
		),
	}
}
