// Iterator defines an iterator for a record.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package record

import (
	"bitbucket.org/pcas/keyvalue/errors"
	"context"
	"fmt"
	"sync/atomic"
	"time"
)

// ConversionFunc returns a record for the given object x.
type ConversionFunc[T any] func(x T) (Record, error)

// Iterator defines the interface satisfied by a Record iterator.
type Iterator interface {
	// Close closes the iterator, preventing further iteration.
	Close() error
	// Err returns the last error, if any, encountered during iteration. Err
	// may be called after Close.
	Err() error
	// Next advances the iterator. Returns true on successful advance of the
	// iterator; false otherwise. Next or NextContext must be called
	// before the first call to Scan.
	Next() bool
	// NextContext advances the iterator. Returns true on successful
	// advance of the iterator; false otherwise. Next or NextContext
	// must be called before the first call to Scan.
	NextContext(ctx context.Context) (bool, error)
	// Scan copies the current record into "dest". Any previously set keys
	// or values in "dest" will be deleted or overwritten.
	Scan(dest Record) error
}

// basicIterator provides a simply way to convert channels to an iterator.
type basicIterator struct {
	isClosed bool            // Have we closed?
	err      error           // The last error (if any)
	next     Record          // The next record
	resC     <-chan Record   // The records provided by the worker
	errC     <-chan error    // Any error reported by the worker
	doneC    chan<- struct{} // Shutdown the worker
}

// emptyIterator is an empty iterator.
type emptyIterator struct {
	hasCalledNext bool // Has the user called Next or NextContext
}

// sliceIterator allows iteration over a Record slice.
type sliceIterator struct {
	s   []Record // The underlying slice of records
	idx int      // The current index in the slice
}

// sliceIteratorWithConversionFunc allows iteration over a slice.
type sliceIteratorWithConversionFunc[T any] struct {
	s   []T               // The underlying slice
	idx int               // The current index in the slice
	f   ConversionFunc[T] // The conversion function
	err error             // The last error (if any)
}

// limitIterator wraps an Iterator, limiting the number of Records returned.
type limitIterator struct {
	Iterator
	n int64 // The remaining number of records to return
}

// limitedSizeIterator wraps an Iterator, limiting the total number of bytes of the returned Records.
type limitedSizeIterator struct {
	Iterator
	n        int    // The remaining number of bytes
	r        Record // The current record
	isClosed bool   // Have we closed?
}

// DurationIterator wraps an Iterator, limiting the total duration over which Records are returned.
type DurationIterator struct {
	Iterator
	t          *time.Timer // The timer over which we iterate
	shouldStop atomic.Bool // Should we stop?
	isClosed   bool        // Have we closed?
}

// CountIterator wraps an Iterator, counting the number of entries iterated over.
type CountIterator struct {
	Iterator
	n int64 // The number of records iterated over
}

/////////////////////////////////////////////////////////////////////////
// basicIterator functions
/////////////////////////////////////////////////////////////////////////

// NewIterator returns an iterator that reads from the channel resC. Iteration will halt when resC closes. Any error in errC will be returned to the user. Iteration should stop if doneC is closed. A typical implementation looks like:
//
//	// Create the communication channels
//	resC := make(chan record.Record) // Buffer this channel if required
//	errC := make(chan error)
//	doneC := make(chan struct{})
//	// Start the background worker running
//	go func() {
//		// Feed resC with records
//		var err error
//		for {
//			r := record.Record{}
//			// ...
//			// Handle any errors
//			if err != nil {
//				break
//			}
//			// Pass the record down the channel
//			select {
//			case <-doneC:
//				break // We've been asked to exit
//			case resC <- r:
//			}
//		}
//		// Iteration has finished, so tidy up
//		close(resC)
//		if err != nil {
//			errC <- err
//		}
//		close(errC)
//	}()
//	// Create the iterator
//	itr := record.NewIterator(resC, errC, doneC)
func NewIterator(resC <-chan Record, errC <-chan error, doneC chan<- struct{}) Iterator {
	return &basicIterator{
		resC:  resC,
		errC:  errC,
		doneC: doneC,
	}
}

// Close prevents future iteration.
func (s *basicIterator) Close() error {
	if !s.isClosed {
		close(s.doneC)
		if err := <-s.errC; err != nil && s.err == nil {
			if errors.ShouldWrap(err) {
				err = errors.IterationCloseError.Wrap(err)
			}
			s.err = err
		}
		s.isClosed = true
		s.next = nil
	}
	return s.err
}

// Err returns the last error on iteration (if any).
func (s *basicIterator) Err() error {
	return s.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *basicIterator) Next() bool {
	ok, err := s.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *basicIterator) NextContext(ctx context.Context) (bool, error) {
	if s.isClosed || s.err != nil {
		return false, nil
	}
	var ok bool
	var r Record
	select {
	case r, ok = <-s.resC:
	case <-ctx.Done():
		s.err = ctx.Err()
	}
	if !ok {
		s.Close()
		return false, s.err
	}
	s.next = r
	return true, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *basicIterator) Scan(dest Record) error {
	if s.isClosed {
		return errors.IterationFinished.New()
	} else if s.next == nil {
		return errors.IterationNotStarted.New()
	}
	Scan(dest, s.next)
	return nil
}

/////////////////////////////////////////////////////////////////////////
// emptyIterator functions
/////////////////////////////////////////////////////////////////////////

// EmptyIterator returns an empty iterator. Err and Close will always return nil.
func EmptyIterator() Iterator {
	return &emptyIterator{}
}

// Close prevents future iteration. Always returns nil.
func (*emptyIterator) Close() error {
	return nil
}

// Err always returns nil.
func (*emptyIterator) Err() error {
	return nil
}

// Buffered always returns true.
func (*emptyIterator) Buffered() bool {
	return true
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *emptyIterator) Next() bool {
	s.hasCalledNext = true
	return false
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *emptyIterator) NextContext(_ context.Context) (bool, error) {
	s.hasCalledNext = true
	return false, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *emptyIterator) Scan(dest Record) error {
	if !s.hasCalledNext {
		return errors.IterationNotStarted.New()
	}
	return errors.IterationFinished.New()
}

/////////////////////////////////////////////////////////////////////////
// sliceIterator functions
/////////////////////////////////////////////////////////////////////////

// SliceIterator returns an iterator for the given slice. There is no need to call Close on the returned iterator. Err and Close will always return nil.
func SliceIterator(S []Record) Iterator {
	return &sliceIterator{
		s:   S,
		idx: -1,
	}
}

// Close prevents future iteration. Always returns nil.
func (s *sliceIterator) Close() error {
	s.idx = len(s.s)
	return nil
}

// Err always returns nil.
func (*sliceIterator) Err() error {
	return nil
}

// Buffered always returns true.
func (*sliceIterator) Buffered() bool {
	return true
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *sliceIterator) Next() bool {
	n := len(s.s)
	if s.idx < n {
		s.idx++
	}
	return s.idx < n
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *sliceIterator) NextContext(_ context.Context) (bool, error) {
	return s.Next(), nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *sliceIterator) Scan(dest Record) error {
	if s.idx == -1 {
		return errors.IterationNotStarted.New()
	} else if s.idx == len(s.s) {
		return errors.IterationFinished.New()
	}
	Scan(dest, s.s[s.idx])
	return nil
}

/////////////////////////////////////////////////////////////////////////
// sliceIteratorWithConversionFunc functions
/////////////////////////////////////////////////////////////////////////

// execConversionFunc executes the conversion function, recovering from any panics.
func execConversionFunc[T any](f ConversionFunc[T], x T) (r Record, err error) {
	// Recover from any panics caused by the user's conversion function f
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in user-provided ConversionFunc: %v", e)
		}
	}()
	// Execute the conversion function
	r, err = f(x)
	return
}

// SliceIteratorWithConversionFunc returns an iterator for the slice S, where the function f is used to convert the elements in S to records.
func SliceIteratorWithConversionFunc[T any](S []T, f ConversionFunc[T]) Iterator {
	return &sliceIteratorWithConversionFunc[T]{
		s:   S,
		idx: -1,
		f:   f,
	}
}

// Close prevents future iteration.
func (s *sliceIteratorWithConversionFunc[_]) Close() error {
	s.idx = len(s.s)
	return s.err
}

// Err returns the last error (if any).
func (s *sliceIteratorWithConversionFunc[_]) Err() error {
	return s.err
}

// Buffered always returns true.
func (*sliceIteratorWithConversionFunc[_]) Buffered() bool {
	return true
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *sliceIteratorWithConversionFunc[_]) Next() bool {
	if s.err != nil {
		return false
	}
	n := len(s.s)
	if s.idx < n {
		s.idx++
	}
	return s.idx < n
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *sliceIteratorWithConversionFunc[_]) NextContext(_ context.Context) (bool, error) {
	return s.Next(), nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *sliceIteratorWithConversionFunc[_]) Scan(dest Record) error {
	// Sanity check
	if s.idx == -1 {
		return errors.IterationNotStarted.New()
	} else if s.idx == len(s.s) {
		return errors.IterationFinished.New()
	} else if s.err != nil {
		return s.err
	}
	// Convert the current item to a record
	r, err := execConversionFunc(s.f, s.s[s.idx])
	if err != nil {
		if errors.ShouldWrap(err) {
			err = errors.RecordUnableToConvertType.Wrap(err)
		}
		s.err = err
		return s.err
	}
	// Copy over the record
	Scan(dest, r)
	return nil
}

/////////////////////////////////////////////////////////////////////////
// limitIterator functions
/////////////////////////////////////////////////////////////////////////

// LimitIterator returns an iterator that will return at most N records. Calling Close on the returned iterator will Close the underlying iterator.
func LimitIterator(itr Iterator, N int64) Iterator {
	if N < 0 {
		N = 0
	}
	return &limitIterator{
		Iterator: itr,
		n:        N,
	}
}

// Close closes the underlying iterator and prevents future iteration.
func (s *limitIterator) Close() error {
	s.n = -1
	return s.Iterator.Close()
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *limitIterator) Next() bool {
	ok, err := s.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *limitIterator) NextContext(ctx context.Context) (bool, error) {
	if s.n == 0 {
		s.n = -1
		return false, nil
	} else if s.n < 0 {
		return false, nil
	}
	ok, err := s.Iterator.NextContext(ctx)
	if err == nil && ok {
		s.n--
		return true, nil
	}
	s.n = -1
	return false, err
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *limitIterator) Scan(dest Record) error {
	if s.n < 0 {
		return errors.IterationFinished.New()
	}
	return s.Iterator.Scan(dest)
}

/////////////////////////////////////////////////////////////////////////
// limitedSizeIterator functions
/////////////////////////////////////////////////////////////////////////

// LimitSizeIterator returns an iterator that will return records from itr until either itr is exhausted, or the total size of the returned records exceeds N bytes. Calling Close on the returned iterator will Close the underlying iterator.
func LimitSizeIterator(itr Iterator, N int) Iterator {
	if N < 0 {
		N = 0
	}
	return &limitedSizeIterator{
		Iterator: itr,
		n:        N,
	}
}

// Close closes the underlying iterator and prevents future iteration.
func (s *limitedSizeIterator) Close() error {
	s.isClosed, s.r = true, nil
	return s.Iterator.Close()
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *limitedSizeIterator) Next() bool {
	ok, err := s.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *limitedSizeIterator) NextContext(ctx context.Context) (bool, error) {
	if s.isClosed {
		return false, nil
	} else if s.n <= 0 {
		s.isClosed, s.r = true, nil
		return false, nil
	}
	ok, err := s.Iterator.NextContext(ctx)
	if err == nil && ok {
		if s.r == nil {
			s.r = make(Record)
		}
		if err = s.Iterator.Scan(s.r); err == nil {
			s.n -= s.r.Size()
			return true, nil
		}
	}
	s.n = -1
	return false, err
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *limitedSizeIterator) Scan(dest Record) error {
	if s.isClosed {
		return errors.IterationFinished.New()
	} else if s.r == nil {
		return errors.IterationNotStarted.New()
	}
	Scan(dest, s.r)
	return nil
}

/////////////////////////////////////////////////////////////////////////
// limitedDurationIterator functions
/////////////////////////////////////////////////////////////////////////

// NewDurationIterator returns an iterator that will return records from itr until either itr is exhausted, or duration d has passed since the iterator was created. Calling Close on the returned iterator will Close the underlying iterator.
func NewDurationIterator(itr Iterator, d time.Duration) *DurationIterator {
	wrappedItr := &DurationIterator{
		Iterator: itr,
		t:        time.NewTimer(d),
	}
	go func() {
		<-wrappedItr.t.C
		wrappedItr.shouldStop.Store(true)
	}()
	return wrappedItr
}

// Stop releases resources used by the duration iterator and prevents further iteration, but does not close the underlying iterator.
func (s *DurationIterator) Stop() {
	s.t.Stop()
	s.isClosed = true
}

// Close closes the underlying iterator and prevents future iteration.
func (s *DurationIterator) Close() error {
	s.Stop()
	return s.Iterator.Close()
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *DurationIterator) Next() bool {
	ok, err := s.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *DurationIterator) NextContext(ctx context.Context) (bool, error) {
	if s.isClosed {
		return false, nil
	} else if s.shouldStop.Load() {
		s.Stop()
		return false, nil
	}
	ok, err := s.Iterator.NextContext(ctx)
	if err != nil || !ok {
		s.Stop()
	}
	return ok, err
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (s *DurationIterator) Scan(dest Record) error {
	if s.isClosed {
		return errors.IterationFinished.New()
	}
	return s.Iterator.Scan(dest)
}

/////////////////////////////////////////////////////////////////////////
// CountIterator functions
/////////////////////////////////////////////////////////////////////////

// NewCountIterator returns an iterator that will count the number of entries iterated over. Calling Close on the returned iterator will Close the underlying iterator.
func NewCountIterator(itr Iterator) *CountIterator {
	return &CountIterator{Iterator: itr}
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *CountIterator) Next() bool {
	ok := s.Iterator.Next()
	if ok {
		atomic.AddInt64(&s.n, 1)
	}
	return ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (s *CountIterator) NextContext(ctx context.Context) (bool, error) {
	ok, err := s.Iterator.NextContext(ctx)
	if err == nil && ok {
		atomic.AddInt64(&s.n, 1)
	}
	return ok, err
}

// Count returns the number of entries iterated over. This is safe for concurrent access.
func (s *CountIterator) Count() int64 {
	return atomic.LoadInt64(&s.n)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// IteratorToSliceMaxLen reads from itr attempting to populate a slice of maximum length n. Returns the slice created, along with any errors.
func IteratorToSliceMaxLen(ctx context.Context, itr Iterator, n int) ([]Record, error) {
	// Sanity check
	if n < 0 {
		return nil, fmt.Errorf("slice length (%d) must be non-negative", n)
	} else if n == 0 {
		return []Record{}, nil
	}
	// Start populating a slice
	S := make([]Record, 0, n)
	ok, err := itr.NextContext(ctx)
	for err == nil && ok {
		// Read in the next record
		r := Record{}
		if err = itr.Scan(r); err == nil {
			// Save the record
			S = append(S, r)
			// Move on
			n--
			if n == 0 {
				ok = false
			} else {
				ok, err = itr.NextContext(ctx)
			}
		}
	}
	// Check for an iteration error and return what we have
	if err == nil {
		err = itr.Err()
	}
	return S, err
}
