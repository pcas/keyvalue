// Record is a key-value record.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package record

import (
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/gobutil"
	"encoding/gob"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

// valueType describes the type of a value in a Record.
type valueType uint8

// The type constants used when serialising a Record.
const (
	tInt = valueType(iota)
	tInt8
	tInt16
	tInt32
	tInt64
	tUint
	tUint8
	tUint16
	tUint32
	tUint64
	tBool
	tFloat64
	tString
	tByteSlice
)

// Record is the type defining a record in a table. The keys of the map are the keys of the record, and the values of the map are the values of the record. The values must take one of the following types:
//
//	int, int8, int16, int32, int64
//	uint, uint8, uint16, uint32, uint64
//	float64
//	bool
//	string
//	[]byte
type Record map[string]interface{}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// convertType attempts to convert the object "obj" to the type represented by "targetType". The type of "targetType" must be one of those specified in the documentation for Record
func convertType(obj interface{}, targetType interface{}) (val interface{}, err error) {
	switch targetType.(type) {
	case int:
		val, err = convert.ToInt(obj)
	case int8:
		val, err = convert.ToInt8(obj)
	case int16:
		val, err = convert.ToInt16(obj)
	case int32:
		val, err = convert.ToInt32(obj)
	case int64:
		val, err = convert.ToInt64(obj)
	case uint:
		val, err = convert.ToUint(obj)
	case uint8:
		val, err = convert.ToUint8(obj)
	case uint16:
		val, err = convert.ToUint16(obj)
	case uint32:
		val, err = convert.ToUint32(obj)
	case uint64:
		val, err = convert.ToUint64(obj)
	case bool:
		val, err = convert.ToBool(obj)
	case float64:
		val, err = convert.ToFloat64(obj)
	case string:
		val, err = convert.ToString(obj)
	case []byte:
		if S, ok := obj.([]byte); ok {
			T := make([]byte, len(S))
			copy(T, S)
			val = T
		} else if str, e := convert.ToString(obj); e != nil {
			err = e
		} else {
			val = []byte(str)
		}
	default:
		err = fmt.Errorf("unsupported target value type (%T)", targetType)
	}
	return
}

// typeForValue returns the valueType for the value x.
func typeForValue(x interface{}) (t valueType, ok bool) {
	ok = true
	switch x.(type) {
	case int:
		t = tInt
	case int8:
		t = tInt8
	case int16:
		t = tInt16
	case int32:
		t = tInt32
	case int64:
		t = tInt64
	case uint:
		t = tUint
	case uint8:
		t = tUint8
	case uint16:
		t = tUint16
	case uint32:
		t = tUint32
	case uint64:
		t = tUint64
	case bool:
		t = tBool
	case float64:
		t = tFloat64
	case string:
		t = tString
	case []byte:
		t = tByteSlice
	default:
		ok = false
	}
	return
}

// sizeForValue returns the number of bytes used to store x. We define an int and uint as requiring 8 bytes of storage, regardless of whether this is a 32- or 64-bit system.
func sizeForValue(x interface{}) int {
	switch v := x.(type) {
	case bool, int8, uint8:
		return 1
	case int16, uint16:
		return 2
	case int32, uint32:
		return 4
	case int, uint, int64, uint64, float64:
		return 8
	case string:
		return len(v)
	case []byte:
		return len(v)
	}
	return 0
}

//////////////////////////////////////////////////////////////////////
// valueType functions
//////////////////////////////////////////////////////////////////////

// Decode attempts to decode a value x of type t from the decoder dec.
func (t valueType) Decode(dec *gob.Decoder) (x interface{}, err error) {
	// Decode the type
	switch t {
	case tInt:
		var y int
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tInt8:
		var y int8
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tInt16:
		var y int16
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tInt32:
		var y int32
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tInt64:
		var y int64
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint:
		var y uint
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint8:
		var y uint8
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint16:
		var y uint16
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint32:
		var y uint32
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tUint64:
		var y uint64
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tBool:
		var y bool
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tFloat64:
		var y float64
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tString:
		var y string
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	case tByteSlice:
		var y []byte
		if err = dec.Decode(&y); err == nil {
			x = y
		}
	default:
		err = fmt.Errorf("unsupported value type (%s)", t)
	}
	// Handle any errors and return
	if errors.ShouldWrap(err) {
		err = errors.RecordUnableToDecodeValue.Wrap(err)
	}
	return
}

// String returns a string description of the type.
func (t valueType) String() (s string) {
	switch t {
	case tInt:
		s = "int"
	case tInt8:
		s = "int8"
	case tInt16:
		s = "int16"
	case tInt32:
		s = "int32"
	case tInt64:
		s = "int64"
	case tUint:
		s = "uint"
	case tUint8:
		s = "uint8"
	case tUint16:
		s = "uint16"
	case tUint32:
		s = "uint32"
	case tUint64:
		s = "uint64"
	case tBool:
		s = "bool"
	case tFloat64:
		s = "float64"
	case tString:
		s = "string"
	case tByteSlice:
		s = "[]byte"
	default:
		s = strconv.Itoa(int(t))
	}
	return
}

//////////////////////////////////////////////////////////////////////
// Record functions
//////////////////////////////////////////////////////////////////////

// IsValid returns true if and only if the keys in the record satisfy IsKey, and the values in the record satisfy IsValue.
func (r Record) IsValid() (bool, error) {
	for k, x := range r {
		if !IsKey(k) {
			err := fmt.Errorf("malformed key \"%s\"", k)
			return false, errors.RecordMalformedKey.Wrap(err)
		} else if !IsValue(x) {
			err := fmt.Errorf("unsupported value type (%T) for key %s", x, k)
			return false, errors.RecordUnsupportedType.Wrap(err)
		}
	}
	return true, nil
}

// Size returns the size, in bytes, of this record.
//
// The size is defined to be the sum of the number of bytes for each key, plus the sum of the number of bytes of storage used by each value. The size is only defined if the record is valid. We define an int and uint as requiring 8 bytes of storage, regardless of whether this is a 32- or 64-bit system.
func (r Record) Size() int {
	var size int
	for k, v := range r {
		size += len(k) + sizeForValue(v)
	}
	return size
}

// Keys returns a slice of keys in this record. The keys are returned sorted using sort.Strings.
func (r Record) Keys() []string {
	keys := make([]string, 0, len(r))
	for key := range r {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return keys
}

// Copy returns a copy of the record.
func (r Record) Copy() Record {
	rc := make(Record, len(r))
	for k, v := range r {
		if S, ok := v.([]byte); ok {
			T := make([]byte, len(S))
			copy(T, S)
			rc[k] = T
		} else {
			rc[k] = v
		}
	}
	return rc
}

// GobEncode returns a byte slice representing the encoding of the record.
func (r Record) GobEncode() ([]byte, error) {
	// Build parallel slices of keys and types
	keys := make([]string, 0, len(r))
	types := make([]valueType, 0, len(r))
	for k, x := range r {
		keys = append(keys, k)
		t, ok := typeForValue(x)
		if !ok {
			err := fmt.Errorf("unsupported type (%T) for key %s", x, k)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
		types = append(types, t)
	}
	// Encode the record
	b, err := func() ([]byte, error) {
		// Create an encoder
		enc := gobutil.NewEncoder()
		defer gobutil.ReuseEncoder(enc)
		// Encode the slice of keys and types
		if err := enc.Encode(keys); err != nil {
			return nil, err
		} else if err := enc.Encode(types); err != nil {
			return nil, err
		}
		// Encode the values one at a time
		for _, k := range keys {
			if err := enc.Encode(r[k]); err != nil {
				return nil, err
			}
		}
		// Return the byte slice
		return enc.Bytes(), nil
	}()
	// Handle any errors and return
	if errors.ShouldWrap(err) {
		err = errors.RecordUnableToEncodeValue.Wrap(err)
	}
	return b, err
}

// GobDecode overwrites the receiver, which must be a pointer, with the value represented by the byte slice, which was written by GobEncode.
func (r *Record) GobDecode(b []byte) error {
	// Decode the record
	err := func() error {
		// Create a decoder
		dec := gobutil.NewDecoder(b)
		// Decode the slice of keys and types
		var keys []string
		if err := dec.Decode(&keys); err != nil {
			return err
		}
		var types []valueType
		if err := dec.Decode(&types); err != nil {
			return err
		}
		// Sanity check
		size := len(keys)
		if len(types) != size {
			return fmt.Errorf("number of value types (%d) does not agree with number of keys (%d)", len(types), size)
		}
		for _, k := range keys {
			if !IsKey(k) {
				err := fmt.Errorf("malformed key \"%s\"", k)
				return errors.RecordMalformedKey.Wrap(err)
			}
		}
		// Allocate the memory
		*r = make(Record, size)
		// Start decoding the values
		for i, t := range types {
			x, err := t.Decode(dec)
			if err != nil {
				return err
			}
			(*r)[keys[i]] = x
		}
		return nil
	}()
	// Handle any errors and return
	if errors.ShouldWrap(err) {
		err = errors.RecordUnableToDecodeValue.Wrap(err)
	}
	return err
}

// String returns a string representation of the record.
func (r Record) String() string {
	if len(r) == 0 {
		return "{}"
	}
	kvs := make([]string, 0, len(r))
	for k, v := range r {
		kvs = append(kvs, fmt.Sprintf("%s:%v", k, v))
	}
	return "{" + strings.Join(kvs, ",") + "}"
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// Convert deletes any keys from r not in template, and converts the values in r to types matching those in template.
func Convert(r Record, template Record) error {
	// Make a list of any keys not in the template
	var del []string
	for key := range r {
		if _, ok := template[key]; !ok {
			if del == nil {
				del = make([]string, 0)
			}
			del = append(del, key)
		}
	}
	// Delete the keys not in the template
	for _, key := range del {
		delete(r, key)
	}
	// Perform the conversion
	return ConvertSkipUnknown(r, template)
}

// ConvertSkipUnknown converts the values in r to types matching those in template. Any key in r that is not in template is ignored.
func ConvertSkipUnknown(r Record, template Record) error {
	// Attempt to convert the values based on the template
	for key, targetType := range template {
		if obj, ok := r[key]; ok {
			val, err := convertType(obj, targetType)
			if err != nil {
				return errors.RecordUnableToConvertType.Wrap(err)
			}
			r[key] = val
		}
	}
	return nil
}

// IsValue returns true if and only if the type of x is one of those specified in the documentation for Record.
func IsValue(x interface{}) (ok bool) {
	switch x.(type) {
	case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64, bool, float64, string, []byte:
		ok = true
	}
	return
}

// CommonValue returns a (zero-valued) value z that covers the types of x and y, where x, y, z are valid Record types. For example, if x is an int16 and y is an int32, then z will be an int32; if x is a bool and y is a string, then z will be a string.
func CommonValue(x interface{}, y interface{}) (interface{}, error) {
	switch x.(type) {
	case int:
		switch y.(type) {
		case int, int8, int16, int32, uint8, uint16, bool:
			return int(0), nil
		case int64, uint32:
			return int64(0), nil
		case uint, uint64, float64, string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case int8:
		switch y.(type) {
		case int:
			return int(0), nil
		case int8, bool:
			return int8(0), nil
		case int16, uint8:
			return int16(0), nil
		case int32, uint16:
			return int32(0), nil
		case int64, uint32:
			return int64(0), nil
		case float64:
			return float64(0), nil
		case uint, uint64, string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case int16:
		switch y.(type) {
		case int:
			return int(0), nil
		case int8, int16, uint8, bool:
			return int16(0), nil
		case int32, uint16:
			return int32(0), nil
		case int64, uint32:
			return int64(0), nil
		case float64:
			return float64(0), nil
		case uint, uint64, string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case int32:
		switch y.(type) {
		case int:
			return int(0), nil
		case int8, int16, int32, uint8, uint16, bool:
			return int32(0), nil
		case int64, uint32:
			return int64(0), nil
		case float64:
			return float64(0), nil
		case uint, uint64, string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case int64:
		switch y.(type) {
		case int, int8, int16, int32, int64, uint8, uint16, uint32, bool:
			return int64(0), nil
		case uint, uint64, float64, string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case uint:
		switch y.(type) {
		case uint, uint8, uint16, uint32, bool:
			return uint(0), nil
		case uint64:
			return uint64(0), nil
		case int, int8, int16, int32, int64, float64, string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case uint8:
		switch y.(type) {
		case int:
			return int(0), nil
		case int8, int16:
			return int16(0), nil
		case int32:
			return int32(0), nil
		case int64:
			return int64(0), nil
		case uint:
			return uint(0), nil
		case uint8, bool:
			return uint8(0), nil
		case uint16:
			return uint16(0), nil
		case uint32:
			return uint32(0), nil
		case uint64:
			return uint64(0), nil
		case float64:
			return float64(0), nil
		case string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case uint16:
		switch y.(type) {
		case int:
			return int(0), nil
		case int8, int16, int32:
			return int32(0), nil
		case int64:
			return int64(0), nil
		case uint:
			return uint(0), nil
		case uint8, uint16, bool:
			return uint16(0), nil
		case uint32:
			return uint32(0), nil
		case uint64:
			return uint64(0), nil
		case float64:
			return float64(0), nil
		case string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case uint32:
		switch y.(type) {
		case int, int8, int16, int32, int64:
			return int64(0), nil
		case uint:
			return uint(0), nil
		case uint8, uint16, uint32, bool:
			return uint32(0), nil
		case uint64:
			return uint64(0), nil
		case float64:
			return float64(0), nil
		case string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case uint64:
		switch y.(type) {
		case uint, uint8, uint16, uint32, uint64, bool:
			return uint64(0), nil
		case int, int8, int16, int32, int64, string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case bool:
		switch y.(type) {
		case int:
			return int(0), nil
		case int8:
			return int8(0), nil
		case int16:
			return int16(0), nil
		case int32:
			return int32(0), nil
		case int64:
			return int64(0), nil
		case uint:
			return uint(0), nil
		case uint8:
			return uint8(0), nil
		case uint16:
			return uint16(0), nil
		case uint32:
			return uint32(0), nil
		case uint64:
			return uint64(0), nil
		case float64:
			return float64(0), nil
		case bool:
			return false, nil
		case string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case float64:
		switch y.(type) {
		case int8, int16, int32, uint8, uint16, uint32, float64, bool:
			return float64(0), nil
		case int, int64, uint, uint64, string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case string:
		switch y.(type) {
		case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64, bool, float64, string, []byte:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	case []byte:
		switch y.(type) {
		case []byte:
			return []byte{}, nil
		case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64, bool, float64, string:
			return "", nil
		default:
			err := fmt.Errorf("unsupported value type (%T)", y)
			return nil, errors.RecordUnsupportedType.Wrap(err)
		}
	default:
		err := fmt.Errorf("unsupported value type (%T)", x)
		return nil, errors.RecordUnsupportedType.Wrap(err)
	}
}

// IsKey returns true if and only if the string k has the format required for a key. Namely, k matches [a-zA-Z]+[a-zA-Z0-9_]*.
func IsKey(k string) bool {
	if len(k) == 0 {
		return false
	}
	isFirst := true
	return strings.IndexFunc(k, func(r rune) bool {
		var ok bool
		if (r >= 'a' && r <= 'z') || (r >= 'A' && r <= 'Z') {
			ok = true
		} else if !isFirst && ((r >= '0' && r <= '9') || (r == '_')) {
			ok = true
		}
		isFirst = false
		return !ok
	}) == -1
}

// Scan copies "src" into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func Scan(dest Record, src Record) {
	// Delete all existing keys
	for key := range dest {
		delete(dest, key)
	}
	// Copy over the keys and values
	for key, val := range src {
		if b, ok := val.([]byte); ok {
			bcopy := make([]byte, len(b))
			copy(bcopy, b)
			val = bcopy
		}
		dest[key] = val
	}
}
