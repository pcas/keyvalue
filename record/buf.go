// Iterator defines an iterator for a record.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package record

import (
	"bitbucket.org/pcas/keyvalue/errors"
	"context"
)

// BufferedIterator described an Iterator with buffer.
type BufferedIterator interface {
	Iterator
	// Buffered returns true iff the next call to Next or NextContext is
	// guaranteed not to block.
	Buffered() bool
}

// itrResult is the result of an iterative step.
type itrResult struct {
	ok  bool   // Did we get a result?
	r   Record // The returned record
	err error  // Any returned error
}

// bufferedIterator provides buffering of entries.
type bufferedIterator struct {
	resC     <-chan itrResult   // The results channel
	doneC    <-chan struct{}    // The done channel
	cancel   context.CancelFunc // The cancel function
	next     Record             // The next record (if known)
	cur      Record             // The current record (if known)
	isClosed bool               // Have we closed?
	err      error              // The error (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// advanceIterator advances the iterator and returns the result.
func advanceIterator(ctx context.Context, itr Iterator) (bool, Record, error) {
	ok, err := itr.NextContext(ctx)
	if err != nil || !ok {
		return false, nil, err
	}
	r := Record{}
	if err = itr.Scan(r); err != nil {
		return false, nil, err
	}
	return true, r, nil
}

// feedResultsChannel feeds the results from itr down the channel resC. Will return when either the context fires, the end of iteration is reached, or an error occurs. Does not close the resC channel on return.
func feedResultsChannel(ctx context.Context, itr Iterator, resC chan<- itrResult) {
	for {
		// Advance the iterator
		ok, r, err := advanceIterator(ctx, itr)
		// Attempt to feed the result down the channel
		select {
		case resC <- itrResult{
			ok:  ok,
			r:   r,
			err: err,
		}:
		case <-ctx.Done():
			err = ctx.Err()
		}
		// Are we done?
		if err != nil || !ok {
			return
		}
	}
}

// iteratorWorker reads from the given iterator, feeding the results down the resC channel and returning when either the context fires, the end of iteration is reached, or an error occurs. On return the iterator will be closed, resC will be closed, and doneC will be closed. Intended to be run in its own go routine.
func iteratorWorker(ctx context.Context, itr Iterator, resC chan<- itrResult, doneC chan<- struct{}) {
	// Defer a graceful shutdown
	defer close(doneC)
	defer itr.Close() // Ignore any errors
	defer close(resC)
	// Feed the iterator down the results channel
	feedResultsChannel(ctx, itr, resC)
}

/////////////////////////////////////////////////////////////////////////
// bufferedIterator functions
/////////////////////////////////////////////////////////////////////////

// NewBufferedIterator returns an iterator wrapping 'itr' that buffers the records. Calling Close on the returned iterator will Close the wrapped iterator 'itr'.
func NewBufferedIterator(itr Iterator, bufSize int) BufferedIterator {
	// Don't buffer a buffer
	if b, ok := itr.(BufferedIterator); ok {
		return b
	}
	// Sanity check
	if bufSize < 0 {
		bufSize = 0
	}
	// Create the communication channels
	resC := make(chan itrResult, bufSize)
	doneC := make(chan struct{})
	// Create a cancellation context
	ctx, cancel := context.WithCancel(context.Background())
	// Start the background worker
	go iteratorWorker(ctx, itr, resC, doneC)
	// Return the iterator
	return &bufferedIterator{
		resC:   resC,
		doneC:  doneC,
		cancel: cancel,
	}
}

// Close prevents future iteration. Note that this will close the wrapped iterator.
func (itr *bufferedIterator) Close() error {
	if !itr.isClosed {
		itr.isClosed = true
		itr.cancel()
		<-itr.doneC
	}
	return itr.err
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *bufferedIterator) Err() error {
	return itr.err
}

// Buffered returns true iff the next call to Next or NextContext is guaranteed not to block.
func (itr *bufferedIterator) Buffered() bool {
	// Is this easy to answer?
	if itr.isClosed || itr.next != nil {
		return true
	}
	// Try to fetch the next record without blocking
	select {
	case res, ok := <-itr.resC:
		if !ok {
			itr.Close()
		} else if res.err != nil || !res.ok {
			if itr.err == nil {
				itr.err = res.err
			}
			itr.Close()
		} else {
			itr.next = res.r
		}
		return true
	default:
	}
	return false
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *bufferedIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *bufferedIterator) NextContext(ctx context.Context) (bool, error) {
	// Sanity check
	if itr.isClosed {
		return false, nil
	}
	// Is the next record already cached?
	if itr.next != nil {
		itr.cur, itr.next = itr.next, nil
		return true, nil
	}
	// Fetch the record
	select {
	case res, ok := <-itr.resC:
		if !ok {
			itr.Close()
			return false, nil
		} else if res.err != nil || !res.ok {
			if itr.err == nil {
				itr.err = res.err
			}
			itr.Close()
			return false, nil
		}
		itr.cur = res.r
	case <-ctx.Done():
		return false, ctx.Err()
	}
	return true, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (itr *bufferedIterator) Scan(dest Record) error {
	if itr.isClosed {
		return errors.IterationFinished.New()
	} else if itr.cur == nil {
		return errors.IterationNotStarted.New()
	}
	Scan(dest, itr.cur)
	return nil
}
