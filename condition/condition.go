// Condition describes a logical condition in a "where" clause.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package condition

import (
	"bitbucket.org/pcas/keyvalue/record"
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// Bool defines a truth valuation.
type Bool bool

// The possible truth valuations.
const (
	True  = Bool(true)
	False = Bool(false)
)

// The string values for true and false.
const (
	trueString  = "TRUE"
	falseString = "FALSE"
)

// OpCode describes a binary operator.
type OpCode int

// The valid OpCodes.
const (
	Eq = OpCode(iota) // Equal to (=)
	Ne                // Not equal to (!=)
	Gt                // Greater than (>)
	Lt                // Less than (<)
	Ge                // Greater than or equal to (>=)
	Le                // Less than or equal to (<=)
)

// nilString represents a nil condition.
const nilString = "<nil>"

// Condition is the interface satisfied by a logical condition. Conditions produced by this package will be of one of the following types:
//	Bool
//	*LeafOp
//	*IsOp
//	*InOp
//	*NotInOp
//	*BetweenOp
//	*NotBetweenOp
//	*AndOp
//	*OrOp
type Condition interface {
	// IsValid returns true iff the condition validates. If false, also
	// returns an error describing why validation failed.
	IsValid() (bool, error)
	// Negate returns the negation of the given operation.
	Negate() Condition
	// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp,
	// and *OrOp conditions only.
	Simplify() Condition
	// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
	IsEqualTo(Condition) bool
}

// LeafOp describes an operator of the form "lhs op rhs" where "lhs" is given by a key, "op" is a valid OpCode, and "rhs" is a value.
type LeafOp struct {
	key   string      // The lhs
	c     OpCode      // The binary operator
	value interface{} // The rhs
}

// IsOp describes an IS operator.
type IsOp struct {
	key   string // The lhs
	value bool   // The rhs
}

// InOp describes an IN operator.
type InOp struct {
	key    string        // The lhs
	values []interface{} // The rhs
}

// NotInOp describes a NOT IN operator.
type NotInOp struct {
	key    string        // The lhs
	values []interface{} // The rhs
}

// BetweenOp describes a BETWEEN operator.
type BetweenOp struct {
	key   string      // The key
	lower interface{} // The lower value
	upper interface{} // The upper value
}

// NotBetweenOp describes a NOT BETWEEN operator.
type NotBetweenOp struct {
	key   string      // The key
	lower interface{} // The lower value
	upper interface{} // The upper value
}

// AndOp describes an AND operator.
type AndOp struct {
	conds []Condition // The conditions to AND
}

// OrOp describes an OR operator.
type OrOp struct {
	conds []Condition // The conditions to OR
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// quoteKey returns a quoted form for the given string as a key.
func quoteKey(s string) string {
	s = strings.ReplaceAll(s, "\\", "\\\\")
	s = strings.ReplaceAll(s, "'", "\\'")
	return "'" + s + "'"
}

// quoteValue returns a quoted form for the given string as a value.
func quoteValue(s string) string {
	s = strings.ReplaceAll(s, "\\", "\\\\")
	s = strings.ReplaceAll(s, "\"", "\\\"")
	s = strings.ReplaceAll(s, "\n", "\\n")
	s = strings.ReplaceAll(s, "\r", "\\r")
	s = strings.ReplaceAll(s, "\t", "\\t")
	return "\"" + s + "\""
}

// formatValue returns a string representation of the given value, suitable for printing in an operator description.
func formatValue(v interface{}) string {
	switch x := v.(type) {
	case int:
		return strconv.Itoa(x)
	case int8:
		return strconv.FormatInt(int64(x), 10)
	case int16:
		return strconv.FormatInt(int64(x), 10)
	case int32:
		return strconv.FormatInt(int64(x), 10)
	case int64:
		return strconv.FormatInt(x, 10)
	case uint:
		return strconv.FormatUint(uint64(x), 10)
	case uint8:
		return strconv.FormatUint(uint64(x), 10)
	case uint16:
		return strconv.FormatUint(uint64(x), 10)
	case uint32:
		return strconv.FormatUint(uint64(x), 10)
	case uint64:
		return strconv.FormatUint(x, 10)
	case bool:
		if x {
			return trueString
		}
		return falseString
	case float64:
		return strconv.FormatFloat(x, 'f', -1, 64)
	case string:
		return quoteValue(x)
	case []byte:
		S := make([]string, 0, len(x))
		for _, b := range x {
			S = append(S, strconv.FormatUint(uint64(b), 10))
		}
		return "[" + strings.Join(S, ", ") + "]"
	default:
		return fmt.Sprintf("%v", v)
	}
}

// areValuesEqual returns true iff x and y are equal at the level of both type and value. Assumes that x and y must be one of the supported Record value types.
func areValuesEqual(x interface{}, y interface{}) bool {
	switch a := x.(type) {
	case int:
		b, ok := y.(int)
		return ok && a == b
	case int8:
		b, ok := y.(int8)
		return ok && a == b
	case int16:
		b, ok := y.(int16)
		return ok && a == b
	case int32:
		b, ok := y.(int32)
		return ok && a == b
	case int64:
		b, ok := y.(int64)
		return ok && a == b
	case uint:
		b, ok := y.(uint)
		return ok && a == b
	case uint8:
		b, ok := y.(uint8)
		return ok && a == b
	case uint16:
		b, ok := y.(uint16)
		return ok && a == b
	case uint32:
		b, ok := y.(uint32)
		return ok && a == b
	case uint64:
		b, ok := y.(uint64)
		return ok && a == b
	case bool:
		b, ok := y.(bool)
		return ok && a == b
	case float64:
		b, ok := y.(float64)
		return ok && a == b
	case string:
		b, ok := y.(string)
		return ok && a == b
	case []byte:
		b, ok := y.([]byte)
		if !ok || len(a) != len(b) {
			return false
		}
		for i, c := range a {
			if b[i] != c {
				return false
			}
		}
		return true
	default:
		return false
	}
}

/////////////////////////////////////////////////////////////////////////
// Bool functions
/////////////////////////////////////////////////////////////////////////

// IsValid always returns true, nil.
func (b Bool) IsValid() (bool, error) {
	return true, nil
}

// Negate returns the negation of the given operation.
func (b Bool) Negate() Condition {
	if b {
		return False
	}
	return True
}

// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp, and *OrOp conditions only.
func (b Bool) Simplify() Condition {
	return b
}

// String returns a string representation of the Bool.
func (b Bool) String() string {
	if b {
		return trueString
	}
	return falseString
}

// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
func (b Bool) IsEqualTo(x Condition) bool {
	y, ok := x.(Bool)
	return ok && b == y
}

/////////////////////////////////////////////////////////////////////////
// OpCode functions
/////////////////////////////////////////////////////////////////////////

// IsValid returns true iff the OpCode is valid.
func (c OpCode) IsValid() bool {
	switch c {
	case Eq, Ne, Gt, Lt, Ge, Le:
		return true
	default:
		return false
	}
}

// String returns a string representation of the OpCode.
func (c OpCode) String() string {
	switch c {
	case Eq:
		return "="
	case Ne:
		return "!="
	case Gt:
		return ">"
	case Lt:
		return "<"
	case Ge:
		return ">="
	case Le:
		return "<="
	default:
		return "?"
	}
}

/////////////////////////////////////////////////////////////////////////
// LeafOp functions
/////////////////////////////////////////////////////////////////////////

// Equal returns the operator "lhs = rhs" (or its equivalent) as a *LeafOp.
func Equal(lhs string, rhs interface{}) Condition {
	return &LeafOp{
		key:   lhs,
		c:     Eq,
		value: rhs,
	}
}

// NotEqual returns the operator "lhs != rhs" (or its equivalent) as a *LeafOp.
func NotEqual(lhs string, rhs interface{}) Condition {
	if b, ok := rhs.(bool); ok {
		return Equal(lhs, !b)
	}
	return &LeafOp{
		key:   lhs,
		c:     Ne,
		value: rhs,
	}
}

// GreaterThan returns the operator "lhs > rhs" (or its equivalent) as a *LeafOp.
func GreaterThan(lhs string, rhs interface{}) Condition {
	return &LeafOp{
		key:   lhs,
		c:     Gt,
		value: rhs,
	}
}

// LessThan returns the operator "lhs < rhs" (or its equivalent) as a *LeafOp.
func LessThan(lhs string, rhs interface{}) Condition {
	return &LeafOp{
		key:   lhs,
		c:     Lt,
		value: rhs,
	}
}

// GreaterThanOrEqualTo returns the operator "lhs >= rhs" (or its equivalent) as a *LeafOp.
func GreaterThanOrEqualTo(lhs string, rhs interface{}) Condition {
	return &LeafOp{
		key:   lhs,
		c:     Ge,
		value: rhs,
	}
}

// LessThanOrEqualTo returns the operator "lhs <= rhs" (or its equivalent) as a *LeafOp.
func LessThanOrEqualTo(lhs string, rhs interface{}) Condition {
	return &LeafOp{
		key:   lhs,
		c:     Le,
		value: rhs,
	}
}

// IsValid returns true iff the operator validates. If false, also returns an error describing why validation failed.
func (op *LeafOp) IsValid() (bool, error) {
	if op == nil {
		return false, errors.New("illegal nil operator")
	}
	c := op.OpCode()
	if !c.IsValid() {
		return false, fmt.Errorf("unknown op code: %d", int(c))
	} else if !record.IsKey(op.Lhs()) {
		return false, fmt.Errorf("malformed key: \"%s\"", op.Lhs())
	} else if !record.IsValue(op.Rhs()) {
		return false, fmt.Errorf("unsupported value type: %T", op.Rhs())
	} else if _, ok := op.Rhs().(bool); ok && c != Eq && c != Ne {
		return false, fmt.Errorf("operator not supported for bool value type: %s", c)
	}
	return true, nil
}

// Lhs returns the lhs of the operator.
func (op *LeafOp) Lhs() string {
	if op == nil {
		return ""
	}
	return op.key
}

// OpCode returns the OpCode of the operator.
func (op *LeafOp) OpCode() OpCode {
	if op == nil {
		return Eq
	}
	return op.c
}

// Rhs returns the rhs of the operator.
func (op *LeafOp) Rhs() interface{} {
	if op == nil {
		return nil
	}
	return op.value
}

// Negate returns the negation of the given operation.
func (op *LeafOp) Negate() Condition {
	if op == nil {
		return nil
	}
	switch op.OpCode() {
	case Eq:
		return NotEqual(op.Lhs(), op.Rhs())
	case Ne:
		return Equal(op.Lhs(), op.Rhs())
	case Gt:
		return LessThanOrEqualTo(op.Lhs(), op.Rhs())
	case Lt:
		return GreaterThanOrEqualTo(op.Lhs(), op.Rhs())
	case Ge:
		return LessThan(op.Lhs(), op.Rhs())
	case Le:
		return GreaterThan(op.Lhs(), op.Rhs())
	}
	return op
}

// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp, and *OrOp conditions only.
func (op *LeafOp) Simplify() Condition {
	return op
}

// String returns a string description of the operator.
func (op *LeafOp) String() string {
	if op == nil {
		return nilString
	}
	return fmt.Sprintf("%s %s %s", quoteKey(op.Lhs()), op.OpCode(), formatValue(op.Rhs()))
}

// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
func (op *LeafOp) IsEqualTo(x Condition) bool {
	y, ok := x.(*LeafOp)
	return ok && y.Lhs() == op.Lhs() && y.OpCode() == op.OpCode() && areValuesEqual(y.Rhs(), op.Rhs())
}

/////////////////////////////////////////////////////////////////////////
// IsOp functions
/////////////////////////////////////////////////////////////////////////

// IsTrue returns the operator "lhs IS True" (or its equivalent) as a *IsOp.
func IsTrue(lhs string) Condition {
	return Is(lhs, true)
}

// IsFalse returns the operator "lhs IS False" (or its equivalent) as a *IsOp.
func IsFalse(lhs string) Condition {
	return Is(lhs, false)
}

// Is returns a new IS operator as an *IsOp. The IS operator can be used to match a value against a boolean value only.
func Is(lhs string, value bool) Condition {
	return &IsOp{
		key:   lhs,
		value: value,
	}
}

// IsValid returns true iff the condition validates. If false, also returns
// an error describing why validation failed.
func (op *IsOp) IsValid() (bool, error) {
	if op == nil {
		return false, errors.New("illegal nil operator")
	} else if !record.IsKey(op.Lhs()) {
		return false, fmt.Errorf("malformed key: \"%s\"", op.Lhs())
	}
	return true, nil
}

// Lhs returns the lhs of the operator.
func (op *IsOp) Lhs() string {
	if op == nil {
		return ""
	}
	return op.key
}

// Values returns the rhs of the operator.
func (op *IsOp) Value() bool {
	if op == nil {
		return false
	}
	return op.value
}

// Negate returns the negation of the given operation.
func (op *IsOp) Negate() Condition {
	if op == nil {
		return nil
	}
	return In(op.Lhs(), !op.Value())
}

// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp, and *OrOp conditions only.
func (op *IsOp) Simplify() Condition {
	if op == nil {
		return nil
	}
	return Equal(op.Lhs(), op.Value())
}

// String returns a string description of the operator.
func (op *IsOp) String() string {
	if op == nil {
		return nilString
	}
	s := trueString
	if !op.Value() {
		s = falseString
	}
	return quoteKey(op.Lhs()) + " IS " + s
}

// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
func (op *IsOp) IsEqualTo(x Condition) bool {
	y, ok := x.(*IsOp)
	return ok && y.Lhs() == op.Lhs() && y.Value() == op.Value()
}

/////////////////////////////////////////////////////////////////////////
// InOp functions
/////////////////////////////////////////////////////////////////////////

// In returns a new IN operator as an *InOp or equivalent Condition. The IN operator can be used to match a value against a list of values and is equivalent to successive OR operators:
//	lhs = value1 OR lhs = value2 OR ... OR lhs = valueN
// If no values are provided then this equivalent to the False valuation.
func In(lhs string, value ...interface{}) Condition {
	if len(value) == 0 {
		return False
	} else if len(value) == 1 {
		return Equal(lhs, value[0])
	}
	cpy := make([]interface{}, len(value))
	copy(cpy, value)
	return &InOp{
		key:    lhs,
		values: cpy,
	}
}

// IsValid returns true iff the condition validates. If false, also returns
// an error describing why validation failed.
func (op *InOp) IsValid() (bool, error) {
	if op == nil {
		return false, errors.New("illegal nil operator")
	} else if !record.IsKey(op.Lhs()) {
		return false, fmt.Errorf("malformed key: \"%s\"", op.Lhs())
	}
	for _, v := range op.values {
		if !record.IsValue(v) {
			return false, fmt.Errorf("unsupported value type: %T", v)
		}
	}
	return true, nil
}

// Lhs returns the lhs of the operator.
func (op *InOp) Lhs() string {
	if op == nil {
		return ""
	}
	return op.key
}

// Values returns the list of values for the operator.
func (op *InOp) Values() []interface{} {
	if op == nil {
		return nil
	}
	values := make([]interface{}, len(op.values))
	copy(values, op.values)
	return values
}

// Negate returns the negation of the given operation.
func (op *InOp) Negate() Condition {
	if op == nil {
		return nil
	}
	return NotIn(op.Lhs(), op.values...)
}

// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp, and *OrOp conditions only.
func (op *InOp) Simplify() Condition {
	if op == nil {
		return nil
	}
	lhs := op.Lhs()
	conds := make([]Condition, 0, len(op.values))
	for _, rhs := range op.values {
		conds = append(conds, Equal(lhs, rhs))
	}
	return Or(conds...)
}

// String returns a string description of the operator.
func (op *InOp) String() string {
	if op == nil {
		return nilString
	}
	S := make([]string, 0, len(op.values))
	for _, v := range op.values {
		S = append(S, formatValue(v))
	}
	return quoteKey(op.Lhs()) + " IN (" + strings.Join(S, ", ") + ")"
}

// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
func (op *InOp) IsEqualTo(x Condition) bool {
	y, ok := x.(*InOp)
	if !ok || y.Lhs() != op.Lhs() {
		return false
	}
	vals1, vals2 := op.Values(), y.Values()
	if len(vals1) != len(vals2) {
		return false
	}
	for i, a := range vals1 {
		if !areValuesEqual(a, vals2[i]) {
			return false
		}
	}
	return true
}

/////////////////////////////////////////////////////////////////////////
// NotInOp functions
/////////////////////////////////////////////////////////////////////////

// NotIn returns a new NOT IN operator as a *NotInOp or equivalent Condition. The NOT IN operator can be used to match a value against a list of values and is equivalent to successive AND operators:
//	lhs != value1 AND lhs != value2 AND ... AND lhs != valueN
// If no values are provided then this equivalent to the True valuation.
func NotIn(lhs string, value ...interface{}) Condition {
	if len(value) == 0 {
		return True
	} else if len(value) == 1 {
		return NotEqual(lhs, value[0])
	}
	cpy := make([]interface{}, len(value))
	copy(cpy, value)
	return &NotInOp{
		key:    lhs,
		values: cpy,
	}
}

// IsValid returns true iff the condition validates. If false, also returns
// an error describing why validation failed.
func (op *NotInOp) IsValid() (bool, error) {
	if op == nil {
		return false, errors.New("illegal nil operator")
	} else if !record.IsKey(op.Lhs()) {
		return false, fmt.Errorf("malformed key: \"%s\"", op.Lhs())
	}
	for _, v := range op.values {
		if !record.IsValue(v) {
			return false, fmt.Errorf("unsupported value type: %T", v)
		}
	}
	return true, nil
}

// Lhs returns the lhs of the operator.
func (op *NotInOp) Lhs() string {
	if op == nil {
		return ""
	}
	return op.key
}

// Values returns the list of values for the operator.
func (op *NotInOp) Values() []interface{} {
	if op == nil {
		return nil
	}
	values := make([]interface{}, len(op.values))
	copy(values, op.values)
	return values
}

// Negate returns the negation of the given operation.
func (op *NotInOp) Negate() Condition {
	if op == nil {
		return nil
	}
	return In(op.Lhs(), op.values...)
}

// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp, and *OrOp conditions only.
func (op *NotInOp) Simplify() Condition {
	if op == nil {
		return nil
	}
	lhs := op.Lhs()
	conds := make([]Condition, 0, len(op.values))
	for _, rhs := range op.values {
		conds = append(conds, NotEqual(lhs, rhs))
	}
	return And(conds...)
}

// String returns a string description of the operator.
func (op *NotInOp) String() string {
	if op == nil {
		return nilString
	}
	S := make([]string, 0, len(op.values))
	for _, v := range op.values {
		S = append(S, formatValue(v))
	}
	return quoteKey(op.Lhs()) + " NOT IN (" + strings.Join(S, ", ") + ")"
}

// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
func (op *NotInOp) IsEqualTo(x Condition) bool {
	y, ok := x.(*NotInOp)
	if !ok || y.Lhs() != op.Lhs() {
		return false
	}
	vals1, vals2 := op.Values(), y.Values()
	if len(vals1) != len(vals2) {
		return false
	}
	for i, a := range vals1 {
		if !areValuesEqual(a, vals2[i]) {
			return false
		}
	}
	return true
}

/////////////////////////////////////////////////////////////////////////
// BetweenOp functions
/////////////////////////////////////////////////////////////////////////

// Between returns a new BETWEEN operator as a *BetweenOp or equivalent Condition. The BETWEEN operator can be used to match against a range of values and is equivalent to:
//	lhs >= lower AND lhs <= upper.
func Between(lhs string, lower interface{}, upper interface{}) Condition {
	return &BetweenOp{
		key:   lhs,
		lower: lower,
		upper: upper,
	}
}

// IsValid returns true iff the condition validates. If false, also returns
// an error describing why validation failed.
func (op *BetweenOp) IsValid() (bool, error) {
	if op == nil {
		return false, errors.New("illegal nil operator")
	} else if !record.IsKey(op.Lhs()) {
		return false, fmt.Errorf("malformed key: \"%s\"", op.Lhs())
	} else if !record.IsValue(op.Lower()) {
		return false, fmt.Errorf("unsupported value type: %T", op.Lower())
	} else if !record.IsValue(op.Upper()) {
		return false, fmt.Errorf("unsupported value type: %T", op.Upper())
	} else if _, ok := op.Lower().(bool); ok {
		return false, fmt.Errorf("operator not supported for bool value type: %s", Ge)
	} else if _, ok := op.Upper().(bool); ok {
		return false, fmt.Errorf("operator not supported for bool value type: %s", Le)
	}
	return true, nil
}

// Lhs returns the lhs of the operator.
func (op *BetweenOp) Lhs() string {
	if op == nil {
		return ""
	}
	return op.key
}

// Lower returns the lower value for of the operator.
func (op *BetweenOp) Lower() interface{} {
	if op == nil {
		return nil
	}
	return op.lower
}

// Upper returns the upper value for of the operator.
func (op *BetweenOp) Upper() interface{} {
	if op == nil {
		return nil
	}
	return op.upper
}

// Negate returns the negation of the given operation.
func (op *BetweenOp) Negate() Condition {
	if op == nil {
		return nil
	}
	return NotBetween(op.Lhs(), op.Lower(), op.Upper())
}

// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp, and *OrOp conditions only.
func (op *BetweenOp) Simplify() Condition {
	if op == nil {
		return nil
	}
	lhs := op.Lhs()
	return And(
		GreaterThanOrEqualTo(lhs, op.Lower()),
		LessThanOrEqualTo(lhs, op.Upper()),
	)
}

// String returns a string description of the operator.
func (op *BetweenOp) String() string {
	if op == nil {
		return nilString
	}
	lower := nilString
	if v := op.Lower(); v != nil {
		lower = formatValue(v)
	}
	upper := nilString
	if v := op.Upper(); v != nil {
		upper = formatValue(v)
	}
	return quoteKey(op.Lhs()) + " BETWEEN " + lower + " AND " + upper
}

// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
func (op *BetweenOp) IsEqualTo(x Condition) bool {
	y, ok := x.(*BetweenOp)
	return ok && y.Lhs() == op.Lhs() && areValuesEqual(y.Lower(), op.Lower()) && areValuesEqual(y.Upper(), op.Upper())
}

/////////////////////////////////////////////////////////////////////////
// NotBetweenOp functions
/////////////////////////////////////////////////////////////////////////

// NotBetween returns a new NOT BETWEEN operator as a *NotBetweenOp or equivalent Condition. The NOT BETWEEN operator can be used to match against a range of values and is equivalent to:
//	lhs < lower OR lhs > upper.
func NotBetween(lhs string, lower interface{}, upper interface{}) Condition {
	return &NotBetweenOp{
		key:   lhs,
		lower: lower,
		upper: upper,
	}
}

// IsValid returns true iff the condition validates. If false, also returns
// an error describing why validation failed.
func (op *NotBetweenOp) IsValid() (bool, error) {
	if op == nil {
		return false, errors.New("illegal nil operator")
	} else if !record.IsKey(op.Lhs()) {
		return false, fmt.Errorf("malformed key: \"%s\"", op.Lhs())
	} else if !record.IsValue(op.Lower()) {
		return false, fmt.Errorf("unsupported value type: %T", op.Lower())
	} else if !record.IsValue(op.Upper()) {
		return false, fmt.Errorf("unsupported value type: %T", op.Upper())
	} else if _, ok := op.Lower().(bool); ok {
		return false, fmt.Errorf("operator not supported for bool value type: %s", Ge)
	} else if _, ok := op.Upper().(bool); ok {
		return false, fmt.Errorf("operator not supported for bool value type: %s", Le)
	}
	return true, nil
}

// Lhs returns the lhs of the operator.
func (op *NotBetweenOp) Lhs() string {
	if op == nil {
		return ""
	}
	return op.key
}

// Lower returns the lower value for of the operator.
func (op *NotBetweenOp) Lower() interface{} {
	if op == nil {
		return nil
	}
	return op.lower
}

// Upper returns the upper value for of the operator.
func (op *NotBetweenOp) Upper() interface{} {
	if op == nil {
		return nil
	}
	return op.upper
}

// Negate returns the negation of the given operation.
func (op *NotBetweenOp) Negate() Condition {
	if op == nil {
		return nil
	}
	return Between(op.Lhs(), op.Lower(), op.Upper())
}

// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp, and *OrOp conditions only.
func (op *NotBetweenOp) Simplify() Condition {
	if op == nil {
		return nil
	}
	lhs := op.Lhs()
	return Or(
		LessThan(lhs, op.Lower()),
		GreaterThan(lhs, op.Upper()),
	)
}

// String returns a string description of the operator.
func (op *NotBetweenOp) String() string {
	if op == nil {
		return nilString
	}
	lower := nilString
	if v := op.Lower(); v != nil {
		lower = formatValue(v)
	}
	upper := nilString
	if v := op.Upper(); v != nil {
		upper = formatValue(v)
	}
	return quoteKey(op.Lhs()) + " NOT BETWEEN " + lower + " AND " + upper
}

// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
func (op *NotBetweenOp) IsEqualTo(x Condition) bool {
	y, ok := x.(*NotBetweenOp)
	return ok && y.Lhs() == op.Lhs() && areValuesEqual(y.Lower(), op.Lower()) && areValuesEqual(y.Upper(), op.Upper())
}

/////////////////////////////////////////////////////////////////////////
// AndOp functions
/////////////////////////////////////////////////////////////////////////

// flattenAnd expands any AND operators appearing in the given slice of conditions. Returns the flattened conditions.
func flattenAnd(conds []Condition) []Condition {
	newConds := make([]Condition, 0, len(conds))
	for _, c := range conds {
		if op, ok := c.(*AndOp); ok {
			newConds = append(newConds, op.conds...)
		} else if b, ok := c.(Bool); ok {
			if !b {
				return []Condition{False}
			}
		} else {
			newConds = append(newConds, c)
		}
	}
	return newConds
}

// And returns a new AND operator as an *AndOp or equivalent Condition. That is, it is equivalent to:
//  cond1 AND cond2 AND ... AND condN.
// If no conditions are provided then this equivalent to the True valuation.
func And(cond ...Condition) Condition {
	cond = flattenAnd(cond)
	if len(cond) == 0 {
		return True
	} else if len(cond) == 1 {
		return cond[0]
	}
	return &AndOp{conds: cond}
}

// IsValid returns true iff the condition validates. If false, also returns
// an error describing why validation failed.
func (op *AndOp) IsValid() (bool, error) {
	if op == nil {
		return false, errors.New("illegal nil operator")
	} else if len(op.conds) < 2 {
		return false, errors.New("an 'and' operator requires two or more conditions")
	}
	for _, c := range op.conds {
		if c == nil {
			return false, errors.New("illegal nil operator")
		} else if ok, err := c.IsValid(); !ok {
			return false, err
		}
	}
	return true, nil
}

// Negate returns the negation of the given operation.
func (op *AndOp) Negate() Condition {
	if op == nil {
		return nil
	}
	conds := make([]Condition, 0, len(op.conds))
	for _, c := range op.conds {
		conds = append(conds, c.Negate())
	}
	return Or(conds...)
}

// Conditions returns the conditions that form the AND operator.
func (op *AndOp) Conditions() []Condition {
	if op == nil {
		return nil
	}
	conds := make([]Condition, len(op.conds))
	copy(conds, op.conds)
	return conds
}

// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp, and *OrOp conditions only.
func (op *AndOp) Simplify() Condition {
	if op == nil {
		return nil
	}
	conds := make([]Condition, 0, len(op.conds))
	for _, c := range op.conds {
		conds = append(conds, c.Simplify())
	}
	return And(conds...)
}

// String returns a string description of the operator.
func (op *AndOp) String() string {
	if op == nil {
		return nilString
	}
	S := make([]string, 0, len(op.conds))
	for _, c := range op.conds {
		if c == nil {
			S = append(S, nilString)
		} else {
			S = append(S, fmt.Sprintf("%s", c))
		}
	}
	return "(" + strings.Join(S, " AND ") + ")"
}

// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
func (op *AndOp) IsEqualTo(x Condition) bool {
	y, ok := x.(*AndOp)
	if !ok {
		return false
	}
	conds1, conds2 := op.Conditions(), y.Conditions()
	if len(conds1) != len(conds2) {
		return false
	}
	for i, c := range conds1 {
		if !c.IsEqualTo(conds2[i]) {
			return false
		}
	}
	return true
}

/////////////////////////////////////////////////////////////////////////
// OrOp functions
/////////////////////////////////////////////////////////////////////////

// flattenOr expands any OR operators appearing in the given slice of conditions. Returns the flattened conditions.
func flattenOr(conds []Condition) []Condition {
	newConds := make([]Condition, 0, len(conds))
	for _, c := range conds {
		if op, ok := c.(*OrOp); ok {
			newConds = append(newConds, op.conds...)
		} else if b, ok := c.(Bool); ok {
			if b {
				return []Condition{True}
			}
		} else {
			newConds = append(newConds, c)
		}
	}
	return newConds
}

// Or returns a new OR operator as an *OrOp or equivalent Condition. That is, it is equivalent to:
//  cond1 OR cond2 OR ... OR condN.
// If no conditions are provided then this equivalent to the False valuation.
func Or(cond ...Condition) Condition {
	cond = flattenOr(cond)
	if len(cond) == 0 {
		return False
	} else if len(cond) == 1 {
		return cond[0]
	}
	return &OrOp{conds: cond}
}

// IsValid returns true iff the condition validates. If false, also returns
// an error describing why validation failed.
func (op *OrOp) IsValid() (bool, error) {
	if op == nil {
		return false, errors.New("illegal nil operator")
	} else if len(op.conds) < 2 {
		return false, errors.New("an 'or' operator requires two or more conditions")
	}
	for _, c := range op.conds {
		if c == nil {
			return false, errors.New("illegal nil operator")
		} else if ok, err := c.IsValid(); !ok {
			return false, err
		}
	}
	return true, nil
}

// Conditions returns the conditions that form the OR operator.
func (op *OrOp) Conditions() []Condition {
	if op == nil {
		return nil
	}
	conds := make([]Condition, len(op.conds))
	copy(conds, op.conds)
	return conds
}

// Negate returns the negation of the given operation.
func (op *OrOp) Negate() Condition {
	if op == nil {
		return nil
	}
	conds := make([]Condition, 0, len(op.conds))
	for _, c := range op.conds {
		conds = append(conds, c.Negate())
	}
	return And(conds...)
}

// Simplify re-expresses the operator in terms of Bool, *LeafOp, *AndOp, and *OrOp conditions only.
func (op *OrOp) Simplify() Condition {
	if op == nil {
		return nil
	}
	conds := make([]Condition, 0, len(op.conds))
	for _, c := range op.conds {
		conds = append(conds, c.Simplify())
	}
	return Or(conds...)
}

// String returns a string description of the operator.
func (op *OrOp) String() string {
	if op == nil {
		return nilString
	}
	S := make([]string, 0, len(op.conds))
	for _, c := range op.conds {
		if c == nil {
			S = append(S, nilString)
		} else {
			S = append(S, fmt.Sprintf("%s", c))
		}
	}
	return "(" + strings.Join(S, " OR ") + ")"
}

// IsEqualTo returns true iff the conditions are equal (note that this does not check for equivalence).
func (op *OrOp) IsEqualTo(x Condition) bool {
	y, ok := x.(*OrOp)
	if !ok {
		return false
	}
	conds1, conds2 := op.Conditions(), y.Conditions()
	if len(conds1) != len(conds2) {
		return false
	}
	for i, c := range conds1 {
		if !c.IsEqualTo(conds2[i]) {
			return false
		}
	}
	return true
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// FromRecord converts a record to a condition.
func FromRecord(r record.Record) Condition {
	if len(r) == 0 {
		return nil
	}
	cs := make([]Condition, 0, len(r))
	for k, v := range r {
		cs = append(cs, Equal(k, v))
	}
	return And(cs...)
}
