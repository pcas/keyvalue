// Table defines a connection to a simple key-value table.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/driver"
	"bitbucket.org/pcas/keyvalue/errors"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/rand"
	"context"
	"fmt"
	"io"
	stdsort "sort"
	"sync"
	"time"
)

// Table provides a database connection to a table.
type Table interface {
	metrics.Metricser
	log.Logger
	io.Closer
	// Name return the table name.
	Name() string
	// Describe returns a best-guess template for the data in this table.
	//
	// Note that the accuracy of this template depends on the underlying
	// storage engine. It might not be possible to return an exact
	// description (for example, in a schema-less database), and in this
	// case we return a good guess based on a sample of the data available.
	Describe(ctx context.Context) (record.Record, error)
	// Count returns the number of records in the table that match
	// "selector".
	Count(ctx context.Context, selector record.Record) (int64, error)
	// CountWhere returns the number of records in the table that satisfy
	// condition "cond" (which may be nil if there are no conditions).
	CountWhere(ctx context.Context, cond condition.Condition) (int64, error)
	// InsertRecords inserts the given records into the table.
	InsertRecords(ctx context.Context, records ...record.Record) error
	// Insert inserts the records in the given iterator into the table.
	Insert(ctx context.Context, itr record.Iterator) error
	// Update updates all records in the table that match "selector" by
	// setting all keys present in "replacement" to the given values.
	// Returns the number of records updated.
	Update(ctx context.Context, replacement record.Record, selector record.Record) (int64, error)
	// UpdateWhere updates all records in the table that satisfy condition
	// "cond" (which may be nil if there are no conditions) by setting all
	// keys present in "replacement" to the given values. Returns the number
	// of records updated.
	UpdateWhere(ctx context.Context, replacement record.Record, cond condition.Condition) (int64, error)
	// Select returns the records matching "selector", sorted as specified
	// by "order" (which may be nil if there is no sort order required).
	// The returned records will be in the form specified by "template".
	Select(ctx context.Context, template record.Record, selector record.Record, order sort.OrderBy) (record.Iterator, error)
	// SelectWhere returns the results satisfying condition "cond" (which
	// may be nil if there are no conditions), sorted as specified by
	// "order" (which may be nil if there is no sort order required). The
	// returned records will be in the form specified by "template".
	SelectWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Iterator, error)
	// SelectLimit returns at most n records matching "selector", sorted as
	// specified by "order" (which may be nil if there is no sort order
	// required). The returned records will be in the form specified by
	// "template".
	SelectLimit(ctx context.Context, template record.Record, selector record.Record, order sort.OrderBy, n int64) (record.Iterator, error)
	// SelectWhereLimit returns at most n results satisfying condition
	// "cond" (which may be nil if there are no conditions), sorted as
	// specified by "order" (which may be nil if there is no sort order
	// required). The returned records will be in the form specified by
	// "template".
	SelectWhereLimit(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error)
	// SelectOne returns the first record matching "selector", sorted as
	// specified by "order" (which may be nil if there is no sort order
	// required). The returned record will be in the form specified by
	// "template". If no records match, then a nil record will be returned.
	SelectOne(ctx context.Context, template record.Record, selector record.Record, order sort.OrderBy) (record.Record, error)
	// SelectOneWhere returns the first record satisfying condition "cond"
	// (which may be nil if there are no conditions), sorted as specified by
	// "order" (which may be nil if there is no sort order required). The
	// returned record will be in the form specified by "template". If no
	// records match, then a nil record will be returned.
	SelectOneWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Record, error)
	// Delete deletes those records in the table that match "selector".
	// Returns the number of records deleted.
	Delete(ctx context.Context, selector record.Record) (int64, error)
	// DeleteWhere deletes those records in the table that satisfy condition
	// "cond" (which may be nil if there are no conditions). Returns the
	// number of records deleted.
	DeleteWhere(ctx context.Context, cond condition.Condition) (int64, error)
	// AddIndex adds an index on the given key. If an index already exists,
	// this index is unmodified and nil is returned.
	AddIndex(ctx context.Context, key string) error
	// DeleteIndex deletes the index on the given key. If no index is
	// present, nil is returned.
	DeleteIndex(ctx context.Context, key string) error
	// ListIndices lists the keys for which indices are present. The keys
	// are sorted in increasing order.
	ListIndices(ctx context.Context) ([]string, error)
	// AddKeys updates each record r the table, adding any keys in rec that
	// are not already present along with the corresponding values. Any keys
	// that are already present in r will be left unmodified.
	AddKeys(ctx context.Context, rec record.Record) error
	// AddKey updates each record r in the table, adding the given
	// key-value pair if key is not already present in r.
	AddKey(ctx context.Context, key string, value interface{}) error
	// DeleteKeys updates each record in the table, deleting all the
	// specified keys if present.
	DeleteKeys(ctx context.Context, keys []string) error
	// DeleteKey updates each record in the table, deleting the specified
	// key if present.
	DeleteKey(ctx context.Context, key string) error
}

// table implements the Table interface.
type table struct {
	name     string            // The table name
	t        driver.Table      // The underlying connection to the table
	s        closeSet          // The set of open results sets
	closef   func()            // The close function
	lg       log.Interface     // The logger
	met      metrics.Interface // The metrics endpoint
	m        sync.RWMutex      // A mutex controlling access to the following
	isClosed bool              // Is the table closed?
	err      error             // The error on close (if any)
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// validateCondition validates the given condition.
func validateCondition(cond condition.Condition) (bool, error) {
	if cond == nil {
		return true, nil
	}
	return cond.IsValid()
}

//////////////////////////////////////////////////////////////////////
// Table functions
//////////////////////////////////////////////////////////////////////

// Name return the table name.
func (t *table) Name() string {
	if t == nil {
		return ""
	}
	return t.name
}

// Close closes the connection to the table.
func (t *table) Close() error {
	// Sanity check
	if t == nil || t.t == nil {
		return nil
	}
	err := func() (err error) {
		// Acquire a write lock
		t.m.Lock()
		defer t.m.Unlock()
		// Is there anything to do?
		if !t.isClosed {
			// Recover from any panics in the driver
			defer func() {
				if e := recover(); e != nil {
					err = fmt.Errorf("panic in Close: %v", e)
				}
			}()
			// Close the open records
			t.s.Close()
			// Close the underlying table and mark us as closed
			t.err, t.isClosed = t.t.Close(), true
			// Finally call the close function
			if t.closef != nil {
				t.closef()
			}
		}
		// Note any error and return
		err = t.err
		return
	}()
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.TableCloseError.Wrap(err)
	}
	// Provide log data before returning
	if err == nil {
		t.Log().Printf("Connection to table closed")
	} else {
		t.Log().Printf("Connection to table closed with error: %v", err)
	}
	return err
}

// Describe returns a best-guess template for the data in this table.
//
// Note that the accuracy of this template depends on the underlying storage engine. It might not be possible to return an exact description (for example, in a schema-less database), and in this case we return a good guess based on a sample of the data available.
func (t *table) Describe(ctx context.Context) (record.Record, error) {
	// Sanity check
	if t == nil || t.t == nil {
		return nil, errors.TableClosed.New()
	}
	now := time.Now()
	r, err := func() (r record.Record, err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in Describe: %v", e)
			}
		}()
		// Fetch the description
		r, err = t.t.Describe(ctx)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.DescribeError.Wrap(err)
	}
	// Provide log and metric data before returning
	if err != nil {
		submitPoint(t.Metrics(), "Describe", metrics.Fields{
			"Success": false,
		}, dur)
		t.Log().Printf("Describe failed after %s: %v", dur, err)
		return nil, err
	}
	submitPoint(t.Metrics(), "Describe", metrics.Fields{
		"Success": true,
	}, dur)
	t.Log().Printf("Describe succeeded in %s", dur)
	return r, nil
}

// Count returns the number of records in the table that match "selector".
func (t *table) Count(ctx context.Context, selector record.Record) (int64, error) {
	if ok, err := selector.IsValid(); !ok {
		return 0, errors.SelectorFailsToValidate.Wrap(err)
	}
	return t.CountWhere(ctx, condition.FromRecord(selector))
}

// CountWhere returns the number of records in the table that satisfy condition "cond" (which may be nil if there are no conditions).
func (t *table) CountWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	// Sanity check
	if t == nil || t.t == nil {
		return 0, errors.TableClosed.New()
	} else if ok, err := validateCondition(cond); !ok {
		return 0, errors.ConditionFailsToValidate.Wrap(err)
	}
	now := time.Now()
	n, err := func() (n int64, err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Sanitise the condition
		if b, ok := cond.(condition.Bool); ok {
			if !b {
				return // False: nothing will match
			}
			cond = nil // True: no conditions
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in CountWhere: %v", e)
			}
		}()
		// Perform the count
		n, err = t.t.CountWhere(ctx, cond)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.CountError.Wrap(err)
	}
	// Provide log and metric data before returning
	if err != nil {
		submitPoint(t.Metrics(), "CountWhere", metrics.Fields{
			"Success": false,
		}, dur)
		t.Log().Printf("CountWhere failed after %s: %v", dur, err)
		return 0, err
	}
	submitPoint(t.Metrics(), "CountWhere", metrics.Fields{
		"Success": true,
		"Number":  n,
	}, dur)
	t.Log().Printf("CountWhere succeeded in %s (found %d records)", dur, n)
	return n, nil
}

// InsertRecords inserts the given records into the table.
func (t *table) InsertRecords(ctx context.Context, records ...record.Record) error {
	return t.Insert(ctx, record.SliceIterator(records))
}

// Insert inserts the records in the given iterator into the table.
func (t *table) Insert(ctx context.Context, itr record.Iterator) error {
	// Sanity check
	if t == nil || t.t == nil {
		return errors.TableClosed.New()
	}
	// Wrap itr in a CountIterator
	now, cItr := time.Now(), record.NewCountIterator(itr)
	err := func() (err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in Insert: %v", e)
			}
		}()
		// Insert the records
		err = t.t.Insert(ctx, cItr)
		return
	}()
	dur, n := time.Since(now), cItr.Count()
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.InsertError.Wrap(err)
	}
	// Provide log and metric data before returning
	if err != nil {
		submitPoint(t.Metrics(), "Insert", metrics.Fields{
			"Success": false,
		}, dur)
		t.Log().Printf("Insert failed after %s: %v", dur, err)
		return err
	}
	submitPoint(t.Metrics(), "Insert", metrics.Fields{
		"Success": true,
		"Number":  n,
	}, dur)
	t.Log().Printf("Insert succeeded in %s (%d records)", dur, n)
	return nil
}

// Update updates all records in the table that match "selector" by setting all keys present in "replacement" to the given values. Returns the number of records updated.
func (t *table) Update(ctx context.Context, replacement record.Record, selector record.Record) (int64, error) {
	if ok, err := selector.IsValid(); !ok {
		return 0, errors.SelectorFailsToValidate.Wrap(err)
	}
	return t.UpdateWhere(ctx, replacement, condition.FromRecord(selector))
}

// UpdateWhere updates all records in the table that satisfy condition "cond" (which may be nil if there are no conditions) by setting all keys present in "replacement" to the given values. Returns the number of records updated.
func (t *table) UpdateWhere(ctx context.Context, replacement record.Record, cond condition.Condition) (int64, error) {
	// Sanity check
	if t == nil || t.t == nil {
		return 0, errors.TableClosed.New()
	} else if ok, err := replacement.IsValid(); !ok {
		return 0, errors.ReplacementFailsToValidate.Wrap(err)
	} else if ok, err := validateCondition(cond); !ok {
		return 0, errors.ConditionFailsToValidate.Wrap(err)
	}
	now := time.Now()
	n, err := func() (n int64, err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Sanitise the condition
		if b, ok := cond.(condition.Bool); ok {
			if !b {
				return // False: nothing will match
			}
			cond = nil // True: no conditions
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in UpdateWhere: %v", e)
			}
		}()
		// Update the records
		n, err = t.t.UpdateWhere(ctx, replacement, cond)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UpdateError.Wrap(err)
	}
	// Provide log and metric data before returning
	if err != nil {
		submitPoint(t.Metrics(), "UpdateWhere", metrics.Fields{
			"Success": false,
		}, dur)
		t.Log().Printf("UpdateWhere failed after %s: %v", dur, err)
		return 0, err
	}
	submitPoint(t.Metrics(), "UpdateWhere", metrics.Fields{
		"Success": true,
		"Number":  n,
	}, dur)
	t.Log().Printf("UpdateWhere succeeded in %s (modified %d records)", dur, n)
	return n, nil
}

// Select returns the records matching "selector", sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (t *table) Select(ctx context.Context, template record.Record, selector record.Record, order sort.OrderBy) (record.Iterator, error) {
	if ok, err := selector.IsValid(); !ok {
		return nil, errors.SelectorFailsToValidate.Wrap(err)
	}
	return t.SelectWhere(ctx, template, condition.FromRecord(selector), order)
}

// SelectWhere returns the results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (t *table) SelectWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Iterator, error) {
	// Sanity check
	if t == nil || t.t == nil {
		return nil, errors.TableClosed.New()
	} else if ok, err := template.IsValid(); !ok {
		return nil, errors.TemplateFailsToValidate.Wrap(err)
	} else if ok, err := validateCondition(cond); !ok {
		return nil, errors.ConditionFailsToValidate.Wrap(err)
	} else if ok, err := order.IsValid(); !ok {
		return nil, errors.SortFailsToValidate.Wrap(err)
	}
	// Make a copy of the template and extract the keys from the template
	templateCopy := make(record.Record, len(template))
	keys := make([]string, 0, len(template))
	for key, value := range template {
		templateCopy[key] = value
		keys = append(keys, key)
	}
	// Generate a random ID for the results set (in anticipation of success)
	id := rand.ID8()
	now := time.Now()
	r, err := func() (r record.Iterator, err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Sanitise the condition
		if b, ok := cond.(condition.Bool); ok {
			if !b {
				r = record.EmptyIterator()
				return // False: nothing will match
			}
			cond = nil // True: no conditions
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in SelectWhere: %v", e)
			}
		}()
		// Select the matching records
		var results record.Iterator
		if results, err = t.t.SelectWhere(ctx, templateCopy, cond, order); err != nil {
			return
		}
		// Assign the ID to this results set
		itr := &recordIterator{
			Iterator: results,
			keys:     keys,
			start:    time.Now(),
			lg:       log.PrefixWith(t.Log(), "[records=0x%s]", id),
		}
		// Add the results set to the set of open results sets
		itr.closef = t.s.Include(itr)
		r = itr
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.SelectError.Wrap(err)
	}
	// Provide log and metric data
	submitPoint(t.Metrics(), "Select", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err != nil {
		t.Log().Printf("Select failed after %s: %v", dur, err)
		return nil, err
	}
	t.Log().Printf("Select succeeded in %s (records=0x%s)", dur, id)
	return r, nil
}

// SelectLimit returns at most n records matching "selector", sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (t *table) SelectLimit(ctx context.Context, template record.Record, selector record.Record, order sort.OrderBy, n int64) (record.Iterator, error) {
	if ok, err := selector.IsValid(); !ok {
		return nil, errors.SelectorFailsToValidate.Wrap(err)
	}
	return t.SelectWhereLimit(ctx, template, condition.FromRecord(selector), order, n)
}

// SelectWhereLimit returns at most n results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (t *table) SelectWhereLimit(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error) {
	// Sanity check
	if n < 0 {
		return nil, errors.LimitOutOfRange.New()
	} else if t == nil || t.t == nil {
		return nil, errors.TableClosed.New()
	} else if ok, err := template.IsValid(); !ok {
		return nil, errors.TemplateFailsToValidate.Wrap(err)
	} else if ok, err := validateCondition(cond); !ok {
		return nil, errors.ConditionFailsToValidate.Wrap(err)
	} else if ok, err := order.IsValid(); !ok {
		return nil, errors.SortFailsToValidate.Wrap(err)
	}
	// Make a copy of the template and extract the keys from the template
	templateCopy := make(record.Record, len(template))
	keys := make([]string, 0, len(template))
	for key, value := range template {
		templateCopy[key] = value
		keys = append(keys, key)
	}
	// Generate a random ID for the results set (in anticipation of success)
	id := rand.ID8()
	now := time.Now()
	r, err := func() (r record.Iterator, err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Sanitise the condition
		if b, ok := cond.(condition.Bool); ok {
			if !b {
				r = record.EmptyIterator()
				return // False: nothing will match
			}
			cond = nil // True: no conditions
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in SelectWhereLimit: %v", e)
			}
		}()
		// Select the matching records
		var results record.Iterator
		results, err = t.t.SelectWhereLimit(ctx, templateCopy, cond, order, n)
		// Assign the ID to this results set
		itr := &recordIterator{
			Iterator: results,
			keys:     keys,
			start:    time.Now(),
			lg:       log.PrefixWith(t.Log(), "[records=0x%s]", id),
		}
		// Add the results set to the set of open results sets
		itr.closef = t.s.Include(itr)
		r = itr
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.SelectError.Wrap(err)
	}
	// Provide log and metric data
	submitPoint(t.Metrics(), "SelectWhereLimit", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err != nil {
		t.Log().Printf("SelectWhereLimit(limit=%d) failed after %s: %v", n, dur, err)
		return nil, err
	}
	t.Log().Printf("SelectWhereLimit(limit=%d) succeeded in %s (records=0x%s)", n, dur, id)
	return r, nil
}

// SelectOne returns the first record matching "selector", sorted as specified by "order" (which may be nil if there is no sort order required). The returned record will be in the form specified by "template". If no records match, then a nil record will be returned.
func (t *table) SelectOne(ctx context.Context, template record.Record, selector record.Record, order sort.OrderBy) (record.Record, error) {
	if ok, err := selector.IsValid(); !ok {
		return nil, errors.SelectorFailsToValidate.Wrap(err)
	}
	return t.SelectOneWhere(ctx, template, condition.FromRecord(selector), order)
}

// SelectOneWhere returns the first record satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned record will be in the form specified by "template". If no records match, then a nil record will be returned.
func (t *table) SelectOneWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Record, error) {
	// Sanity check
	if t == nil || t.t == nil {
		return nil, errors.TableClosed.New()
	} else if ok, err := template.IsValid(); !ok {
		return nil, errors.TemplateFailsToValidate.Wrap(err)
	} else if ok, err := validateCondition(cond); !ok {
		return nil, errors.ConditionFailsToValidate.Wrap(err)
	} else if ok, err := order.IsValid(); !ok {
		return nil, errors.SortFailsToValidate.Wrap(err)
	}
	// Does the driver support this optional method?
	if d, ok := t.t.(driver.SelectOneWherer); ok {
		now := time.Now()
		r, err := func() (r record.Record, err error) {
			// Acquire a read lock
			t.m.RLock()
			defer t.m.RUnlock()
			// Are we closed?
			if t.isClosed {
				err = errors.TableClosed.New()
				return
			}
			// Sanitise the condition
			if b, ok := cond.(condition.Bool); ok {
				if !b {
					return // False: nothing will match
				}
				cond = nil // True: no conditions
			}
			// Recover from any panics in the driver
			defer func() {
				if e := recover(); e != nil {
					err = fmt.Errorf("panic in SelectOneWhere: %v", e)
				}
			}()
			// Perform the select one
			r, err = d.SelectOneWhere(ctx, template, cond, order)
			return
		}()
		dur := time.Since(now)
		// Check the error type
		if errors.ShouldWrap(err) {
			err = errors.UpdateError.Wrap(err)
		}
		// Provide log and metric data before returning
		if err != nil {
			submitPoint(t.Metrics(), "SelectOneWhere", metrics.Fields{
				"Success": false,
			}, dur)
			t.Log().Printf("SelectOneWhere failed after %s: %v", dur, err)
			return nil, err
		}
		submitPoint(t.Metrics(), "SelectOneWhere", metrics.Fields{
			"Success": true,
		}, dur)
		t.Log().Printf("SelectOneWhere succeeded in %s", dur)
		return r, nil
	}
	// Otherwise we do this via a select with limit 1
	r, err := t.SelectWhereLimit(ctx, template, cond, order, 1)
	if err != nil {
		return nil, err
	}
	ok, err := r.NextContext(ctx)
	if err != nil {
		r.Close()
		return nil, err
	} else if !ok {
		if err = r.Close(); err == nil {
			err = r.Err()
		}
		return nil, err
	}
	dest := make(record.Record, len(template))
	err = r.Scan(dest)
	r.Close()
	if err != nil {
		return nil, err
	}
	return dest, nil
}

// Delete deletes those records in the table that match "selector". Returns the number of records deleted.
func (t *table) Delete(ctx context.Context, selector record.Record) (int64, error) {
	if ok, err := selector.IsValid(); !ok {
		return 0, errors.SelectorFailsToValidate.Wrap(err)
	}
	return t.DeleteWhere(ctx, condition.FromRecord(selector))
}

// DeleteWhere deletes those records in the table that satisfy condition "cond" (which may be nil if there are no conditions). Returns the number of records deleted.
func (t *table) DeleteWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	// Sanity check
	if t == nil || t.t == nil {
		return 0, errors.TableClosed.New()
	} else if ok, err := validateCondition(cond); !ok {
		return 0, errors.ConditionFailsToValidate.Wrap(err)
	}
	now := time.Now()
	n, err := func() (n int64, err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Sanitise the condition
		if b, ok := cond.(condition.Bool); ok {
			if !b {
				return // False: nothing will match
			}
			cond = nil // True: no conditions
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in DeleteWhere: %v", e)
			}
		}()
		// Perform the deletion
		n, err = t.t.DeleteWhere(ctx, cond)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.DeleteError.Wrap(err)
	}
	// Provide log and metric data before returning
	if err != nil {
		submitPoint(t.Metrics(), "DeleteWhere", metrics.Fields{
			"Success": false,
		}, dur)
		t.Log().Printf("DeleteWhere failed after %s: %v", dur, err)
		return 0, err
	}
	submitPoint(t.Metrics(), "DeleteWhere", metrics.Fields{
		"Success": true,
		"Number":  n,
	}, dur)
	t.Log().Printf("DeleteWhere succeeded in %s (deleted %d records)", dur, n)
	return n, nil
}

// AddIndex adds an index on the given key. If an index already exists, this index is unmodified and nil is returned.
func (t *table) AddIndex(ctx context.Context, key string) error {
	// Sanity check
	if t == nil || t.t == nil {
		return errors.TableClosed.New()
	} else if !record.IsKey(key) {
		return errors.InvalidKey.New()
	}
	now := time.Now()
	err := func() (err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in AddIndex: %v", e)
			}
		}()
		// Add the index
		err = t.t.AddIndex(ctx, key)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.AddIndexError.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(t.Metrics(), "AddIndex", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		t.Log().Printf("AddIndex succeeded in %s", dur)
	} else {
		t.Log().Printf("AddIndex failed after %s: %v", dur, err)
	}
	return err
}

// DeleteIndex deletes the index on the given key. If no index is present, nil is returned.
func (t *table) DeleteIndex(ctx context.Context, key string) error {
	// Sanity check
	if t == nil || t.t == nil {
		return errors.TableClosed.New()
	} else if !record.IsKey(key) {
		return errors.InvalidKey.New()
	}
	now := time.Now()
	err := func() (err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in DeleteIndex: %v", e)
			}
		}()
		// Delete the index
		err = t.t.DeleteIndex(ctx, key)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.DeleteIndexError.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(t.Metrics(), "DeleteIndex", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		t.Log().Printf("DeleteIndex succeeded in %s", dur)
	} else {
		t.Log().Printf("DeleteIndex failed after %s: %v", dur, err)
	}
	return err
}

// ListIndices lists the keys for which indices are present. The keys are sorted in increasing order.
func (t *table) ListIndices(ctx context.Context) ([]string, error) {
	// Sanity check
	if t == nil || t.t == nil {
		return nil, errors.TableClosed.New()
	}
	now := time.Now()
	indices, err := func() (indices []string, err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in ListIndices: %v", e)
			}
		}()
		// List the indices
		indices, err = t.t.ListIndices(ctx)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.ListIndexError.Wrap(err)
	}
	// Provide log and metric data before returning
	if err != nil {
		submitPoint(t.Metrics(), "ListIndices", metrics.Fields{
			"Success": false,
		}, dur)
		t.Log().Printf("ListIndices failed after %s: %v", dur, err)
		return nil, err
	}
	submitPoint(t.Metrics(), "ListIndices", metrics.Fields{
		"Success": true,
	}, dur)
	t.Log().Printf("ListIndices succeeded in %s", dur)
	// Sort the keys before returning
	stdsort.Strings(indices)
	return indices, nil
}

// AddKeys updates each record r in t, adding any keys in rec that are not already present along with the corresponding values. Any keys that are already present in r will be left unmodified.
func (t *table) AddKeys(ctx context.Context, rec record.Record) error {
	// Sanity checks
	if t == nil || t.t == nil {
		return errors.TableClosed.New()
	} else if ok, err := rec.IsValid(); !ok {
		return err
	}
	now := time.Now()
	err := func() (err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in AddKeys: %v", e)
			}
		}()
		// Hand off to the driver
		err = t.t.AddKeys(ctx, rec)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.AddKeyError.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(t.Metrics(), "AddKeys", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		t.Log().Printf("AddKeys succeeded in %s", dur)
	} else {
		t.Log().Printf("AddKeys failed after %s: %v", dur, err)
	}
	return err
}

// AddKey updates each record r in t, adding the given key-value pair if key is not already present in r.
func (t *table) AddKey(ctx context.Context, key string, value interface{}) error {
	return t.AddKeys(ctx, record.Record{key: value})
}

// DeleteKeys updates each record r in t, deleting all the specified keys if present.
func (t *table) DeleteKeys(ctx context.Context, keys []string) error {
	// Sanity checks
	if t == nil || t.t == nil {
		return errors.TableClosed.New()
	}
	for _, k := range keys {
		if !record.IsKey(k) {
			return errors.InvalidKey.New()
		}
	}
	// Do the delete
	now := time.Now()
	err := func() (err error) {
		// Acquire a read lock
		t.m.RLock()
		defer t.m.RUnlock()
		// Are we closed?
		if t.isClosed {
			err = errors.TableClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in DeleteKeys: %v", e)
			}
		}()
		// Hand off to the driver
		err = t.t.DeleteKeys(ctx, keys)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.DeleteKeyError.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(t.Metrics(), "DeleteKeys", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		t.Log().Printf("DeleteKeys succeeded in %s", dur)
	} else {
		t.Log().Printf("DeleteKeys failed after %s: %v", dur, err)
	}
	return err
}

// DeleteKey updates each record r in t, deleting the specified key if present.
func (t *table) DeleteKey(ctx context.Context, key string) error {
	return t.DeleteKeys(ctx, []string{key})
}

// Log returns the log for this table.
func (t *table) Log() log.Interface {
	if t == nil || t.lg == nil {
		return log.Discard
	}
	return t.lg
}

// Metrics returns the metrics endpoint for this table.
func (t *table) Metrics() metrics.Interface {
	if t == nil || t.met == nil {
		return metrics.Discard
	}
	return t.met
}
