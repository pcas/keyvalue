// Errors defines an error type used to represent common keyvalue errors.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package errors

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"runtime"
	"strconv"
)

// Code represents a common keyvalue error.
type Code int

// The valid error codes.
const (
	ConnectionClosed = Code(iota)
	UnableToOpenConnection
	ConnectionCloseError
	DatabaseClosed
	DatabaseCloseError
	OperationNotSupported
	OperationNotPermitted
	UnableToListDatabases
	InvalidDatabaseName
	DatabaseAlreadyExists
	DatabaseDoesNotExist
	UnableToCreateDatabase
	UnableToDeleteDatabase
	UnableToConnectToDatabase
	UnableToListTables
	InvalidTableName
	CreateTableError
	DeleteTableError
	TableDoesNotExist
	UnableToConnectToTable
	TableClosed
	TableCloseError
	DescribeError
	CountError
	InsertError
	UpdateError
	SelectError
	LimitOutOfRange
	DeleteError
	TemplateFailsToValidate
	SelectorFailsToValidate
	ConditionFailsToValidate
	SortFailsToValidate
	ReplacementFailsToValidate
	RecordFailsToValidate
	RecordUnsupportedType
	RecordMalformedKey
	RecordUnableToDecodeValue
	RecordUnableToEncodeValue
	RecordUnableToConvertType
	IterationNotStarted
	IterationError
	IterationFinished
	IterationCloseError
	ListIndexError
	InvalidKey
	AddIndexError
	DeleteIndexError
	AddKeyError
	DeleteKeyError
	RenameTableError
)

// codeToString is a map of error codes to strings.
var codeToString = map[Code]string{
	ConnectionClosed:           "connection closed",
	UnableToOpenConnection:     "error opening connection",
	ConnectionCloseError:       "error closing connection",
	DatabaseClosed:             "database closed",
	DatabaseCloseError:         "error closing database",
	OperationNotSupported:      "operation not supported",
	OperationNotPermitted:      "operation not permitted",
	UnableToListDatabases:      "unable to list databases",
	InvalidDatabaseName:        "invalid database name",
	DatabaseAlreadyExists:      "database already exists",
	DatabaseDoesNotExist:       "database does not exist",
	UnableToCreateDatabase:     "unable to create database",
	UnableToDeleteDatabase:     "unable to delete database",
	UnableToConnectToDatabase:  "unable to connect to database",
	UnableToListTables:         "unable to list tables",
	InvalidTableName:           "invalid table name",
	CreateTableError:           "error creating table",
	DeleteTableError:           "error deleting table",
	TableDoesNotExist:          "table does not exist",
	UnableToConnectToTable:     "unable to connect to table",
	TableClosed:                "the table is closed",
	TableCloseError:            "error closing table",
	DescribeError:              "error describing table",
	CountError:                 "error performing Count",
	InsertError:                "error performing Insert",
	UpdateError:                "error performing Update",
	SelectError:                "error performing Select",
	LimitOutOfRange:            "limit must be a non-negative integer",
	DeleteError:                "error performing Delete",
	TemplateFailsToValidate:    "template fails to validate",
	SelectorFailsToValidate:    "selector fails to validate",
	ConditionFailsToValidate:   "condition fails to validate",
	SortFailsToValidate:        "sort order fails to validate",
	ReplacementFailsToValidate: "replacement fails to validate",
	RecordFailsToValidate:      "record fails to validate",
	RecordUnsupportedType:      "unsupported value type for Record",
	RecordMalformedKey:         "malformed key for Record",
	RecordUnableToDecodeValue:  "error decoding value for Record",
	RecordUnableToEncodeValue:  "error encoding value for Record",
	RecordUnableToConvertType:  "unable to convert value type for Record",
	IterationNotStarted:        "attempt to read from Records without calling Next",
	IterationError:             "error on iteration of Records",
	IterationFinished:          "attempt to read from Records past the end of iteration",
	IterationCloseError:        "error closing Records",
	ListIndexError:             "error listing indices",
	InvalidKey:                 "invalid key",
	AddIndexError:              "error adding index",
	DeleteIndexError:           "error deleting index",
	AddKeyError:                "error adding key",
	DeleteKeyError:             "error deleting key",
	RenameTableError:           "error renaming table",
}

// Error is the interface satisfied by an error with a Code.
type Error interface {
	// Code returns the code associated with this error.
	Code() Code
	// Error returns a string description of the error.
	Error() string
}

// stack represents a stack of program counters.
type stack []uintptr

// kvError describes a keyvalue error.
type kvError struct {
	code  Code  // The error code
	cause error // The cause (if any)
	*stack
}

/////////////////////////////////////////////////////////////////////////
// Code functions
/////////////////////////////////////////////////////////////////////////

// String returns a description of the error with error code c.
func (c Code) String() string {
	s, ok := codeToString[c]
	if !ok {
		s = "Unknown error (error code: " + strconv.Itoa(int(c)) + ")"
	}
	return s
}

// New returns a new error with error code c. The error satisfies the Error interface.
func (c Code) New() error {
	return &kvError{
		code:  c,
		stack: callers(1),
	}
}

// Wrap returns a new error with error code c and cause e. The error satisfies the Error interface.
func (c Code) Wrap(e error) error {
	if err, ok := e.(Error); ok && err.Code() == c {
		return err
	}
	return &kvError{
		code:  c,
		cause: e,
		stack: callers(1),
	}
}

/////////////////////////////////////////////////////////////////////////
// stack functions
/////////////////////////////////////////////////////////////////////////

// StackTrace returns the associated stack trace.
func (s *stack) StackTrace() errors.StackTrace {
	f := make([]errors.Frame, len(*s))
	for i := 0; i < len(f); i++ {
		f[i] = errors.Frame((*s)[i])
	}
	return f
}

// callers returns a new stack trace. The argument skip is the number of stack frames to ascend, with 0 identifying the calling function.
func callers(skip int) *stack {
	if skip < 0 {
		skip = 0
	}
	const depth = 32
	var pcs [depth]uintptr
	n := runtime.Callers(skip+2, pcs[:])
	var st stack = pcs[0:n]
	return &st
}

/////////////////////////////////////////////////////////////////////////
// kvError functions
/////////////////////////////////////////////////////////////////////////

// Code returns the error code.
func (e *kvError) Code() Code {
	return e.code
}

// Unwrap returns the underlying cause of the error, if known.
func (e *kvError) Unwrap() error {
	return e.cause
}

// Is returns true if and only if target is an Error with the same error code.
func (e *kvError) Is(target error) bool {
	f, ok := target.(Error)
	return ok && e.Code() == f.Code()
}

// Error returns a description of the error.
func (e *kvError) Error() string {
	msg := e.Code().String()
	if cause := e.Unwrap(); cause != nil {
		msg += ": " + cause.Error()
	}
	return msg
}

// Format implements fmt.Formatter with support for the following verbs:
//
//	%s    print the error.
//	%v    see %s
//	%+v   extended format. The location of the error will be printed.
func (e *kvError) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		if s.Flag('+') {
			if cause := e.Unwrap(); cause != nil {
				fmt.Fprintf(s, "%+v", cause)
			}
			e.StackTrace().Format(s, verb)
			io.WriteString(s, "\n"+e.Code().String()) // Ignore any error
			return
		}
		fallthrough
	case 's', 'q':
		io.WriteString(s, e.Error()) // Ignore any error
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ShouldWrap returns true if e satisfies all of the following:
//   - e is not nil;
//   - e is not equal to io.EOF;
//   - e is not equal to one of context.Canceled or context.DeadlineExceeded;
//   - e is does not satisfy the Error interface.
func ShouldWrap(e error) bool {
	if e == nil ||
		e == io.EOF ||
		e == context.Canceled ||
		e == context.DeadlineExceeded {
		return false
	}
	_, ok := e.(Error)
	return !ok
}
