// Driver defines the interface for working with very simple key-value databases.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package driver

import (
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"context"
)

// Connection is the interface encapsulating a driver connection.
type Connection interface {
	// DriverName returns the name associated with this driver.
	DriverName() string
	// ListDatabases returns the names of the available databases on this
	// connection.
	ListDatabases(ctx context.Context) ([]string, error)
	// CreateDatabase creates a database with the given name on this
	// connection.
	CreateDatabase(ctx context.Context, name string) error
	// DeleteDatabase deletes the indicated database on this connection.
	// Does not return an error if the database does not exist.
	DeleteDatabase(ctx context.Context, name string) error
	// ConnectToDatabase connects to the indicated database.
	ConnectToDatabase(ctx context.Context, name string) (Database, error)
	// Close closes the connection.
	Close() error
}

// Database is the interface encapsulating a database connection.
type Database interface {
	// ListTables returns the names of the tables in the database.
	ListTables(ctx context.Context) ([]string, error)
	// CreateTable creates a table with the given name in the database.
	// The provided template should be used by the driver to create the
	// table (if appropriate).
	//
	// The driver is free to ignore the template if it is not required:
	// for example, a schema-less storage engine does not require a
	// template. The driver is free to assume that "template" is valid.
	CreateTable(ctx context.Context, name string, template record.Record) error
	// DeleteTable deletes the indicated table from the database. Does not
	// return an error if the table does not exist.
	DeleteTable(ctx context.Context, name string) error
	// RenameTable changes the name of a table in the database. The effect
	// of renaming a table to which there are open connections is undefined.
	RenameTable(ctx context.Context, oldname string, newname string) error
	// ConnectToTable connects to the indicated table in the database.
	ConnectToTable(ctx context.Context, name string) (Table, error)
	// Close closes the connection to the database.
	Close() error
}

// Table is the interface encapsulating a table in the database.
type Table interface {
	// Close closes the connection to the table.
	Close() error
	// Describe returns a best-guess template for the data in this table.
	//
	// Note that the accuracy of this template depends on the underlying
	// storage engine. It might not be possible to return an exact
	// description (for example, in a schema-less database), and in this
	// case we return a good guess based on a sample of the data available.
	Describe(ctx context.Context) (record.Record, error)
	// CountWhere returns the number of records in the table that satisfy
	// condition "cond" (which may be nil if there are no conditions).
	//
	// The driver is free to assume that "cond" is either nil, or "cond" is
	// non-nil, valid, and is not of type condition.Bool.
	CountWhere(ctx context.Context, cond condition.Condition) (int64, error)
	// Insert inserts the records from the given iterator into the table.
	//
	// Note: Insert should NOT call Close on the iterator.
	Insert(ctx context.Context, itr record.Iterator) error
	// UpdateWhere updates all records in the table that satisfy condition
	// "cond" (which may be nil if there are no conditions) by setting all
	// keys present in "replacement" to the given values. Returns the number
	// of records updated.
	//
	// The driver is free to assume that "replacement" is valid; and that
	// "cond" is either nil, or "cond" is non-nil, valid, and is not of
	// type condition.Bool.
	UpdateWhere(ctx context.Context, replacement record.Record, cond condition.Condition) (int64, error)
	// SelectWhere returns the results satisfying condition "cond" (which
	// may be nil if there are no conditions), sorted as specified by
	// "order" (which may be nil if there is no sort order required).
	// The returned records will be in the form specified by "template".
	//
	// The driver is free to assume that "template" is valid; that "cond"
	// is either nil, or "cond" is non-nil, valid, and is not of type
	// condition.Bool; and that "order" is either nil, or "order" is
	// non-nil and valid
	SelectWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Iterator, error)
	// SelectWhereLimit returns at most n results satisfying condition
	// "cond" (which may be nil if there are no conditions), sorted as
	// specified by "order" (which may be nil if there is no sort order
	// required). The returned records will be in the form specified by
	// "template".
	//
	// The driver is free to assume that "template" is valid; that "cond"
	// is either nil, or "cond" is non-nil, valid, and is not of type
	// condition.Bool; that "order" is either nil, or "order" is non-nil
	// and valid; and that n is a non-negative integer.
	SelectWhereLimit(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error)
	// DeleteWhere deletes those records in the table that satisfy condition
	// "cond" (which may be nil if there are no conditions). Returns the
	// number of records deleted.
	//
	// The driver is free to assume that "cond" is either nil, or "cond" is
	// non-nil, valid, and is not of type condition.Bool.
	DeleteWhere(ctx context.Context, cond condition.Condition) (int64, error)
	// AddIndex adds an index on the given key. If an index already exists,
	// this index is unmodified and nil is returned. The driver is free to
	// assume that the key is well-formed.
	AddIndex(ctx context.Context, key string) error
	// DeleteIndex deletes the index on the given key. If no index is
	// present, nil is returned. The driver is free to assume that the
	// key is well-formed.
	DeleteIndex(ctx context.Context, key string) error
	// ListIndices lists the keys for which indices are present. The driver
	// is free to assume that the key is well-formed.
	ListIndices(ctx context.Context) ([]string, error)
	// AddKeys updates each record r in the table, adding any keys in rec
	// that are not already present along with the corresponding values.
	// Any keys that are already present in r will be left unmodified. The
	// driver may assume that rec has been validated.
	AddKeys(ctx context.Context, rec record.Record) error
	// DeleteKeys updates all records in the table, deleting all the
	// specified keys if present. The driver may assume that the keys are
	// well-formed.
	DeleteKeys(ctx context.Context, keys []string) error
}

// SelectOneWherer is an optional interface a Table may support if it provides its own SelectOne method.
type SelectOneWherer interface {
	// SelectOneWhere returns the first record satisfying condition "cond"
	// (which may be nil if there are no conditions), sorted as specified
	// by "order" (which may be nil if there is no sort order required).
	// The returned record will be in the form specified by "template".
	// If no records match, then a nil record will be returned.
	//
	// The driver is free to assume that "template" is valid; that "cond"
	// is either nil, or "cond" is non-nil, valid, and is not of type
	// condition.Bool; that "order" is either nil, or "order" is non-nil
	// and valid; and that n is a non-negative integer.
	SelectOneWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Record, error)
}
