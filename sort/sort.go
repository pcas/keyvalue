// Sort describes a sort order.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package sort

import (
	"bitbucket.org/pcas/keyvalue/record"
	"fmt"
	"strings"
)

// Direction defines the sort direction.
type Direction bool

// The possible sort directions.
const (
	Ascending  = Direction(false)
	Descending = Direction(true)
)

// Order describes the sort direction for a key.
type Order struct {
	Key       string    // The key
	Direction Direction // The direction
}

// OrderBy represents a slice of sort orders.
type OrderBy []Order

/////////////////////////////////////////////////////////////////////////
// Direction functions
/////////////////////////////////////////////////////////////////////////

// String returns a string description of the direction.
func (d Direction) String() string {
	if d.IsAscending() {
		return "ASC"
	}
	return "DESC"
}

// IsAscending returns true iff the sort direction is ascending.
func (d Direction) IsAscending() bool {
	return d == Ascending
}

// IsDescending returns true iff the sort direction is descending.
func (d Direction) IsDescending() bool {
	return d == Descending
}

/////////////////////////////////////////////////////////////////////////
// Order functions
/////////////////////////////////////////////////////////////////////////

// String returns a string description of the sort.
func (s Order) String() string {
	return s.Key + " " + s.Direction.String()
}

// IsAscending returns true iff the direction is ascending.
func (s Order) IsAscending() bool {
	return s.Direction.IsAscending()
}

// IsDescending returns true iff the direction is descending.
func (s Order) IsDescending() bool {
	return s.Direction.IsDescending()
}

// IsValid returns true if and only if the key satisfies record.IsKey.
func (s Order) IsValid() (bool, error) {
	if !record.IsKey(s.Key) {
		return false, fmt.Errorf("malformed key \"%s\"", s.Key)
	}
	return true, nil
}

/////////////////////////////////////////////////////////////////////////
// OrderBy functions
/////////////////////////////////////////////////////////////////////////

// By returns the OrderBy slice described by the given Order terms.
func By(s ...Order) OrderBy {
	return OrderBy(s)
}

// String returns a string description.
func (S OrderBy) String() string {
	strs := make([]string, 0, len(S))
	for i := range S {
		strs = append(strs, S[i].String())
	}
	return strings.Join(strs, ", ")
}

// IsValid validates the OrderBy.
func (S OrderBy) IsValid() (bool, error) {
	for i := range S {
		if _, err := S[i].IsValid(); err != nil {
			return false, err
		}
	}
	return true, nil
}
